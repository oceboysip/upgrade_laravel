function render_view(tpl_source,target,data,callback){
    try{
        var View = Backbone.View.extend({
            initialize: function(){
                this.render();
            },
            render: function(){
                var variables = data;
                var template = _.template($(tpl_source).html(),variables);
                this.$el.html(template);
                if(typeof callback !== 'undefined'){
                  callback();
                }
            }
        });
        var view = new View({el:$(target)});
        
   }catch(error){
        console.log(error.message);
   }
}
function prepend_view(tpl_source,target,data){
    try{
        var View = Backbone.View.extend({
            initialize: function(){
                this.render();
            },
            render: function(){
                var variables = data;
                var template = _.template($(tpl_source).html(),variables);
                this.$el.prepend(template);
            }
        });
        var view = new View({el:$(target)});
   }catch(error){
        
   }
}
function append_view(tpl_source,target,data){
    try{
        var View = Backbone.View.extend({
            initialize: function(){
                this.render();
            },
            render: function(){
                var variables = data;
                var template = _.template($(tpl_source).html(),variables);
                this.$el.append(template);
                this.$el.css('display','none');
                this.$el.fadeIn();
            }
        });
        var view = new View({el:$(target)});
        
   }catch(error){
     
   }
}

function getApplicationList(choosenTelcoId, choosenSdc) {
    $.getJSON("{{ url('api/get_list_umb_applcation/') }}" + "/" + choosenTelcoId + "/" + choosenSdc,
    {ajax: 'true'}, function (data) {
    $("#applicationSelect").html('');
    $("#applicationSelect").append('<optgroup label="Choose Business Application">');
        var firstData = null;
        $.each(data, function () {
        if (!firstData) {firstData = this.id;}
        $("#applicationSelect").append('<option value="' + this.id + '">' + this.name + '</option>')
        })
        $("#applicationSelect").append('</optgroup>');
    $("#applicationSelect").select2('val',firstData);
    });
}