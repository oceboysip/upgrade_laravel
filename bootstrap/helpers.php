<?php


function filter_badwords($matches) {
    $bad_words = Config::get('badwords');
    $replace = in_array(strtolower($matches[0]), $bad_words);
    return ($replace) ? '***' : $matches[0];
}