<?php

/**
 * Resides all content related DAO
 * 
 */
class Content extends Eloquent {

    /**
     * Save new push content
     * @param $applicationId application id
     * @param $content
     * @param $sequence
     */
    public static function saveNewPushContent($applicationId, $content, $sequence, $content_type) {
        $maaping_clone = [
            '2000' => '6008',
            '2007' => '6005',
            '7006' => '6006',
            '7002' => '6007'
        ];
        $content = Content::filterContent($content);
        $db = Config::get('reporting.database_application');

        $id = DB::table("{$db}.content")->insertGetId(
            array('application_id' => $applicationId, 'text' => $content, 'sequence' => $sequence,
                'content_type' => $content_type)
        );

        if(isset($maaping_clone[$applicationId]))//fitur duplikasi buat xl
        {
            $data = DB::insert("INSERT INTO {$db}.content(application_id, text, sequence, parent_id, content_type)
                VALUES (?,?,?,?,?)",
                array($maaping_clone[$applicationId], $content, $sequence, $id, $content_type)
            );
        }

    }

    /**
     * Save new pull content
     * @param $applicationId application id
     * @param $content
     * @param $sequence
     */
    public static function saveNewPullContent($applicationId, $content, $sequence) {
        $content = Content::filterContent($content);
        $db = Config::get('reporting.database_application');
        DB::insert("INSERT INTO {$db}.pull_content(application_id, text, sequence) VALUES (?,?,?)",
            array($applicationId,$content, $sequence)
        );
    }

    /**
     * Edit push content
     * @param $id
     * @param $content
     * @param $sequence
     * @param $isDisabled
     */
    public static function editPushContent($id, $content, $sequence, $isDisabled, $content_type) {
        $db = Config::get('reporting.database_application');
        $content = Content::filterContent($content);
        DB::insert("UPDATE {$db}.content
                    SET text = ?, sequence = ?, is_disabled = ?, content_type = ?
                    WHERE id = ?",
            array($content, $sequence, $isDisabled, $content_type, $id)
        );

        DB::insert("UPDATE {$db}.content
                    SET text = ?, sequence = ?, is_disabled = ?, content_type = ?
                    WHERE parent_id = ?",
            array($content, $sequence, $isDisabled, $content_type, $id)
        );
    }


    /**
     * Edit pull content
     * @param $id
     * @param $content
     * @param $sequence
     * @param $isDisabled
     */
    public static function editPullContent($id, $content, $sequence, $isDisabled) {
        $db = Config::get('reporting.database_application');
        $content = Content::filterContent($content);
        DB::insert("UPDATE {$db}.pull_content
                    SET text = ?, sequence = ?, is_disabled = ?
                    WHERE id = ?",
            array($content, $sequence, $isDisabled, $id)
        );
    }


    /**
     * NOTE THAT THIS IS NOT UNICODE FRIENDLYY!
     *
     * @param $content
     * @return string
     */
    public static function filterContent($content) {
        $arr = str_split($content);
        $newContent = '';
        foreach ($arr as $char) {
            $i = ord($char);
            if ($i == 9 || $i == 10 || $i == 13 || ($i >= 32 && $i <= 126)) {
                $newContent .= $char;
            }
        }
        return $newContent;
    }


    /**
     * Get pull content by id
     * @param $id
     */
    public static function getPullContentById($id) {
        $db = Config::get('reporting.database_application');
        return DB::table("{$db}.pull_content")
            ->select('id', 'text', 'is_disabled', 'dtscheduled', 'sequence', 'is_new', 'dtexpired', 'charging_param', 'composite_type')
            ->where('id', '=', $id)->get();
    }

    /**
     * Get push content by id
     * @param $id
     */
    public static function getPushContentById($id) {
        $db = Config::get('reporting.database_application');
        return DB::table("{$db}.content")
            ->select('id', 'text', 'is_disabled', 'dtscheduled', 'sequence', 'is_new', 'dtexpired', 'charging_param', 'composite_type', 'content_type')
            ->where('id', '=', $id)->get();
    }

}
