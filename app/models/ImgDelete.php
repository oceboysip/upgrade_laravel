<?php

class ImgDelete extends Eloquent
{
    protected $connection= 'umb-app';
    protected $table = 'img_delete_queue';
    protected $fillable = array('image_file', 'created_at', 'updated_at', 'n_status');
}
