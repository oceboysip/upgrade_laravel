<?php

/**
 * Service DAO
 * 
 */
class GatewayService extends Eloquent {

    /**
     * Check is service exists
     * @param $name service name
     * @param $sdc SDC
     * @param $telcoId telco id
     * @param int $serviceGroupId service group id
     * @return mixed
     */
    public static function isServiceExists($name, $sdc, $telcoId, $serviceGroupId = 0) {
        $db = Config::get('reporting.database_config');
        $result = DB::select("SELECT id FROM {$db}.service
                              WHERE sdc = ? AND telcoId = ? AND name = ? AND serviceGroupId = ?",
                              array($sdc, $telcoId, $name, $serviceGroupId)
        );
        return $result;
    }


    /**
     * Get service by id
     * @param $id id
     * @return mixed
     */
    public static function getById($id) {
        $db = Config::get('reporting.database_config');
        return DB::select("SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                           FROM {$db}.service WHERE id = {$id}");
    }


    /**
     * Save new service
     * @param $name the application Name
     * @param $url the URL for business delegate
     * @param $sdc SDC
     * @param $telcoId telco Id
     * @param int $serviceGroupId service group id
     * @return assigned id
     */
    public static function saveService($name, $url, $sdc, $telcoId, $serviceGroupId = 0){
        $db = Config::get('reporting.database_config');
        $checkExistingEntry = GatewayService::isServiceExists($name, $sdc, $telcoId, $serviceGroupId);
        if ($checkExistingEntry && sizeof($checkExistingEntry) > 0) {
            Log::warning("Service of telcoId: {$telcoId}, name: {$name}, ".
            "sdc: {$sdc} already exists with Id: " . $checkExistingEntry[0]->id);
            return $checkExistingEntry[0]->id;
        }
        $assignedId = GatewayService::assignIdForService($telcoId, $sdc);
        DB::insert("INSERT INTO {$db}.service(id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active)
                    VALUES (?,?,?,?,?,?,NOW(),?)",
                    array($assignedId, $name, $url, $sdc, $telcoId, $serviceGroupId, 1)
        );
        return $assignedId;
    }


    /**
     * Edit service
     * @param $serviceId
     * @param $name
     * @param $url
     * @param $isActive
     * @param int $serviceGroupId
     */
    public static function editService($serviceId, $name, $url, $isActive, $serviceGroupId = 0){
        $db = Config::get('reporting.database_config');
        DB::insert("UPDATE {$db}.service
                    SET name = ?, url = ?, serviceGroupId = ?, is_active = ?
                    WHERE id = ?",
            array($name, $url,$serviceGroupId, $isActive, $serviceId)
        );
    }



    /* ----------- Service Keyword ----------- */

    /**
     * Save sevice keyword
     * @param $serviceId service id
     * @param $sdc SDC
     * @param $keyword keyword
     * @param $telcoId telco id
     * @param int $isDefault
     */
    public static function saveServiceKeyword($serviceId, $sdc, $keyword, $telcoId, $isDefault = 0) {
        $db = Config::get('reporting.database_config');
        if (GatewayService::isServiceKeywordExists($serviceId, $sdc, $keyword, $telcoId)) {
            return;
        }
        DB::insert("INSERT INTO {$db}.service_keyword(service_id, sdc, keyword, telco_id, is_default)
                    VALUES (?,?,?,?,?)",
                    array($serviceId, $sdc, $keyword, $telcoId, $isDefault)
        );
    }


    /**
     * Edit service keyword
     * @param $serviceKeywordId
     * @param $keyword
     * @param int $isDefault
     */
    public static function editServiceKeyword($serviceKeywordId, $keyword, $isDefault = 0) {
        $db = Config::get('reporting.database_config');
        DB::insert("UPDATE {$db}.service_keyword
                    SET keyword = ?, is_default = ?
                    WHERE id = ?",
            array($serviceKeywordId, $keyword, $isDefault)
        );
    }




    /**
     * Check if service keyword exists
     * @param $serviceId service id
     * @param $sdc SDC
     * @param $keyword keyword
     * @param $telcoId telco id
     * @return mixed
     */
    public static function isServiceKeywordExists($serviceId, $sdc, $keyword, $telcoId) {
        $db = Config::get('reporting.database_config');
        $result = DB::select("SELECT id FROM {$db}.service_keyword
                              WHERE service_id = ? AND sdc = ? AND keyword = ? AND telco_id = ? ",
            array($serviceId, $sdc, $keyword, $telcoId)
        );
        return $result;
    }


    /**
     * Get service keywords
     * @param $serviceId service id
     * @return mixed
     */
    public static function getServiceKeywords($serviceId) {
        $db = Config::get('reporting.database_config');
        return DB::select("SELECT id, sdc, service_id, telco_id, keyword, is_default
                           FROM {$db}.service_keyword WHERE service_id = {$serviceId}");
    }

    /**
     * Get service keyword entry by id. This should approximately 1 records
     * @param $id id
     * @return mixed
     */
    public static function getServiceKeywordsById($id) {
        $db = Config::get('reporting.database_config');
        return DB::select("SELECT id, sdc, service_id, telco_id, keyword, is_default
                           FROM {$db}.service_keyword WHERE id = {$id}");
    }


    /* ----------- From the API  ----------- */


    /**
     * Get All Service
     *
     * @param $telcoId telco id
     * @param $sdc SDC
     * @return mixed
     */
    public static function getListOfService($telcoId, $sdc){
        $db = Config::get('reporting.database_config');
        /*
         $results = DB::table("{$db}.service as s")
                        ->join("{$db}.telco as t", "s.telcoId",'=', "t.id")
                        ->leftJoin("{$db}.service_group as cg", "s.serviceGroupId",'=', "cg.id")
                        ->where("s.telcoId","=",$telcoId)
                        ->where("s.sdc", "=", $sdc)
            ->get(array('s.id', 's.name', 't.name as telco_name', 's.url', 's.sdc', 's.telcoId', "IFNULL(`cg`.`name`, '--') as group_name",
                        's.is_active'));
        ;
         */
        $results = DB::select(
            "SELECT s.id, s.name, t.name as telco_name, s.url, s.sdc, s.telcoId, IFNULL(cg.name, '--') as group_name, s.is_active
             FROM {$db}.service s INNER JOIN {$db}.telco t ON s.telcoId = t.id
                  LEFT OUTER JOIN {$db}.service_group cg ON s.serviceGroupId = cg.id
             WHERE s.telcoId = ? AND s.sdc = ?", array($telcoId, $sdc));
        return $results;
    }


    /**
     * Get all service except the given list of service id
     * @param $telcoId
     * @param $sdc
     * @param $arrayExceptServiceIdList
     * @return mixed
     */
    public static function getListOfSimpleServiceExcept($telcoId, $sdc, $arrayExceptServiceIdList) {
        $db = Config::get('reporting.database_config');
         $results = DB::table("{$db}.service as s")
                        ->join("{$db}.telco as t", "s.telcoId",'=', "t.id")
                        ->leftJoin("{$db}.service_group as cg", "s.serviceGroupId",'=', "cg.id")
                        ->where("s.telcoId","=",$telcoId)
                        ->where("s.sdc", "=", $sdc)
                        ->whereNotIn("s.id",$arrayExceptServiceIdList)
            ->get(array('s.id', 's.name', 't.name as telco_name', 's.url', 's.sdc', 's.telcoId', "cg.name as group_name",
                        's.is_active'));
        ;
        return $results;
    }


    /**
     * Get All Service by given keyword, telco_id, and sdc
     * @param $telcoId
     * @param $sdc
     * @param $keyword
     * @return mixed
     */
    public static function getListOfServiceByKeyword($telcoId, $sdc, $keyword){
        $keyword = mysql_real_escape_string($keyword);
        $db = Config::get('reporting.database_config');
        //$dbApplication = Config::get('reporting.database_application');
        $results = DB::select(
            "SELECT s.id, s.name, t.name as telco_name, s.url, s.sdc, s.telcoId, IFNULL(cg.name, '--') as group_name, s.is_active
             FROM {$db}.service s INNER JOIN {$db}.telco t ON s.telcoId = t.id
                  LEFT OUTER JOIN {$db}.service_group cg ON s.serviceGroupId = cg.id
             WHERE s.telcoId = ? AND s.sdc = ? AND EXISTS (
                SELECT 1 FROM {$db}.service_keyword sk
                WHERE sk.service_id = s.id AND sk.keyword LIKE '{$keyword}%'
             )", array($telcoId, $sdc));
        return $results;
    }

    /**
     * Get All Service by given keyword
     * @param $keyword
     * @return mixed
     */
    public static function getListOfServiceByKeywordOnly($keyword){
        //$keyword = mysql_real_escape_string($keyword);
        $db = Config::get('reporting.database_config');
        //$dbApplication = Config::get('reporting.database_application');
        $results = DB::select(
            "SELECT s.id, s.name, t.name as telco_name, s.url, s.sdc, s.telcoId, IFNULL(cg.name, '--') as group_name, s.is_active
             FROM {$db}.service s INNER JOIN {$db}.telco t ON s.telcoId = t.id
                  LEFT OUTER JOIN {$db}.service_group cg ON s.serviceGroupId = cg.id
             WHERE EXISTS (
                SELECT 1 FROM {$db}.service_keyword sk
                WHERE sk.service_id = s.id AND sk.keyword LIKE '{$keyword}%'
             )");
        return $results;
    }




    /* ----------- Id management  ----------- */

    /**
     * Get last id for charging id
     * @param $proposeId
     * @param $maxProposeId
     * @return mixed
     */
    public static function getLastIdForServiceId($proposeId, $maxProposeId) {
        $db = Config::get('reporting.database_config');
        $result = DB::select("SELECT MAX(id)+1 theId FROM {$db}.service WHERE id BETWEEN {$proposeId} AND {$maxProposeId}");
        return (sizeof($result) > 0 && $result[0]->theId ? $result[0]->theId : $proposeId);
    }


    /**
     * The core idea of assigning 'Ring' of Id management in Routing
     *
     * @param $telcoId
     * @param $sdc
     * @return the new id for table 'routing'
     */
    public static function assignIdForService($telcoId, $sdc) {
        $resSdc = MainServiceConfig::getSdcByName($sdc);
        $SDC_MULTIPLIER = 5000;
        //$ringDownsdcId = 0;
        $sdcId = 0;
        if (sizeof($resSdc) > 0) {
            $sdcId = $resSdc[0]->id - 1;
        }
        /*
        if (!$sdcId) {
            //Unknown?
            $sdcId = 35;
            $ringDownsdcId = 35 * $SDC_MULTIPLIER;
        } else $ringDownsdcId = $sdcId * $SDC_MULTIPLIER;
        */
        $ringDownsdcId = $sdcId * $SDC_MULTIPLIER;

        //$TELCO_MULTIPLIER = 1000;
        /**
         * Since we already started it :p Thus, we're not following the current cdb.telco
         * instead:
         *
         * XL: 0 - 1000
         * Telkomsel: 1001: 2000
         * Indosat: 2001: 3000
         * Unknown: 20000 - 21000
         *
         */
        $TELCO_MULTIPLIER = 1000;
        $telcoBaseId = 0;
        switch ($telcoId) {
            case 3:
                break;
            case 1:
                $telcoBaseId = 1000;
                break;
            case 2:
                $telcoBaseId = 2000;
                break;
            default:
                $telcoBaseId = 20000;
                break;
        }

        $suggestedId =
            GatewayService::getLastIdForServiceId(($ringDownsdcId + $telcoBaseId), ($ringDownsdcId + $telcoBaseId + $TELCO_MULTIPLIER));

        //echo PHP_EOL. 'SuggestedId: '. $suggestedId . PHP_EOL;
        return $suggestedId;
    }


}
