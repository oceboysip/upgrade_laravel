<?php

/**
 * Application DAO
 *
 * @version 1.0
 *
 */
class Application extends Eloquent {

    /**
     * Get list of application in given Telco Id and SDC
     * @param $telcoId
     * @param $sdc
     * @param $applicationType
     * @return mixed
     */
    public static function getListApplication($telcoId, $sdc, $applicationType = -1) {
        $db = Config::get('reporting.database_application');
        $cdb = Config::get('reporting.database_config');
        $q = DB::table("{$db}.application as a")
            ->join("{$cdb}.service as s", 's.id', '=', 'a.service_id')
            ->join("{$db}.application_type as apt", 'apt.id', '=', 'a.application_type_id')
            ->where('s.telcoId', $telcoId)
            ->where('s.sdc', $sdc);

        if ($applicationType > -1) {
            $q = $q->where('a.application_type_id','=',$applicationType);
        }

        return $q->get(array('a.id','a.name', 'a.dtcreated', 's.name as service_name', 'apt.name as application_type',
                's.sdc', 's.telcoId','a.default_charging_param', 'a.is_active', 'a.dtmodified','a.application_type_id',
                'a.service_id', 'a.default_header','a.push_day_interval'));
    }



    /**
     * Get list of application delegate, by application_id
     * @param $applicationId
     * @return mixed
     */
    public static function getListApplicationDelegate($applicationId) {
        $db = Config::get('reporting.database_application');
        $q = DB::table("{$db}.application_delegate as a")
            ->where('a.application_id', $applicationId)
            ;

        return $q->get(array('a.id','a.application_id', 'a.default_header',
                            'a.reg_done_delegate','a.reg_error_delegate','a.unreg_done_delegate',
                            'a.unreg_error_delegate','a.formatter_delegate','a.pull_delegate'
                            ));
    }



    /**
	 * Get all application type
	 *
     * @param int $id the id (if any)
	 * @return mixed
	 */
	public static function getApplicationType($id = 0) {
        $db = Config::get('reporting.database_application');
		$results = ($id > 0) ? DB::select("SELECT id, name FROM {$db}.application_type WHERE id = {$id}")
                    :
                    DB::select("SELECT id, name FROM {$db}.application_type")
        ;
        return $results;
	}


    /**
     * Get application charging param
     * @param int $applicationId application id
     * @return mixed
     */
    public static function getApplicationChargingParam($applicationId = 0) {
        $db = Config::get('reporting.database_application');
        return DB::table("{$db}.application_charging_param")
                ->where("application_id",$applicationId)
                ->get(array('id','application_id','telco_id', 'reg', 'reg_error','unreg', 'unreg_error', 'pull', 'pull_error'));
    }


    /**
     * Save new application based on 'Service route'. Note that the application Id is based on service Id
     * for better management. But if it founds that the service id is less than or equal 0, it will generate its own
     * id, with the same technique with Service.id allocation.
     *
     * @param $name
     * @param $serviceId
     * @param $defaultChargingParam
     * @param $applicationTypeId
     * @param $sdc
     * @param $isActive
     * @param $defaultHeader
     * @param $pushDayInterval
     * @param int $telcoId
     * @return int the inserted id
     */
    public static function saveApplication($name, $serviceId,
                                           $defaultChargingParam, $applicationTypeId, $sdc,
                                           $isActive, $defaultHeader, $pushDayInterval, $telcoId = 0) {
        $db = Config::get('reporting.database_application');
        $checkExistingEntry = Application::isApplicationExists($sdc, $serviceId);
        if ($checkExistingEntry && sizeof($checkExistingEntry) > 0) {
            Log::warning("Application of sdc: {$sdc}, serviceId: {$serviceId} ".
                          "already exists with Id: " . $checkExistingEntry[0]->id);
            return $checkExistingEntry[0]->id;
        }

        $theId = $serviceId;
        if ($theId <= 0) {
            Log::warning("Given service id not valid (<= 0). Will create our own id");
            $theId = Application::assignIdForApplication($telcoId, $sdc);
        }
        /**
         * Always check the existing id. If not increament it
         * Just let it thrown then
         *
        $currentAppId = Application::getById($theId);
        if ($currentAppId && sizeof($currentAppId) > 0) {
            //Log::warning("Application already exists with Id: " . $currentAppId[0]->id . ' Simply add the Id with ONE');
            Log::error("Application already exists with Id: " . $currentAppId[0]->id . ' FIX THIS!');
            return $currentAppId[0]->id;
            //$theId = $theId + 1;
        }
        */

        DB::insert("INSERT INTO {$db}.application(id, name, service_id,
                      default_charging_param, application_type_id, sdc, is_active,default_header, push_day_interval)
                    VALUES (?,?,?,?,?,?,?,?,?)",
                    array($theId, $name, $serviceId, $defaultChargingParam,$applicationTypeId, $sdc,
                          $isActive, $defaultHeader, $pushDayInterval)
        );
        return $theId;
    }


    /**
     * Edit application
     * @param $applicationId application id
     * @param $name application id
     * @param $defaultChargingParam
     * @param $applicationTypeId
     * @param $isActive
     * @param $defaultHeader
     * @param $pushDayInterval
     */
    public static function editApplication($applicationId, $name,
                                       $defaultChargingParam, $applicationTypeId,
                                       $isActive, $defaultHeader, $pushDayInterval) {
        $db = Config::get('reporting.database_application');
        DB::insert("UPDATE {$db}.application
                    SET name = ?, default_charging_param = ?, application_type_id = ?,
                        is_active = ?, default_header = ?, push_day_interval = ?
                    WHERE id = ?",
            array($name, $defaultChargingParam, $applicationTypeId,
                  $isActive, $defaultHeader, $pushDayInterval, $applicationId)
        );
    }


    /**
     * Check is application exists. An application CANNOT have the same sdc AND serviceId.
     *
     * @param $sdc SDC
     * @param $serviceId service id
     * @return mixed
     */
    public static function isApplicationExists(/* $name, */ $sdc, $serviceId)
    {
        $db = Config::get('reporting.database_application');
        /*
        $result = DB::select("SELECT id FROM {$db}.application
                              WHERE sdc = ? AND service_id = ? AND name = ? AND serviceGroupId = ?",
            array($sdc, $serviceId, $name, $serviceGroupId)
        );
        */
        $result = DB::select("SELECT id FROM {$db}.application
                              WHERE sdc = ? AND service_id = ? ",
            array($sdc, $serviceId)
        );
        return $result;
    }


    /**
     * Get service by id
     * @param $id id
     * @return mixed
     */
    public static function getById($id) {
        $db = Config::get('reporting.database_application');
        return DB::select("SELECT id, name, sdc, service_id, is_active, push_day_interval,
                              default_charging_param, dtcreated, dtmodified, default_header, application_type_id
                           FROM {$db}.application WHERE id = {$id}");
    }


    /**
     * Save new application charging param
     * @param $applicationId application id
     * @param $telcoId telco id
     * @param $reg charging for success REG
     * @param $regError charging for not success REG
     * @param $unreg charging for success UNREG
     * @param $unregError charging for failed UNREG
     * @param $pull charging for success PULL
     * @param $pullError charging for failed PULL
     */
    public static function saveApplicationChargingParam($applicationId, $telcoId,
                                                        $reg, $regError, $unreg,$unregError, $pull, $pullError) {

        $checkExistingEntry = Application::isApplicationChargingParamExists($telcoId, $applicationId);
        if ($checkExistingEntry && sizeof($checkExistingEntry) > 0) {
            Log::warning("Application of telco_id: {$telcoId}, applicationId: {$applicationId} ".
            "already exists with Id: " . $checkExistingEntry[0]->id);
            return $checkExistingEntry[0]->id;
        }

        $db = Config::get('reporting.database_application');
        DB::insert("INSERT INTO {$db}.application_charging_param(application_id, telco_id, reg, reg_error,
                    unreg, unreg_error, pull, pull_error)
                    VALUES (?,?,?,?,?,?,?,?)",
            array($applicationId, $telcoId, $reg, $regError, $unreg, $unregError, $pull, $pullError)
        );
    }


    /**
     * Edit existsing application charging param
     * @param $applicationId application id
     * @param $telcoId telco id
     * @param $reg charging for success REG
     * @param $regError charging for not success REG
     * @param $unreg charging for success UNREG
     * @param $unregError charging for failed UNREG
     * @param $pull charging for success PULL
     * @param $pullError charging for failed PULL
     */
    public static function editApplicationChargingParam($applicationId, $telcoId,
                                                        $reg, $regError, $unreg,$unregError, $pull, $pullError) {
        $db = Config::get('reporting.database_application');
        DB::insert("UPDATE {$db}.application_charging_param
                    SET telco_id = ?, reg = ?, reg_error = ?, unreg = ?, unreg_error = ?, pull = ?, pull_error = ?
                    WHERE application_id = ?",
            array($telcoId, $reg, $regError, $unreg,$unregError, $pull, $pullError, $applicationId)
        );
    }


    /**
     * Check if application charging param records exists
     * @param $telcoId
     * @param $applicationId
     * @return mixed
     */
    public static function isApplicationChargingParamExists($telcoId, $applicationId) {
        $db = Config::get('reporting.database_application');
        $result = DB::select("SELECT id FROM {$db}.application_charging_param
                              WHERE telco_id = ? AND application_id = ? ",
            array($telcoId, $applicationId)
        );
        return $result;
    }



    /**
     * Save new application delegate
     * @param $applicationId
     * @param $defaultHeader
     * @param $regDoneDelegate
     * @param $regErrorDelegate
     * @param $unregDoneDelegate
     * @param $unregErrorDelegate
     * @param $formatterDelegate
     * @param $pullDelegate
     * @param string $blacklistTelcoIds
     */
    public static function saveApplicationDelegate($applicationId, $defaultHeader, $regDoneDelegate,
                                                   $regErrorDelegate, $unregDoneDelegate, $unregErrorDelegate,
                                                   $formatterDelegate, $pullDelegate, $blacklistTelcoIds = '') {
        $db = Config::get('reporting.database_application');
        $checkExistingEntry = Application::isApplicationDelegateExists($applicationId);
        if ($checkExistingEntry && sizeof($checkExistingEntry) > 0) {
            Log::warning("Application delegate of applicationId: {$applicationId} ".
            "already exists with Id: " . $checkExistingEntry[0]->id);
            return $checkExistingEntry[0]->id;
        }

        DB::insert("INSERT INTO {$db}.application_delegate(application_id, default_header,
		                              reg_done_delegate, reg_error_delegate, unreg_done_delegate, unreg_error_delegate,
		                              formatter_delegate, pull_delegate)
                    VALUES (?,?,?,?,?,?,?,?)",
            array($applicationId, $defaultHeader, $regDoneDelegate,
                $regErrorDelegate, $unregDoneDelegate, $unregErrorDelegate,
                $formatterDelegate, $pullDelegate
            )
        );
    }


    /**
     * Edit application delegate
     * @param $applicationId
     * @param $defaultHeader
     * @param $regDoneDelegate
     * @param $regErrorDelegate
     * @param $unregDoneDelegate
     * @param $unregErrorDelegate
     * @param $formatterDelegate
     * @param $pullDelegate
     * @param string $blacklistTelcoIds
     */
    public static function editApplicationDelegate($applicationId, $defaultHeader, $regDoneDelegate,
                                                   $regErrorDelegate, $unregDoneDelegate, $unregErrorDelegate,
                                                   $formatterDelegate, $pullDelegate, $blacklistTelcoIds = '') {
        $db = Config::get('reporting.database_application');
        DB::insert("UPDATE {$db}.application_delegate
                    SET default_header = ?, reg_done_delegate = ?, reg_error_delegate = ?, unreg_done_delegate = ?,
                       unreg_error_delegate = ?, formatter_delegate = ?, pull_delegate = ?
                    WHERE application_id = ?",
            array($defaultHeader, $regDoneDelegate,
                $regErrorDelegate, $unregDoneDelegate, $unregErrorDelegate,
                $formatterDelegate, $pullDelegate, $applicationId
            )
        );
    }


    /**
     * Check if application delegate for given application id exists
     * @param $applicationId application id
     * @return mixed
     */
    public static function isApplicationDelegateExists($applicationId) {
        $db = Config::get('reporting.database_application');
        $result = DB::select("SELECT id FROM {$db}.application_delegate
                              WHERE application_id = ? ",
            array($applicationId)
        );
        return $result;
    }


    /**
     * Save application message
     *
     * @param $applicationId
     * @param $telcoId
     * @param $regWelcome
     * @param $regError
     * @param $alreadyReg
     * @param $unreg
     * @param $unregError
     * @param $pullStandard
     * @param $unregNonMember
     * @return int
     */
    public static function saveApplicationMessage($applicationId, $telcoId, $regWelcome,$regError, $alreadyReg,
                                                   $unreg, $unregError, $pullStandard, $unregNonMember) {
        $db = Config::get('reporting.database_application');
        $checkExistingEntry = Application::isApplicationMessageExists($applicationId, $telcoId);
        if ($checkExistingEntry && sizeof($checkExistingEntry) > 0) {
            Log::warning("Application message of applicationId: {$applicationId}, telco_id: {$telcoId}" .
                         "already exists");
            //Always 1
            return 1;
        }

        DB::insert("INSERT INTO {$db}.application_message(application_id, telco_id,
    		                              reg_welcome, reg_error, already_reg, unreg, unreg_error,
    		                              pull_standard, unreg_non_member)
                        VALUES (?,?,?,?,?,?,?,?,?)",
            array($applicationId, $telcoId, $regWelcome, $regError, $alreadyReg,
                  $unreg, $unregError, $pullStandard, $unregNonMember
            )
        );
    }


    /**
     * Edit application message
     * @param $applicationId application id
     * @param $telcoId telco id
     * @param $regWelcome reg welcome
     * @param $regError reg error
     * @param $alreadyReg already reg
     * @param $unreg unreg message
     * @param $unregError unreg error
     * @param $pullStandard pull standard
     * @param $unregNonMember unreg non member
     */
    public static function editApplicationMessage($applicationId, $telcoId, $regWelcome,$regError, $alreadyReg,
                                                  $unreg, $unregError, $pullStandard, $unregNonMember) {
        $db = Config::get('reporting.database_application');
        DB::insert("UPDATE {$db}.application_message
                    SET reg_welcome = ?, reg_error = ?, already_reg = ?, unreg = ?,
                        unreg_error = ?, pull_standard = ?, unreg_non_member = ?
                    WHERE application_id = ? AND telco_id = ? ",
            array($regWelcome, $regError, $alreadyReg,
                $unreg, $unregError, $pullStandard, $unregNonMember, $applicationId, $telcoId
            )
        );
    }


    /**
     * Check if application message for given application id and telco id exists
     * @param $applicationId application id
     * @param $telcoId
     * @return mixed
     */
    public static function isApplicationMessageExists($applicationId, $telcoId){
        $db = Config::get('reporting.database_application');
        $result = DB::select("SELECT 1 FROM {$db}.application_message WHERE application_id = ? AND telco_id = ?",
            array($applicationId,$telcoId)
        );
        return $result;
    }




    /* ----------- Id management  ----------- */

    /**
     * Get last id for application id
     * @param $proposeId
     * @param $maxProposeId
     * @return mixed
     */
    public static function getLastIdForCharingId($proposeId, $maxProposeId) {
        $db = Config::get('reporting.database_application');
        $result = DB::select("SELECT MAX(id)+1 theId FROM {$db}.application WHERE id BETWEEN {$proposeId} AND {$maxProposeId}");
        return (sizeof($result) > 0 && $result[0]->theId ? $result[0]->theId : $proposeId);
    }


    /**
     * The core idea of assigning 'Ring' of Id management in application ids
     * Currently the id assignment follows what Service id is
     *
     * @param $telcoId
     * @param $sdc
     * @return the new id for table 'routing'
     */
    public static function assignIdForApplication($telcoId, $sdc) {
        $resSdc = MainServiceConfig::getSdcByName($sdc);
        $SDC_MULTIPLIER = 5000;
        //$ringDownsdcId = 0;
        $sdcId = 0;
        if (sizeof($resSdc) > 0) {
            $sdcId = $resSdc[0]->id - 1;
        }
        /*
        if (!$sdcId) {
            //Unknown?
            $sdcId = 35;
            $ringDownsdcId = 35 * $SDC_MULTIPLIER;
        } else $ringDownsdcId = $sdcId * $SDC_MULTIPLIER;
        */
        $ringDownsdcId = $sdcId * $SDC_MULTIPLIER;

        //$TELCO_MULTIPLIER = 1000;
        /**
         * Since we already started it :p Thus, we're not following the current cdb.telco
         * instead:
         *
         * XL: 0 - 1000
         * Telkomsel: 1001: 2000
         * Indosat: 2001: 3000
         * Unknown: 20000 - 21000
         *
         */
        $TELCO_MULTIPLIER = 1000;
        $telcoBaseId = 0;
        switch ($telcoId) {
            case 3:
                break;
            case 1:
                $telcoBaseId = 1000;
                break;
            case 2:
                $telcoBaseId = 2000;
                break;
            default:
                $telcoBaseId = 20000;
                break;
        }

        $suggestedId =
            Service::getLastIdForCharingId(($ringDownsdcId + $telcoBaseId), ($ringDownsdcId + $telcoBaseId + $TELCO_MULTIPLIER));

        //echo PHP_EOL. 'SuggestedId: '. $suggestedId . PHP_EOL;
        return $suggestedId;
    }


}
