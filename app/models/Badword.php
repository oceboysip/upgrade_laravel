<?php

class Badword extends Eloquent
{
    protected $connection= 'umb-app';
    //protected $primaryKey = 'gameid';    
    protected $table = 'bad_words';
    protected $fillable = array('keyword', 'regex_filter', 'exact');
}
