<?php

/**
 * Resides all Menu related DAO
 * 
 */
class Menu extends Eloquent {

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public static function getMenuByRole() {
        $role_id = Auth::user()->role_id;
        /**
         * The '1' is a superuser, they can see EVERYTHING
         *
         */
        $str = '1 = 1';
        if ($role_id != 1) {
            //if its not Admin
            $str = "id IN (SELECT menu_id FROM webadmin.dashboard_role_menu
                           WHERE role_id = {$role_id})";
        }
		$results = DB::select("SELECT id, parent_id, title, module, controller, menu_order
                               FROM dashboard_menu
                               WHERE {$str} AND isactive = 1
                               ORDER BY menu_order");
		return $results;
	}


}
