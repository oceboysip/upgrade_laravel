<?php

class KepoReportUser extends Eloquent
{
    protected $connection= 'montessori';
    //protected $primaryKey = 'gameid';    
    protected $table = 'kepo_report_user';
    protected $fillable = array('tindak_lanjut', 'updated_at', 'n_status');
}
