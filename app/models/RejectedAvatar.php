<?php

class RejectedAvatar extends Eloquent
{
    protected $connection= 'monaco';
    protected $table = 'rejected_avatar';
    protected $fillable = array('updated_at', 'n_status');
}
