<?php

class FaqConversation extends Eloquent {
	protected $table = 'faq_conversations';
	protected $connection = 'umb-app';

	public function faq()
	{
		return $this->hasMany('Faq');
	}

	public function user()
	{
		return $this->hasOne('UmbUser', 'id', 'user_id');
	}
}