<?php

/**
 * Resides all Indosat related DAO
 * 
 */
class Indosat extends Eloquent {

	/**
	 * Get all entry in isat_sdm_code
	 *
	 * @return mixed
	 */
	public static function getAllRawSdmCode() {
        $db = Config::get('reporting.database_config');
        $results = DB::select("SELECT service_id, sdc, keyword, code
                               FROM {$db}.isat_sdm_code");
        return $results;
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public static function getAllSdmCode($sdc = '', $service_id = 0, $keyword = '') {
		$db = Config::get('reporting.database_config');
		if ($service_id < 1 && empty($sdc) && empty($keyword)) {
			$results = DB::select("SELECT isc.service_id, isc.sdc, isc.keyword, isc.code, s.name
								FROM {$db}.isat_sdm_code isc INNER JOIN {$db}.service s ON isc.service_id = s.id ORDER BY isc.keyword");
		} else {
			if (empty($keyword)) {
				$results = DB::select("SELECT isc.service_id, isc.sdc, isc.keyword, isc.code, s.name
								FROM {$db}.isat_sdm_code isc INNER JOIN {$db}.service s ON isc.service_id = s.id
								WHERE isc.service_id = ? AND isc.sdc = ?  ORDER BY isc.keyword",array($service_id, $sdc));

			} else {
				$results = DB::select("SELECT isc.service_id, isc.sdc, isc.keyword, isc.code, s.name
								FROM {$db}.isat_sdm_code isc INNER JOIN {$db}.service s ON isc.service_id = s.id
								WHERE isc.service_id = ? AND isc.sdc = ? AND isc.keyword = ? ORDER BY isc.keyword",array($service_id, $sdc, $keyword));
			}
		}
		return $results;
	}


	/**
	 * Check if SDM Code Exists
	 * @param $serviceId
	 * @param $sdc
	 * @param $keyword
	 * @return mixed
	 */
	public static function isSdmCodeExists($sdc, $keyword, $serviceId) {

		//Log::debug('--> SDC ' . $sdc);
		//Log::debug('--> Keyword ' . $keyword);
		//Log::debug('--> Service ID ' . $serviceId);

		$db = Config::get('reporting.database_config');
		$result = DB::select("SELECT code FROM {$db}.isat_sdm_code WHERE service_id = ? AND sdc = ? AND keyword = ? ",
					array($serviceId, $sdc, $keyword)
        );

		//Log::debug('--> ' . implode('--',$result));

        return count($result) > 0;
    }



	/**
	 * Save SDM Code
	 * @param $serviceId service id
	 * @param $sdc SDC
	 * @param $keyword keyword
	 * @param int $isDefault
	 */
	public static function saveSdmCode($serviceId, $sdc, $keyword, $code) {
		$db = Config::get('reporting.database_config');
		DB::insert("INSERT INTO {$db}.isat_sdm_code(service_id, sdc, keyword, code)
                    VALUES (?,?,?,?)",
			array($serviceId, $sdc, $keyword, $code)
		);
	}


	/**
	 * Remove the
	 * @param $service_id
	 * @param $sdc
	 * @param $keyword
	 */
	public static function removeSdmCode($code, $serviceId, $sdc, $keyword) {
		//Log::debug('--> Code ' . $code);
		//Log::debug('--> SDC ' . $sdc);
		//Log::debug('--> Keyword ' . $keyword);
		//Log::debug('--> Service ID ' . $serviceId);


		$db = Config::get('reporting.database_config');
		DB::delete("DELETE FROM {$db}.isat_sdm_code WHERE code = ? AND service_id = ? AND sdc = ? AND keyword = ?", array($code, $serviceId,$sdc, $keyword));
	}

}
