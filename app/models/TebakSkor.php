<?php

class TebakSkor extends Eloquent
{
    protected $connection= 'umb-app';
    protected $primaryKey = 'gameid';    
    protected $table = 'tebak_skor';
    protected $fillable = array('club', 'score', 'match_date');
}
