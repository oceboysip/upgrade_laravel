<?php

/**
 * ActivityLog DAO
 *
 * @version 1.0
 *
 */
class ActivityLog extends Eloquent {

	/**
	 * Write activity log
	 * @param $action
	 * @param string $module
	 */
	public static function writeActiviy($action, $module = '', $info = ''){
		try {
			//$userId = isset(Auth::user()) ? Auth::user()->id : -1;
			$userId = -1;
			if (Auth::user()) {
				$userId = Auth::user()->id;
			}
			$url = $_SERVER['REQUEST_URI'];
			$note = $_SERVER['REMOTE_ADDR'] . '::' . $_SERVER['HTTP_USER_AGENT'] . ' - ' . $info;
			DB::insert(
				"INSERT INTO dashboard_user_logs(user_id, user_predicate, dtactivity, module, url,note)
				 VALUES (?,?,NOW(),?,?,?)",
				array($userId, $action, $module, $url, $note)
			);
		}catch (Exception $e) {
			Log::error('Error while saving activity: '. $e);
		}

	}

}

