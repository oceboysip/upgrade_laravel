<?php

/**
 * Charging DAO
 * 
 */
class Charging extends Eloquent {

	/**
	 * Get All SDC
	 *
     * @param $telcoId telco id
     * @param $sdc SDC
	 * @return mixed
	 */
	public static function getListOfCharging($telcoId, $sdc) {
        $db = Config::get('reporting.database_config');
		$results = DB::select(
                "SELECT cr.code, c.id, c.eu_price, c.cp_price, c.cost_bearer, c.telco_sid, c.descr, cr.telco_id, c.is_active, c.sdc
                 FROM {$db}.charging c INNER JOIN {$db}.charging_routing cr ON c.id = cr.charging_id
                 WHERE c.telco_id = {$telcoId} AND c.sdc = '{$sdc}' ");
		return $results;
	}


    /**
     * Save new charging
     *
     * @param $telcoId telco id
     * @param $telcoSid telco sid
     * @param $sdc sdc
     * @param $cpPrice content provide price
     * @param $euPrice end user price
     * @param $costBearer cost bearer
     * @param $descr description
     * @return new assignedId
     */
    public static function saveCharging($telcoId, $telcoSid, $sdc, $cpPrice, $euPrice, $costBearer, $descr) {
        $checkExistingEntry = Charging::isChargingExists($telcoId, $sdc, $telcoSid, $cpPrice);
        if ($checkExistingEntry && sizeof($checkExistingEntry) > 0) {
            Log::warning("Charging of telcoId: {$telcoId}, telcoSid: {$telcoSid}, ".
                        "sdc: {$sdc}, cp_price: {$cpPrice} already exists with Id: " . $checkExistingEntry[0]->id);
            return $checkExistingEntry[0]->id;
        }
        $assignedId = Charging::assignIdForCharging($telcoId, $sdc);
        //$assignedId = 21755;
        $db = Config::get('reporting.database_config');
        DB::insert("INSERT INTO {$db}.charging(id, telco_id, telco_sid, sdc, cp_price, eu_price, cost_bearer, descr)
                    VALUES (?,?,?,?,?,?,?,?)", array($assignedId, $telcoId, $telcoSid, $sdc, $cpPrice, $euPrice, $costBearer, $descr)
        );
        return $assignedId;
    }


    /**
     * Save charging routing
     * @param $chargingId
     * @param $telcoId telco id
     * @param $code
     */
    public static function saveChargingRouting($chargingId, $telcoId,  $code) {
        $db = Config::get('reporting.database_config');
        $success = DB::insert(
            "INSERT INTO {$db}.charging_routing(charging_id, telco_id, code)
             VALUES (?,?,?) ON DUPLICATE KEY UPDATE dtlastupdate = NOW()",
             array($chargingId, $telcoId, $code));
        if (!$success) {
            Log::warning("Routing already exists for charging_id: {$chargingId}, telcoId: {$telcoId}, code: {$code}");
        }
    }


    /**
     * Edit charging
     * @param $chargingId
     * @param $telcoSid
     * @param $cpPrice
     * @param $euPrice
     * @param $costBearer
     * @param $descr
     * @param $isctive is_active
     */
    public static function saveEditCharging($chargingId, $telcoSid, $cpPrice, $euPrice, $costBearer, $descr, $isctive) {
        $db = Config::get('reporting.database_config');
        $success = DB::update(
            "UPDATE {$db}.charging SET telco_sid = ?, cp_price = ?, eu_price = ?, cost_bearer = ?, descr = ?, is_active = ?
             WHERE id = ? ", array($telcoSid, $cpPrice, $euPrice, $costBearer, $descr, $isctive, $chargingId));
        if (!$success) {
            Log::warning("Problem while editing charging_id: {$chargingId}, telcoSid: {$telcoSid}, ".
                         "cpPrice: {$cpPrice}, euPrice: {$euPrice}, costBearer: {$costBearer}, descr: {$descr}");
        }
    }


    /**
     * Get charging by id
     * @param $id
     * @return mixed
     */
    public static function getChargingById($id) {
        $db = Config::get('reporting.database_config');
        return DB::select("SELECT id, telco_id, telco_sid, sdc, cp_price, eu_price, cost_bearer, descr, is_active
                           FROM {$db}.charging
                           WHERE id = {$id}");
    }


    /**
     * Set charging status
     * @param $chargingId the charging id
     * @param int $activeFlag active flag
     */
    public static function setActive($chargingId, $activeFlag = 1) {
        $db = Config::get('reporting.database_config');
        return DB::update("UPDATE {$db}.charging SET is_active = {$activeFlag} WHERE id = {$chargingId}");
    }



    /**
     * Check is charging code exists
     * @param $chargingCode
     * @return mixed
     */
    public static function isChargingCodeExists($chargingCode) {
        $db = Config::get('reporting.database_config');
        $result = DB::select("SELECT cr.code
                           FROM {$db}.charging_routing cr INNER JOIN cdb.charging c ON cr.charging_id = c.id
                           WHERE cr.code = '{$chargingCode}'");
        return (sizeof($result) > 0);
    }


    /**
     * Check is charging exists
     * @param $telcoId telco id
     * @param $sdc SDC
     * @param $telcoSid telco Sid
     * @param $cpPrice cp price
     * @return mixed
     */
    public static function isChargingExists($telcoId, $sdc, $telcoSid, $cpPrice){
        $db = Config::get('reporting.database_config');
        $result = DB::select("SELECT id FROM {$db}.charging
                              WHERE sdc = '{$sdc}' AND telco_id = {$telcoId}
                                    AND telco_sid = '{$telcoSid}' AND cp_price = {$cpPrice}");
        return $result;
    }


    /**
     * Get last id for charging id
     * @param $proposeId
     * @param $maxProposeId
     * @return mixed
     */
    public static function getLastIdForCharingId($proposeId, $maxProposeId) {
        /*
        echo 'ProposeId: ' . $proposeId . PHP_EOL;
        echo 'Max ProposeId: ' . $MaxproposeId . PHP_EOL;
        */
        $db = Config::get('reporting.database_config');
        $result = DB::select(
            "SELECT MAX(id)+1 theId FROM {$db}.charging WHERE id BETWEEN {$proposeId} AND {$maxProposeId}");

        return (sizeof($result) > 0 && $result[0]->theId ? $result[0]->theId : $proposeId);
    }




    /**
     * The core idea of assigning 'Ring' of Id management in Routing
     *
     * @param $telcoId
     * @param $sdc
     * @return the new id for table 'routing'
     */
    public static function assignIdForCharging($telcoId, $sdc) {
        $resSdc = MainServiceConfig::getSdcByName($sdc);
        $SDC_MULTIPLIER = 50;
        $ringDownsdcId = 0;
        $sdcId = 0;
        if (sizeof($resSdc) > 0) {$sdcId = $resSdc[0]->id;}
        if (!$sdcId) {
            //Unknown?
            $sdcId = 35;
            $ringDownsdcId = 35*$SDC_MULTIPLIER;
        }
        else $ringDownsdcId = $sdcId*$SDC_MULTIPLIER;

        //$TELCO_MULTIPLIER = 1000;
        /**
         * Since we already started it :p Thus, we're not following the current cdb.telco
         * instead:
         *
         * XL: 0 - 1000
         * Telkomsel: 1001: 2000
         * Indosat: 2001: 3000
         * Unknown: 20000 - 21000
         *
         */
        $TELCO_MULTIPLIER = 1000;
        $telcoBaseId = 0;
        switch ($telcoId) {
            case 3:
                break;
            case 1:
                $telcoBaseId = 1000;
                break;
            case 2:
                $telcoBaseId = 2000;
                break;
            default:
                $telcoBaseId = 20000;
                break;
        }

        $suggestedId =
            Charging::getLastIdForCharingId(($ringDownsdcId+$telcoBaseId), ($ringDownsdcId+$telcoBaseId+$TELCO_MULTIPLIER));

        //echo PHP_EOL. 'SuggestedId: '. $suggestedId . PHP_EOL;
        return $suggestedId;
    }

}
