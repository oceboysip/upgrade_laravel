<?php

/**
 * Resides all main service configuration DAO
 * 
 */
class Shio extends Eloquent {

    /**
	 * Get All SDC
	 *
	 * @return mixed
	 */
	public static function getShio() {
        $db = Config::get('reporting.database_application');
		$results = DB::select("SELECT UPPER(zodiac) AS zodiac FROM {$db}.content_ilive_isat WHERE 1 GROUP BY zodiac ORDER BY zodiac");
		return $results;
	}


}
