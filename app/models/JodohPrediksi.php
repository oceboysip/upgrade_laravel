<?php

class JodohPrediksi extends Eloquent
{
    protected $connection= 'umb-app';
    protected $table = 'jodoh_prediksi';
    protected $fillable = array('text', 'prediksi_start','prediksi_end','created_at', 'updated_at', 'n_status');
}
