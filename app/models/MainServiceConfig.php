<?php

/**
 * Resides all main service configuration DAO
 * 
 */
class MainServiceConfig extends Eloquent {

    /**
	 * Get All SDC
	 *
	 * @return mixed
	 */
	public static function getAllSdc() {
        //$role_id = Auth::user()->role_id;
        $db = Config::get('reporting.database_config');
		$role_array3 = array(9);
        if ( in_array(Auth::user()->role_id, $role_array3) ) {
            $results = DB::select("SELECT id, TRIM(name) as name
                               FROM {$db}.sdc
                               WHERE id = 3
                               ORDER BY id");
        } else {
            $results = DB::select("SELECT id, TRIM(name) as name
                               FROM {$db}.sdc
                               ORDER BY id");
        }
        
		return $results;
	}


    /**
     * Get All Telco
     *
     * @return mixed
     */
    public static function getAllTelco() {
        $db = Config::get('reporting.database_config');
        $results = DB::select("SELECT id, TRIM(name) as name
                               FROM {$db}.telco
                               WHERE active = 1
                               ORDER BY name");
        return $results;
    }


    /**
     * Get Telco by Id
     *
     * @param $id
     * @return mixed
     */
    public static function getTelcoById($id) {
        $db = Config::get('reporting.database_config');
        $results = DB::select("SELECT id, TRIM(name) as name
                               FROM {$db}.telco
                               WHERE id = {$id}");
        return $results;
    }


    /**
     * Get SDC by Id
     *
     * @param $id
     * @return mixed
     */
    public static function getSdcById($id) {
        $db = Config::get('reporting.database_config');
        $results = DB::select("SELECT id, TRIM(name) as name
                                   FROM {$db}.sdc
                                   WHERE id = {$id}");
        return $results;
    }


    /**
     * Get SDC by Name
     *
     * @param $name
     * @return mixed
     */
    public static function getSdcByName($name) {
        $db = Config::get('reporting.database_config');
        $results = DB::select("SELECT id, TRIM(name) as name
                                   FROM {$db}.sdc
                                   WHERE name = '{$name}'");
        return $results;
    }


    /**
     * Get list of service group
     */
    public static function getServiceGroup() {
        $db = Config::get('reporting.database_config');
        $results = DB::select("SELECT id, TRIM(name) as name
                               FROM {$db}.service_group");
        return $results;
    }


    /**
     * Get All Service
     *
     * @param $telcoId
     * @param $sdc
     * @return mixed
     */
    public static function getService($telcoId = 0, $sdc = '') {
        $db = Config::get('reporting.database_config');
        $results = array();
        if ($telcoId > 0 && $sdc == '') {
            $results = DB::select(
                "SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                 FROM {$db}.service
                 WHERE telcoId = {$telcoId} ORDER BY name");
        } else if ($telcoId == 0 && $sdc != '') {
            $results = DB::select(
                            "SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                             FROM {$db}.service
                             WHERE sdc = '{$sdc}' ORDER BY name");
        } else if ($telcoId > 0 && $sdc != '') {
            $results = DB::select(
                            "SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                             FROM {$db}.service
                             WHERE telcoId = {$telcoId} AND sdc = '{$sdc}' ORDER BY name");
        } else {
            $results = DB::select(
                "SELECT id, name, url, sdc,telcoId, serviceGroupId, dtlive, is_active
                 FROM {$db}.service ORDER BY name");
        }
        return $results;
    }


    /**
     * Get All Active Service
     *
     * @param $telcoId
     * @param $sdc
     * @return mixed
     */
    public static function getActiveService($telcoId = 0, $sdc = '') {
        $results = array();
        $db = Config::get('reporting.database_config');
        if ($telcoId > 0 && $sdc == '') {
            $results = DB::select(
                "SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                 FROM {$db}.service
                 WHERE telcoId = {$telcoId} AND is_active = 1 ORDER BY name");
        } else if ($telcoId == 0 && $sdc != '') {
            $results = DB::select(
                "SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                 FROM {$db}.service
                 WHERE sdc = '{$sdc}' AND is_active = 1 ORDER BY name");
        } else if ($telcoId > 0 && $sdc != '') {
            $results = DB::select(
                "SELECT id, name, url, sdc, telcoId, serviceGroupId, dtlive, is_active
                 FROM {$db}.service
                 WHERE telcoId = {$telcoId} AND sdc = '{$sdc}' AND is_active = 1 ORDER BY name");
        } else {
            $results = DB::select(
                "SELECT id, name, url, sdc,telcoId, serviceGroupId, dtlive, is_active
                 FROM {$db}.service WHERE is_active = 1 ORDER BY name");
        }
        return $results;
    }

    /**
     * Get All Registered Keywords
     *
     * @param $telcoId
	 * @param $sdc
     * @param $serviceId
     * @return mixed
     */
    public static function getActiveKeywords($telcoId = 0, $sdc = '',$serviceId = 0) {
        $results = array();
        $db = Config::get('reporting.database_config');
        if ($telcoId > 0 && $sdc != '') {
			if ($serviceId > 0) {
				$results = DB::select(
					"SELECT id, service_id, sdc, keyword, telco_id, is_default
                 FROM {$db}.service_keyword
                 WHERE telco_id = {$telcoId} AND service_id = {$serviceId} AND sdc = '{$sdc}' ORDER BY keyword");
			} else {
				$results = DB::select(
					"SELECT id, service_id, sdc, keyword, telco_id, is_default
                 FROM {$db}.service_keyword
                 WHERE telco_id = {$telcoId} AND sdc = '{$sdc}' ORDER BY keyword");
			}
        } else if ($telcoId > 0 && $sdc == '') {
			if ($serviceId > 0) {
				$results = DB::select(
					"SELECT id, service_id, sdc, keyword, telco_id, is_default
                 FROM {$db}.service_keyword
                 WHERE telco_id = {$telcoId} AND service_id = {$serviceId} ORDER BY keyword");
			} else {
				$results = DB::select(
					"SELECT id, service_id, sdc, keyword, telco_id, is_default
                 FROM {$db}.service_keyword
                 WHERE telco_id = {$telcoId} ORDER BY keyword");
			}
        } else {
            $results = DB::select(
                "SELECT id, service_id, sdc, keyword, telco_id, is_default
                 FROM {$db}.service_keyword
                 ORDER BY keyword");
        }
        return $results;
    }
}
