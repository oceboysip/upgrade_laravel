<?php

/**
 * Resides all subscriber related DAO
 * 
 */
class Subscriber extends Eloquent {

    /**
     * Get registration report between dates
	 * @param $dateStart date start
	 * @param $dateEnd date end
     * @param $applicationId application id
     */
    public static function getRegOnDate($dateStart, $dateEnd, $applicationId) {
        $db = Config::get('reporting.database_application');

		$results = DB::select(
			"SELECT b.name, dt, SUM(total) as total, SUM(total_unreg) as total_unreg, SUM(total_unreg_on_day) as total_unreg_on_day
			 FROM (
				SELECT application_id, DATE(dtregister) as dt, COUNT(*) as total, 0 as total_unreg, 0 as total_unreg_on_day
				FROM {$db}.subscriber
				WHERE application_id = {$applicationId}
					AND dtregister BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
				GROUP BY application_id,dt
				UNION ALL
				SELECT application_id,DATE(dtregister) as dt, COUNT(*) as total, 0 as total_unreg, 0 as total_unreg_on_day
				FROM {$db}.deleted_subscriber
				WHERE application_id = {$applicationId}
					AND dtregister BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
				GROUP BY application_id,dt
				UNION ALL
				SELECT application_id,DATE(dtdeleted) as dt, 0 as total, COUNT(*) as total_unreg, 0 as total_unreg_on_day
				FROM {$db}.deleted_subscriber
				WHERE application_id = {$applicationId}
					AND dtdeleted BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
				GROUP BY application_id,dt
				UNION ALL
				SELECT  application_id,DATE(dtregister) as dt, 0 as total, 0 as total_unreg, COUNT(*) as total_unreg_on_day 
				FROM {$db}.deleted_subscriber
		    		WHERE application_id = {$applicationId}
				AND DATE_FORMAT(dtregister,'%Y-%m-%d') = DATE_FORMAT(dtdeleted,'%Y-%m-%d')
					AND dtregister BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
				GROUP BY application_id,dt

			) a INNER JOIN {$db}.application b on a.application_id = b.id
			GROUP BY b.name, dt
			ORDER BY dt DESC");

		return $results;
    }

    public static function getRegPromoChannel($dateStart, $dateEnd, $keyword, $applicationId) {
        $db = Config::get('reporting.database_application');

        $results = DB::select(
            "SELECT b.name, dt, SUM(total) as total, SUM(total_unreg) as total_unreg, SUM(total_unreg_on_day) as total_unreg_on_day
			 FROM (
				SELECT application_id, DATE(dtregister) as dt, COUNT(*) as total, 0 as total_unreg, 0 as total_unreg_on_day
				FROM {$db}.subscriber
				WHERE application_id = {$applicationId}
					AND dtregister BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
					AND reg_sms = 'REG {$keyword}'
				GROUP BY application_id,dt
				UNION ALL
				SELECT application_id,DATE(dtregister) as dt, COUNT(*) as total, 0 as total_unreg, 0 as total_unreg_on_day
				FROM {$db}.deleted_subscriber
				WHERE application_id = {$applicationId}
					AND dtregister BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
					AND reg_sms = 'REG {$keyword}'
				GROUP BY application_id,dt
				UNION ALL
				SELECT application_id,DATE(dtdeleted) as dt, 0 as total, COUNT(*) as total_unreg, 0 as total_unreg_on_day
				FROM {$db}.deleted_subscriber
				WHERE application_id = {$applicationId}
					AND dtdeleted BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
					AND reg_sms = 'REG {$keyword}'
				GROUP BY application_id,dt
				UNION ALL
				SELECT  application_id,DATE(dtregister) as dt, 0 as total, 0 as total_unreg, COUNT(*) as total_unreg_on_day
				FROM {$db}.deleted_subscriber
		    		WHERE application_id = {$applicationId}
				AND DATE_FORMAT(dtregister,'%Y-%m-%d') = DATE_FORMAT(dtdeleted,'%Y-%m-%d')
					AND dtregister BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
					AND reg_sms = 'REG {$keyword}'
				GROUP BY application_id,dt

			) a INNER JOIN {$db}.application b on a.application_id = b.id
			GROUP BY b.name, dt
			ORDER BY dt DESC");

        return $results;
    }


	/**
	 * Get unregistration report between dates
	 * @param $dateStart date start
	 * @param $dateEnd date end
	 * @param $applicationId application id
	 */
	public static function getUnregOnDate($dateStart, $dateEnd, $applicationId) {
		$db = Config::get('reporting.database_application');
		$results = DB::select(
			"SELECT b.name, dt, SUM(total) as total
				 FROM (
					SELECT application_id,DATE(dtdeleted) as dt, COUNT(*) as total
					FROM {$db}.deleted_subscriber
					WHERE application_id = {$applicationId}
						AND dtdeleted BETWEEN '{$dateStart} 00:00:00' AND '{$dateEnd} 23:59:59'
					GROUP BY application_id,dt
				) a INNER JOIN {$db}.application b on a.application_id = b.id
				GROUP BY b.name, dt
				ORDER BY dt");

		return $results;
	}

}
