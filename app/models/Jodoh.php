<?php

class Jodoh extends Eloquent
{
    protected $connection= 'umb-app';
    protected $table = 'jodoh';
    protected $fillable = array('text', 'created_at', 'updated_at', 'n_status');
}
