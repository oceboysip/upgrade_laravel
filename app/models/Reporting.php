<?php

/**
 * Resides all reporting related DAO
 * 
 */
class Reporting extends Eloquent {

	/**
	 * Get MO stat on date
	 * @param $dateStart a date start
	 * @param $dateEnd a date end
	 * @param $telcoId telco id
	 * @param int $serviceId
	 * @param string $sdc
	 * @param int $useKeyword
	 * @return array
	 */
	public static function getMoStatOnDate($dateStart, $dateEnd, $telcoId, $serviceId = 0, $sdc = '', $useKeyword = 0) {
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');
		$serviceSql = $serviceId > 0 ? " AND mo.service_id = {$serviceId} " : "";

		if ($useKeyword) {
			$results = DB::select(
				"SELECT mo.stat_date as dtstat, c.name, mo.keyword, SUM(mo.total) AS total
				FROM {$rptdb}.daily_incoming_mo_stat mo INNER JOIN {$cdb}.service c ON mo.service_id = c.id
				WHERE mo.telco_id = {$telcoId} {$serviceSql}
						AND mo.sdc = {$sdc} AND mo.stat_date BETWEEN '{$dateStart}' AND '{$dateEnd}'
				GROUP BY mo.stat_date, c.name,mo.keyword
				ORDER BY dtstat DESC");

		} else {
			$results = DB::select(
				"SELECT mo.stat_date as dtstat, c.name, ' ' as keyword, SUM(mo.total) AS total
				FROM {$rptdb}.daily_incoming_mo_stat mo INNER JOIN {$cdb}.service c ON mo.service_id = c.id
				WHERE mo.telco_id = {$telcoId} {$serviceSql}
						AND mo.sdc = {$sdc} AND mo.stat_date BETWEEN '{$dateStart}' AND '{$dateEnd}'
				GROUP BY mo.stat_date, c.name, keyword
				ORDER BY dtstat DESC");

		}

		return $results;
	}


	/**
	 * Get revenue on date
	 * @param $date a date
	 * @param int $serviceId
	 * @param string $sdc
	 * @return array
	 */
	public static function getRevenueOnDate($date, $serviceId = 0, $sdc = '') {
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');

		$total = array();
		if ($serviceId && $sdc) {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
							AND t.sdc = {$sdc} AND dtstat = '{$date}'");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
			if ($results && sizeof($results) > 0) {
				$total = $results[0]->total;
			}
		} else {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = dtstat = '{$date}'");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
		}

		return $total;
	}

    public static function getRevenueOnDateISAT($date, $serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
							AND t.sdc in ('99858','98858') AND dtstat = '{$date}' AND t.telco_id=2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = dtstat = '{$date}' AND t.telco_id=2 AND t.sdc in ('99858','98858')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getRevenueOnDateISATPlaymedia($date, $serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
							AND t.sdc in ('99199','99919') AND dtstat = '{$date}' AND t.telco_id=2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = dtstat = '{$date}' AND t.telco_id=2 AND t.sdc in ('99199','99919')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getRevenueOnDateXL($date, $serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
							AND t.sdc = {$sdc} AND dtstat = '{$date}' AND t.telco_id=3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = dtstat = '{$date}' AND t.telco_id=3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getRevenueOnDateXLPlaymedia($date, $serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
							AND t.sdc in ('99199','99919') AND dtstat = '{$date}' AND t.telco_id=3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = dtstat = '{$date}' AND t.telco_id=3 AND t.sdc in ('99199','99919')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getRevenueOnDateTSEL($date, $serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
							AND t.sdc = {$sdc} AND dtstat = '{$date}' AND t.telco_id=1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = dtstat = '{$date}' AND t.telco_id=1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }


	/**
	 * Get revenue between date and date
	 * @param $dateStart a start date
	 * @param $dateEnd an end date
	 * @param $telcoId telco id
	 * @param int $serviceId
	 * @param string $sdc
	 * @return array
	 */
	public static function getRevenueOnDateBetween($dateStart, $dateEnd, $telcoId, $serviceId = 0, $sdc = '', $keyword) {
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');
		$mts = Config::get('reporting.database_application');

		$serviceSql = $serviceId > 0 ? " AND t.service_id = {$serviceId} " : "";
        $channel_promo = $keyword != 'undefined' ? "AND a.keyword = '{$keyword}'" : "";

		$results = DB::select(
			//"SELECT dtstat, c.telco_sid, c.cp_price, SUM(total_original) as total_original, SUM(total_sent) as total_sent,
			//	SUM(total_success) as total_success,IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			//	IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net,
			//	SUM(total_retry) as total_retry, SUM(total_success_retry) as total_success_retry
			//FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
			//WHERE t.telco_id = {$telcoId} {$serviceSql} {$channel_promo}
			//		AND t.sdc = {$sdc} AND dtstat BETWEEN '{$dateStart}' AND '{$dateEnd}'
			//GROUP BY dtstat, c.telco_sid
			//ORDER BY dtstat DESC");

			"SELECT dtstat, c.telco_sid, c.cp_price, SUM(total_sent) as total_sent, SUM(total_success) as total_success,
				SUM(total_original) as total_original, SUM(total_original_real) as total_original_real, SUM(total_success_ori) as total_success_ori,
				SUM(total_retry) as total_retry, SUM(total_success_retry) as total_success_retry,
				IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
				IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net,
				SUM(total_push1) as total_push1, SUM(total_push1_original) as total_push1_original, SUM(total_success_push1) as total_success_push1
			FROM {$rptdb}.traffic_mt_summary_channel t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
			INNER JOIN {$mts}.application_channel_promo a ON a.id = t.channel
			WHERE t.telco_id = {$telcoId} {$serviceSql} {$channel_promo}
					AND t.sdc = {$sdc} AND dtstat BETWEEN '{$dateStart}' AND '{$dateEnd}'
			GROUP BY dtstat, c.telco_sid, t.charging_id
			ORDER BY dtstat DESC");

		//$results = DB::select(
                //        "SELECT dtstat, c.telco_sid, c.cp_price, SUM(total_original) as total_original, SUM(total_sent) as total_sent,
                //                SUM(total_success) as total_success,IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
		//		IFNULL(ROUND((total_success / total_sent) * 100),0) as total_net
                //        FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
                //        WHERE t.telco_id = {$telcoId} {$serviceSql}
                //                        AND t.sdc = {$sdc} AND dtstat BETWEEN '{$dateStart}' AND '{$dateEnd}'
                //        GROUP BY dtstat, c.telco_sid
                //        ORDER BY dtstat DESC");

		return $results;
	}

    public static function getTrafficOnDateBetween($dateStart, $dateEnd, $telcoId, $serviceId = 0, $sdc = '', $keyword) {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');
        $mts = Config::get('reporting.database_application');

        $serviceSql = $serviceId > 0 ? " AND a.application_id = {$serviceId} " : "";
        $channel_promo = $keyword != 'undefined' ? "AND a.keyword = '{$keyword}'" : "";

        $results = DB::select("SELECT dtreport, sum(success_original) as success_original,
                          sum(original_sent) as original_sent,
                          sum(success_retry) as success_retry,
                          sum(retry_sent) as retry_sent,
                          sum(success_push_1) as success_push_1,
                          sum(push_1_sent) as push_1_sent,
                          sum(success_retry_push_1) as success_retry_push_1,
                          sum(retry_sent_push_1) as retry_sent_push_1 FROM {$mts}.application_channel_promo a
                        INNER JOIN {$rptdb}.traffic_mt_summary_revenue b ON a.id = b.channel
                        WHERE a.telco_id={$telcoId} AND a.sdc = {$sdc}
                        {$serviceSql} {$channel_promo}
                        AND dtreport BETWEEN '{$dateStart}' AND '{$dateEnd}'
                        GROUP BY dtreport");

        return $results;
    }



	/**
	 * Get Today's revenue
	 * @param int $serviceId
	 * @param string $sdc
	 * @return array
	 */
	public static function getTodayRevenue($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');

		$total = array();
		if ($serviceId && $sdc) {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
						AND t.sdc = {$sdc} AND dtstat = DATE(NOW())");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
			if ($results && sizeof($results) > 0) {
				$total = $results[0]->total;
			}
		} else {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = DATE(NOW())");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
		}

		return $total;
    }

    public static function getTodayRevenueISAT($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
						AND t.sdc in ('99858','98858') AND dtstat = DATE(NOW()) AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = DATE(NOW()) AND t.telco_id = 2 AND t.sdc in ('99858','98858')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getTodayRevenueISATPlaymedia($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
						AND t.sdc in ('99199','99919') AND dtstat = DATE(NOW()) AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = DATE(NOW()) AND t.telco_id = 2 AND t.sdc in ('99199','99919')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getTodayRevenueXL($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
						AND t.sdc = {$sdc} AND dtstat = DATE(NOW()) AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = DATE(NOW()) AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getTodayRevenueXLPlaymedia($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
						AND t.sdc in ('99199','99919') AND dtstat = DATE(NOW()) AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = DATE(NOW()) AND t.telco_id = 3 AND t.sdc in ('99199','99919')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

    public static function getTodayRevenueTSEL($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId}
						AND t.sdc = {$sdc} AND dtstat = DATE(NOW()) AND t.telco_id = 1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE dtstat = DATE(NOW()) AND t.telco_id = 1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }

        return $total;
    }

	/**
	 * Get this months' revenue
	 * @param $year
	 * @param $month
	 * @param int $serviceId
	 * @param string $sdc
	 * @return array
	 */
	public static function getMonthRevenue($year,$month, $serviceId = 0, $sdc = '')
	{
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');

		$total = array();
		if ($serviceId && $sdc) {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc = {$sdc} AND MONTH(dtstat) =  MONTH(NOW())");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
			if ($results && sizeof($results) > 0) {
				$total = $results[0]->total;
			}
		} else {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE MONTH(dtstat) = {$month} AND YEAR(dtstat) = {$year}");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
		}
		return $total;
	}

    public static function getMonthRevenueISAT($year,$month, $serviceId = 0, $sdc = '')
    {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc in ('99858','98858') AND MONTH(dtstat) =  MONTH(NOW())
				AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE MONTH(dtstat) = {$month} AND YEAR(dtstat) = {$year} AND t.sdc in ('99858','98858')
				AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getMonthRevenueISATPlaymedia($year,$month, $serviceId = 0, $sdc = '')
    {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc in ('99199','99919') AND MONTH(dtstat) =  MONTH(NOW())
				AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE MONTH(dtstat) = {$month} AND YEAR(dtstat) = {$year} AND t.sdc in ('99199','99919')
				AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getMonthRevenueXL($year,$month, $serviceId = 0, $sdc = '')
    {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc = {$sdc} AND MONTH(dtstat) =  MONTH(NOW())
				AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE MONTH(dtstat) = {$month} AND YEAR(dtstat) = {$year}
				AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getMonthRevenueXLPlaymedia($year,$month, $serviceId = 0, $sdc = '')
    {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc in ('99199','99919') AND MONTH(dtstat) =  MONTH(NOW())
				AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE MONTH(dtstat) = {$month} AND YEAR(dtstat) = {$year} AND t.sdc in ('99199','99919')
				AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getMonthRevenueTSEL($year,$month, $serviceId = 0, $sdc = '')
    {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc = {$sdc} AND MONTH(dtstat) =  MONTH(NOW())
				AND t.telco_id = 1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
						IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE MONTH(dtstat) = {$month} AND YEAR(dtstat) = {$year}
				AND t.telco_id = 1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

	/**
	 * Get this months' revenue
	 * @param int $serviceId
	 * @param string $sdc
	 * @return array
	 */
	public static function getYesterdayRevenue($serviceId = 0, $sdc = '') {
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');

		$total = array();
		if ($serviceId && $sdc) {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			   							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc = {$sdc}
						AND dtstat = DATE(NOW()) - INTERVAL 1 DAY");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
			if ($results && sizeof($results) > 0) {
				$total = $results[0]->total;
			}
		} else {
			$results = DB::select(
				"SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
					FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
					WHERE dtstat = DATE(NOW()) - INTERVAL 1 DAY");
			if ($results && sizeof($results) > 0) {
				$total['gross'] = $results[0]->total_gross;
				$total['net'] = $results[0]->total_net;
			}
		}
		return $total;
	}

    public static function getYesterdayRevenueISAT($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			   							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc in ('99858','98858')
						AND dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
					FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
					WHERE dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 2 AND t.sdc in ('99858','98858')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getYesterdayRevenueISATPlaymedia($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			   							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc in ('99199','99919')
						AND dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 2");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
					FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
					WHERE dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 2 AND t.sdc in ('99199','99919')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getYesterdayRevenueXL($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			   							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc = {$sdc}
						AND dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
					FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
					WHERE dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getYesterdayRevenueXLPlaymedia($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			   							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc in ('99199','99919')
						AND dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 3");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
					FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
					WHERE dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 3 AND t.sdc in ('99199','99919')");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

    public static function getYesterdayRevenueTSEL($serviceId = 0, $sdc = '') {
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');

        $total = array();
        if ($serviceId && $sdc) {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
			   							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
				FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
				WHERE t.service_id = {$serviceId} AND t.sdc = {$sdc}
						AND dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
            if ($results && sizeof($results) > 0) {
                $total = $results[0]->total;
            }
        } else {
            $results = DB::select(
                "SELECT IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
							IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
					FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
					WHERE dtstat = DATE(NOW()) - INTERVAL 1 DAY AND t.telco_id = 1");
            if ($results && sizeof($results) > 0) {
                $total['gross'] = $results[0]->total_gross;
                $total['net'] = $results[0]->total_net;
            }
        }
        return $total;
    }

	/**
	 * Total all revenue this week
	 * @param  $sdc
	 * @param  integer $serviceId
	 * @return set of data
	 *
	 */
	public static function totalAllRevenueThisWeek($sdc = '',$serviceId = 0){
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');
		if ($sdc == '') {
			$results = DB::select(
				"SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
									IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
								FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
								WHERE
									/* t.service_id = {$serviceId} AND t.sdc = {$sdc} AND */
									dtstat BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW()
								GROUP BY dtstat ORDER BY dtstat");
		} else {
			$results = DB::select(
				"SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
												IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
											FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
											WHERE
												t.sdc = {$sdc} AND
												dtstat BETWEEN DATE(NOW()) - INTERVAL 7 DAY AND NOW()
											GROUP BY dtstat ORDER BY dtstat");
		}

		$today = 0;
		$yesterday1 = 0;
		$yesterday2 = 0;
		$yesterday3 = 0;
		$yesterday4 = 0;
		$yesterday5 = 0;
		$yesterday6 = 0;

		foreach ($results as $tmp) {
			if ($tmp->dtstat == date("Y-m-d"))
				$today += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 24)))
				$yesterday1 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 48)))
				$yesterday2 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 72)))
				$yesterday3 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 96)))
				$yesterday4 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 120)))
				$yesterday5 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 144)))
				$yesterday6 += $tmp->total_gross;
		}

		$day[0] = $yesterday6;
		$day[1] = $yesterday5;
		$day[2] = $yesterday4;
		$day[3] = $yesterday3;
		$day[4] = $yesterday2;
		$day[5] = $yesterday1;
		$day[6] = $today;

		return $day;
	}

    public static function totalAllRevenueThisWeekISAT($sdc = '',$serviceId = 0){
		//2018-02-08 - saat ini KMI hanya 99858 & 98858, selain itu dibuat 0
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');
        if (($sdc == '99858')||($sdc == '98858')) {
			$results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
												IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
											FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
											WHERE
												t.sdc = {$sdc} AND
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND t.telco_id = 2
											GROUP BY dtstat ORDER BY dtstat");			
        } else {
			//hanya ambil tanggal saja
            $results = DB::select(
                "SELECT dtstat, 0 as total_gross, 0 as total_net
											FROM {$rptdb}.traffic_mt_summary
											WHERE
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND telco_id = 2
											GROUP BY dtstat ORDER BY dtstat");
        }

        $today = 0;
		$yesterday1 = 0;
		$yesterday2 = 0;
		$yesterday3 = 0;
		$yesterday4 = 0;
		$yesterday5 = 0;
		$yesterday6 = 0;
		$yesterday7 = 0;
		$yesterday8 = 0;
		$yesterday9 = 0;
		$yesterday10 = 0;
		$yesterday11 = 0;
		$yesterday12 = 0;
		$yesterday13 = 0;
		$yesterday14 = 0;
		$yesterday15 = 0;
		$yesterday16 = 0;
		$yesterday17 = 0;
		$yesterday18 = 0;
		$yesterday19 = 0;
		$yesterday20 = 0;
		$yesterday21 = 0;
		$yesterday22 = 0;
		$yesterday23 = 0;
		$yesterday24 = 0;
		$yesterday25 = 0;
		$yesterday26 = 0;
		$yesterday27 = 0;
		$yesterday28 = 0;
		$yesterday29 = 0;
		$yesterday30 = 0;

		foreach ($results as $tmp) {
			if ($tmp->dtstat == date("Y-m-d"))
				$today += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 24)))
				$yesterday1 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 48)))
				$yesterday2 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 72)))
				$yesterday3 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 96)))
				$yesterday4 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 120)))
				$yesterday5 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 144)))
				$yesterday6 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 168)))
				$yesterday7 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 192)))
				$yesterday8 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 216)))
				$yesterday9 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 240)))
				$yesterday10 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 264)))
				$yesterday11 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 288)))
				$yesterday12 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 312)))
				$yesterday13 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 336)))
				$yesterday14 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 360)))
				$yesterday15 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 384)))
				$yesterday16 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 408)))
				$yesterday17 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 432)))
				$yesterday18 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 456)))
				$yesterday19 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 480)))
				$yesterday20 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 504)))
				$yesterday21 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 528)))
				$yesterday22 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 552)))
				$yesterday23 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 576)))
				$yesterday24 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 600)))
				$yesterday25 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 624)))
				$yesterday26 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 648)))
				$yesterday27 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 672)))
				$yesterday28 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 696)))
				$yesterday29 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 720)))
				$yesterday30 += $tmp->total_gross;
			
		}

		$day[0] = $yesterday30;
		$day[1] = $yesterday29;
		$day[2] = $yesterday28;
		$day[3] = $yesterday27;
		$day[4] = $yesterday26;
		$day[5] = $yesterday25;
		$day[6] = $yesterday24;
		$day[7] = $yesterday23;
		$day[8] = $yesterday22;
		$day[9] = $yesterday21;
		$day[10] = $yesterday20;
		$day[11] = $yesterday19;
		$day[12] = $yesterday18;
		$day[13] = $yesterday17;
		$day[14] = $yesterday16;
		$day[15] = $yesterday15;
		$day[16] = $yesterday14;
		$day[17] = $yesterday13;
		$day[18] = $yesterday12;
		$day[19] = $yesterday11;
		$day[20] = $yesterday10;
		$day[21] = $yesterday9;
		$day[22] = $yesterday8;
		$day[23] = $yesterday7;
		$day[24] = $yesterday6;
		$day[25] = $yesterday5;
		$day[26] = $yesterday4;
		$day[27] = $yesterday3;
		$day[28] = $yesterday2;
		$day[29] = $yesterday1;
		$day[30] = $today;

        return $day;
    }

    public static function totalAllRevenueThisWeekISATPlaymedia($sdc = '',$serviceId = 0){
		//2018-02-08 - saat ini Playmedia hanya 99199 & 99919, selain itu dibuat 0
		$cdb = Config::get('reporting.database_config');
		$rptdb = Config::get('reporting.database_reporting');
		
		if (($sdc == '99199')||($sdc == '99919')) {
			$results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
												IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
											FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
											WHERE
												t.sdc = {$sdc} AND
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND t.telco_id = 2
											GROUP BY dtstat ORDER BY dtstat");			
        } else {
			//hanya ambil tanggal saja
            $results = DB::select(
                "SELECT dtstat, 0 as total_gross, 0 as total_net
											FROM {$rptdb}.traffic_mt_summary
											WHERE
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND telco_id = 2
											GROUP BY dtstat ORDER BY dtstat");
        }

        $today = 0;
		$yesterday1 = 0;
		$yesterday2 = 0;
		$yesterday3 = 0;
		$yesterday4 = 0;
		$yesterday5 = 0;
		$yesterday6 = 0;
		$yesterday7 = 0;
		$yesterday8 = 0;
		$yesterday9 = 0;
		$yesterday10 = 0;
		$yesterday11 = 0;
		$yesterday12 = 0;
		$yesterday13 = 0;
		$yesterday14 = 0;
		$yesterday15 = 0;
		$yesterday16 = 0;
		$yesterday17 = 0;
		$yesterday18 = 0;
		$yesterday19 = 0;
		$yesterday20 = 0;
		$yesterday21 = 0;
		$yesterday22 = 0;
		$yesterday23 = 0;
		$yesterday24 = 0;
		$yesterday25 = 0;
		$yesterday26 = 0;
		$yesterday27 = 0;
		$yesterday28 = 0;
		$yesterday29 = 0;
		$yesterday30 = 0;

		foreach ($results as $tmp) {
			if ($tmp->dtstat == date("Y-m-d"))
				$today += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 24)))
				$yesterday1 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 48)))
				$yesterday2 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 72)))
				$yesterday3 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 96)))
				$yesterday4 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 120)))
				$yesterday5 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 144)))
				$yesterday6 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 168)))
				$yesterday7 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 192)))
				$yesterday8 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 216)))
				$yesterday9 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 240)))
				$yesterday10 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 264)))
				$yesterday11 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 288)))
				$yesterday12 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 312)))
				$yesterday13 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 336)))
				$yesterday14 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 360)))
				$yesterday15 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 384)))
				$yesterday16 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 408)))
				$yesterday17 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 432)))
				$yesterday18 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 456)))
				$yesterday19 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 480)))
				$yesterday20 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 504)))
				$yesterday21 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 528)))
				$yesterday22 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 552)))
				$yesterday23 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 576)))
				$yesterday24 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 600)))
				$yesterday25 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 624)))
				$yesterday26 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 648)))
				$yesterday27 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 672)))
				$yesterday28 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 696)))
				$yesterday29 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 720)))
				$yesterday30 += $tmp->total_gross;
			
		}

		$day[0] = $yesterday30;
		$day[1] = $yesterday29;
		$day[2] = $yesterday28;
		$day[3] = $yesterday27;
		$day[4] = $yesterday26;
		$day[5] = $yesterday25;
		$day[6] = $yesterday24;
		$day[7] = $yesterday23;
		$day[8] = $yesterday22;
		$day[9] = $yesterday21;
		$day[10] = $yesterday20;
		$day[11] = $yesterday19;
		$day[12] = $yesterday18;
		$day[13] = $yesterday17;
		$day[14] = $yesterday16;
		$day[15] = $yesterday15;
		$day[16] = $yesterday14;
		$day[17] = $yesterday13;
		$day[18] = $yesterday12;
		$day[19] = $yesterday11;
		$day[20] = $yesterday10;
		$day[21] = $yesterday9;
		$day[22] = $yesterday8;
		$day[23] = $yesterday7;
		$day[24] = $yesterday6;
		$day[25] = $yesterday5;
		$day[26] = $yesterday4;
		$day[27] = $yesterday3;
		$day[28] = $yesterday2;
		$day[29] = $yesterday1;
		$day[30] = $today;

        return $day;
    }

    public static function totalAllRevenueThisWeekXL($sdc = '',$serviceId = 0){
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');
        if ($sdc == '') {
            $results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
									IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
								FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
								WHERE
									/* t.service_id = {$serviceId} AND t.sdc = {$sdc} AND */
									dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
									AND t.telco_id = 3
								GROUP BY dtstat ORDER BY dtstat");
        } else {
            $results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
												IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
											FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
											WHERE
												t.sdc = {$sdc} AND
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND t.telco_id = 3
											GROUP BY dtstat ORDER BY dtstat");
        }

        $today = 0;
		$yesterday1 = 0;
		$yesterday2 = 0;
		$yesterday3 = 0;
		$yesterday4 = 0;
		$yesterday5 = 0;
		$yesterday6 = 0;
		$yesterday7 = 0;
		$yesterday8 = 0;
		$yesterday9 = 0;
		$yesterday10 = 0;
		$yesterday11 = 0;
		$yesterday12 = 0;
		$yesterday13 = 0;
		$yesterday14 = 0;
		$yesterday15 = 0;
		$yesterday16 = 0;
		$yesterday17 = 0;
		$yesterday18 = 0;
		$yesterday19 = 0;
		$yesterday20 = 0;
		$yesterday21 = 0;
		$yesterday22 = 0;
		$yesterday23 = 0;
		$yesterday24 = 0;
		$yesterday25 = 0;
		$yesterday26 = 0;
		$yesterday27 = 0;
		$yesterday28 = 0;
		$yesterday29 = 0;
		$yesterday30 = 0;

		foreach ($results as $tmp) {
			if ($tmp->dtstat == date("Y-m-d"))
				$today += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 24)))
				$yesterday1 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 48)))
				$yesterday2 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 72)))
				$yesterday3 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 96)))
				$yesterday4 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 120)))
				$yesterday5 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 144)))
				$yesterday6 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 168)))
				$yesterday7 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 192)))
				$yesterday8 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 216)))
				$yesterday9 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 240)))
				$yesterday10 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 264)))
				$yesterday11 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 288)))
				$yesterday12 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 312)))
				$yesterday13 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 336)))
				$yesterday14 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 360)))
				$yesterday15 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 384)))
				$yesterday16 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 408)))
				$yesterday17 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 432)))
				$yesterday18 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 456)))
				$yesterday19 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 480)))
				$yesterday20 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 504)))
				$yesterday21 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 528)))
				$yesterday22 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 552)))
				$yesterday23 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 576)))
				$yesterday24 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 600)))
				$yesterday25 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 624)))
				$yesterday26 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 648)))
				$yesterday27 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 672)))
				$yesterday28 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 696)))
				$yesterday29 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 720)))
				$yesterday30 += $tmp->total_gross;
			
		}

		$day[0] = $yesterday30;
		$day[1] = $yesterday29;
		$day[2] = $yesterday28;
		$day[3] = $yesterday27;
		$day[4] = $yesterday26;
		$day[5] = $yesterday25;
		$day[6] = $yesterday24;
		$day[7] = $yesterday23;
		$day[8] = $yesterday22;
		$day[9] = $yesterday21;
		$day[10] = $yesterday20;
		$day[11] = $yesterday19;
		$day[12] = $yesterday18;
		$day[13] = $yesterday17;
		$day[14] = $yesterday16;
		$day[15] = $yesterday15;
		$day[16] = $yesterday14;
		$day[17] = $yesterday13;
		$day[18] = $yesterday12;
		$day[19] = $yesterday11;
		$day[20] = $yesterday10;
		$day[21] = $yesterday9;
		$day[22] = $yesterday8;
		$day[23] = $yesterday7;
		$day[24] = $yesterday6;
		$day[25] = $yesterday5;
		$day[26] = $yesterday4;
		$day[27] = $yesterday3;
		$day[28] = $yesterday2;
		$day[29] = $yesterday1;
		$day[30] = $today;

        return $day;
    }

    public static function totalAllRevenueThisWeekXLPlaymedia($sdc = '',$serviceId = 0){
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');
        
        if (($sdc == '99199')||($sdc == '99919')) {
			$results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
												IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
											FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
											WHERE
												t.sdc = {$sdc} AND
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND t.telco_id = 3
											GROUP BY dtstat ORDER BY dtstat");			
        } else {
            $results = DB::select(
                "SELECT dtstat, 0 as total_gross, 0 as total_net
											FROM {$rptdb}.traffic_mt_summary
											WHERE
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND telco_id = 3
											GROUP BY dtstat ORDER BY dtstat");
        }

        $today = 0;
		$yesterday1 = 0;
		$yesterday2 = 0;
		$yesterday3 = 0;
		$yesterday4 = 0;
		$yesterday5 = 0;
		$yesterday6 = 0;
		$yesterday7 = 0;
		$yesterday8 = 0;
		$yesterday9 = 0;
		$yesterday10 = 0;
		$yesterday11 = 0;
		$yesterday12 = 0;
		$yesterday13 = 0;
		$yesterday14 = 0;
		$yesterday15 = 0;
		$yesterday16 = 0;
		$yesterday17 = 0;
		$yesterday18 = 0;
		$yesterday19 = 0;
		$yesterday20 = 0;
		$yesterday21 = 0;
		$yesterday22 = 0;
		$yesterday23 = 0;
		$yesterday24 = 0;
		$yesterday25 = 0;
		$yesterday26 = 0;
		$yesterday27 = 0;
		$yesterday28 = 0;
		$yesterday29 = 0;
		$yesterday30 = 0;

		foreach ($results as $tmp) {
			if ($tmp->dtstat == date("Y-m-d"))
				$today += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 24)))
				$yesterday1 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 48)))
				$yesterday2 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 72)))
				$yesterday3 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 96)))
				$yesterday4 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 120)))
				$yesterday5 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 144)))
				$yesterday6 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 168)))
				$yesterday7 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 192)))
				$yesterday8 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 216)))
				$yesterday9 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 240)))
				$yesterday10 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 264)))
				$yesterday11 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 288)))
				$yesterday12 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 312)))
				$yesterday13 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 336)))
				$yesterday14 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 360)))
				$yesterday15 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 384)))
				$yesterday16 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 408)))
				$yesterday17 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 432)))
				$yesterday18 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 456)))
				$yesterday19 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 480)))
				$yesterday20 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 504)))
				$yesterday21 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 528)))
				$yesterday22 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 552)))
				$yesterday23 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 576)))
				$yesterday24 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 600)))
				$yesterday25 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 624)))
				$yesterday26 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 648)))
				$yesterday27 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 672)))
				$yesterday28 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 696)))
				$yesterday29 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 720)))
				$yesterday30 += $tmp->total_gross;
			
		}

		$day[0] = $yesterday30;
		$day[1] = $yesterday29;
		$day[2] = $yesterday28;
		$day[3] = $yesterday27;
		$day[4] = $yesterday26;
		$day[5] = $yesterday25;
		$day[6] = $yesterday24;
		$day[7] = $yesterday23;
		$day[8] = $yesterday22;
		$day[9] = $yesterday21;
		$day[10] = $yesterday20;
		$day[11] = $yesterday19;
		$day[12] = $yesterday18;
		$day[13] = $yesterday17;
		$day[14] = $yesterday16;
		$day[15] = $yesterday15;
		$day[16] = $yesterday14;
		$day[17] = $yesterday13;
		$day[18] = $yesterday12;
		$day[19] = $yesterday11;
		$day[20] = $yesterday10;
		$day[21] = $yesterday9;
		$day[22] = $yesterday8;
		$day[23] = $yesterday7;
		$day[24] = $yesterday6;
		$day[25] = $yesterday5;
		$day[26] = $yesterday4;
		$day[27] = $yesterday3;
		$day[28] = $yesterday2;
		$day[29] = $yesterday1;
		$day[30] = $today;

        return $day;
    }

    public static function totalAllRevenueThisWeekTSEL($sdc = '',$serviceId = 0){
        $cdb = Config::get('reporting.database_config');
        $rptdb = Config::get('reporting.database_reporting');
        if ($sdc == '') {
            $results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
									IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
								FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
								WHERE
									/* t.service_id = {$serviceId} AND t.sdc = {$sdc} AND */
									dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
									AND t.telco_id = 1
								GROUP BY dtstat ORDER BY dtstat");
        } else {
            $results = DB::select(
                "SELECT dtstat, IFNULL(SUM(total_success * c.cp_price),0) as total_gross,
												IFNULL(SUM(total_success * (c.cp_price-c.cost_bearer)),0) as total_net
											FROM {$rptdb}.traffic_mt_summary t INNER JOIN {$cdb}.charging c ON c.id = t.charging_id
											WHERE
												t.sdc = {$sdc} AND
												dtstat BETWEEN DATE(NOW()) - INTERVAL 30 DAY AND NOW()
												AND t.telco_id = 1
											GROUP BY dtstat ORDER BY dtstat");
        }

        $today = 0;
		$yesterday1 = 0;
		$yesterday2 = 0;
		$yesterday3 = 0;
		$yesterday4 = 0;
		$yesterday5 = 0;
		$yesterday6 = 0;
		$yesterday7 = 0;
		$yesterday8 = 0;
		$yesterday9 = 0;
		$yesterday10 = 0;
		$yesterday11 = 0;
		$yesterday12 = 0;
		$yesterday13 = 0;
		$yesterday14 = 0;
		$yesterday15 = 0;
		$yesterday16 = 0;
		$yesterday17 = 0;
		$yesterday18 = 0;
		$yesterday19 = 0;
		$yesterday20 = 0;
		$yesterday21 = 0;
		$yesterday22 = 0;
		$yesterday23 = 0;
		$yesterday24 = 0;
		$yesterday25 = 0;
		$yesterday26 = 0;
		$yesterday27 = 0;
		$yesterday28 = 0;
		$yesterday29 = 0;
		$yesterday30 = 0;

		foreach ($results as $tmp) {
			if ($tmp->dtstat == date("Y-m-d"))
				$today += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 24)))
				$yesterday1 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 48)))
				$yesterday2 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 72)))
				$yesterday3 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 96)))
				$yesterday4 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 120)))
				$yesterday5 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 144)))
				$yesterday6 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 168)))
				$yesterday7 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 192)))
				$yesterday8 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 216)))
				$yesterday9 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 240)))
				$yesterday10 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 264)))
				$yesterday11 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 288)))
				$yesterday12 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 312)))
				$yesterday13 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 336)))
				$yesterday14 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 360)))
				$yesterday15 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 384)))
				$yesterday16 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 408)))
				$yesterday17 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 432)))
				$yesterday18 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 456)))
				$yesterday19 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 480)))
				$yesterday20 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 504)))
				$yesterday21 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 528)))
				$yesterday22 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 552)))
				$yesterday23 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 576)))
				$yesterday24 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 600)))
				$yesterday25 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 624)))
				$yesterday26 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 648)))
				$yesterday27 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 672)))
				$yesterday28 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 696)))
				$yesterday29 += $tmp->total_gross;
			else if ($tmp->dtstat == date("Y-m-d", time() - (60 * 60 * 720)))
				$yesterday30 += $tmp->total_gross;
			
		}

		$day[0] = $yesterday30;
		$day[1] = $yesterday29;
		$day[2] = $yesterday28;
		$day[3] = $yesterday27;
		$day[4] = $yesterday26;
		$day[5] = $yesterday25;
		$day[6] = $yesterday24;
		$day[7] = $yesterday23;
		$day[8] = $yesterday22;
		$day[9] = $yesterday21;
		$day[10] = $yesterday20;
		$day[11] = $yesterday19;
		$day[12] = $yesterday18;
		$day[13] = $yesterday17;
		$day[14] = $yesterday16;
		$day[15] = $yesterday15;
		$day[16] = $yesterday14;
		$day[17] = $yesterday13;
		$day[18] = $yesterday12;
		$day[19] = $yesterday11;
		$day[20] = $yesterday10;
		$day[21] = $yesterday9;
		$day[22] = $yesterday8;
		$day[23] = $yesterday7;
		$day[24] = $yesterday6;
		$day[25] = $yesterday5;
		$day[26] = $yesterday4;
		$day[27] = $yesterday3;
		$day[28] = $yesterday2;
		$day[29] = $yesterday1;
		$day[30] = $today;

        return $day;
    }

}
