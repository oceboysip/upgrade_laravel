<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Database Configuration 
	|--------------------------------------------------------------------------
	|
	| Set your database that holds general configuration here
	|
	*/
	'database_config' => 'cdb',

    /*
    |--------------------------------------------------------------------------
    | Business Application
    |--------------------------------------------------------------------------
    |
    | Set your database that holds the business application
    |
    */
    //'database_application' => 'montessori',
    'database_application' => 'batam'

);
