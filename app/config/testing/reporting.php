<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Database Configuration 
	|--------------------------------------------------------------------------
	|
	| Set your database that holds general configuration here
	|
	*/
	'database_config' => 'cdb',

    /*
    |--------------------------------------------------------------------------
    | Application Database
    |--------------------------------------------------------------------------
    |
    | Set your database that holds 'main' application database here
    | In production change this to 'montessori'
    */
    'database_application' => 'montessori',

);
