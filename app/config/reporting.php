<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Database Configuration 
	|--------------------------------------------------------------------------
	|
	| Set your database that holds general configuration here
	|
	*/
	'database_config' => 'cdb',

    /*
    |--------------------------------------------------------------------------
    | Business Application
    |--------------------------------------------------------------------------
    |
    | Set your database that holds the business application
    |
    */
    'database_application' => 'montessori',

	/*
	|--------------------------------------------------------------------------
	| Reporting Database
	|--------------------------------------------------------------------------
	|
	| Set your database that holds the business application
	|
	*/
	//'database_application' => 'montessori',
	'database_reporting' => 'traffic_rpt',

    'database_umb' => 'umb-app',
    'database_umb_microsite' => 'umb-microsite',

    'database_monaco' => 'monaco'

);
