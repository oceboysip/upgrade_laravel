<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/users', function() {
    return 'Users!';
});


/**
 * ------------------
 *
 *   Trias Specific
 *
 * ------------------
 *
 */

/* --- Logins --- */
Route::get('/login', array('as' => 'login', 'uses'=>'AuthController@showLogin'));
Route::post('/login', 'AuthController@doLogin');
Route::get('/isLogin', array('uses'=>'AuthController@checkLogin'));
Route::get('/logout', array('as' => 'logout', 'uses'=> 'AuthController@doLogout'));
Route::get('/change_password', array('as' => 'change_password', 'uses'=> 'AuthController@viewChangePassword'));
Route::post('/api/change_password', array('before' => array('auth'), 'uses' => 'AuthController@apiSaveChangePassword'));


/* --- Admin ---- */
/*
Route::get('/', array('before' => array('auth')), function() {
    return View::make('hello');
});
*/
Route::get('/', array('before' => array('auth'),'uses' => 'HomeController@mainView'));
Route::get('/admin', array('before' => array('auth'),'uses' => 'HomeController@mainView'));
Route::get('/main', array('as' => 'main', 'before' => array('auth'),'uses' => 'HomeController@mainView'));

/* --- Reports --- */
Route::get('/traffic/traffic', array('as' => 'traffic/traffic',
                                            'before' => array('auth'),'uses' => 'HomeController@mainView'));

Route::get('/traffic/revenue_report', array('as' => 'traffic/revenue_report',
                                            'before' => array('auth'),'uses' => 'TrafficController@showRevenueReport'));

Route::get('/traffic/traffic_report', array('as' => 'traffic/traffic_report',
    'before' => array('auth'),'uses' => 'TrafficController@traffic_report'));

Route::post('/traffic/traffic_report/{telcoId}/{sdc}/{serviceId}',
    array('before' => array('auth'),'uses' => 'TrafficController@apiGetTrafficReport'));

Route::post('/traffic/revenue_report/{telcoId}/{sdc}/{serviceId}',
						array('before' => array('auth'),'uses' => 'TrafficController@apiGetRevenueReport'));

Route::get('/traffic/delivery_report', array('as' => 'traffic/delivery_report',
    'before' => array('auth'),'uses' => 'TrafficController@delivery_report'));

Route::post('/traffic/delivery_report', array('as' => 'traffic/delivery_report_act',
    'before' => array('auth'),'uses' => 'TrafficController@delivery_report_action'));

Route::get('/traffic/xl_delivery_report', array('as' => 'traffic/xl_delivery_report',
    'before' => array('auth'),'uses' => 'TrafficController@xl_delivery_report'));

Route::post('/traffic/xl_delivery_report', array('as' => 'traffic/xl_delivery_report_act',
    'before' => array('auth'),'uses' => 'TrafficController@xl_delivery_report_action'));

Route::get('/traffic/tsel_delivery_report', array('as' => 'traffic/tsel_delivery_report',
    'before' => array('auth'),'uses' => 'TrafficController@tsel_delivery_report'));

Route::post('/traffic/tsel_delivery_report', array('as' => 'traffic/tsel_delivery_report_act',
    'before' => array('auth'),'uses' => 'TrafficController@tsel_delivery_report_action'));

Route::get('/traffic/coa_report', array('as' => 'traffic/coa_report',
    'before' => array('auth'),'uses' => 'TrafficController@coa_report'));
Route::post('/traffic/coa_report_act/{telcoId}/{sdc}/{serviceId}', array('as' => 'traffic/coa_report_act',
    'before' => array('auth'),'uses' => 'TrafficController@coa_report_act'));

Route::get('/mo_stat/mo_stat', array('as' => 'mo_stat/mo_stat',
									'before' => array('auth'),'uses' => 'TrafficController@showMoStat'));
Route::post('/mo_stat/mo_stat/{telcoId}/{sdc}/{serviceId}',
								array('before' => array('auth'),'uses' => 'TrafficController@apiGetMoStat'));


/* --- Content --- */
Route::get('/content/pull_content', array('as' => 'content/pull_content',
                                            'before' => array('auth'),'uses' => 'ContentController@pullContentMenu'));

Route::get('/content/loker', array('as' => 'content/loker',
    'before' => array('auth'),'uses' => 'ContentController@loker'));

Route::get('/content/chatbola', array('as' => 'content/chatbola',
    'before' => array('auth'),'uses' => 'ContentController@chatbola'));

Route::get('/content/chat', array('as' => 'content/chat',
    'before' => array('auth'),'uses' => 'ContentController@chat'));

Route::get('/content/model', array('as' => 'content/model',
    'before' => array('auth'),'uses' => 'ContentController@model'));

Route::get('/content/loker_umb', array('as' => 'content/loker_umb',
    'before' => array('auth'),'uses' => 'ContentController@loker_umb'));


Route::post('/content/loker_umb_edit/{id}', array('as' => 'content/loker_umb_edit',
    'before' => array('auth'),'uses' => 'ContentController@loker_umb_edit_act'));

Route::get('/content/loker/add', array('as' => '/content/loker/add',
    'before' => array('auth'),'uses' => 'ContentController@loker_add'));
Route::post('/content/loker/add', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_add_action'));

Route::get('/content/chatbola/add', array('as' => '/content/chatbola/add',
    'before' => array('auth'),'uses' => 'ContentController@chatbola_add'));

Route::post('/content/chatbola/add', array('as' => '/content/chatbola/add',
    'before' => array('auth'),'uses' => 'ContentController@chatbola_add_action'));

Route::get('/content/chat/add', array('as' => '/content/chat/add',
    'before' => array('auth'),'uses' => 'ContentController@chat_add'));

Route::post('/content/chat/add', array('as' => '/content/chat/add',
    'before' => array('auth'),'uses' => 'ContentController@chat_add_action'));

Route::get('/content/model/add', array('as' => '/content/model/add',
    'before' => array('auth'),'uses' => 'ContentController@model_add'));

Route::post('/content/model/add', array('as' => '/content/model/add',
    'before' => array('auth'),'uses' => 'ContentController@model_add_action'));

Route::get('/content/loker/del/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_del'));
Route::get('/content/loker/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_enable'));

Route::get('/content/chatbola/del/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chatbola_del'));
Route::get('/content/chatbola/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chatbola_enable'));

Route::get('/content/chat/del/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chat_del'));
Route::get('/content/chat/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chat_enable'));

Route::get('/content/model/del/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@model_del'));
Route::get('/content/model/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@model_enable'));

Route::get('/content/loker_umb/del/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_umb_del'));
Route::get('/content/loker_umb/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_umb_enable'));

Route::get('/content/chatbola/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chatbola_edit'));

Route::get('/content/chat/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chat_edit'));

Route::get('/content/model/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@model_edit'));

Route::get('/content/loker/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_edit'));

Route::post('/content/loker_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_edit_act'));

Route::post('/content/chatbola_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chatbola_edit_act'));

Route::post('/content/chat_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@chat_edit_act'));

Route::post('/content/model_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@model_edit_act'));

Route::post('/content/loker_sms/add', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_sms_add_action'));

Route::post('/content/chatbola_sms/add', array(
    'before' => array('auth'),'uses' => 'ContentController@chatbola_sms_add_action'));

Route::post('/content/chat_sms/add', array(
    'before' => array('auth'),'uses' => 'ContentController@chat_sms_add_action'));

Route::post('/content/model_sms/add', array(
    'before' => array('auth'),'uses' => 'ContentController@model_sms_add_action'));

Route::post('/content/loker_umb/add', array(
    'before' => array('auth'),'uses' => 'ContentController@loker_umb_add_action'));
Route::get('/content/loker_umb_edit/{id}', array('as' => 'content/loker_umb_edit',
    'before' => array('auth'),'uses' => 'ContentController@loker_umb_edit'));

Route::get('/content/discount_content', array('as' => 'content/discount_content',
    'before' => array('auth'),'uses' => 'ContentController@discount_content'));

Route::get('/content/discount_content/add', array('as' => 'content/discount_content/add',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_add'));

Route::get('/content/discount_content/del/{id}', array('as' => 'content/discount_content/del',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_dell'));
Route::get('/content/discount_content/enable/{id}', array('as' => 'content/discount_content/enable',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_enable'));

Route::post('/content/discount_content/add', array(
    'before' => array('auth'),'uses' => 'ContentController@discount_content_add_action'));

Route::get('/content/discount_content/{id}', array('as' => 'content/discount_content/edit',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_edit'));

Route::post('/content/discount_content/{id}', array('as' => 'content/discount_content/edit',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_edit_act'));

Route::get('/content/umb', array('as' => 'content/umb',
    'before' => array('auth'),'uses' => 'ContentController@content_umb'));

Route::get('/content/discount_content_umb', array('as' => 'content/discount_content_umb',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_umb'));
Route::get('/content/discount_umb', array('as' => 'content/discount_umb',
    'before' => array('auth'),'uses' => 'ContentController@discount_umb'));
Route::get('/content/discount_content_umb/del/{id}', array('as' => 'content/discount_content_umb_del',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_umb_del'));

Route::get('/content/discount_content_umb/add', array('as' => 'content/discount_content_umb_add',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_umb_add'));

Route::get('/content/loker_umb/add', array('as' => 'content/loker_umb_add',
    'before' => array('auth'),'uses' => 'ContentController@loker_umb_add'));


Route::post('/content/discount_content_umb/add', array('as' => 'content/discount_content_umb_add_action',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_umb_add_action'));

Route::get('/content/discount_content_umb/{id}', array('as' => 'content/discount_content_umb/edit',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_umb_edit'));

Route::post('/content/discount_content_umb/{id}', array('as' => 'content/discount_content_umb/edit',
    'before' => array('auth'),'uses' => 'ContentController@discount_content_umb_edit_act'));

Route::post('/content/upload', array('as' => 'content/upload',
    'before' => array('auth'),'uses' => 'ContentController@upload'));

Route::post('/content/upload_image_tinymce', array('as' => 'content/upload_image_tinymce',
    'before' => array('auth'),'uses' => 'ContentController@upload_image_tinymce'));

Route::get('/content/push_content', array('as' => 'content/push_content',
        'before' => array('auth'),'uses' => 'ContentController@pushContentMenu'));

Route::get('/content/push_one/{id}', array('as' => 'content/push_one',
    'before' => array('auth'),'uses' => 'ContentController@push_one'));
Route::post('/content/push_one/{id}', array('as' => 'content/push_one',
    'before' => array('auth'),'uses' => 'ContentController@push_one_action'));

Route::get('/content/universal_push_one/{id}', array('as' => 'content/universal_push_one',
    'before' => array('auth'),'uses' => 'ContentController@universal_push_one'));
Route::post('/content/universal_push_one/{id}', array('as' => 'content/universal_push_one',
    'before' => array('auth'),'uses' => 'ContentController@universal_push_one_action'));

Route::get('/content/umb_promo/edit/{id}', array('as' => 'content/umb_promo_edit',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_edit'));

Route::get('/content/umb_promo/stop/{id}', array('as' => 'content/umb_promo_stop',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_stop'));

Route::get('/content/umb_promo/start/{id}', array('as' => 'content/umb_promo_start',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_start'));

Route::post('/content/umb_promo/edit', array('as' => 'content/umb_promo_edit_save',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_edit_save'));

Route::get('/content/umb_promo', array('as' => 'content/umb_promo',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo'));

Route::get('/content/umb_promo/add', array('as' => 'content/umb_promo/add',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_add'));

Route::post('/content/umb_promo/add', array('as' => 'content/umb_promo/add',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_save'));

//umb promo indosat
Route::get('/content/umb_promo_indosat/edit/{id}', array('as' => 'content/umb_promo_edit_indosat',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_edit_indosat'));

Route::get('/content/umb_promo_indosat/stop/{id}', array('as' => 'content/umb_promo_stop_indosat',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_stop_indosat'));

Route::get('/content/umb_promo_indosat/start/{id}', array('as' => 'content/umb_promo_start_indosat',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_start_indosat'));

Route::post('/content/umb_promo_indosat/edit', array('as' => 'content/umb_promo_edit_save_indosat',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_edit_save_indosat'));

Route::get('/content/umb_promo_indosat', array('as' => 'content/umb_promo_indosat',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_indosat'));

Route::get('/content/umb_promo_indosat/add', array('as' => 'content/umb_promo_indosat/add',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_add_indosat'));

Route::post('/content/umb_promo_indosat/add', array('as' => 'content/umb_promo_indosat/add',
    'before' => array('auth'),'uses' => 'ContentController@umb_promo_save_indosat'));
//eof umb promo indosat

Route::get('/content/quest', array('as' => 'content/questionnaire', 'before' => array('auth'), 
    'uses' => 'ContentController@questionnaire' ));

//Content, webtool tebakskor
Route::get('/content/tebakskor', array('as' => 'content/tebakskor',
    'before' => array('auth'),'uses' => 'ContentController@tebakskor'));
Route::get('/content/tebakskor/add', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_add'));
Route::post('/content/tebakskor/add', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_add_save'));
Route::get('/content/tebakskor_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_edit'));
Route::post('/content/tebakskor_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_edit_save'));
Route::get('/content/tebakskor_process/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_prepare'));
Route::post('/content/tebakskor_process/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_process'));
Route::get('/content/tebakskor_coin/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_coin'));
Route::post('/content/tebakskor_coin_process/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_coin_process'));
Route::get('/content/tebakskor_result/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_result'));
Route::get('/content/tebakskor_displayed/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@tebakskor_displayed'));

//Content Badword
Route::get('/content/badword', array('as' => 'content/badword',
    'before' => array('auth'),'uses' => 'ContentController@badword'));
Route::post('/content/badword', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_search'));
Route::get('/content/badword/add', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_add'));
Route::post('/content/badword/add', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_add_save'));
Route::get('/content/badword_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_edit'));
Route::post('/content/badword_edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_edit_save'));
Route::get('/content/badword_generate/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_generate'));
Route::get('/content/badword_delete/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@badword_delete'));
Route::get('/content/badword_to_file', array(
    'before' => array('auth'),'uses' => 'ContentController@regex_to_file_btn'));
Route::get('/content/regex_db', array(
    'before' => array('auth'),'uses' => 'ContentController@generate_regex_db'));

//Content, Jodoh
Route::get('/content/jodoh', array('as' => 'content/jodoh',
    'before' => array('auth'),'uses' => 'ContentController@jodoh'));
Route::post('/content/jodoh', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_search'));
Route::get('/content/jodoh/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_enable'));
Route::get('/content/jodoh/delete/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_delete'));
Route::get('/content/jodoh/add', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_add'));
Route::post('/content/jodoh/add', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_add_save'));
Route::get('/content/jodoh/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_edit'));
Route::post('/content/jodoh/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodoh_edit_save'));

Route::get('/content/jodohprediksi', array('as' => 'content/jodohprediksi',
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi'));
Route::post('/content/jodohprediksi', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_search'));
Route::get('/content/jodohprediksi_enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_enable'));
Route::get('/content/jodohprediksi_delete/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_delete'));
Route::get('/content/jodohprediksi/add', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_add'));
Route::post('/content/jodohprediksi/add', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_add_save'));
Route::get('/content/jodohprediksi/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_edit'));
Route::post('/content/jodohprediksi/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_edit_save'));
Route::get('/content/jodohprediksi/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_enable'));
Route::get('/content/jodohprediksi/delete/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@jodohprediksi_delete'));


Route::get('/api/get_subcategory_loker/{category_id}', array('before' => array('auth'),
    'uses' => 'ContentController@apiGetSubcategoryLoker'));

Route::get('/api/get_list_pull_content/{applicationId}', array('before' => array('auth'),
                                                            'uses' => 'ContentController@apiGetListPullContent'));
Route::get('/api/get_list_push_content/{applicationId}', array('before' => array('auth'),
                                                             'uses' => 'ContentController@apiGetListPushContent'));
Route::get('/api/add_new_push_content/{applicationId}', array('before' => array('auth'),
                                                        'uses' => 'ContentController@apiAddPushContent'));
Route::get('/api/add_new_pull_content/{applicationId}', array('before' => array('auth'),
                                                         'uses' => 'ContentController@apiAddPullContent'));
Route::post('/api/create_new_push_content/{applicationId}', array('before' => array('auth'),
                                                                'uses' => 'ContentController@apiSaveNewPushContent'));
Route::post('/api/create_new_pull_content/{applicationId}', array('before' => array('auth'),
                                                                 'uses' => 'ContentController@apiSaveNewPullContent'));

Route::get('/api/shortenurl', array('before' => array('auth'), 'uses' => 'ContentController@apiShortenUrl'));

Route::get('/api/edit_content/{contentId}', array('before' => array('auth'),
                                       'uses' => 'ContentController@apiViewEditContent'));
Route::post('/api/edit_content/{contentId}', array('before' => array('auth'),
                                        'uses' => 'ContentController@apiSaveEditContent'));

Route::post('/api/get_traffic_for_subscriber', array('before' => array('auth'),
    'uses' => 'ContentController@get_traffic_for_subscriber'));



/* --- Admin --- */
Route::get('/admin/manage_application', array('as' => 'admin/manage_application',
                                            'before' => array('auth'),'uses' => 'BusinessApplicationController@home'));
Route::get('/admin/manage_service', array('as' => 'admin/manage_service',
                                          'before' => array('auth'),'uses' => 'ServiceController@home'));
Route::get('/admin/manage_user', array('as' => 'admin/manage_user',
                                       'before' => array('auth'),'uses' => 'UserController@home'));
Route::get('/admin/manage_charging', array('as' => 'admin/manage_charging',
                                           'before' => array('auth'),'uses' => 'ChargingController@home'));
Route::get('/admin/isat_sdm', array('as' => 'admin/isat_sdm',
                                            'before' => array('auth'),'uses' => 'IndosatController@sdmHome'));



/* --- API --- */
//Main
Route::get('/api/get_list_active_service', array('as' => 'api/get_list_active_service',
                                          'before' => array('auth'),
                                           'uses' => 'ApiController@getListOfActiveService'));
Route::get('/api/get_list_active_service/{telcoId}/{sdc}', array('before' => array('auth'),
                                            'uses' => 'ApiController@getListOfActiveService'));
Route::get('/api/get_list_active_service/{telcoId}', array('before' => array('auth'),
                                            'uses' => 'ApiController@getListOfActiveService'));
Route::get('/api/get_list_active_keyword/{telcoId}/{sdc}', array('before' => array('auth'),
											'uses' => 'ApiController@getListOfKeyword'));
Route::get('/api/get_list_active_keyword/{telcoId}/{sdc}/{serviceId}', array('before' => array('auth'),
											'uses' => 'ApiController@getListOfKeyword'));




//Charging
Route::get('/api/get_list_charging', array('before' => array('auth'),
                                            'uses' => 'ChargingController@apiGetListCharging'));
Route::get('/api/create_new_charging', array('before' => array('auth'),
                                            'uses' => 'ChargingController@apiViewCreateNewCharging'));
Route::post('/api/create_new_charging', array('before' => array('auth'),
                                                'uses' => 'ChargingController@apiSaveNewCharging'));
Route::get('/api/edit_charging', array('before' => array('auth'),
                                       'uses' => 'ChargingController@apiViewEditCharging'));
Route::post('/api/edit_charging', array('before' => array('auth'),
                                                'uses' => 'ChargingController@apiSaveEditCharging'));

//Service Group
Route::get('/api/get_service_group', array('before' => array('auth'),
                                          'uses' => 'ServiceController@apiGetListServiceGroup'));

//Service
Route::get('/api/get_list_service', array('before' => array('auth'),
                                            'uses' => 'ServiceController@apiGetListService'));
Route::get('/api/create_new_service', array('before' => array('auth'),
                                             'uses' => 'ServiceController@apiViewCreateNewService'));
Route::post('/api/create_new_service', array('before' => array('auth'),
                                               'uses' => 'ServiceController@apiSaveNewService'));
Route::get('/api/edit_service', array('before' => array('auth'),
                                        'uses' => 'ServiceController@apiViewEditService'));
Route::post('/api/edit_service', array('before' => array('auth'),
                                        'uses' => 'ServiceController@apiSaveEditService'));

//Service Keywords
Route::get('/api/get_service_keywords', array('before' => array('auth'),
                                                'uses' => 'ServiceController@apiGetListServiceKeyword'));
Route::get('/api/create_new_service_keyword', array('before' => array('auth'),
                                            'uses' => 'ServiceController@apiViewCreateNewServiceKeyword'));
Route::post('/api/create_new_service_keyword', array('before' => array('auth'),
                                            'uses' => 'ServiceController@apiSaveNewServiceKeyword'));

Route::get('/api/get_list_service_by_keyword', array('before' => array('auth'),
                                                     'uses' => 'ServiceController@apiGetListServiceByKeyword'));

//User Management
Route::get('/api/get_list_user', array('before' => array('auth'), 'uses' => 'UserController@apiGetListUser'));
Route::get('/api/get_list_role', array('before' => array('auth'), 'uses' => 'UserController@apiGetListRole'));
Route::get('/api/get_role_menu', array('before' => array('auth'), 'uses' => 'UserController@apiGetListRoleMenu'));
Route::post('/api/remove_menu_from_role', array('before' => array('auth'), 'uses' => 'UserController@apiRemoveRoleMenu'));
Route::post('/api/save_menu_role', array('before' => array('auth'), 'uses' => 'UserController@apiSaveRoleMenu'));
Route::get('/api/create_new_user', array('before' => array('auth'), 'uses' => 'UserController@apiViewCreateNewUser'));
Route::post('/api/create_new_user', array('before' => array('auth'),'uses' => 'UserController@apiSaveNewUser'));
Route::get('/api/get_user/{userId}', array('before' => array('auth'), 'uses' => 'UserController@apiViewEditUser'));
Route::post('/api/edit_user', array('before' => array('auth'), 'uses' => 'UserController@apiSaveEditUser'));


//Business Application
Route::get('/api/get_list_application', array('before' => array('auth'),
                                            'uses' => 'BusinessApplicationController@apiGetListApplication'));

Route::get('/api/create_new_application', array('before' => array('auth'),
                                            'uses' => 'BusinessApplicationController@apiViewCreateNewApplication'));
Route::post('/api/create_new_application', array('before' => array('auth'),
                                             'uses' => 'BusinessApplicationController@apiSaveNewApplication'));
Route::get('/api/edit_application', array('before' => array('auth'),
                                            'uses' => 'BusinessApplicationController@apiViewEditApplication'));
Route::post('/api/edit_application', array('before' => array('auth'),
                                            'uses' => 'BusinessApplicationController@apiSaveEditApplication'));
Route::get('/api/get_application_charging_param', array('before' => array('auth'),
                                                        'uses' => 'BusinessApplicationController@apiGetListApplicationCharging'));
Route::get('/api/get_charging_param_for_application', array('before' => array('auth'),
                                                            'uses' => 'BusinessApplicationController@apiGetApplicationCharging'));
Route::post('/api/save_charging_param', array('before' => array('auth'),
                                                'uses' => 'BusinessApplicationController@apiSaveEditApplicationCharging'));

Route::get('/api/get_list_pull_applcation/{telcoId}/{sdc}', array('before' => array('auth'),
                                                     'uses' => 'ContentController@apiGetListPullApplication'));
Route::get('/api/get_list_push_applcation/{telcoId}/{sdc}', array('before' => array('auth'),
                                                     'uses' => 'ContentController@apiGetListPushApplication'));

Route::get('/api/get_list_umb_content/{appname}', array('before' => array('auth'),
    'uses' => 'ContentController@apiGetListUmbContent'));

Route::get('/api/edit_application_delegate', array('before' => array('auth'),
                                            'uses' => 'BusinessApplicationController@apiViewEditApplicationDelegate'));

Route::post('/api/edit_application_delegate', array('before' => array('auth'),
                                            'uses' => 'BusinessApplicationController@apiSaveEditApplicationDelegate'));


//Subscriber
Route::get('/subscriber/manage_subscriber', array('as' => 'subscriber/manage_subscriber',
			'before' => array('auth'),'uses' => 'SubscriberController@home'));
Route::get('/subscriber/reg_report', array('as' => 'subscriber/reg_report',
			'before' => array('auth'),'uses' => 'SubscriberController@showRegReport'));
Route::get('/subscriber/promo_channel', array('as' => 'subscriber/promo_channel',
    'before' => array('auth'),'uses' => 'SubscriberController@promo_channel'));
Route::get('/subscriber/upload_msisdn', array('as' => 'subscriber/upload_msisdn',
    'before' => array('auth'),'uses' => 'SubscriberController@upload_msisdn'));
Route::post('/subscriber/upload_msisdn', array('as' => 'subscriber/upload_msisdn',
    'before' => array('auth'),'uses' => 'SubscriberController@upload_msisdn_act'));

Route::post('/api/get_list_subscriber', array('before' => array('auth'), 'uses' => 'SubscriberController@apiFind'));
Route::post('/api/do_unregister_subscriber', array('before' => array('auth'),'uses' => 'SubscriberController@apiUnregisterSubscriber'));
Route::get('/api/get_reg_report/{applicationId}', array('before' => array('auth'), 'uses' => 'SubscriberController@apiGetRegReport'));
Route::get('/api/get_reg_promo_channel_report/{applicationId}', array('before' => array('auth'), 'uses' => 'SubscriberController@apiGetRegPromo'));
Route::get('/api/get_keyword_promo/{applicationId}', array('before' => array('auth'), 'uses' => 'SubscriberController@apiGetKeywordPromo'));


//Indosat Related Stuff
Route::get('/api/isat/get_sdm_code_list/{sdc}/{serviceId}', array('before' => array('auth'), 'uses' => 'IndosatController@apiGetSdmCodeList'));

Route::post('/api/isat/create_new_sdm_code', array('before' => array('auth'), 'uses' => 'IndosatController@apiCreateSdmCode'));

Route::post('/api/isat/remove_sdm_code', array('before' => array('auth'), 'uses' => 'IndosatController@apiRemoveSdmCode'));


//JODOH - iLive - ilive_isat
Route::get('/content/ilive_isat', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat'));
Route::get('/content/ilive_isat/add', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat_add'));
Route::post('/content/ilive_isat/add', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat_add_action'));
Route::get('/content/ilive_isat/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat_edit'));
Route::post('/content/ilive_isat/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat_edit_action'));
Route::get('/content/ilive_isat/disable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat_disable'));
Route::get('/content/ilive_isat/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_isat_enable'));

//LIVE - XL
Route::get('/content/ilive_xl', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl'));
Route::get('/content/ilive_xl/add', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl_add'));
Route::post('/content/ilive_xl/add', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl_add_action'));
Route::get('/content/ilive_xl/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl_edit'));
Route::post('/content/ilive_xl/edit/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl_edit_action'));
Route::get('/content/ilive_xl/disable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl_disable'));
Route::get('/content/ilive_xl/enable/{id}', array(
    'before' => array('auth'),'uses' => 'ContentController@ilive_xl_enable'));


Route::get('/content/coin_redeem_telkomsel', array('as' => 'content/coin_redeem_telkomsel',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem_telkomsel'));

Route::get('/content/coin_redeem_telkomsel/{id}', array('as' => 'content/coin_redeem_telkomsel/{id}',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem_telkomsel_edit'));

Route::post('/content/coin_redeem_telkomsel_edit', array('as' => 'content/coin_redeem_telkomsel_update',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem_telkomsel_update'));

Route::post('/content/coin_redeem_telkomsel_date', array('as' => 'content/coin_redeem_telkomsel_date',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem_telkomsel_date'));



Route::get('/content/coin_redeem', array('as' => 'content/coin_redeem',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem'));

Route::get('/content/coin_redeem/{id}', array('as' => 'content/coin_redeem/{id}',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem_edit'));

Route::post('/content/coin_redeem_edit', array('as' => 'content/coin_redeem_update',
    'before' => array('auth'),'uses' => 'ContentController@coin_redeem_update'));

Route::post('/content/coin_redem_date', array('as' => 'content/coin_redem_date',
    'before' => array('auth'),'uses' => 'ContentController@coin_redem_date'));

Route::get('/content/coin_telkomsel', array('as' => 'content/coin_telkomsel',
    'before' => array('auth'),'uses' => 'ContentController@coin_telkomsel'));


Route::get('/content/cnd', array('as' => 'content/cnd',
    'before' => array('auth'),'uses' => 'ContentController@cnd'));

Route::post('/content/cnd', array('as' => 'content/cnd',
    'before' => array('auth'),'uses' => 'ContentController@cnd_save'));

Route::get('/content/cnd/{id}', array('as' => 'content/cnd/{id}',
    'before' => array('auth'),'uses' => 'ContentController@cnd_edit'));
Route::post('/content/cnd/{id}', array('as' => 'content/cnd/{id}',
    'before' => array('auth'),'uses' => 'ContentController@cnd_update'));

Route::get('/content/cnd/del/{id}', array('as' => 'content/cnd/del/{id}',
    'before' => array('auth'),'uses' => 'ContentController@cnd_del'));

Route::get('/content/cnd/enable/{id}', array('as' => 'content/cnd/enable/{id}',
    'before' => array('auth'),'uses' => 'ContentController@cnd_enable'));


Route::get('/content/fb', array('as' => 'content/fb',
    'before' => array('auth'),'uses' => 'ContentController@fb'));

Route::post('/content/fb', array('as' => 'content/fb',
    'before' => array('auth'),'uses' => 'ContentController@fb_save'));

Route::get('/content/fb/{id}', array('as' => 'content/fb/{id}',
    'before' => array('auth'),'uses' => 'ContentController@fb_edit'));
Route::post('/content/fb/{id}', array('as' => 'content/fb/{id}',
    'before' => array('auth'),'uses' => 'ContentController@fb_update'));

Route::get('/content/fb/del/{id}', array('as' => 'content/fb/del/{id}',
    'before' => array('auth'),'uses' => 'ContentController@fb_del'));

Route::get('/content/fb/enable/{id}', array('as' => 'content/fb/enable/{id}',
    'before' => array('auth'),'uses' => 'ContentController@fb_enable'));

Route::get('/subscriber/faq', array('as' => 'subscriber/faq',
    'before' => array('auth'),'uses' => 'ContentController@faq'));

Route::get('/subscriber/faq/reply/{id}', array('as' => 'subscriber/faq/reply',
    'before' => array('auth'),'uses' => 'ContentController@faq_reply'));

Route::post('/subscriber/faq/reply/{id}', array('as' => 'subscriber/faq/reply',
    'before' => array('auth'),'uses' => 'ContentController@faq_reply_act'));

Route::get('/subscriber/faq/del/{id}', array('as' => 'subscriber/faq/del',
    'before' => array('auth'),'uses' => 'ContentController@remove_faq'));

//nodesms
Route::get('/nodesms/get_area', array('as' => 'nodesms/get_area',
    'before' => array('auth'),'uses' => 'NodesmsController@get_area'));

Route::get('/nodesms/get_pcname/{area}', array('as' => 'nodesms/get_pcname/{area}',
    'before' => array('auth'),'uses' => 'NodesmsController@get_pcname'));


Route::get('/nodesms/server', array('as' => 'nodesms/server',
    'before' => array('auth'),'uses' => 'NodesmsController@server'));

Route::get('/nodesms/get_device/{area}/{pc_name}', array('as' => 'nodesms/get_device/{area}/{pc_name}',
    'before' => array('auth'),'uses' => 'NodesmsController@get_device'));

Route::get('/nodesms/start', array('as' => 'nodesms/start/{server}/{id}',
    'before' => array('auth'),'uses' => 'NodesmsController@start'));

Route::get('/nodesms/start_all', array('as' => 'nodesms/start_all',
    'before' => array('auth'),'uses' => 'NodesmsController@start_all'));

Route::get('/nodesms/stop_all', array('as' => 'nodesms/stop_all',
    'before' => array('auth'),'uses' => 'NodesmsController@stop_all'));

Route::get('/nodesms/stop', array('as' => 'nodesms/stop/{server}/{id}',
    'before' => array('auth'),'uses' => 'NodesmsController@stop'));

//Monitoring Server
Route::get('/monitoring/server', array('as' => 'monitoring/server',
    'before' => array('auth'),'uses' => 'MonitoringController@index'));
Route::get('/monitoring/server/{id}/{tipe}', array('as' => 'monitoring/server/{id}/{tipe}',
    'before' => array('auth'),'uses' => 'MonitoringController@server_chart'));
Route::get('/monitoring/mapping', array('as' => 'monitoring/map',
    'before' => array('auth'),'uses' => 'MonitoringController@server_mapping'));
Route::get('/monitoring/liveroom', array('as' => 'monitoring/liveroom',
    'before' => array('auth'),'uses' => 'StatisticController@index'));
Route::get('/monitoring/liveroom/{gid}', array('as' => 'monitoring/liveroom/{gid}',
    'before' => array('auth'),'uses' => 'StatisticController@chart'));
Route::post('/monitoring/liveroom/{gid}', array('as' => 'monitoring/liveroom/{gid}',
    'before' => array('auth'),'uses' => 'StatisticController@chart_custom'));
Route::get('/monitoring/liveroom/edit/{gid}', array('as' => 'monitoring/liveroom/edit/{gid}',
    'before' => array('auth'),'uses' => 'StatisticController@edit_view'));
Route::post('/monitoring/liveroom/edit/{gid}', array('as' => 'monitoring/liveroom/edit/{gid}',
    'before' => array('auth'),'uses' => 'StatisticController@edit_process'));
Route::get('/monitoring/liveroom/stop/{gid}', array('as' => 'monitoring/liveroom/stop/{gid}',
    'before' => array('auth'),'uses' => 'StatisticController@stop'));
Route::get('/monitoring/liveroom/status/{enable}/{gid}', array('as' => 'monitoring/liveroom/stop/{gid}',
    'before' => array('auth'),'uses' => 'StatisticController@toggle_status'));
Route::get('/monitoring/trending', array('as' => 'monitoring/hashtag_result',
    'before' => array('auth'),'uses' => 'StatisticController@hashtag_result'));
Route::get('/monitoring/trending/{source}', array('as' => 'monitoring/hashtag_result/{source}',
    'before' => array('auth'),'uses' => 'StatisticController@hashtag_result'));
Route::get('/monitoring/challenges', array('as' => 'monitoring/challenges',
    'before' => array('auth'),'uses' => 'StatisticController@challenges'));
Route::post('/monitoring/challenges', array('as' => 'monitoring/challenges',
    'before' => array('auth'),'uses' => 'StatisticController@challenges_result'));

//Laporan penyalahgunaan konten / user
Route::get('/monitoring/abuse', array('as' => 'monitoring/user', 'before' => array('auth'),'uses' => 'AbuseController@redirect'));
Route::get('/monitoring/abuse/user', array('as' => 'monitoring/user', 'before' => array('auth'),'uses' => 'AbuseController@user'));
Route::post('/monitoring/abuse/user', array('as' => 'monitoring/user', 'before' => array('auth'),'uses' => 'AbuseController@user'));
Route::get('/monitoring/abuse/avatar', array('as' => 'monitoring/avatar', 'before' => array('auth'),'uses' => 'AbuseController@avatar'));
Route::get('/monitoring/abuse/reject', array('as' => 'monitoring/reject', 'before' => array('auth'),'uses' => 'AbuseController@rejected_avatar'));
Route::get('/monitoring/abuse/reject/{id}/approve', array('as' => 'monitoring/reject/{id}', 'before' => array('auth'),'uses' => 'AbuseController@rejected_avatar_ok'));
Route::get('/monitoring/abuse/reject/{id}/close', array('as' => 'monitoring/reject/{id}', 'before' => array('auth'),'uses' => 'AbuseController@rejected_avatar_reject'));
Route::get('/monitoring/abuse/avatar/del/{id}', array('as' => 'monitoring/avatar/{id}', 'before' => array('auth'),'uses' => 'AbuseController@avatar_del'));
Route::get('/monitoring/abuse/detail/{id}', array('as' => 'monitoring/detail', 'before' => array('auth'),'uses' => 'AbuseController@detail_user'));
Route::post('/monitoring/abuse/process/{id}', array('as' => 'monitoring/process', 'before' => array('auth'),'uses' => 'AbuseController@process'));


//KEPO Web Chat
Route::get('/webchat', array('as' => 'webchat',
    'before' => array('auth'),'uses' => 'WebchatController@index'));
    

/** Push Notification KEPO **/
Route::get('/content/pushKepo', array('as' => 'content/pushKepo',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo'));

Route::post('/content/pushKepo', array('as' => 'content/pushKepo',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo_save'));

Route::get('/content/pushKepo/{id}', array('as' => 'content/pushKepo/{id}',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo_edit'));

Route::post('/content/pushKepo/{id}', array('as' => 'content/pushKepo/{id}',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo_update'));

Route::get('/content/pushKepo/del/{id}', array('as' => 'content/pushKepo/del/{id}',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo_del'));

Route::get('/content/pushKepo/disable/{id}', array('as' => 'content/pushKepo/disable/{id}',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo_disable'));

Route::get('/content/pushKepo/enable/{id}', array('as' => 'content/pushKepo/enable/{id}',
    'before' => array('auth'),'uses' => 'ContentController@pushKepo_enable'));

