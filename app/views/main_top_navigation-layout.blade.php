<?php
/**
 * Top Navigation Layout
 * User: pandupradhana
 * Date: 4/20/14
 * Time: 12:24 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="header navbar navbar-inverse ">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="navbar-inner">
            <div class="header-seperation">
                <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">
                    <li class="dropdown"><a id="main-menu-toggle" href="#main-menu" class="">
                            <div class="iconset top-menu-toggle-white"></div>
                        </a></li>
                </ul>

                <!-- BEGIN LOGO -->
                @include("logo-layout")

            </div>

            <!-- END RESPONSIVE MENU TOGGLER -->
            <div class="header-quick-nav">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="pull-left">
                    <ul class="nav quick-section">
                        <li class="quicklinks">
                            <a href="#" class="" id="layout-condensed-toggle">
                                <div class="iconset top-menu-toggle-dark"></div>
                            </a>
                        </li>
                        <li class="quicklinks"><span class="h-seperate"></span></li>
                        <li class="quicklinks">
                            <a href="" class="" id="roundToRefreshPage">
                                <div class="iconset top-reload"></div>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav quick-section">
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
                <!-- BEGIN CHAT TOGGLER -->
                <div class="pull-right">
                    <div class="chat-toggler">
                        <!--a href="#" class="dropdown-toggle" id="my-task-list" data-placement="bottom"
                           data-content='' data-toggle="dropdown" data-original-title="Notifications"-->
                            <div class="user-details">
                                <div class="username">
                                    <!-- <span class="badge badge-important">3</span> -->
                                    @yield("greet")
                                    <span class="bold">{{ Auth::user()->fullname }}</span>
                                </div>
                            </div>
                            <!--div class="iconset top-down-arrow"></div-->
                        <!--/a-->
                        <!-- this is the profile picture -->
                        <div class="profile-pic"><img src="{{ asset('assets/img/profiles/avatar2.png') }}"
                                                      alt=""
                                                      data-src="{{ asset('assets/img/profiles/avatar2.png') }}"
                                                      data-src-retina="{{ asset('assets/img/profiles/avatar2.png') }}"
                                                      width="35"
                                                      height="35"/>
                        </div>
                    </div>


                    <ul class="nav quick-section">
                        <li class="quicklinks"><a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#"
                                                  id="user-options">
                                <div class="iconset top-settings-dark "></div>
                            </a>
                            <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                                <!--
                                <li><a href="user-profile.html"> My Account</a></li>
                                <li><a href="calender.html">My Calendar</a></li>
                                <li><a href="email.html"> My Inbox&nbsp;&nbsp;<span
                                            class="badge badge-important animated bounceIn">2</span></a></li>
                                -->
                                <li><a href="{{ route('change_password') }}">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- END CHAT TOGGLER -->
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END TOP NAVIGATION BAR -->
    </div>
