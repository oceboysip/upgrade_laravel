@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">KEPO / iLIVE PUSH NOTIFICATION</span></h3>
                        <br/>
                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control"
                           href="#" data-toggle="modal" data-target="#myModal">
                            Add Content
                        </a>

                        <div class="modal fade popupModal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            <form id="myform" method="POST" action="{{url('/content/pushKepo')}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                            </button>
                                            <br>
                                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                                            <h4 id="myModalLabel" class="semi-bold">Add content for <span id="applicationName">KEPO / iLIVE Push Notification</span></h4>
                                            <!-- <p class="no-margin">Please review the value again</p> -->
                                            <br>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Push Type</label>
                                                    <select style="width:50%" name="push_type" id="push_type">
                                                        <option value="0">
                                                            CHOOSE PUSH TYPE
                                                        </option>
                                                        <option value="0">
                                                            Push Notification
                                                        </option>
                                                        <option value="1">
                                                            Announcement
                                                        </option>
                                                    </select>
                                                </div>
                                                 <div class="col-md-12">
                                                    <label class="form-label">Application Type</label>
                                                    <select style="width:50%" name="app_type" id="app_type">
                                                        <option value="0">
                                                            CHOOSE APPLICATION TYPE
                                                        </option>
                                                        <option value="0">
                                                            KEPO
                                                        </option>
                                                        <option value="1">
                                                            iLIVE
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                <label class="form-label">Content Queue</label>
                                                    <input style="width:30%;" class="form-control" name="content_queue" type="text" value="0">
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Title</label>
                                                    <textarea onkeyup="countChar(this, 25)" name="title" cols="70" rows="1"></textarea>
                                                    <div id="charNum"></div>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Message</label>
                                                    <textarea onkeyup="countChar(this, 255)" name="message" cols="70" rows="5"></textarea>
                                                    <div id="charNum"></div>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Image</label>
                                                    <textarea onkeyup="countChar(this, 255)" name="image" cols="70" rows="1"></textarea>
                                                    <div id="charNum"></div>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">DATE SCHEDULE (<i>yyyy-mm-dd H:i:s</i>)</label>
                                                    <input style="width:30%;" class="form-control theDateTime" name="dtscheduled" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" id="btnCommitChanges" class="btn btn-primary">Save</button>
                                        </div>
                                    <!-- /.modal-content -->
                                </div>
                            </div>
                            </form>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="theContent">
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            @if (isset($results))
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>PUSH TYPE</th>
                                        <th>APP TYPE</th>
                                        <th>TITLE</th>
                                        <th>MESSAGE</th>
                                        <th>IMAGE</th>
                                        <th>DATE SUBMITTED</th>
                                        <th>DATE SHCEDULE</th>
                                        <th>STATUS</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results as $each_data)
                                        <tr class="even gradeA">
                                            <td>{{$each_data->id}}</td>
                                            <td>{{$each_data->push_type}}</td>
                                            <td>{{$each_data->app_type}}</td>
                                            <td>{{$each_data->title}}</td>
                                            <td>{{$each_data->message}}</td>
                                            <td>{{$each_data->image}}</td>
                                            <td>{{$each_data->dtsubmitted}}</td>
                                            <td>{{$each_data->dtscheduled}}</td>
                                            @if($each_data->n_status == 1)
                                                <td><span class="label label-primary">Enable</span></td>
                                            @elseif($each_data->n_status == 0)
                                                <td><span class="label label-danger">Disable</span></td>
                                            @elseif($each_data->n_status == 2)
                                                <td><span class="label label-danger">Deleted</span></td>
                                            @elseif($each_data->n_status == 3)
                                                <td><span class="label label-success">Done</span></td>
                                            @endif
                                            <td>
                                                @if($each_data->n_status == 1)
                                                    <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/pushKepo/'.$each_data->id)}}" >
                                                        Edit
                                                    </a>
                                                    <a href="{{url('/content/pushKepo/disable/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                        Disable
                                                    </a>
                                                @elseif($each_data->n_status == 2)
                                                    
                                                @elseif($each_data->n_status == 3)
                                                    
                                                @else
                                                    <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/pushKepo/'.$each_data->id)}}" >
                                                        Edit
                                                    </a>
                                                    <a href="{{url('content/pushKepo/enable/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                        Enable
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                                No Result
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <ul class="pagination">
                @if($prev_page != '')
                    <li>
                        <a href="{{$prev_page}}">«</a>
                    </li>
                @endif
                @if($next_page != '')
                    <li>
                        <a href="{{$next_page}}">»</a>
                    </li>
                @endif
            </ul>
    </div>
@stop
