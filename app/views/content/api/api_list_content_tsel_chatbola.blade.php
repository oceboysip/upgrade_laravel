@if (isset($the_data))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>List of <span class="semi-bold">{{ $title }} {{ $application->name }}</span></h4>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>
        <div class="grid-body ">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a id="content-diskon" href="javascript:;">DAILY PUSH CONTENT</a></li>
                <li role="presentation"><a id="content-one" href="javascript:;">PUSH #1 CONTENT</a></li>
            </ul>
            <table class="table table-hover table-bordered" id="content-diskon-table">
                <thead>
                <tr>
                    <th>CONTENT ID</th>
                    <th>CONTENT TYPE</th>
                    <th>TEXT</th>
                    <th>SEQUENCE</th>
                    <th>DATE NEXT PUSH</th>
                    <th>DISABLED</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (sizeof($the_data) < 1)
                    <tr class="even gradeA">
                        <td colspan=7> No data </td>
                    </tr>
                @else
                    @foreach ($the_data as $each_data)
                        <tr class="even gradeA">
                            <td>{{$each_data->id}}</td>
                            <td>{{$each_data->content_type}}</td>
                            <td>{{ $each_data->text }}</td>
                            <td>{{ $each_data->sequence }}</td>
                            <td>{{ $each_data->dtnext_push }}</td>
                            @if($each_data->n_status == 1)
                            <td><span class="label label-success">No</span></td>
                            @else
                            <td><span class="label label-important">Yes</span></td>
                            @endif
                            <td class="center">
                                <a href="{{url('content/chatbola/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    Edit
                                </a>

                                @if($each_data->n_status == 1)
                                    <!-- <a href="{{url('content/chatbola/del/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                        Disable
                                    </a> -->
                                @else
                                    <a href="{{url('content/chatbola/enable/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                        Enable
                                    </a>
                                @endif
                            </td>
                        </tr>

                    @endforeach
                @endif
                </tbody>
            </table>
            <table class="table table-hover table-bordered hide" id="content-one-table">
                <thead>
                <tr>
                    <th>Content</th>
                    <th>DATE EXPIRED</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @if (sizeof($push_one) < 1)
                    <tr class="even gradeA">
                        <td colspan=6> No data </td>
                    </tr>
                @else
                    @foreach ($push_one as $each_data)
                        <tr class="even gradeA">
                            <td>{{ $each_data->text }}</td>
                            <td>{{ $each_data->dtexpired }}</td>
                            <td class="center">
                                <a href="{{url('content/universal_push_one/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    Edit
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalEditContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalContent"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#content-one').click(function(){
        $(this).parent().addClass('active');
        $('#content-diskon').parent().removeClass('active');
        $('#content-diskon-table').addClass('hide');
        $('#content-one-table').removeClass('hide');
    });
    $('#content-diskon').click(function(){
        $(this).parent().addClass('active');
        $('#content-one').parent().removeClass('active');
        $('#content-one-table').addClass('hide');
        $('#content-diskon-table').removeClass('hide');
    });

    $(".btnEdit").click(function() {
        $.get("{{ url('/api/edit_content') }}" + "/" + this.getAttribute('contentId')
            + "?contentType={{ $contentType }}" + "&applicationId=" + this.getAttribute('applicationId'),
            {ajax: 'true'},
            function (data) {
                $("#theModalContent").html('');
                $("#theModalContent").html(data);
            }
        );
    });

    $(".pagination li a").click(function() {
        $("#theContent").html('<div class="span12">Querying...<div class="col-md-8 col-sm-8 col-xs-8">' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

        var str = this.getAttribute("href");
        var thePage = str.substring(str.lastIndexOf('page'),str.length);

        $.get("{{ url('/api/get_list_push_content') }}" + "/" + $("#applicationSelect").val() + "?" + thePage,
            {ajax: 'true'},function (data) {
                $("#theContent").html('');
                $("#theContent").html(data);
            }).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {window.location = "{{ url('/login') }}";}
                else {
                    $("#idAlertSuccess").html('Error while getting list of content! Please try again later or contact system Administrator')
                }
            });
        return false;
    });

</script>
@endif
