<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST">
            <h4>Add {{ $typeLabel }} content for <b>{{ $application->name }}</b></h4><br/>
            <input type="hidden" name="applicationId" value="{{ $applicationId }}">
            <textarea id="text-description" name="content"
                      placeholder="Content" class="span12" cols="70" rows="5"></textarea>
            <br/>
            <br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <span class="help">Sequence number for the content</span>
                        <div class="controls">
                            <div class="controls">
                                <input id="idApplicationName" style="width:10%;" type="text" class="form-control" name="sequence" value="0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <button id="btnSave" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Save</button>
        </form>
    </div>
</div>

<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var doneEdit = false;
    $("#myform").validate({
        rules: {
            content: {required: true}
        }
    });
</script>
