<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <br>
    <h4 id="myModalLabel" class="semi-bold">Edit Content</h4>

    <p class="no-margin">Carefully review the values</p>
    <br>
</div>
<div class="modal-body">
    <form id="myEditContentForm" method="POST">
        <input type="hidden" name="contentType" value="{{ $contentType }}">
        <input type="hidden" name="contentId" value="{{ $contentId }}">
        <input type="hidden" name="applicationId" value="{{ $applicationId }}">
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <div class="panel panel-default" style="margin-bottom:0;">
                        <div class="">
                            <span class="btn btn-success fileinput-button" style="margin-top:17px;">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload2" type="file" name="files" multiple>
                            </span>
                        </div>
                        <div class="">
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                        </div>
                        <div id="text-url2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <textarea id="text-description" name="content"
                              placeholder="Content" class="span12" cols="70" rows="5">{{ $content->text }}</textarea>
                </div>
            </div>
        </div>
            @if($applicationId == 2006)
                <div id="content_type" class="row form-row">
                    <div class="col-md-10">
                        <label class="form-label">Content Type</label>
                        <div class="controls">
                            <select name="content_type">
                                @if($content->content_type == 0)
                                    <option selected value="0">GENERAL</option>
                                    <option value="1">FANTASI BOLA</option>
                                @else
                                    <option value="0">GENERAL</option>
                                    <option selected value="1">FANTASI BOLA</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            @endif

        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Sequence</label>
                    <span class="help">*Sequence number for the content</span>
                    <div class="controls">
                        <div class="controls">
                            <input id="idApplicationName" style="width:10%;" type="text" class="form-control"
                                   name="sequence" value="{{ $content->sequence }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Disabled</label><br/>
                    <div class="controls">
                        <select id="idChargingDisabled" style="width:40%;" name="isDisabled">
                            <option value="1" {{ $content->is_disabled ? "selected='1'" : "" }}>Yes</option>
                            <option value="0" {{ $content->is_disabled ? "" : "selected='1'" }}>No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="waitCommitEditContent" class="row form-row"></div>
    <div id="idEditContentErrorMessage" class="row form-row"></div>
    <div id="idEditContentAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
    <button type="button" id="btnDismissEditContent" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" id="btnCommitEditChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">

    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = '{{url('/content/upload')}}';
        var image_folder = $('input[name="image_folder"]').val();
        $('#fileupload2').fileupload({
            url: url,
            dataType: 'json',
            formData: {_token:'{{csrf_token()}}',image_folder:image_folder},
            done: function (e, data) {
                $.each(data.result, function (index, file) {
                    $('#text-url2').text(file);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

    var doneEditContent = false;
    $("#idChargingDisabled").select2();
    $("#btnDismissEditContent").click(function () {
        if (doneEditContent) {
            /*
            window.location = "{{ url('/admin/manage_service') }}?telcoId="
                + $("#telcoSelect").val() + "&givenSdc=" + $("#sdcSelect").val();
            */
            getListContent();
        }
    });
    $("#btnCommitEditChanges").click(function () {
        $("#waitCommitEditContent").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitEditChanges").attr("disabled", "disabled");
        $.post("{{ url('/api/edit_content') }}" + "/" + {{ $contentId }}, $("#myEditContentForm").serialize(),
            function (data) {
                $("#btnCommitEditChanges").removeAttr("disabled");
                $("#waitCommitEditContent").html('');
                if (data['Error']) {
                    $("#idEditContentErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEditContent = true;
                    $("#idEditContentAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success&nbsp;edit content </div>');
                }
            })
            .done(function () {
                $("#btnCommitEditChanges").removeAttr("disabled");
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                $("#btnCommitEditChanges").removeAttr("disabled");
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idEditContentAlertSuccess").html(
                        'Error while edit service! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });
</script>
