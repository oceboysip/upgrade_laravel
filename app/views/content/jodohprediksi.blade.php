@extends('content.manage_content_simple-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">Jodoh Prediksi</span></h3>
                        <br/>
                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/content/jodohprediksi/add')}}">Add Content</a>
                        <br/>
                        @if (\Session::has('success'))
                          <p>
                          <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                          </div>
                        @endif
                        @if (\Session::has('err'))
                          <p>
                          <div class="alert alert-danger">
                            <p>{{ \Session::get('err') }}</p>
                          </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="col-md-4">
                    <form action="{{ url('/content/jodohprediksi') }}" method="POST" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="qs" placeholder="Search keyword jodoh"> 
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Text</th>
                                    <th>Prediksi</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                <tr class="even gradeA">
                                    <td colspan=7> No data </td>
                                </tr>
                                @else
                                    @foreach ($results as $data)
                                        <tr class="even gradeA">
                                            <td>{{ $data->id }}</td>
                                            <td>{{ $data->text }}</td>
                                            <td>{{ $data->prediksi_start }} - {{ $data->prediksi_end }}</td>
                                            <td>
                                                @if ($data->n_status == 0)
                                                    <span class="label label-warning">New</span>
                                                @elseif ($data->n_status == 1)
                                                    <span class="label label-success">Active</span>
                                                @else
                                                    <span class="label label-important">Deleted</span>
                                                @endif
                                            </td>
                                            <td class="center">
                                                @if ($data->n_status == 0)
                                                    <a href="{{url('content/jodohprediksi/enable/'.$data->id)}}" class="btn btn-small btn-success btnEdit">
                                                        Enable
                                                    </a>
                                                @endif

                                                @if ($data->n_status < 2)
                                                    <a href="{{url('content/jodohprediksi/edit/'.$data->id)}}" class="btn btn-small btn-white btnEdit">
                                                        Edit
                                                    </a>
                                                    <a href="{{url('content/jodohprediksi/delete/'.$data->id)}}" class="btn btn-small btn-danger btnEdit">
                                                        Delete
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $results->links() }}
                        </div>
                        
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

