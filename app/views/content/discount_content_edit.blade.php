@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/discount_content/'.$result->content_id)}}">
            <h4>Edit Discount Content</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="panel panel-default" style="margin-bottom:0;">
                        <div class="" style="margin-top:17px;">
                            <span class="btn btn-success fileinput-button" style="margin-top:-17px;">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files" multiple>
                            </span>
                            <div id="text-url">

                            </div>
                        </div>
                        <div class="">
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                        </div>

                    </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Text</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="text" cols="100" rows="5">{{$result->text}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label"> URL to short:</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" type="text" class="" name="shorten" id="shorten">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Shorten URL:</label><br/>
                        <div class="shorten_url">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Category</label><br/>
                        <div class="controls">
                            <select id="idPushDayInterval" style="width:80%;" name="category_id">
                            @foreach ($categories as $category)
                                @if($category->category_id == $result->category_id)
                                    <option value="{{ $category->category_id }}" selected>
                                        {{ $category->category_name }}
                                    </option>
                                @else
                                    <option value="{{ $category->category_id }}">
                                        {{ $category->category_name }}
                                    </option>
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Scheduled</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->dtscheduled}}" type="text" class="form-control" name="dtscheduled">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Expired</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->dtexpired}}" placeholder="" type="text" class="form-control" name="dtexpired">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->sequence}}" placeholder="" type="text" class="form-control" name="sequence">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@stop
