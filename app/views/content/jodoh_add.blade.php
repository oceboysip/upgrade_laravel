@extends('content.manage_content_simple-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
    @if (\Session::has('err'))
        <div class="alert alert-error">
            <p>{{ \Session::get('err') }}</p>
        </div>
    @endif

        <form id="myform" method="POST" action="{{url('/content/jodoh/add')}}">
            <h4>Add New Jodoh Content</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Text</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" placeholder="" type="text" class="" name="text" id="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br/>
            <br/>
                <a href="{{ url('/content/jodoh') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
@stop
