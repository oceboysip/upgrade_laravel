@extends('content.manage_content-layout')

@section('main_content')
<div class="row">

    <div class="grid simple">
        <div class="grid-body no-border" id="start_stop">

    	</div>
	</div>

<div id="theContent">
    <div class="span12">
        <div class="grid simple ">
	        <div class="grid-title">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">List Of SMSGW</li>
				</ol>
		    </div>
            <div class="grid-body" id="LiveMonitoring">
            </div>
        </div>
    </div>
</div>

<script id="tplarea" type="text/template">
	<% $.each(data, function(i, obj) { %>
		<button type="button" class="btn btn-success" onclick="get_pcname('<%=obj.area%>')">
			<%=obj.area.toUpperCase()%>
		</button>
	<% }); %>
</script>

<script id="tplpc" type="text/template">
	<% $.each(data, function(i, obj) { %>
		<button type="button" class="btn btn-success" onclick="get_device('<%=obj.area%>', '<%=obj.pc_name%>')">
			<%=obj.pc_name.toUpperCase()%>
		</button>
	<% }); %>
</script>

<script id="tpldevice" type="text/template">
	<table class="table table-hover table-bordered">
	    <thead>
	        <tr>
	            <th>ID</th>
	            <th>Com Name</th>
	            <th>TimeSleep</th>
	            <th>STATUS</th>
	            <th>&nbsp;</th>
	            <th>MESSAGE</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<% 
	    		$.each(data, function(i, obj) {
	    			var device_status = '';
	    			var button_utils = '';
	    			if(obj.n_status === 0) {
	    				device_status = '<span class="label label-danger">STOPPING</span>';
	    				button_utils = '';
	    			} else if(obj.n_status === 1) {
	    				device_status = '<span class="label label-primary">READY</span>';
	    				button_utils = '<a onclick="start_device('+obj.id+', \''+obj.area+'\', \''+obj.pc_name+'\')" href="javascript:;" class="btn btn-small btn-white btn-cons btnEdit">START</a>';
	    			} else if(obj.n_status === 2) {
	    				device_status = '<span class="label label-success">START</span>';
	    			} else if(obj.n_status === 3) {
	    				device_status = '<span class="label label-warning">RUNNING</span>';
	    				button_utils = '<a onclick="stop_device('+obj.id+', \''+obj.area+'\', \''+obj.pc_name+'\')" href="javascript:;" class="btn btn-small btn-white btn-cons btnEdit">STOP</a>';
	    			}
	    	%>
	    			<tr>
	                    <td><%=obj.id%></td>
	                    <td><%=obj.com_name%></td>
	                    <td><%=obj.timesleep%></td>
	                    <td>
	                    	<%=device_status%>
	                    </td>
	                    <td class="center">
	                    	<%=button_utils%>
	                    </td>
	                    <td class="center">
	                    	<%=obj.last_error_message%>
	                    </td>
	                </tr>
	        <%        
	    		});
	    	%>

	    </tbody>
	</table>
</script>
@stop

@section('js_application_list')

var event_template = null;

function get_area() {
	breadcrumb_level_one();
	clearTimeout(event_template);
	$.ajax({
		url: "{{url('/nodesms/get_area')}}",
		data: '',
		success: function(data, textStatus, jqXHR) {
			render_view(tplarea, '#LiveMonitoring', {data:data});
		},
		dataType: 'json'
	});
}

function get_pcname(area) {
	clearTimeout(event_template);
	breadcrumb_level_two(area);
	$.ajax({
		url: "{{url('/nodesms/get_pcname')}}/"+area,
		data: '',
		success: function(data, textStatus, jqXHR) {
		console.log(data);
			render_view(tplpc, '#LiveMonitoring', {data:data});
		},
		dataType: 'json'
	});
}

function get_device(area, pc_name) {
	breadcrumb_level_three(area, pc_name);
	console.log(event_template);
	$.ajax({
	  url: "{{url('/nodesms/get_device')}}/"+area+"/"+pc_name,
	  data: '',
	  success: function(data, textStatus, jqXHR) {
	  	render_view(tpldevice, '#LiveMonitoring', {data:data});
	  },
	  dataType: 'json'
	});

	event_template = setTimeout(function() { get_device(area, pc_name) }, 5000);
}

function start_device(id, area, pc_name) {
	$.ajax({
	  url: "{{url('/nodesms/start')}}",
	  data: {id: id},
	  success: function(data, textStatus, jqXHR) {
	  	console.log(data);
	  	clearTimeout(event_template);
	  	get_device(area, pc_name);
	  },
	  dataType: 'json'
	});
}

function start_all_device(area, pc_name) {
	$.ajax({
	  url: "{{url('/nodesms/start_all')}}",
	  data: {area: area, pc_name: pc_name},
	  success: function(data, textStatus, jqXHR) {
	  	console.log(data);
	  	clearTimeout(event_template);
	  	get_device(area, pc_name);
	  },
	  dataType: 'json'
	});
}

function stop_device(id, area, pc_name) {
	$.ajax({
	  url: "{{url('/nodesms/stop')}}",
	  data: {id: id},
	  success: function(data, textStatus, jqXHR) {
	  	console.log(data);
	  	clearTimeout(event_template);
	  	get_device(area, pc_name);
	  },
	  dataType: 'json'
	});
}

function stop_all_device(area, pc_name) {
	$.ajax({
	  url: "{{url('/nodesms/stop_all')}}",
	  data: {area: area, pc_name: pc_name},
	  success: function(data, textStatus, jqXHR) {
	  	console.log(data);
	  	clearTimeout(event_template);
	  	get_device(area, pc_name);
	  },
	  dataType: 'json'
	});
}

function breadcrumb_level_one() {
	$('#start_stop').html('');
	$('.grid-title').html('<ol class="breadcrumb">\
		<li class="breadcrumb-item">List Of SMSGW</li>\
	</ol>');
}

function breadcrumb_level_two(area) {
	$('#start_stop').html('');
	$('.grid-title').html('<ol class="breadcrumb">\
		<li class="breadcrumb-item"><a href="javascript:;" onclick="get_area();">List Of SMSGW</a></li>\
		<li class="breadcrumb-item active">'+area+'</li>\
	</ol>');
}

function breadcrumb_level_three(area, pc_name) {
	$('#start_stop').html('<div class="row">&nbsp;</div>\
	<div class="row">\
		<div class="col-md-12">\
		    <a class="btn btn-primary my-colorpicker-control" href="javascript:;" \
		    onclick="start_all_device(\''+area+'\', \''+pc_name+'\')">START ALL</a>\
		    <a class="btn btn-danger my-colorpicker-control" href="javascript:;"\
		    onclick="stop_all_device(\''+area+'\', \''+pc_name+'\')">STOP ALL</a>\
		</div>\
	</div>');

	$('.grid-title').html('<ol class="breadcrumb"> \
		<li class="breadcrumb-item"><a href="javascript:;" onclick="get_area();">List Of SMSGW</a></li>\
		<li class="breadcrumb-item"><a href="javascript:;" onclick="get_pcname(\''+area+'\')">'+area.toUpperCase()+'</a></li>\
		<li class="breadcrumb-item active">'+pc_name.toUpperCase()+'</li>\
	</ol>');
}

breadcrumb_level_one()
get_area();
//get_device();
@stop