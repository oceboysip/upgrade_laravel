@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">

            <h4>TebakSkor Result </h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Match : {{ $club[0] }} vs {{ $club[1] }}</label><br/>
                        <label class="form-label">Score : {{ ($score[0]=='-') ? '' : $score[0]}} - {{ ($score[1]=='-') ? '' : $score[1] }}</label><br/>
                        
                    </div>
                </div>
            </div>

            <h4>Peserta Tebakan</h4>
            <div class="row form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Jumlah Peserta</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ $partisipan }}" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Skor Benar 20 KOIN</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ $tepat }}" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Klub Benar 10 KOIN</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ $sedang }}" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tebakan Salah</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="{{ $salah }}" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-label">Process time : {{ number_format(($time),4,',','.') }} seconds.</label>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="{{ url('/content/tebakskor') }}" class="btn btn-success">Menu</a>
            </div>
    </div>
</div>
@stop
