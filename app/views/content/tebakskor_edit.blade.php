@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">

        <form id="myform" method="POST" action="{{url('/content/tebakskor_edit/'.$id)}}">
            <h4>Edit TebakSkor Content</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Club 1</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" onkeyup="countChar2(this, 20, 'charNum1')" type="text" class="" name="club1" id="club1"
                                       value="{{ $club[0] }}">
                                <input placeholder="Score" type="text" class="" name="score1" id="score1" value="{{ ($score[0]=='-') ? '' : $score[0] }}">
                                <div id="charNum1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Club 2</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" onkeyup="countChar2(this, 20, 'charNum2')" type="text" class="" name="club2" id="club2"
                                       value="{{ $club[1] }}">
                                <input placeholder="Score" type="text" class="" name="score2" id="score2" value="{{ ($score[1]=='-') ? '' : $score[1] }}">
                                <div id="charNum2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Match Date</label><br/>
                        <div class="controls">
                            <input style="width:80%;"
                                   type="text" class="form-control theDateTime" name="match_date"
                                   value="{{ $tebakskor->match_date }}">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="{{ url('/content/tebakskor') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
@stop
