@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/fb/'.$result->id)}}">
            <h4>Edit Content Fantasi Bola</h4><br/>
            <div class="row form-row">
                <div class="col-md-12">
                    <label class="form-label">Telco</label>
                    <select style="width:50%" name="telco_id" id="umbapp">
                        @if($result->telco_id == '2')
                            <option value="2">
                                INDOSAT
                            </option>
                        @elseif($result->telco_id == '3')
                            <option value="3">
                                XL
                            </option>
                        @endif
                        <option value="2">
                            INDOSAT
                        </option>
                        <option value="3">
                            XL
                        </option>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">User Type</label>
                    <select style="width:50%" name="user_type" id="umbapp">
                        @if($result->user_type == '0')
                            <option value="0">
                                NON MEMBER FB
                            </option>
                        @elseif($result->user_type == '1')
                            <option value="1">
                                MEMBER FB
                            </option>
                        @endif
                        <option value="0">
                            NON MEMBER FB
                        </option>
                        <option value="1">
                            MEMBER FB
                        </option>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">Content Type</label>
                    <select style="width:50%" name="content_type" id="umbapp">
                        @if($result->content_type == '0')
                            <option value="0">
                                PAID
                            </option>
                        @elseif($result->content_type == '1')
                            <option value="1">
                                FREE
                            </option>
                        @endif
                        <option value="0">
                            PAID
                        </option>
                        <option value="1">
                            FREE
                        </option>
                    </select>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="panel panel-default" style="margin-bottom:0;">
                        <div class="" style="margin-top:17px;">
                            <span class="btn btn-success fileinput-button" style="margin-top:-17px;">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files" multiple>
                            </span>
                            <div id="text-url">

                            </div>
                        </div>
                        <div class="">
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                        </div>

                    </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Text</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea onkeyup="countChar(this, 110)" name="text" cols="100" rows="5">{{$result->text}}</textarea>                                                    <div id="charNum"></div>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">DATE NEXT PUSH(<i>yyyy-mm-dd</i>)</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->dtnext_push}}" type="text" class="form-control" name="dtnext_push">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->sequence}}" placeholder="" type="text" class="form-control" name="sequence">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@stop
