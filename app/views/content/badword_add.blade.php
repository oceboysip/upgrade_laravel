@extends('content.manage_content_simple-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
    @if (\Session::has('err'))
        <div class="alert alert-error">
            <p>{{ \Session::get('err') }}</p>
        </div>
    @endif

        <form id="myform" method="POST" action="{{url('/content/badword/add')}}">
            <h4>Add New Badword Content</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Keyword</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" onkeyup="countChar2(this, 20, 'charNum1')" type="text" class="" name="keyword" id="keyword">
                                <div id="charNum1"></div>
                                <input type="checkbox" id="exact" name="exact" value="exact" > Exact word
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br/>
            <br/>
                <a href="{{ url('/content/badword') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
@stop
