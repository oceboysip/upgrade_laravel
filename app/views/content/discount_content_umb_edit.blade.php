@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/discount_content_umb/'.$result->content_id)}}">
            <h4>Edit Discount Content UMB</h4><br/>
            <div class="row form-row">
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Text</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="content" cols="100" rows="5" onkeyup="countChar(this, 135)">{{$result->content}}</textarea>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label"> URL to short:</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" type="text" class="" name="shorten" id="shorten">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Shorten URL:</label><br/>
                        <div class="shorten_url">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Category</label><br/>
                        <div class="controls">
                            <select id="idPushDayInterval" style="width:80%;" name="category_id">
                            @foreach ($categories as $category)
                                @if($category->category_id == $result->discount_category_id)
                                    <option value="{{ $category->category_id }}" selected>
                                        {{ $category->category_name }}
                                    </option>
                                @else
                                    <option value="{{ $category->category_id }}">
                                        {{ $category->category_name }}
                                    </option>
                                @endif
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Scheduled</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->start_at}}" type="text" class="form-control" name="start_at">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Expired</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   value="{{$result->expired_at}}" placeholder="" type="text" class="form-control" name="expired_at">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="{{url('/content/discount_content_umb')}}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@stop
