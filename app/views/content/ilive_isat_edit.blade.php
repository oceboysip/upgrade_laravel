@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-12">
        <form id="myform" method="POST" action="{{url('/content/ilive_isat/edit/'.$id)}}">
            <div class="row form-row">
                <input type="hidden" name="id" value="{{$results->id}}" >
                <h2>Edit iLive ISAT Content</h2><br/>
                @if (\Session::has('err'))
                    <p>
                    <div class="alert alert-danger">
                      <p>{{ \Session::get('err') }}</p>
                    </div>
                @endif
            </div>

            <div class="row form-row">
                <div class="row form-row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="form-label">Content Type</label><br/>
                            <div class="controls">
                                <div class="controls">
                                    <select name="content_type" id="content_type">
                                        @for ($i =1; $i <= 6; $i++)
                                            @if ($i == $results->content_type)
                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                            @else
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="hidden_shio" style="display: none;">
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">ZODIAC</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <select name="zodiac" id="zodiac">
                                        @foreach($shio as $data)
                                            <option value="{{ $data->zodiac }}" {{{ ((strtolower($data->zodiac) == $results->zodiac) ? "selected" : "") }}} >{{ $data->zodiac }}</option>
                                        @endforeach
                                        </select>&nbsp;<font color="red">*</font>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="hidden_URL" style="display: none;">
                    <div class="panel panel-default" style="margin-bottom:0;">
                        <div class="panel-heading" style="margin-top:17px;">
                            <span class="btn btn-success fileinput-button" style="margin-top:-17px;">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select image files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files" multiple>
                            </span>
                            <div id="text-url"></div>
                        </div>
                        <div class="panel-body">
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">URL IMAGE</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                    <input style="width:60%;" type="text" class="" name="url_image" id="url_image" value="{{ $results->url_image }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">TEXT</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="text" cols="100" rows="2" 
                                onkeyup="countChar2(this, 95, 'charNum2')">{{$results->text}}</textarea>
                                <div id="charNum2"></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">DTNEXT PUSH</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   type="text" class="form-control theDate" name="dtnext_push" value="{{$results->dtnext_push}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control" name="sequence" value="{{$results->sequence}}">
                        </div>
                    </div>
                </div>
                </div>
            </div>

            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    //Date Pickers
    $('.date_discount').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });
    //Set today's default
    $(".date_discount").datepicker("setDate", new Date());

</script>
@stop


@section("js_application_list")
    document.getElementById('content_type').addEventListener('change', function () {
        //var style = this.value == 1 ? 'block' : 'none';
        //document.getElementById('hidden_shio').style.display = style;
        if (this.value == 2){
            document.getElementById('hidden_shio').style.display = 'block';
            document.getElementById('hidden_URL').style.display = 'none';
        } else if (this.value == 3){
            document.getElementById('hidden_shio').style.display = 'none';
            document.getElementById('hidden_URL').style.display = 'block';
        } else {
            document.getElementById('hidden_shio').style.display = 'none';
            document.getElementById('hidden_URL').style.display = 'none';
        }
    });


@stop
