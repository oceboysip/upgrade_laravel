@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Koin <span class="semi-bold">Redeem Tsel</span></h3>
                        <div class="modal fade popupModal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            <form id="myform" method="POST">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                            </button>
                                            <br>
                                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                                            <h4 id="myModalLabel" class="semi-bold">Add content for <span id="applicationName"></span></h4>
                                            <!-- <p class="no-margin">Please review the value again</p> -->
                                            <br>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    asdf
                                                </div>
                                            </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </form>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="theContent">
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            @if (isset($results))
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>MSISDN</th>
                                        <th>NAME</th>
                                        <th>ITEM</th>
                                        <th>SUBMIT AT</th>
                                        <th>STATUS</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results as $each_data)
                                        <tr class="even gradeA">
                                            <td>{{$each_data->id}}</td>
                                            <td>{{$each_data->msisdn}}</td>
                                            <td>{{$each_data->name}}</td>
                                            <td>{{$each_data->redem_text}}</td>
                                            <td>{{$each_data->created_at}}</td>
                                            <td class="center">
                                                @if($each_data->n_status == "1")
                                                    <span class="label label-important">Pending</span>
                                                @elseif($each_data->n_status == "2")
                                                    <span class="label label-info">Done</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($each_data->n_status == "1")
                                                    <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/coin_redeem_telkomsel/'.$each_data->id)}}" >
                                                        Process
                                                    </a>
                                                @elseif($each_data->n_status == "2")
                                                    <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/coin_redeem_telkomsel/'.$each_data->id)}}" >
                                                        Edit
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                                No Result
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <ul class="pagination">
                @if($prev_page != '')
                    <li>
                        <a href="{{$prev_page}}">«</a>
                    </li>
                @endif
                @if($next_page != '')
                    <li>
                        <a href="{{$next_page}}">»</a>
                    </li>
                @endif
            </ul>
    </div>
@stop

@section("js_application_list")
    $('#viewContentBtn').on('click', function(){
        var start_date = $('#dateStart').val();
        var end_date = $('#toDate').val();
        $.ajax({
            type: "POST",
            url: "{{url('/content/coin_redem_date')}}",
            data: {start_date: start_date, end_date: end_date},
            success: function(result){
                $('#theContent').html(result);
            },
            dataType: 'html'
        });
    });
@stop
