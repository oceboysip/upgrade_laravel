@extends('content.manage_content-layout')

@section('main_content')
<div class="row">
    <div class="grid simple">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Manage Questions from <span class="semi-bold">User</span></h3>
                    <p>
                        &nbsp;
                    </p>
            </div>
        </div>
    </div>
</div>

<div id="theContent">
    <div class="span12">
        <div class="grid simple ">
            <div class="grid-title">
                <h4><span class="semi-bold">{{$user->name}} - {{$user->msisdn}}</span> Question(s) :</h4>
            </div>
            <div class="grid-body ">
                <ul style="list-style: none; padding-left: 0;">
                    @foreach($faqs as $value)
                        @if($value['user_id'] != 1)
                            <li class="">{{$user->name}}: {{$value['faq']}}</li>
                        @else
                            <li class=""><b>Admin: {{$value['faq']}}</b></li>
                        @endif
                    @endforeach
                </ul>
                <form method="POST" action="{{url('/subscriber/faq/reply/'.$results->id)}}">
                    <textarea id="text-Content" name="reply" onkeyup="countChar(this, 130)" class="span12" cols="70" rows="5"></textarea>
                    <button type="submit" data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewContentBtn" class="btn btn-primary my-colorpicker-control" href="#" style="margin-right: 30px;" data-colorpicker-guid="8">Balas</button>
                </form>
                <div id="charNum"></div>
            </div>

        </div>
    </div>
</div>
@stop
