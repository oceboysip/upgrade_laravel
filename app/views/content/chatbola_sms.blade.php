@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/chatbola_sms/add')}}">
            <h4>SMS Text ChatBola</h4><br/>
            <div class="row form-row">
            <div class="row form-row">
                <div class="col-md-10">Shorten Url : {{trim($shorten_url)}}</div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Text</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="text" cols="100" rows="5" onkeyup="countChar(this, 100)"></textarea>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
                <input type="hidden" name="dtscheduled" value="{{$dtscheduled}}">
                <input type="hidden" name="dtexpired" value="{{$dtexpired}}">
                <input type="hidden" name="sequence" value="{{$sequence}}">
                <input type="hidden" name="chatbola_detail_id" value="{{$chatbola_detail_id}}">
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    //Date Pickers
    $('.date_discount').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });
    //Set today's default
    $(".date_discount").datepicker("setDate", new Date());

</script>
@stop
