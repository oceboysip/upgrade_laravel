<div class="row">
    <div class="grid simple">
        <div class="col-md-12">
            <div class="grid-body ">
                @if (isset($results))
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>MSISDN</th>
                            <th>NAME</th>
                            <th>ITEM</th>
                            <th>REDEEM SOURCE</th>
                            <th>SUBMIT AT</th>
                            <th>STATUS</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($results as $each_data)
                            <tr class="even gradeA">
                                <td>{{$each_data->id}}</td>
                                <td>{{$each_data->msisdn}}</td>
                                <td>{{$each_data->name}}</td>
                                <td>{{$each_data->redem_text}}</td>
                                <td>{{$each_data->redem_source}}</td>
                                <td>{{$each_data->created_at}}</td>
                                <td class="center">
                                    @if($each_data->n_status == "1")
                                        <span class="label label-warning">Pending</span>
                                    @elseif($each_data->n_status == "2")
                                        <span class="label label-info">Done</span>
                                    @elseif($each_data->n_status == "3")
                                        <span class="label label-important">Cancel</span>
                                    @endif
                                </td>
                                <td>
                                    @if($each_data->n_status == "1")
                                        <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/coin_redeem/'.$each_data->id)}}" >
                                            Process
                                        </a>
                                    @elseif($each_data->n_status == "2")
                                        <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/coin_redeem/'.$each_data->id)}}" >
                                            Edit
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    No Result
                @endif
            </div>
        </div>
    </div>
</div>