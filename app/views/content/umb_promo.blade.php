@extends('content.manage_content-layout')

@section('main_content')
<div class="row">
    <div class="grid simple">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Manage Content for <span class="semi-bold">UMB PROMO</span></h3>
                    <p>
                        &nbsp;
                    </p>
                    <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                       class="btn btn-primary my-colorpicker-control" href="{{url('/content/umb_promo/add')}}">Add PROMO</a>
            </div>
        </div>
    </div>
</div>

<div id="theContent">
    <div class="span12">
        <div class="grid simple ">
            <div class="grid-title">
                <h4>List of <span class="semi-bold">UMB PROMO</span></h4>
            </div>
            <div class="grid-body ">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>EVENT NAME</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>REDEEM DATE</th>
                            <th>KOIN</th>
                            <th>REWARD</th>
                            <th>STATUS</th>
                            <th colspan="2">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($results as $result)
                        <tr>
                            <td>{{$result->id}}</td>
                            <td>{{$result->promo_name}}</td>
                            <td>{{$result->start_date}}</td>
                            <td>{{$result->end_date}}</td>
                            <td>{{$result->redeem_date}}</td>
                            <td>{{$result->amount_coin}}</td>
                            <td>{{$result->reward}}</td>
                            <td>
                                @if($result->n_status == 1)
                                    <span class="label label-primary">SCHEDULED</span>
                                @elseif($result->n_status == 2)
                                    <span class="label label-warning">RUNNING</span>
                                @elseif($result->n_status == 3)
                                    <span class="label label-success">DONE</span>
                                @elseif($result->n_status == 4)
                                    <span class="label label-danger">REQUEST DISABLE</span>
                                @elseif($result->n_status == 5)
                                    <span class="label label-danger">DISABLED</span>
                                @endif
                            </td>
                            <td class="center">
                            @if($result->n_status == 1 || $result->n_status == 2 || $result->n_status == 3)
                                <a href="{{url('/content/umb_promo/edit/'.$result->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    Edit
                                </a>
                            @endif
                            @if($result->n_status == 2)
                                <a href="{{url('/content/umb_promo/stop/'.$result->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    STOP
                                </a>
                            @endif
                            @if($result->n_status == 5)
                                <a href="{{url('/content/umb_promo/start/'.$result->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    ENABLE
                                </a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
