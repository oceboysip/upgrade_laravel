@extends('content.manage_content-layout')

@section('main_content')
    <div class="span12">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <form id="myform" method="POST" action="{{url('/content/loker_umb/add')}}">
                <h4>Add New Loker Content</h4><br/>
                <div class="row form-row">
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">Title</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input style="width:60%;"
                                               placeholder=""
                                               onkeyup="countChar(this, 86)" type="text" class="" name="title" id="title">
                                        <div id="charNum"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">Description</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <textarea name="description" cols="100" rows="5" class="tinymce"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">Base on Category</label><br/>
                                <div class="controls">
                                    <select id="category_id" style="width:80%;" name="category_id">
                                        <option value="">Please Select</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">Sub Category</label><br/>
                                <div class="controls" id="subcategory_id">
                                    <select id="subcategory_id" style="width:80%;" name="subcategory_id">
                                        <option value="">Choose Category First</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">IS TOP LOKER ?</label><br/>
                                <div class="controls" id="subcategory_id">
                                    <select id="top_loker" style="width:80%;" name="top_loker">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">Date Scheduled</label><br/>
                                <div class="controls">
                                    <input style="width:60%;"
                                           type="text" class="form-control theDate" name="started_at">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">Date Expired</label><br/>
                                <div class="controls">
                                    <input style="width:60%;"
                                           placeholder="" type="text" class="form-control date_expired" name="expired_at">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                    <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        //Date Pickers
        $('.date_discount').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });
        //Set today's default
        $(".date_discount").datepicker("setDate", new Date());

    </script>
@stop
