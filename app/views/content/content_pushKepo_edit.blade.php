@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/pushKepo/'.$result->id)}}">
            <h4>Edit KEPO / iLIVE Push Notification</h4><br/>
            <div class="row form-row">
                <div class="col-md-12">
                    <label class="form-label">Push Type</label>
                    <select style="width:50%" name="push_type" id="umbapp">
                        @if($result->push_type == '0')
                            <option value="0">
                                Push Notification
                            </option>
                        @elseif($result->push_type == '1')
                            <option value="1">
                                Announcement
                            </option>
                        @endif
                        <option value="0">
                            Push Notification
                        </option>
                        <option value="1">
                            Announcement
                        </option>
                    </select>
                </div>
                <div class="col-md-12">
                    <label class="form-label">Application Type</label>
                    <select style="width:50%" name="push_type" id="umbapp">
                        @if($result->app_type == '0')
                            <option value="0">
                                KEPO
                            </option>
                        @elseif($result->app_type == '1')
                            <option value="1">
                                iLIVE
                            </option>
                        @endif
                        <option value="0">
                            KEPO
                        </option>
                        <option value="1">
                            iLIVE
                        </option>
                    </select>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Title</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea onkeyup="countChar(this, 25)" name="title" cols="70" rows="1">{{$result->title}}</textarea>                                                    <div id="charNum"></div>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Message</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea onkeyup="countChar(this, 255)" name="message" cols="70" rows="5">{{$result->message}}</textarea>                                                    <div id="charNum"></div>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Image</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea onkeyup="countChar(this, 255)" name="image" cols="70" rows="1">{{$result->image}}</textarea>                                                    <div id="charNum"></div>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">DATE SCHEDULE(<i>yyyy-mm-dd</i>)</label><br/>
                        <div class="controls">
                            <input style="width:80%;"
                                   value="{{$result->dtscheduled}}" type="text" class="form-control theDateTime" name="dtscheduled">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@stop
