@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
        </div>
    </div>
@stop


@section("js_application_list")
    $("#myform").validate({
        rules: {
            content: {required: true}
        }
    });
    var doneEdit = false;

    function getApplicationList(choosenTelcoId, choosenSdc) {
        $.getJSON("{{ url('api/get_list_pull_applcation/') }}" + "/" + choosenTelcoId + "/" + choosenSdc,
        {ajax: 'true'}, function (data) {
            $("#applicationSelect").html('');
            $("#applicationSelect").append('<optgroup label="Choose Business Application">');
            var firstData = null;
            $.each(data, function () {
                if (!firstData) {firstData = this.id;}
                $("#applicationSelect").append('<option value="' + this.id + '">' + this.name + '</option>')
            })
            $("#applicationSelect").append('</optgroup>');
            $("#applicationSelect").select2('val',firstData);
        });
    }

    function countChar(val) {
        var _s = val.value;
        _s = _s.replace('“','"'); //8220
        _s = _s.replace('”','"'); //8221
        _s = _s.replace('‘','"'); //8216
        val.value = _s;
        var len = _s.length;
        if (len >= 159) {
            val.value = _s.substring(0, 159);
        } else {
            //$('#charNum').text((160 - len) + ' character left' + ' char First: ' + val.value.charCodeAt(0));
            $('#charNum').text((159 - len) + ' character left');
        }
    };    


    $("#viewContentBtn").click(function() {
        $("#theContent").html('<div class="span12">Querying...<div class="col-md-8 col-sm-8 col-xs-8">' +
                        '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

        getListContent();
    });

    $("#addContentBtn").click(function() {
        $("#applicationName").html($("#applicationSelect option:selected").text());
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled","disabled");
        //$("#btnCommitChanges").removeAttr('disabled');
        $.post("{{ url('/api/create_new_pull_content') }}" + "/" + $("#applicationSelect").val(), $( "#myform" ).serialize(),
            function (data) {
                $("#btnCommitChanges").removeAttr('disabled');
                $("#waitCommit").html('');
                if (data['Error']) {
                $("#idErrorMessage").html('<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                    'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html('<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                        'Success:&nbsp;Add new content for ' + $("#applicationSelect option:selected").text() + '</div>');
            }
        })
        .done(function () {
            $("#btnCommitChanges").removeAttr('disabled');
            //alert("second success");
        })
        .fail(function () {
            $("#btnCommitChanges").removeAttr('disabled');
            $("#idAlertSuccess").html('Error while creating new pull content! Please try again later or contact system Administrator')
        })
        .always(function () {
            $("#btnCommitChanges").removeAttr('disabled');
            //alert("finished");
        })
        .fail(function (jqXHR, textStatus) {
            if (jqXHR.status == 401) window.location = "{{ url('/login') }}";
        })
        ;
    });

    function getListContent() {
        var pInfo = $("#pagingInfo").attr("page");
        $.get("{{ url('/api/get_list_pull_content') }}" + "/" + $("#applicationSelect").val(),
            {ajax: 'true', page: pInfo},function (data) {
                $("#theContent").html('');
                $("#theContent").html(data);
            }).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {window.location = "{{ url('/login') }}";}
                else {
                    $("#idAlertSuccess").html('Error while getting list of content! Please try again later or contact system Administrator')
                }
            });
    }

    $("#btnDismiss").click(function() {
        $("#text-Content").val("");
        $("#idErrorMessage").html("");
        $("#idAlertSuccess").html("");
        if (doneEdit) {
            doneEdit = false;
            getListContent();
        }
    });


@stop
