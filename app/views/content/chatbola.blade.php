@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">ChatBola</span></h3>
                        <br/>
                        <!-- <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/content/chatbola/add')}}">Add Content</a> -->
                        <br/>

                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>CONTENT ID</th>
                                    <th>CONTENT TYPE</th>
                                    <th>TEXT</th>
                                    <th>SEQUENCE</th>
                                    <th>DATE NEXT PUSH</th>
                                    <th>DISABLED</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="even gradeA">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $each_data)
                                        <tr class="even gradeA">
                                            <td>{{$each_data->id}}</td>
                                            <td>{{ $each_data->content_type }}</td>
                                            <td>{{ $each_data->text }}</td>
                                            <td>{{ $each_data->sequence }}</td>
                                            @if(time() > strtotime($each_data->dtnext_push))
                                                <td style="color:red;">{{ $each_data->dtnext_push }}</td>
                                            @else
                                                <td>{{ $each_data->dtnext_push }}</td>
                                            @endif
                                            @if($each_data->n_status == 1)
                                                <td><span class="label label-success">No</span></td>
                                            @else
                                                <td><span class="label label-important">Yes</span></td>
                                            @endif
                                            <td class="center">
                                                <a href="{{url('content/chatbola/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                    Edit
                                                </a>

                                                @if($each_data->n_status == 1)
                                                    <!-- <a href="{{url('content/chatbola/del/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                        Disable
                                                    </a> -->
                                                @else
                                                    <a href="{{url('content/chatbola/enable/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                        Enable
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

