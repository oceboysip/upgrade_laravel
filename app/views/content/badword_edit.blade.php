@extends('content.manage_content_simple-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">

        <form id="myform" method="POST" action="{{url('/content/badword_edit/'.$id)}}">
            <h4>Edit Badword Content</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Keyword</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" onkeyup="countChar2(this, 20, 'charNum1')" type="text" class="" name="keyword" id="keyword"
                                       value="{{ $keyword }}">
                                <div id="charNum1"></div>
                                <input type="checkbox" id="exact" name="exact" value="1" {{ ($exact <= 0 ? '':'checked') }}> Exact word
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">REGEX</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" type="text" class="" name="regex_filter" id="regex_filter"
                                       value="{{ $regex_filter }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br/>
            <br/>
                <a href="{{ url('/content/badword') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
@stop
