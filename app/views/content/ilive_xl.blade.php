@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">iLive XL</span></h3>
                        <br/>
                        <!-- <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/content/model/add')}}">Add Content</a> -->
                        <br/>

                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/content/ilive_xl/add')}}">Add Content</a>
                        <br/>
                        @if (\Session::has('success'))
                          <p>
                          <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                          </div>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>C. ID</th>
                                    <th>C. TYPE</th>
                                    <th>TEXT</th>
                                    <th>ZODIAC</th>
                                    <th>URL IMAGE</th>
                                    <th>SEQUENCE</th>
                                    <th>DATE NEXT PUSH</th>
                                    <th>DISABLED</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="even gradeA">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                        <tr class="even gradeA">
                                            <td>{{$data->id}}</td>
                                            <td>{{ $data->content_type }}</td>
                                            <td>{{ $data->text }}</td>
                                            <td>{{ $data->zodiac }}</td>
                                            <td>{{ $data->url_image }}</td>
                                            <td>{{ $data->sequence }}</td>
                                            @if(time() > strtotime($data->dtnext_push))
                                                <td style="color:red;">{{ $data->dtnext_push }}</td>
                                            @else
                                                <td>{{ $data->dtnext_push }}</td>
                                            @endif
                                            @if($data->n_status == 1)
                                                <td><span class="label label-success">No</span></td>
                                            @else
                                                <td><span class="label label-important">Yes</span></td>
                                            @endif
                                            <td class="center">
                                                <a href="{{url('content/ilive_xl/edit/'.$data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                    Edit
                                                </a>

                                                @if($data->n_status == 1)
                                                    <a href="{{url('content/ilive_xl/disable/'.$data->id)}}" class="btn btn-small btn-error btn-cons btnEdit">
                                                        Disable
                                                    </a>
                                                @else
                                                    <a href="{{url('content/ilive_xl/enable/'.$data->id)}}" class="btn btn-small btn-success btn-cons btnEdit">
                                                        Enable
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

