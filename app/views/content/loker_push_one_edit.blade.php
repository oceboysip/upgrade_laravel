@extends('content.manage_content-layout')

@section('main_content')
    <div class="span12">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <form id="myform" method="POST" action="{{url('/content/push_one/'.$result->id)}}">
                <h4>Edit Push One Content</h4><br/>
                <div class="row form-row">
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">Text</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <textarea onkeyup="countChar(this, 160)" name="text" cols="100" rows="5">{{$result->text}}</textarea>
                                    </div>
                                    <div id="charNum"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">Date Expired</label><br/>
                                <div class="controls">
                                    <input style="width:60%;"
                                           value="{{$result->dtexpired}}" placeholder="" type="text" class="form-control P theDate" name="dtexpired">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                    <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@endsection
