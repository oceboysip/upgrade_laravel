@extends('content.manage_content-layout')

@section('main_content')
<div class="col-md-12">
<div class="span12 tiles-container tiles">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/umb_promo_indosat/edit')}}">
            <h4><b>Add New UMB PROMO INDOSAT</b></h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label"> EVENT PROMO NAME: <small><em>(ex. imlek, valentine, etc)</em></small></label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" type="text" class="" name="promo_name" value="{{$row_data['promo_name']}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Start</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   type="text" class="form-control theDate" name="start_date" value="{{$row_data['start_date']}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date End</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control theDate" name="end_date" value="{{$row_data['end_date']}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">TEXT PROMO</label><br/>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseOne .panel-body', 'header_utama[]', 45)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseOne">
                                  <b>HEADER - MENU UTAMA</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_utama']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_utama'][$i]}}" type="text" class="" name="header_utama[]" id="shorten"
                                                onkeyup="char_left(this, 45)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_utama'][$i]}}" 
                                            type="text" class="" name="header_utama[]" onkeyup="char_left(this, 45)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseEleven .panel-body', 'header_pesta_koin[]', 130)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseEleven">
                                  <b>HEADER - PESTA KOIN</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseEleven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwoEleven">
                              <div class="panel-body">
                                @for($i=0;$i<count(@$row_data['header_pesta_koin']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_pesta_koin'][$i]}}" type="text" class="" name="header_pesta_koin[]" id="shorten"
                                                onkeyup="char_left(this, 130)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_pesta_koin'][$i]}}" 
                                            type="text" class="" name="header_pesta_koin[]" onkeyup="char_left(this, 130)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseTwo .panel-body', 'header_cnd[]', 45)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseTwo">
                                  <b>HEADER - CND</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_cnd']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_cnd'][$i]}}" type="text" class="" name="header_cnd[]" id="shorten"
                                                onkeyup="char_left(this, 45)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_cnd'][$i]}}" 
                                            type="text" class="" name="header_cnd[]" onkeyup="char_left(this, 45)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseThree .panel-body', 'header_coin_pulsa[]', 45)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseThree">
                                  <b>HEADER - TUKAR PULSA / KOIN&PULSA</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_coin_pulsa']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_coin_pulsa'][$i]}}" type="text" class="" name="header_coin_pulsa[]" id="shorten"
                                                onkeyup="char_left(this, 45)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_coin_pulsa'][$i]}}" 
                                            type="text" class="" name="header_coin_pulsa[]" onkeyup="char_left(this, 45)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseFour .panel-body', 'header_main_game[]', 45)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseFour">
                                  <b>HEADER - MAIN GAME</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_main_game']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_main_game'][$i]}}" type="text" class="" name="header_main_game[]" id="shorten"
                                                onkeyup="char_left(this, 45)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_main_game'][$i]}}" 
                                            type="text" class="" name="header_main_game[]" onkeyup="char_left(this, 45)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseSix .panel-body', 'header_info_how[]', 45)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseSix">
                                  <b>INFO - Gimana sih cara dpt koin?</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_info_how']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_info_how'][$i]}}" type="text" class="" name="header_info_how[]" id="shorten"
                                                onkeyup="char_left(this, 45)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_info_how'][$i]}}" 
                                            type="text" class="" name="header_info_how[]" onkeyup="char_left(this, 45)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseSeven .panel-body', 'header_info_for[]', 45)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseSeven">
                                  <b>INFO - Buat apa sih?</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_info_for']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_info_for'][$i]}}" type="text" class="" name="header_info_for[]" id="shorten"
                                                onkeyup="char_left(this, 45)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_info_for'][$i]}}" 
                                            type="text" class="" name="header_info_for[]" onkeyup="char_left(this, 45)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>
                          </div>

                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseEight .panel-body', 'header_periode_koin[]', 100)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseEight">
                                  <b>INFO - Periode Tukar Koin</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_periode_koin']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_periode_koin'][$i]}}" type="text" class="" name="header_periode_koin[]" id="shorten"
                                                onkeyup="char_left(this, 100)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_periode_koin'][$i]}}" 
                                            type="text" class="" name="header_periode_koin[]" onkeyup="char_left(this, 100)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>

                            <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <h4 class="panel-title">
                                <a onclick="add_textfield('collapseNine .panel-body', 'header_sample_kuis[]', 100)" class="collapsed" role="button" data-parent="#accordion" href="javascript:;" aria-expanded="true" aria-controls="collapseNine">
                                  <b>INFO - Sample Kuis Promo</b>
                                </a>
                              </h4>
                            </div>
                            <div id="collapseNine" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                @for($i=0;$i<count($row_data['header_sample_kuis']);$i++)
                                    @if($i==0)
                                        <div class="controls">
                                          <input style="width:80%;"
                                               value="{{$row_data['header_sample_kuis'][$i]}}" type="text" class="" name="header_sample_kuis[]" id="shorten"
                                                onkeyup="char_left(this, 100)">
                                                <label class="form-label"></label>
                                        </div>
                                    @else
                                        <div class="controls">
                                            <input style="width: 80%;" value="{{$row_data['header_sample_kuis'][$i]}}" 
                                            type="text" class="" name="header_sample_kuis[]" onkeyup="char_left(this, 100)">
                                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> 
                                            <label class="form-label"></label>
                                        </div>
                                    @endif
                                @endfor
                              </div>
                            </div>

                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">DATE REDEEM KOIN</label>
                        <div class="controls">
                            <input style="width:60%;"
                                   type="text" class="form-control theDate" name="redeem_date" value="{{$row_data['redeem_date']}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">KOIN REWARD</label><br/>
                        <div class="controls">
                            <label for="one">
                                @if($coin_reward == "3000#PULSA5000")
                                    <input type="radio" name="coin_reward" value="3000#PULSA5000" id="one" checked="checked"> 
                                @else
                                    <input type="radio" name="coin_reward" value="3000#PULSA5000" id="one" > 
                                @endif
                                <span class="text-danger">3000 - PULSA#5000</span>
                            </label>
                        </div>
                        <div class="controls">
                            <label for="two">
                                @if($coin_reward == "5000#PULSA5000")
                                    <input type="radio" name="coin_reward" value="5000#PULSA5000" id="two" checked="checked"> 
                                @else
                                    <input type="radio" name="coin_reward" value="5000#PULSA5000" id="two" > 
                                @endif
                                <span class="text-danger">5000 - PULSA#5000</span>
                            </label>
                        </div>
                        <div class="controls">
                            <label for="three">
                                @if($coin_reward == "5000#PULSA10000")
                                    <input type="radio" name="coin_reward" value="5000#PULSA10000" id="three" checked="checked"> 
                                @else
                                    <input type="radio" name="coin_reward" value="5000#PULSA10000" id="three"> 
                                @endif
                                <span class="text-danger">5000 - PULSA#10000</span>
                            </label>
                        </div>
                        <div class="controls">
                            <label for="four">
                                @if($coin_reward == "50000#PULSA50000")
                                    <input type="radio" name="coin_reward" value="50000#PULSA50000" id="four" checked="checked"> 
                                @else
                                    <input type="radio" name="coin_reward" value="50000#PULSA50000" id="four"> 
                                @endif
                                <span class="text-danger">50000 - PULSA#50000</span>
                            </label>
                        </div>
                        <div class="controls">
                            <label for="five">
                                @if($coin_reward == "20000#PULSA20000")
                                    <input type="radio" name="coin_reward" value="20000#PULSA20000" id="five" checked="checked"> 
                                @else
                                    <input type="radio" name="coin_reward" value="20000#PULSA20000" id="five" > 
                                @endif
                                <span class="text-danger">20000 - PULSA#20000</span>
                            </label>
                        </div>
                         <div class="controls">
                            <label for="six">
                                @if($coin_reward == "25000#PULSA25000")
                                    <input type="radio" name="coin_reward" value="25000#PULSA25000" id="six" checked="checked"> 
                                @else
                                    <input type="radio" name="coin_reward" value="25000#PULSA25000" id="six"> 
                                @endif
                                <span class="text-danger">25000 - PULSA#25000</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">GAME CONFIG</label>
                        <table class='table borderless'>
                            <tr>
                                <td>HANGMAN</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[hangman]" value="{{$coin_config['hangman']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>TEBAK LIRIK</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[tebaklirik]" value="{{$coin_config['tebaklirik']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>BERHASIL CHARGE</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[charging]" value="{{$coin_config['charging']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>ONLINE CND</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[onlinechat]" value="{{$coin_config['onlinechat']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>KIRIM PESAN</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[sendmessage]" value="{{$coin_config['sendmessage']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>DAFTAR CND</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[cndreg]" value="{{$coin_config['cndreg']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>PINTAR BOLA</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[triviabola1]" value="{{$coin_config['triviabola1']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>PINALTI KICK</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[pinaltikick]" value="{{$coin_config['pinaltikick']}}"> KOIN</td>
                            </tr>
                            <tr>
                                <td>TEBAK SKOR</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[tebakskor]" value="{{$coin_config['tebakskor']}}"> KOIN</td>
                            </tr>
                             <tr>
                                <td>TEBAK SKOR - KLUB MENANG</td>
                                <td>:</td>
                                <td><input style="width:20%;" type="text" name="coin_config[tebakklub]" value="{{$coin_config['tebakklub']}}"> KOIN</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <a href="{{url('/content/umb_promo_indosat')}}" class="btn btn-danger">Cancel</a>
            <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            <br />
            <br />
            <input type="hidden" name="id" value="{{$id}}">
        </form>
    </div>
</div>
@stop
@section("js_application_list")
    //append text field
    function add_textfield(target, textname, max_char) {
        $('#'+target).append('<div class="controls"><input style="width: 80%;" placeholder="" type="text" \
                            class="" name="'+textname+'" onkeyup="char_left(this, max_char)"> \
                            <a class="delete_textfield" href="javascript:;" onclick="$(this).parent().remove();">[delete]</a> <label class="form-label"></label><div>');
    }

    //Date Pickers
    $('.date_discount').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });

    //Set today's default
    $(".date_discount").datepicker("setDate", new Date());

    function char_left(val, maxchar, target_element) {
        var len = val.value.length;
        if (len > maxchar) {
            val.value = val.value.substring(0, maxchar);
        } else {
            $(val).parent().find('label').text(maxchar - len+' left');
        }
    };
@stop