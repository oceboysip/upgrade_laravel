@extends('content.manage_content_simple-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">Badword</span></h3>
                        <br/>
                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/content/badword/add')}}">Add Content</a>
                        <br/>
                        @if (\Session::has('success'))
                          <p>
                          <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                          </div>
                        @endif
                        @if (\Session::has('err'))
                          <p>
                          <div class="alert alert-danger">
                            <p>{{ \Session::get('err') }}</p>
                          </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="col-md-4">
                    <form action="{{ url('/content/badword') }}" method="POST" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="qs" placeholder="Search keyword"> 
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                        </div>
                    </form>
                </div>
                <div class="col-md4">
                    <a href="{{ url('/content/regex_db') }}" class="btn btn-success">Regenerate REGEX DB</a>
                    <!-- <a href="{{ url('/content/badword_to_file') }}" class="btn btn-warning">Write all REGEX DB data to Files</a> -->
                </div>
            </div>
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                        {{ $results->links() }}
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Keyword</th>
                                    <th>Exact</th>
                                    <th>Regex</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                <tr class="even gradeA">
                                    <td colspan=7> No data </td>
                                </tr>
                                @else
                                    @foreach ($results as $data)
                                        <tr class="even gradeA">
                                            <td>{{ $data->id }}</td>
                                            <td>{{ $data->keyword }}</td>
                                            <td>
                                                {{ ($data->exact == 0 ? '<span class="glyphicon glyphicon-unchecked"></span>': '<span class="glyphicon glyphicon-check"></span>')}}
                                                
                                            </td>
                                            <td>{{ $data->regex_filter }}</td>
                                            <td class="center">

                                                <a href="{{url('content/badword_edit/'.$data->id)}}" class="btn btn-small btn-white btnEdit">
                                                    Edit
                                                </a>

                                                <a href="{{url('content/badword_generate/'.$data->id)}}?ref={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-small btn-success btnEdit">
                                                    Generate
                                                </a>

                                                <a href="{{url('content/badword_delete/'.$data->id)}}" class="btn btn-small btn-danger btnEdit">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $results->links() }}
                        </div>
                        
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

