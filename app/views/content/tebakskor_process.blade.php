@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">

        <form id="myform" method="POST" action="{{url('/content/tebakskor_process/'.$id)}}">
            <h4>Process TebakSkor</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Match : {{ $club[0] }} vs {{ $club[1] }}</label><br/>
                        <label class="form-label">Score : {{ ($score[0]=='-') ? '' : $score[0]}} - {{ ($score[1]=='-') ? '' : $score[1] }}</label><br/>
                        
                    </div>
                </div>
            </div>

            <h4>Peserta Tebakan</h4>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">{{ $partisipan }}</label><br/>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="{{ url('/content/tebakskor') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Process</button>
            </div>
        </form>
    </div>
</div>
@stop
