@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-12">
        <form id="myform" method="POST" action="{{url('/content/chatbola_edit/'.$id)}}">
            <input type="hidden" name="id" value="{{$results->id}}" >
            <h4>Edit ChatBola Content</h4><br/>
            <div class="row form-row">
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Content Type</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <select name="content_type">
                                    <option value="{{$results->content_type}}">{{$results->content_type}}</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">TEXT</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="text" cols="100" rows="15" 
                                onkeyup="countChar2(this, 110, 'charNum2')">{{$results->text}}</textarea>
                                <div id="charNum2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">DTNEXT PUSH</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   type="text" class="form-control theDate" name="dtnext_push" value="{{$results->dtnext_push}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control" name="sequence" value="{{$results->sequence}}">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    //Date Pickers
    $('.date_discount').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });
    //Set today's default
    $(".date_discount").datepicker("setDate", new Date());

</script>
@stop
