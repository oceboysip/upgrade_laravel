<div class="row">
    @if (isset($results))
        <div class="row">
            <div class="grid simple">
                <div class="col-md-12">
                    <div class="grid-body ">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>CONTENT ID</th>
                                <th>Content</th>
                                <th>Category</th>
                                <th>DATE SCHEDULED</th>
                                <th>DATE EXPIRED</th>
                                <th>DISABLED</th>
                                <th>KEYWORD</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (sizeof($results) < 1)
                                <tr class="even gradeA">
                                    <td colspan=7> No data </td>
                                </tr>
                            @else
                                @foreach ($results as $each_data)
                                    <tr class="even gradeA">
                                        <td>{{$each_data->id}}</td>
                                        <td>{{ $each_data->job_title }}</td>
                                        <td>{{ $each_data->category }} - {{ $each_data->sub_category }}</td>
                                        <td>{{ $each_data->started_at }}</td>
                                        @if(time() > strtotime($each_data->expired_at))
                                            <td style="color:red;">{{ $each_data->expired_at }}</td>
                                        @else
                                            <td>{{ $each_data->expired_at }}</td>
                                        @endif
                                        @if($each_data->n_status == 1)
                                            <td><span class="label label-success">No</span></td>
                                        @else
                                            <td><span class="label label-important">Yes</span></td>
                                        @endif
                                        <td class="center">
                                            <a href="{{url('content/loker_umb_edit/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                Edit
                                            </a>
                                            @if($each_data->n_status == 0)
                                                <a href="{{url('content/loker_umb/enable/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                    Enable
                                                </a>
                                            @else
                                                <a href="{{url('content/loker_umb/del/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                    Disable
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
