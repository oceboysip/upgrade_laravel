@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">UMB</span></h3>
                        <p>
                            <select style="width:50%" name="applicationId" id="umbapp">
                                <option value="">
                                    CHOOSE TYPE OF UMB
                                </option>
                                @if(isset($results))
                                    @foreach ($results as $each_service)
                                        <option value="{{ $each_service }}"
                                                >{{ $each_service }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </p>

                        <br/>
                        <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="#"
                           style="margin-right: 30px;"
                           data-colorpicker-guid="8">View</a>

                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="#"
                           data-toggle="modal" data-target="#disable">Add Content</a>

                        <div class="modal fade popupModal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            <form id="myform" method="POST">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                            </button>
                                            <br>
                                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                                            <h4 id="myModalLabel" class="semi-bold">Add content for <span id="applicationName"></span></h4>
                                            <!-- <p class="no-margin">Please review the value again</p> -->
                                            <br>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default" style="margin-bottom:0;">
                                                        <div class="panel-heading" style="margin-top:17px;">
                                                    <span class="btn btn-success fileinput-button" style="margin-top:-17px;">
                                                        <i class="glyphicon glyphicon-plus"></i>
                                                        <span>Select files...</span>
                                                        <!-- The file input field used as target for the file upload widget -->
                                                        <input id="fileupload" type="file" name="files" multiple>
                                                    </span>
                                                            <div id="text-url">

                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="progress" class="progress">
                                                                <div class="progress-bar progress-bar-success"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-8">
                                            <textarea id="text-Content" name="content" onkeyup="countChar(this, 100)"
                                                      placeholder="Content" class="span12" cols="70"
                                                      rows="5"></textarea>
                                                    <div id="charNum"></div><br/>
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-8">
                                                    <label class="form-label">Sequence</label>
                                                    <div class="controls">
                                                        <input id="idApplicationName" style="width:10%;" type="text"
                                                               class="form-control" name="sequence" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="waitCommit" class="row form-row"></div>
                                            <div id="idErrorMessage" class="row form-row"></div>
                                            <div id="idAlertSuccess" class="row form-row"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" id="btnCommitChanges" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </form>
                            <!-- /.modal-dialog -->
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section("js_application_list")
    $("#myform").validate({
    rules: {
    content: {required: true}
    }
    });

    var doneEdit = false;
    function getApplicationList(choosenTelcoId, choosenSdc) {
    $.getJSON("{{ url('api/get_list_umb_applcation/') }}" + "/" + choosenTelcoId + "/" + choosenSdc,
    {ajax: 'true'}, function (data) {
    $("#applicationSelect").html('');
    $("#applicationSelect").append('<optgroup label="Choose Business Application">');
        var firstData = null;
        $.each(data, function () {
        if (!firstData) {firstData = this.id;}
        $("#applicationSelect").append('<option value="' + this.id + '">' + this.name + '</option>')
        })
        $("#applicationSelect").append('</optgroup>');
    $("#applicationSelect").select2('val',firstData);
    });
    }

    $("#viewContentBtn").click(function() {
    $("#theContent").html('<div class="span12">Querying...<div class="col-md-8 col-sm-8 col-xs-8">' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

    getListContent();
    });

    function countChar(val) {
    var _s = val.value;
    _s = _s.replace('“','"'); //8220
    _s = _s.replace('”','"'); //8221
    _s = _s.replace('‘','"'); //8216
    val.value = _s;
    var len = _s.length;
    if (len >= 159) {
    val.value = _s.substring(0, 159);
    } else {
    //$('#charNum').text((160 - len) + ' character left' + ' char First: ' + val.value.charCodeAt(0));
    $('#charNum').text((159 - len) + ' character left');
    }
    };

    function getListContent() {
    var pInfo = $("#pagingInfo").attr("page");
    $.get("{{ url('/api/get_list_umb_content') }}" + "/" + $("#umbapp").val(),
    {ajax: 'true', page: pInfo},function (data) {
    $("#theContent").html('');
    $("#theContent").html(data);
    }).fail(function (jqXHR, textStatus) {
    if (jqXHR.status == 401) {window.location = "{{ url('/login') }}";}
    else {
    $("#idAlertSuccess").html('Error while getting list of content! Please try again later or contact system Administrator')
    }
    });
    }

    $('#umbapp').change(function(){
    console.log('appid', $(this).val());
    if($(this).val() == 'D'){
    $('#addContentBtn').attr('data-target', '#disable');
    }else{
    $('#addContentBtn').attr('data-target', '#disable');
    }
    });

    $("#addContentBtn").click(function() {
    var appid = $('#umbapp').val();
    console.log('appid', appid);
    if(appid == 'Loker'){
    window.location.href = "{{url('/content/loker_umb/add')}}";
    }else if(appid == 'Discount'){
    window.location.href = "{{url('/content/discount_content_umb/add')}}";
    }else if(appid == 'TebakSkor'){
    window.location.href = "{{url('/content/tebakskor/add')}}";
    }else if(appid == 'Badword'){
    window.location.href = "{{url('/content/badword/add')}}";
    }else{
    $("#applicationName").html($("#applicationSelect option:selected").text());
    }
    });

    $("#btnCommitChanges").click(function () {
    $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
        '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

    $("#btnCommitChanges").attr("disabled","disabled");
    //$("#btnCommitChanges").removeAttr('disabled');
    $.post("{{ url('/api/create_new_push_content') }}" + "/" + $("#applicationSelect").val(), $( "#myform" ).serialize(),
    function (data) {
    $("#btnCommitChanges").removeAttr('disabled');
    $("#waitCommit").html('');
    if (data['Error']) {
    $("#idErrorMessage").html('<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
        'Error while saving to database: ' + data['Message'] + '</div>');
    } else {
    doneEdit = true;
    $("#idAlertSuccess").html('<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
        'Success:&nbsp;Add new content for ' + $("#applicationSelect option:selected").text() + '</div>');
    }
    })
    .done(function () {
    $("#btnCommitChanges").removeAttr('disabled');
    //alert("second success");
    })
    .fail(function () {
    $("#btnCommitChanges").removeAttr('disabled');
    $("#idAlertSuccess").html('Error while creating new push content! Please try again later or contact system Administrator')
    })
    .always(function () {
    $("#btnCommitChanges").removeAttr('disabled');
    //alert("finished");
    })
    .fail(function (jqXHR, textStatus) {
    if (jqXHR.status == 401) window.location = "{{ url('/login') }}";
    })
    ;
    });


    $("#btnDismiss").click(function() {
    $("#text-Content").val("");
    $("#idErrorMessage").html("");
    $("#idAlertSuccess").html("");
    if (doneEdit) {
    doneEdit = false;
    getListContent();
    }
    });

@stop
