@extends('content.manage_content-layout')

@section('main_content')
<div class="row">
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>CONTENT ID</th>
                                    <th>Content</th>
                                    <th>Category</th>
                                    <th>DATE SCHEDULED</th>
                                    <th>DATE EXPIRED</th>
                                    <th>KEYWORD</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="even gradeA">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $each_data)
                                        <tr class="even gradeA">
                                            <td>{{$each_data->id}}</td>
                                            <td>{{ $each_data->content }}</td>
                                            <td>{{ $category[$each_data->discount_category_id] }}</td>
                                            <td>{{ $each_data->start_at }}</td>
                                            @if(time() > strtotime($each_data->expired_at))
                                                <td style="color:red;">{{ $each_data->expired_at }}</td>
                                            @else
                                                <td>{{ $each_data->expired_at }}</td>
                                            @endif
                                            <td class="center">
                                                <a href="{{url('content/discount_content_umb/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                    Edit
                                                </a>
                                                <a href="{{url('content/discount_content_umb/del/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
