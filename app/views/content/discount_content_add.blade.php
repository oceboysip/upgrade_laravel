@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST" action="{{url('/content/discount_content/add')}}">
            <h4>Add New Diskon Content</h4><br/>
            <div class="row form-row">
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Text</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="text" cols="100" rows="5" onkeyup="countChar(this, 97)"></textarea>
                                <div id="charNum"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label"> URL to short:</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" type="text" class="" name="shorten" id="shorten">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Shorten URL:</label><br/>
                        <div class="shorten_url">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Category</label><br/>
                        <div class="controls">
                            <select id="idPushDayInterval" style="width:80%;" name="category_id">
                            @foreach ($categories as $category)
                                <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Scheduled</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   type="text" class="form-control theDate" name="dtscheduled">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date Expired</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control theDate" name="dtexpired">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control" name="sequence" value="10">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    //Date Pickers
    $('.date_discount').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });
    //Set today's default
    $(".date_discount").datepicker("setDate", new Date());

</script>
@stop
