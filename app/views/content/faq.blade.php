@extends('content.manage_content-layout')

@section('main_content')
<div class="row">
    <div class="grid simple">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Manage Questions from <span class="semi-bold">User</span></h3>
                    <p>
                        &nbsp;
                    </p>
            </div>
        </div>
    </div>
</div>

<div id="theContent">
    <div class="span12">
        <div class="grid simple ">
            <div class="grid-title">
                <h4>List of <span class="semi-bold">Questions</span></h4>
            </div>
            <div class="grid-body ">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>MSISDN</th>
                            <th>NAME</th>
                            <th>QUESTION</th>
                            <th>DATE</th>
                            <th colspan="2">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($results as $result)
                        @if(count($result->faq) > 0)
                        <tr>
                            <td>{{$result->id}}</td>
                            <td>{{$result->user->msisdn}}</td>
                            <td>{{$result->user->name}}</td>
                            <td>{{$result->faq[0]->faq}}</td>
                            <td>{{$result->faq[0]->created_at}}</td>
                            <td class="center">
                            @if($result->n_status == 0)
                                <a href="{{url('/subscriber/faq/reply/'.$result->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    Balas
                                </a>
                            @endif
                                <a href="{{url('/subscriber/faq/del/'.$result->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                    DELETE
                                </a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <ul class="pagination">
                @if($prev_page != '')
                    <li>
                        <a href="{{$prev_page}}">«</a>
                    </li>
                @endif
                @if($next_page != '')
                    <li>
                        <a href="{{$next_page}}">»</a>
                    </li>
                @endif
            </ul>
    </div>
</div>
@stop
