<div class="row">
    @if (isset($results))
        <div class="row">
            <div class="grid simple">
                <div class="col-md-12">
                    <div class="grid-body ">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Club</th>
                                <th>Score</th>
                                <th>Show in UMB</th>
                                <th>Expired</th>
                                <th>Processed</th>
                                <th>Match Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (sizeof($results) < 1)
                                <tr class="even gradeA">
                                    <td colspan=7> No data </td>
                                </tr>
                            @else
                                @foreach ($results as $skor)
                                    <tr class="even gradeA">
                                        <td>{{$skor->gameid}}</td>
                                        <td>{{ str_replace('|',' vs ',$skor->club) }}</td>
                                        <td>{{ str_replace('|',' - ',$skor->score) }}</td>
                                        
                                        @if($skor->displayed == 1)
                                            <td><span class="label label-success">Yes</span></td>
                                        @else
                                            <td><span class="label label-important">No</span></td>
                                        @endif

                                        @if(time() > strtotime($skor->match_date))
                                            <td style="color:red;">{{ $skor->match_date }}</td>
                                        @else
                                            <td>{{ $skor->match_date }}</td>
                                        @endif
                                        
                                        <td>
                                            {{ ($skor->processed > 0 ? '<span class="label label-success">Done</span>' : '') }}<br />
                                            {{ ($skor->expired > 0 ? '<span class="label label-success">Coin Sent</span>' : '') }}
                                        </td>
                                        
                                        <td class="center">
                                        @if($skor->processed == 0)
                                            <a href="{{url('content/tebakskor_edit/'.$skor->gameid)}}" class="btn btn-small btn-white btnEdit">
                                                Edit
                                            </a>
                                            <a href="{{url('content/tebakskor_process/'.$skor->gameid)}}" class="btn btn-small btn-warning btnEdit">
                                                Process
                                            </a>
                                        @else
                                            <a href="{{url('content/tebakskor_result/'.$skor->gameid)}}" class="btn btn-small btn-success btnEdit">
                                                Result
                                            </a>
                                        @endif

                                        @if(($skor->processed == 1) && ($skor->expired == 0))
                                            <a href="{{url('content/tebakskor_coin/'.$skor->gameid)}}" class="btn btn-small btn-info btnEdit">
                                                Send Coin
                                            </a>
                                        @endif

                                        @if($skor->displayed == 0)
                                            <a href="{{url('content/tebakskor_displayed/'.$skor->gameid)}}" class="btn btn-small btn-danger btnEdit">
                                                Enable
                                            </a>
                                        @else
                                            <a href="{{url('content/tebakskor_displayed/'.$skor->gameid)}}" class="btn btn-small btn-danger btnEdit">
                                                Disable
                                            </a>
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
