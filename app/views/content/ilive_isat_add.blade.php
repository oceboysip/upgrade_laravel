@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-12">
        <form id="myform" method="POST" action="{{url('/content/ilive_isat/add')}}">
            <div class="row form-row">
                <h2>Add New iLive ISAT Content</h2><br/>
                @if (\Session::has('err'))
                    <p>
                    <div class="alert alert-danger">
                      <p>{{ \Session::get('err') }}</p>
                    </div>
                @endif
            </div>
            <div class="row form-row">
                <div class="row form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Content Type</label><br/>
                            <div class="controls">
                                <div class="controls">
                                    <select name="content_type" id="content_type">
                                        @for ($i =1; $i <= 6; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>&nbsp;<font color="red">*</font>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="hidden_shio" style="display: none;">
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">ZODIAC</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <select name="zodiac" id="zodiac">
                                        @foreach($shio as $data)
                                            <option value="{{ $data->zodiac }}">{{ $data->zodiac }}</option>
                                        @endforeach
                                        </select>&nbsp;<font color="red">*</font>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="hidden_URL" style="display: none;">
                    <div class="panel panel-default" style="margin-bottom:0;">
                        <div class="panel-heading" style="margin-top:17px;">
                            <span class="btn btn-success fileinput-button" style="margin-top:-17px;">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select image files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files" multiple>
                            </span>
                            <div id="text-url"></div>
                        </div>
                        <div class="panel-body">
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">URL IMAGE</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                    <input style="width:60%;" type="text" class="" name="url_image" id="url_image" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row form-row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="form-label">TEXT</label><br/>
                            <div class="controls">
                                <div class="controls">
                                    <textarea name="text" cols="100" rows="3" 
                                    onkeyup="countChar2(this, 95, 'charNum2')"></textarea>&nbsp;<font color="red">*</font>
                                    <div id="charNum2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row form-row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="form-label">Sequence</label><br/>
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" type="text" class="form-control" name="sequence" value="10">&nbsp;<font color="red">*</font>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="form-label">Date NEXTPUSH</label><br/>
                            <div class="controls">
                                <input style="width:60%;"
                                       placeholder="" type="text" class="form-control date_expired" name="dtnext_push">&nbsp;<font color="red">*</font>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
@stop

@section("js_application_list")
    document.getElementById('content_type').addEventListener('change', function () {
        if (this.value == 2){
            document.getElementById('hidden_shio').style.display = 'block';
            document.getElementById('hidden_URL').style.display = 'none';
        } else if (this.value == 3){
            document.getElementById('hidden_shio').style.display = 'none';
            document.getElementById('hidden_URL').style.display = 'block';
        } else {
            document.getElementById('hidden_shio').style.display = 'none';
            document.getElementById('hidden_URL').style.display = 'none';
        }
    });


@stop
