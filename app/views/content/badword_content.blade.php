<div class="row">
    @if (isset($results))
        <div class="row">
            <div class="col-md-4">
                <form action="{{ url('/content/badword') }}" method="POST" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="qs" placeholder="Search keyword"> 
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="grid simple">
                <div class="col-md-12">
                    <div class="grid-body ">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Keyword</th>
                                <th>Exact</th>
                                <th>Regex</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (sizeof($results) < 1)
                                <tr class="even gradeA">
                                    <td colspan=7> No data </td>
                                </tr>
                            @else
                                @foreach ($results as $data)
                                    <tr class="even gradeA">
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->keyword }}</td>
                                        <td>{{ $data->exact }}</td>
                                        <td>{{ $data->regex_filter }}</td>
                                        <td class="center">

                                            <a href="{{url('content/badword_edit/'.$data->id)}}" class="btn btn-small btn-white btnEdit">
                                                Edit
                                            </a>

                                            <a href="{{url('content/badword_generate/'.$data->id)}}" class="btn btn-small btn-success btnEdit">
                                                Generate
                                            </a>

                                            <a href="{{url('content/badword_delete/'.$data->id)}}" class="btn btn-small btn-danger btnEdit">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    @endif
</div>
