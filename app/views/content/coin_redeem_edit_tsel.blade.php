@extends('content.manage_content-layout')

@section('main_content')
    <div class="span12">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <h4>Proses Redeem</h4><br/>
            <form id="myform" method="POST" action="{{url('/content/coin_redeem_telkomsel_edit')}}">
                <div class="row form-row">
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">MSISDN</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input style="width:60%;"
                                               value="{{$result->msisdn}}"
                                               type="text" readonly name="msisdn" id="shorten">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">NAME</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input style="width:60%;"
                                               value="{{preg_replace_callback('!\w+!', 'filter_badwords', $result->name)}}"
                                               type="text" readonly name="name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">REWARD</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input style="width:60%;"
                                               value="{{$result->redem_text}}"
                                               type="text" readonly name="redem_text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">CREATED AT</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input style="width:60%;"
                                               value="{{$result->created_at}}"
                                               type="text" readonly name="redem_text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="form-label">STATUS</label><br/>
                                <div class="controls">
                                    <div class="controls">
                                        <input style="width:60%;"
                                               value="{{$status}}"
                                               type="text" readonly name="redem_text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <input type="hidden" name="id" value="{{$result->id}}" >
                    <input type="hidden" name="n_status" value="{{$result->n_status}}" >
                    <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                    @if($result->n_status == 1)
                        <button type="submit" id="btnSave" class="btn btn-warning">Process</button>
                    @elseif($result->n_status == 2)
                        <button type="submit" id="btnSave" class="btn btn-warning">UnProcess</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop
