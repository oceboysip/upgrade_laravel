@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Manage Content for <span class="semi-bold">FANTASI BOLA</span></h3>
                        <br/>
                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control"
                           href="#" data-toggle="modal" data-target="#myModal">
                            Add Content
                        </a>

                        <div class="modal fade popupModal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            <form id="myform" method="POST" action="{{url('/content/fb')}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                            </button>
                                            <br>
                                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                                            <h4 id="myModalLabel" class="semi-bold">Add content for <span id="applicationName">Fantasi Bola</span></h4>
                                            <!-- <p class="no-margin">Please review the value again</p> -->
                                            <br>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Telco</label>
                                                    <select style="width:50%" name="telco_id" id="umbapp">
                                                        <option value="2">
                                                            CHOOSE TELCO
                                                        </option>
                                                        <option value="2">
                                                            INDOSAT
                                                        </option>
                                                        <option value="3">
                                                            XL
                                                        </option>
                                                    </select>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">User Type</label>
                                                    <select style="width:50%" name="user_type" id="umbapp">
                                                        <option value="0">
                                                            CHOOSE USER TYPE
                                                        </option>
                                                        <option value="0">
                                                            NON MEMBER FB
                                                        </option>
                                                        <option value="1">
                                                            MEMBER FB
                                                        </option>
                                                    </select>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Content Type</label>
                                                    <select style="width:50%" name="content_type" id="umbapp">
                                                        <option value="0">
                                                            CHOOSE CONTENT TYPE
                                                        </option>
                                                        <option value="0">
                                                            PAID
                                                        </option>
                                                        <option value="1">
                                                            FREE
                                                        </option>
                                                    </select>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default" style="margin-bottom:0;">
                                                        <div class="panel-heading" style="margin-top:17px;">
                                                    <span class="btn btn-success fileinput-button" style="margin-top:-17px;">
                                                        <i class="glyphicon glyphicon-plus"></i>
                                                        <span>Select files...</span>
                                                        <!-- The file input field used as target for the file upload widget -->
                                                        <input id="fileupload" type="file" name="files" multiple>
                                                    </span>
                                                            <div id="text-url">

                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="progress" class="progress">
                                                                <div class="progress-bar progress-bar-success"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Text</label>
                                                    <textarea onkeyup="countChar(this, 110)" name="text" cols="70" rows="5"></textarea>
                                                    <div id="charNum"></div>
                                                </div>
                                            </div><br />
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">DATE NEXT PUSH (<i>yyyy-mm-dd</i>)</label>
                                                    <input style="width:30%;" class="form-control theDate" name="dtnext_push" type="text">
                                                </div>
                                            </div>
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Sequence</label>
                                                    <input style="width:30%;" class="form-control" name="sequence" type="text" value="10">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" id="btnCommitChanges" class="btn btn-primary">Save</button>
                                        </div>
                                    <!-- /.modal-content -->
                                </div>
                            </div>
                            </form>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="theContent">
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            @if (isset($results))
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>TELCO</th>
                                        <th>USER TYPE</th>
                                        <th>CONTENT TYPE</th>
                                        <th>CONTENT</th>
                                        <th>SEQUENCE</th>
                                        <th>DATE NEXT PUSH</th>
                                        <th>DISABLED</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results as $each_data)
                                        <tr class="even gradeA">
                                            <td>{{$each_data->id}}</td>
                                            <td>{{$each_data->telco_id}}</td>
                                            <td>{{$each_data->user_type}}</td>
                                            <td>{{$each_data->content_type}}</td>
                                            <td>{{$each_data->text}}</td>
                                            <td>{{$each_data->sequence}}</td>
                                            <td>{{$each_data->dtnext_push}}</td>
                                            @if($each_data->n_status == 1)
                                                <td><span class="label label-success">No</span></td>
                                            @else
                                                <td><span class="label label-important">Yes</span></td>
                                            @endif
                                            <td>
                                                <a class="btn btn-small btn-white btn-cons btnEdit" href="{{url('/content/fb/'.$each_data->id)}}" >
                                                    Edit
                                                </a>
                                                @if($each_data->n_status == 1)
                                                    <a href="{{url('/content/fb/del/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                        Disable
                                                    </a>
                                                @else
                                                    <a href="{{url('content/fb/enable/'.$each_data->id)}}" class="btn btn-small btn-white btn-cons btnEdit">
                                                        Enable
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                                No Result
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <ul class="pagination">
                @if($prev_page != '')
                    <li>
                        <a href="{{$prev_page}}">«</a>
                    </li>
                @endif
                @if($next_page != '')
                    <li>
                        <a href="{{$next_page}}">»</a>
                    </li>
                @endif
            </ul>
    </div>
@stop
