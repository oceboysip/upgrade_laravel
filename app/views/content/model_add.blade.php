@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-12">
        <form id="myform" method="POST" action="{{url('/content/model/add')}}">
            <h4>Add New Model ISAT Content</h4><br/>
            <div class="row form-row">
                <div class="row form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Content Type</label><br/>
                            <div class="controls">
                                <div class="controls">
                                    <select name="content_type">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-label">TEXT</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <textarea name="text" cols="100" rows="10" onkeyup="countChar2(this, 95, 'charNum2')"></textarea>
                                <div id="charNum2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Sequence</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control" name="sequence" value="10">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Date NEXTPUSH</label><br/>
                        <div class="controls">
                            <input style="width:60%;"
                                   placeholder="" type="text" class="form-control date_expired" name="dtnext_push">
                        </div>
                    </div>
                </div>
            </div>
                </div>
            <br/>
            <br/>
                <a href="javascript:history.back();" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>
            </div>
        </form>
    </div>
</div>
@stop
