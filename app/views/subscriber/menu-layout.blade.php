<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>@yield("header.title","KMI Webtools")</title>
    @include("main_css-layout")
</head>

<!-- BEGIN BODY -->
<body class="">

    <!-- BEGIN HEADER -->
    @include("main_top_navigation-layout")
    <!-- END HEADER -->


    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">

        @include("sidebar-layout")

        <!-- BEGIN PAGE CONTAINER-->

        <div class="page-content">
            <div class="content">
                <div class="page-title">
                </div>
                <div class="row">
                    <div class="grid simple">
                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-10">
                                    <form id="myform" method="POST">
                                        <input type="hidden" name="ajax" value="true">
                                        <h3>Manage <span class="semi-bold">Subscriber</span></h3>
                                        <p>Check if subscriber is registered to service(s)</p>
                                        <div class="row form-row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label class="form-label">Mobile Number (format : 628xxxx)</label>
                                                        <input id="idMsisdn" style="width:60%;" type="text" class="form-control" name="msisdn">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a data-color="rgb(255, 255, 255)" data-color-format="hex"
                                                   id="findSubscriber"
                                                   class="btn btn-primary my-colorpicker-control" href="#"
                                                   style="margin-right: 30px;"
                                                   data-colorpicker-guid="8">Find</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @yield("content")

            </div>
        </div>

        <!-- END MAIN PAGE -->

    </div>

    <!-- END CONTAINER -->

    @include("main_js-layout")

    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}"
            type="text/javascript"></script>

    <!--
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/d3.v2.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js') }}"></script>
    -->

    <script src="{{ asset('assets/plugins/jquery-sparkline/jquery-sparkline.js') }}"></script>
    <script src="{{ asset('assets/plugins/skycons/skycons.js') }}"></script>
    <script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-polymaps/polymaps.min.js') }}" type="text/javascript"></script>


    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/chat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/dashboard_v2.js') }}" type="text/javascript"></script> -->

    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
        });

        $("#myform").validate({
            rules: { msisdn: {required: true} }
        });

        $("#findSubscriber").click(function (e) {
            e.preventDefault();
            $("#theData").html('<div class="span12">Querying data...<div class="col-md-8 col-sm-8 col-xs-8">' +
                '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

            $.post("{{ url('/api/get_list_subscriber') }}",$( "#myform" ).serialize(),
                function (data) {
                    $("#theData").html('');
                    $("#theData").html(data);
                }).fail(function (jqXHR, textStatus) {
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html(
                            'Error while getting list of service! Please try again later or contact system Administrator')
                    }
                })
            ;
        });


        $("#idMsisdn").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
            	event.preventDefault();
                //alert('You pressed a "enter" key in textbox');
                $("#theData").html('<div class="span12">Querying data...<div class="col-md-8 col-sm-8 col-xs-8">' +
                    '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

                $.post("{{ url('/api/get_list_subscriber') }}", $("#myform").serialize(),
                    function (data) {
                        $("#theData").html('');
                        $("#theData").html(data);
                    }).fail(function (jqXHR, textStatus) {
                        if (jqXHR.status == 401) {
                            window.location = "{{ url('/login') }}";
                        }
                        else {
                            $("#idAlertSuccess").html(
                                'Error while getting list of service! Please try again later or contact system Administrator')
                        }
                    })
                ;
            }
        });
    </script>

</body>
</html>
