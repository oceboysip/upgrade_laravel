@extends('subscriber.menu-layout')

@section('content')
<div id="waitCommit" class="row form-row"></div>
<div id="idErrorMessage" class="row form-row"></div>
<div class="row" id="theData"></div>
@stop
