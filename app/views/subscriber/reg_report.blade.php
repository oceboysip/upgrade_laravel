<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@yield("header.title","KMI Webtools")</title>

    <!-- BEGIN CORE CSS FRAMEWORK -->

    <link href="{{ asset('assets/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/jquery-datatable/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrap-checkbox/css/bootstrap-checkbox.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/magic_space.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/tiles_responsive.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->

</head>

<!-- BEGIN BODY -->
<body class="">

<!-- BEGIN HEADER -->
@include("main_top_navigation-layout")
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">

    @include("sidebar-layout")

    <!-- BEGIN PAGE CONTAINER-->


    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here</div>
        </div>

        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title">
            </div>

            @include("subscriber.reg_report_main-layout")

        </div>
    </div>

    <!-- END MAIN PAGE -->

</div>

<!-- END CONTAINER -->

@include("main_js-layout")

<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN CORE TEMPLATE JS -->

<script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('assets/plugins/jquery-datatable/extra/js/TableTools.min.js') }}" type="text/javascript" ></script>

<script type="text/javascript" src="{{ asset('assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/datatables-responsive/js/lodash.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$(".live-tile,.flip-list").liveTile();
	});
	//Date Pickers
	$('.theDate').datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		todayHighlight: true
	});
	//Set today's default
	$(".theDate").datepicker("setDate", new Date());

	var nCloneTh = document.createElement( 'th' );
	var nCloneTd = document.createElement( 'td' );
	nCloneTd.innerHTML = '<i class="fa fa-plus-circle"></i>';
	nCloneTd.className = "center";

	$('#example2 thead tr').each( function () {
		this.insertBefore( nCloneTh, this.childNodes[0] );
	} );

	$('#example2 tbody tr').each( function () {
		this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
	} );

	/*
	 * Initialse DataTables, with no sorting on the 'details' column
	 */
	var oTable = $('#example2').dataTable( {
		"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
		"aaSorting": [],
		"oLanguage": {
			"sLengthMenu": "_MENU_ ",
			"sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
		}
	});

	$('#example2_wrapper .dataTables_filter input').addClass("input-medium ");
	$('#example2_wrapper .dataTables_length select').addClass("select2-wrapper span12");

	$(".select2-wrapper").select2({minimumResultsForSearch: -1});

	/* Add event listener for opening and closing details
	 * Note that the indicator for showing which row is open is not controlled by DataTables,
	 * rather it is done here
	$('#example2 tbody td i').live('click', function () {
		var nTr = $(this).parents('tr')[0];
		if ( oTable.fnIsOpen(nTr) )
		{
			this.removeClass = "fa fa-plus-circle";
			this.addClass = "fa fa-minus-circle";
			oTable.fnClose( nTr );
		}
		else
		{
			this.removeClass = "fa fa-minus-circle";
			this.addClass = "fa fa-plus-circle";
			oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
		}
	});
	*/


	/* Formating function for row details
	function fnFormatDetails ( oTable, nTr )
	{
		var aData = oTable.fnGetData( nTr );
		var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="inner-table">';
		sOut += '<tr><td>Rendering engine:</td><td>'+aData[1]+' '+aData[4]+'</td></tr>';
		sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
		sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
		sOut += '</table>';
		return sOut;
	}
	 */

	$("#sdcSelect")
		.select2({placeholder: "Choose SDC"})
		.on("change", function () {
			getApplicationList($("#telcoSelect").val(), $("#sdcSelect").val());
		});

	$("#telcoSelect")
		.select2({placeholder: "Choose Telco"})
		.on("change", function() {getApplicationList($("#telcoSelect").val(), $("#sdcSelect").val()); });

	$("#applicationSelect").select2({placeholder: "Choose Telco and SDC First"});


	$("#sdcSelect").change(function() {getApplicationList($("#telcoSelect").val(), $("#sdcSelect").val());});
	$("#telcoSelect").change(function() {getApplicationList($("#telcoSelect").val(), $("#sdcSelect").val());});

	function getApplicationList(choosenTelcoId, choosenSdc) {
		$.getJSON("{{ url('api/get_list_push_applcation/') }}" + "/" + choosenTelcoId + "/" + choosenSdc,
			{ajax: 'true'}, function (data) {
				$("#applicationSelect").html('');
				$("#applicationSelect").append('<optgroup label="Choose Business Application">');
				var firstData = null;
				$.each(data, function () {
					if (!firstData) {
						firstData = this.id;
					}
					$("#applicationSelect").append('<option value="' + this.id + '">' + this.name + '</option>')
				})
				$("#applicationSelect").append('</optgroup>');
				$("#applicationSelect").select2('val', firstData);
			});
	}

	getApplicationList($("#telcoSelect").val(), $("#sdcSelect").val());


	$("#viewRegReport").click(function () {
		$("#theRegReportData").html('<div class="span12">Querying...<div class="col-md-8 col-sm-8 col-xs-8">' +
			'<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

		var pInfo = $("#pagingInfo").attr("page");
		$.get("{{ url('/api/get_reg_report') }}" + "/" + $("#applicationSelect").val()
			+ "?dtStart=" + $("#dateStart").val() + "&dtEnd=" + $("#toDate").val(),
			{ajax: 'true', page: pInfo},function (data) {
				$("#theRegReportData").html('');
				$("#theRegReportData").html(data);
			}).fail(function (jqXHR, textStatus) {
				if (jqXHR.status == 401) {
					window.location = "{{ url('/login') }}";
				}
				else {
					$("#idAlertSuccess").html('Error while getting reg report! Please try again later or contact system Administrator')
				}
			});
	});

</script>


</body>
</html>
