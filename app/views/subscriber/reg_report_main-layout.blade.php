<?php
/**
 * This is the main for Main Revenue Report page
 *
 */
?>
<div class="row">
    <div class="grid simple">
		<form id="myTrafficForm" method="POST">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Registration Report for <span class="semi-bold">Push Services</span></h3>
					<p>
						<select id="sdcSelect" style="width:20%;" name="sdc">
							<optgroup label="Choose SDC">
								@foreach ($sdc as $each_sdc)
								<option value="{{ $each_sdc->name }}">{{ $each_sdc->name }}</option>
								@endforeach
							</optgroup>
						</select>
						<select id="telcoSelect" style="width:20%;" name="telcoId">
							<optgroup label="Choose Telco">
								@foreach ($telco as $each_telco)
								<option value="{{ $each_telco->id }}">{{ $each_telco->name }}</option>
								@endforeach
							</optgroup>
						</select>
					</p>
					<p>
						<select id="applicationSelect" style="width:50%" name="applicationId">
							<optgroup label="Choose Telco and SDC First">
								@if(isset($service))
								@foreach ($service as $each_service)
								<option value="{{ $service->id }}">{{ $service->name }}</option>
								@endforeach
								@endif
							</optgroup>
						</select>
					</p>

                    Date Range:<br/>
                    <input type="text" name="dtStart" id="dateStart" class="theDate">
                    <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
                    &nbsp; - &nbsp;
                    <input type="text" name="dtEnd" id="toDate" class="theDate">
                    <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>

                    <br/>
                    <br/>
                    <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewRegReport"
                       class="btn btn-primary my-colorpicker-control" href="#"
                       data-colorpicker-guid="8">View</a>

					<div id="waitCommit" class="row form-row"></div>
					<div id="idErrorMessage" class="row form-row"></div>
					<div id="idAlertSuccess" class="row form-row"></div>

				</div>
            </div>
        </div>
		</form>
		<div id="waitQuery" class="row form-row"></div>
	</div>
</div>

<div class="row" id="theRegReportData">
    @if (isset($subscriber_data))
    <div class="span12">
        <div class="grid simple ">
            <div class="grid-title">
                <h4>Total Active Subscribers</span></h4>
            </div>
            <div class="grid-body ">
                <table class="table table-hover table-bordered" id="example22">
                    <thead>
                    <tr>
                        <th>Service</th>
                        <th>Total Active Subscribers</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $tR = 0;$tUR = 0;  ?>
                    @if (sizeof($subscriber_data) < 1)
                    <tr class="even gradeA">
                        <td colspan=10> No data</td>
                    </tr>
                    @else
                    <?php $rowOpen = false; ?>
                    @foreach ($subscriber_data as $each_data)
                    <tr class="even gradeA hideme hm-{{ $each_data->name }}">
                        <td><a class="closeBelow">{{ $each_data->name }}</a></td>
                        <td>{{ number_format($each_data->total) }}</td>
                    </tr>
                    <?php
                    $tR += $each_data->total;
                    ?>
                    @endforeach

                    <tr class="even gradeA">
                        <td><b>TOTAL </b></td>
                        <td>{{ number_format($tR) }}</td>
                    </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

</div>
