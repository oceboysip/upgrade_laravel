@extends('subscriber.promo_channel')
@section('main_content')
<div class="row">
    <div class="grid simple">
		<form id="myTrafficForm" method="POST">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Registration Report per <span class="semi-bold">Channel Promo</span></h3>
					<p>
						<select id="sdcSelect" style="width:20%;" name="sdc">
							<optgroup label="Choose SDC">
								@foreach ($sdc as $each_sdc)
								<option value="{{ $each_sdc->name }}">{{ $each_sdc->name }}</option>
								@endforeach
							</optgroup>
						</select>
						<select id="telcoSelect" style="width:20%;" name="telcoId">
							<optgroup label="Choose Telco">
								@foreach ($telco as $each_telco)
								<option value="{{ $each_telco->id }}">{{ $each_telco->name }}</option>
								@endforeach
							</optgroup>
						</select>
					</p>
					<p>
						<select id="applicationSelect" style="width:50%" name="applicationId">
							<optgroup label="Choose Telco and SDC First">
								@if(isset($service))
								@foreach ($service as $each_service)
								<option value="{{ $service->id }}">{{ $service->name }}</option>
								@endforeach
								@endif
							</optgroup>
						</select>
					</p>

                    Date Range:<br/>
                    <input type="text" name="dtStart" id="dateStart" class="theDate">
                    <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
                    &nbsp; - &nbsp;
                    <input type="text" name="dtEnd" id="toDate" class="theDate">
                    <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
                    <br/>
                    <br/>
                    Keyword:<br/>
                    <p id="keyword">
                    </p>

                    <br/>
                    <br/>
                    <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewRegReport"
                       class="btn btn-primary my-colorpicker-control" href="#"
                       data-colorpicker-guid="8">View</a>

					<div id="waitCommit" class="row form-row"></div>
					<div id="idErrorMessage" class="row form-row"></div>
					<div id="idAlertSuccess" class="row form-row"></div>

				</div>
            </div>
        </div>
		</form>
		<div id="waitQuery" class="row form-row"></div>
	</div>
</div>

<div class="row" id="theRegReportData">

</div>

@stop

@section('javascript')
<script>
    $("#viewRegReport").click(function () {
        $("#theRegReportData").html('<div class="span12">Querying...<div class="col-md-8 col-sm-8 col-xs-8">' +
        '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');

        var pInfo = $("#pagingInfo").attr("page");
        $.get("{{ url('/api/get_reg_promo_channel_report') }}" + "/" + $("#applicationSelect").val()
                + "?dtStart=" + $("#dateStart").val() + "&dtEnd=" + $("#toDate").val()+"&keyword=" + $("#keyword_promo").val(),
                {ajax: 'true', page: pInfo},function (data) {
                    $("#theRegReportData").html('');
                    $("#theRegReportData").html(data);
                }).fail(function (jqXHR, textStatus) {
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html('Error while getting reg report! Please try again later or contact system Administrator')
                    }
                });
    });

    $("#applicationSelect").change(function(){
        var appid = $("#applicationSelect").val();
        $.get("{{ url('/api/get_keyword_promo') }}" + "/" +appid,
                {ajax: 'true'},function (data) {
                    $("#keyword").html(data);
                }).fail(function (jqXHR, textStatus) {
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html('Error while getting reg report! Please try again later or contact system Administrator')
                    }
                });
    });

    function get_keyword(appid)
    {
        $.get("{{ url('/api/get_keyword_promo') }}" + "/" +appid,
                {ajax: 'true'},function (data) {
                    $("#keyword").html(data);
                }).fail(function (jqXHR, textStatus) {
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html('Error while getting reg report! Please try again later or contact system Administrator')
                    }
                });
    }

    get_keyword(2000);

</script>
@stop