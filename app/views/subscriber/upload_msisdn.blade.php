@extends('subscriber.promo_channel')
@section('main_content')
    <div class="row">
        <div class="grid simple">
            <form id="myTrafficForm" method="POST">
                <div class="grid-body no-border">
                    <div class="row">
                        <div class="col-md-10">
                            <h3>Upload MSISDN <span class="semi-bold">Complaint</span></h3>
                            <div class="panel panel-default">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Select files...</span>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input id="fileupload" name="files" multiple="" type="file">
                                        </span>
                                <div id="text-url">

                                </div>
                                <p>Using 628xxxx Format</p>
                                <div class="panel-body">
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div id="waitQuery" class="row form-row"></div>
        </div>
    </div>

@stop
