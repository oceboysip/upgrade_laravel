@if (isset($the_data))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Registration Report of <span class="semi-bold">{{ $application[0]->name }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<table class="table table-hover table-bordered" id="example2">
				<thead>
				<tr>
					<th>Date</th>
					<th>Total Registration</th>
					<th>Total Unregistration</th>
					<th>Total Unregistration on Same Day</th>
				</tr>
				</thead>
				<tbody>
				<?php $tR = 0;$tUR = 0;$tURoD = 0  ?>
				@if (sizeof($the_data) < 1)
				<tr class="even gradeA">
					<td colspan=10> No data</td>
				</tr>
				@else
				<?php $rowOpen = false; ?>
					@foreach ($the_data as $each_data)
				<tr class="even gradeA hideme hm-{{ $each_data->dt }}">
					<td><a class="closeBelow" date="{{ $each_data->dt }}">{{ $each_data->dt }}</a></td>
					<td>{{ number_format($each_data->total) }}</td>
					<td>{{ number_format($each_data->total_unreg) }}</td>
					<td>{{ number_format($each_data->total_unreg_on_day) }}</td>
				</tr>
				<?php
				$tR += $each_data->total;
				$tUR += $each_data->total_unreg;
				$tURoD += $each_data->total_unreg_on_day;
				?>
					@endforeach

					<tr class="even gradeA">
						<td><b>TOTAL </b></td>
						<td>{{ number_format($tR) }}</td>
						<td>{{ number_format($tUR) }}</td>
						<td>{{ number_format($tURoD) }}</td>
					</tr>
				@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

@endif
