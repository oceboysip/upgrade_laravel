@if(count($keyword) > 0)
    <select id="keyword_promo" style="width:50%" name="keyword">
        @if(isset($keyword))
            @foreach ($keyword as $each_service)
                <option value="{{ $each_service->keyword }}">{{ $each_service->name }}</option>
            @endforeach
        @endif
    </select>
@endif