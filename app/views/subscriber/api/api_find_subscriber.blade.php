@if (isset($reg_services))
<div class="span12">
    <div class="grid simple">
        <div class="grid-title">
            <h4>List of Service(s) registered to <b>{{ $msisdn }}</b></h4>
            <!--
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
            -->
        </div>

        <div class="grid-body">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a id="reg-history" href="javascript:;">REG HISTORY</a></li>
                <li role="presentation"><a id="unreg-history" href="javascript:;">UNREG HISTORY</a></li>
            </ul>
            <table id="reg-table" cellpadding="0" cellspacing="0" border="0" class="table table-hover table-bordered table-responsive datatable">
            <thead>
            <tr>
                <th data-class="expand">Service</th>
                <th data-class="expand">Date-Time Registered</th>
                <th>Channel</th>
                <th>SMS</th>
                <!-- <th>Active</th> -->
                <th>Time Last Push</th>
                <th>Time Data Modified</th>
                <th>Sent MT</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($reg_services) < 1)
                <tr class="even gradeA">
                    <td colspan=14> No data</td>
                </tr> 
            @else    
                @foreach ($reg_services as $each_data)
                    <tr class="even gradeA">
                        <td title="Subscriber Id: {{ $each_data->id }}">{{ $each_data->application_name }}</td>
                        <td class="center">{{ $each_data->dtregister }}</td>
                        <td class="center">{{ $each_data->channel }}</td>
                        <td class="center">{{ $each_data->reg_sms }}</td>

                        @if (is_null($each_data->dtlast_push) || empty($each_data->dtlast_push))
                        <td class="center">-- never --</td>
                        @else
                        <td class="center">{{ $each_data->dtlast_push }}</td>
                        @endif

                        <td class="center">{{ $each_data->dtmodified }}</td>
                        <td class="center">
                            <button serviceId="{{ $each_data->service_id }}" msisdn="{{ $msisdn }}"
                                    applicationName="{{ $each_data->application_name }}"
                                    applicationId="{{ $each_data->application_id }}"
                                    class="btn btn-small btn-white btn-cons btnSeeMT"
                                    type="button" data-toggle="modal" data-target="#myModalMT"><i class="fa fa-search"></i>
                            </button>
                        </td>
                        <td class="center">
                            <button subscriberId="{{ $each_data->id }}" msisdn="{{ $msisdn }}" applicationName="{{ $each_data->application_name }}"
                                    applicationId="{{ $each_data->application_id }}"
                                    class="btn btn-small btn-white btn-cons btnUnreg"
                                    type="button" data-toggle="modal" data-target="#myModalUnreg">Unregister</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            </table>

            <table id="unreg-table" cellpadding="0" cellspacing="0" border="0" class="hide table table-hover table-bordered table-responsive datatable">
                <thead>
                <tr>
                    <th data-class="expand">Service</th>
                    <th data-class="expand">Date-Time Registered</th>
                    <th data-class="expand">Date-Time Deleted</th>
                    <th>Channel</th>
                    <th>SMS</th>
                    <!-- <th>Active</th> -->
                    <th>Time Last Push</th>
                    <th>Time Data Modified</th>
                </tr>
                </thead>
                <tbody>
                @if (sizeof($unreg_services) < 1)
                    <tr class="even gradeA">
                        <td colspan=14> No data</td>
                    </tr>
                @else
                    @foreach ($unreg_services as $each_data)
                        <tr class="even gradeA">
                            <td title="Subscriber Id: {{ $each_data->id }}">{{ $each_data->application_name }}</td>
                            <td class="center">{{ $each_data->dtregister }}</td>
                            <td class="center">{{ $each_data->dtdeleted }}</td>
                            <td class="center">{{ $each_data->channel }}</td>
                            <td class="center">{{ $each_data->reg_sms }}</td>

                            @if (is_null($each_data->dtlast_push) || empty($each_data->dtlast_push))
                                <td class="center">-- never --</td>
                            @else
                                <td class="center">{{ $each_data->dtlast_push }}</td>
                            @endif

                            <td class="center">{{ $each_data->dtmodified }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalUnreg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width: 830px;" >
        <div class="modal-content">
            <div id="theModalUnregContent"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalMT" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width: 830px;" >
        <div class="modal-content">
            <div id="theModalMTContent"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#unreg-history').click(function(){
        $(this).parent().addClass('active');
        $('#reg-history').parent().removeClass('active');
        $('#reg-table').addClass('hide');
        $('#unreg-table').removeClass('hide');
    });
    $('#reg-history').click(function(){
        $(this).parent().addClass('active');
        $('#unreg-history').parent().removeClass('active');
        $('#unreg-table').addClass('hide');
        $('#reg-table').removeClass('hide');
    });


    $(".btnUnreg").click(function() {
        $("#theModalUnregContent").html('');
        var str = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><div class="alert alert-block alert-error fade in">' +
                    '<div id="paletteWholeSet"><h5 class="alert-heading"><i class="icon-warning-sign"></i> Confirm!</h5>' +
                    '<p>Are you sure to unregister from ' + this.getAttribute('applicationName') + '? </p>' +
                    '<div id="thisButtonSet" class="button-set">' +
                        '<button onclick="doUnreg(this)" id="theExecUnregButton" subscriberId="' + this.getAttribute('subscriberId') + '" type="button" class="btn btn-danger btn-cons">Yes</button>' +
                        '<button type="button" data-dismiss="modal"class="btn btn-white btn-cons">No</button> ' +
                    '</div></div>' +
                    '<div id="waitUnreg" class="row form-row"></div>' +
                    '<div id="idUnregErrorMessage" class="row form-row"></div>' +
                    '<div id="idUnregAlertSuccess" class="row form-row"></div>' +
            '</div>';
        $("#theModalUnregContent").html(str);
    });


    $("#theExecUnregButton").click(function() {
        alert('WOW');
    });


    function doRefresh() {
        $.post("{{ url('/api/get_list_subscriber') }}", $("#myform").serialize(),
            function (data) {
                $("#theData").html('');
                $("#theData").html(data);
            }).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while getting list of service! Please try again later or contact system Administrator')
                }
            })
        ;
        return true;
    }


    function doUnreg(theButton) {
        $("#waitUnreg").html('<div class="col-md-8 col-sm-8 col-xs-8">Unregister from database... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#theExecUnregButton").attr("disabled", "disabled");
        $.post("{{ url('/api/do_unregister_subscriber') }}", {ajax: 'true', subscriberId: theButton.getAttribute("subscriberId")},
            function (data) {
                $("#waitUnreg").html('');
                if (data['Error']) {
                    $("#idUnregErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while unregister from database: ' + data['Message'] + '</div>');
                } else {
                    $("#paletteWholeSet").html('');
                    //$("#thisButtonSet").html('');
                    $("#idUnregAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success:&nbsp;unregister subscriber</div>' +
                            '<button type="button" onclick="doRefresh()" data-dismiss="modal"class="btn btn-white btn-cons">Dismiss</button>'
                    );
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#waitUnreg").html('');
                    $("#idUnregAlertSuccess").html(
                        '<br/>Error while unregister subscriber! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    }

    $('.seeMT .clickable').on('click', function () {
        var el = jQuery(this).parents(".seeMT").children(".grid-body");
        //console.log(el);
        if (!el.is(":visible") ) {
            var theServiceId = this.getAttribute('serviceId');
            var subscriberId = this.getAttribute('subscriberId');
            $("#ctx-" + subscriberId).html('<div class="span12"><div class="col-md-8 col-sm-8 col-xs-8">' +
                'Loading...<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');
            $.post("{{ url('/api/get_traffic_for_subscriber') }}",
                {ajax: 'true', msisdn: {{ $msisdn }}, serviceId: theServiceId },
                function (data) {
                    $("#ctx-" + subscriberId).html('');
                    $("#ctx-" + subscriberId).html(data);
                }
            ).fail(function (jqXHR, textStatus) {
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html(
                            '<br/>Error while getting traffic MT! Please try again later or contact system Administrator')
                    }
                })
            ;
        }
        el.slideToggle(200);
    });


    $(".btnSeeMT").click(function () {
        var theServiceId = this.getAttribute('serviceId');
        $.post("{{ url('/api/get_traffic_for_subscriber') }}",
            {ajax: 'true', msisdn: {{ $msisdn }}, serviceId: theServiceId },
            function (data) {
                $("#theModalMTContent").html('');
                $("#theModalMTContent").html(data);
            }
        ).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while get service keywords! Please try again later or contact system Administrator')
                }
            })
        ;
    });

</script>
@endif
