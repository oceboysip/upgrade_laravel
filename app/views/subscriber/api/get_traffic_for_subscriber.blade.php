<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>List of <span class="semi-bold"></span></h4>
            <div class="tools">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
        </div>
        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2" width="100%">
                <thead>
                <tr>
                    <th>DATE</th>
                    <th>TEXT</th>
                    <th>PRICE</th>
                    <th>TELCO TRANS ID</th>
                    <th>TELCO SID</th>
                    <th>TELCO STATUS</th>
                </tr>
                </thead>
                <tbody>
                @if (sizeof($rs_data) < 1)
                    <tr class="even gradeA">
                        <td colspan=5> No data </td>
                    </tr>
                @else
                    @foreach ($rs_data as $each_data)
                        <tr class="even gradeA">
                            @foreach ($each_data as $value)
                                <td>{{$value}}</td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>