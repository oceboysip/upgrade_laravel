<?php

/**
 * ---------------------------------------------------
 *
 *     This layout is configurable for company logo
 *
 * ---------------------------------------------------
 *
 */

?><a href="#">
    <span style="padding-left: 2%" />
    <img src="{{ asset('assets/img/logo-login.png') }}" class="logo" alt="Home"
         data-src="{{ asset('assets/img/logo-login.png') }}"
         data-src-retina="{{ asset('assets/img/logo-login.png') }}"
         width="195"/>
    <br/>
</a>
