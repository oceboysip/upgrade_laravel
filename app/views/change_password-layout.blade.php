<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>@yield("header.title","Phynx Message ~ Appliance Integrated Platform")</title>
    @include("main_css-layout")
</head>

<!-- BEGIN BODY -->
<body class="">

    <!-- BEGIN HEADER -->
    @include("main_top_navigation-layout")
    <!-- END HEADER -->


    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">

        @include("sidebar-layout")

        <!-- BEGIN PAGE CONTAINER-->

        <div class="page-content">
            <div class="content">
                <div class="page-title">
                </div>
                <div class="row">
                    <div class="grid simple">
                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3>Change Your <span class="semi-bold">Password</span></h3>
                                    @yield("content")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- END MAIN PAGE -->

    </div>

    <!-- END CONTAINER -->

    @include("main_js-layout")

    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}"
            type="text/javascript"></script>

    <!--
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/d3.v2.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js') }}"></script>
    -->

    <script src="{{ asset('assets/plugins/jquery-sparkline/jquery-sparkline.js') }}"></script>
    <script src="{{ asset('assets/plugins/skycons/skycons.js') }}"></script>
    <script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-polymaps/polymaps.min.js') }}" type="text/javascript"></script>


    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/chat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/dashboard_v2.js') }}" type="text/javascript"></script> -->

    <script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>


    <script type="text/javascript">
        var doneEdit = false;
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
        });

        $("#myform").validate({
            rules: {
                oldPassword: {required: true},
                newPassword: {required: true},
                confirmPassword: {required: true}
            }
        });

        $("#idUpdatePassword").click(function () {
            $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
                '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

            $("#idUpdatePassword").attr("disabled", "disabled");
            $.post("{{ url('/api/change_password') }}", $("#myform").serialize(),
                function (data) {
                    $("#idUpdatePassword").removeAttr('disabled');
                    $("#waitCommit").html('');
                    if (data['Error']) {
                        $("#idErrorMessage").html(
                            '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                                'Error while saving to database: ' + data['Message'] + '</div>');
                    } else {
                        doneEdit = true;
                        $("#idAlertSuccess").html(
                            '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                                'Success&nbsp;change password </div>');
                    }
                })
                .done(function () {
                    $("#idUpdatePassword").removeAttr('disabled');
                    //alert("second success");
                })
                .fail(function (jqXHR, textStatus) {
                    $("#idUpdatePassword").removeAttr('disabled');
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html(
                            'Error while changing password! Please try again later or contact system Administrator')
                    }
                })
                .always(function () {
                    //alert("finished");
					$("#waitCommit").html('');
                });
        });
    </script>

</body>
</html>
