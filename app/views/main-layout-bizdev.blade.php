<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@yield("header.title","KMI Webtools")</title>
    @include("main_css-layout")

    <link rel="stylesheet" href="{{ url('assets/css/ourchart-rickshaw.css') }}" type="text/css" media="screen">

</head>

<!-- BEGIN BODY -->
<body class="">

    <!-- BEGIN HEADER -->
    @include("main_top_navigation-layout")
    <!-- END HEADER -->


    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">

        @include("sidebar-layout")

        <!-- BEGIN PAGE CONTAINER-->

        @include("home.main-layout-bizdev")

        <!-- END MAIN PAGE -->

    </div>

    <!-- END CONTAINER -->

    @include("main_js-layout")


    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/d3.v2.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js') }}"></script>
    <script>

        // Indosat KMI
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var graph = new Rickshaw.Graph({
            element: document.querySelector("#theChartISAT"),
            width: 400,
            height: 200,
            renderer: 'area',
            stroke: true,
            series: [
                {
                    color: 'steelblue',
                    name: 'Revenue (IDR)',
                    data: {{ $dataLast7DaysISAT }}
                }
            ]
        });

        graph.render();


        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            /*
            xFormatter: function (x) {
                var _o = new Date(x*1000);
                return _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
            },
            yFormatter: function (y) {return Math.floor(y).formatMoney(0);}
            */
            formatter: function (series, x, y) {
                var _o = new Date(x * 1000);
                var _y = _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
                var date = '<span class="date">' + _y + '</span>';
                var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                var content = swatch + series.name + ": " + Math.floor(y).formatMoney(0) + '<br>' + date;
                return content;
            }
        });


        // Indosat Playmedia
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var graph = new Rickshaw.Graph({
            element: document.querySelector("#theChartISATPlaymedia"),
            width: 400,
            height: 200,
            renderer: 'area',
            stroke: true,
            series: [
                {
                    color: 'steelblue',
                    name: 'Revenue (IDR)',
                    data: {{ $dataLast7DaysISATPlaymedia }}
                }
            ]
        });

        graph.render();


        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            /*
            xFormatter: function (x) {
                var _o = new Date(x*1000);
                return _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
            },
            yFormatter: function (y) {return Math.floor(y).formatMoney(0);}
            */
            formatter: function (series, x, y) {
                var _o = new Date(x * 1000);
                var _y = _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
                var date = '<span class="date">' + _y + '</span>';
                var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                var content = swatch + series.name + ": " + Math.floor(y).formatMoney(0) + '<br>' + date;
                return content;
            }
        });


        //XL KMI
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var graph = new Rickshaw.Graph({
            element: document.querySelector("#theChartXL"),
            width: 400,
            height: 200,
            renderer: 'area',
            stroke: true,
            series: [
                {
                    color: 'steelblue',
                    name: 'Revenue (IDR)',
                    data: {{ $dataLast7DaysXL }}
                }
            ]
        });

        graph.render();


        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            /*
             xFormatter: function (x) {
             var _o = new Date(x*1000);
             return _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
             },
             yFormatter: function (y) {return Math.floor(y).formatMoney(0);}
             */
            formatter: function (series, x, y) {
                var _o = new Date(x * 1000);
                var _y = _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
                var date = '<span class="date">' + _y + '</span>';
                var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                var content = swatch + series.name + ": " + Math.floor(y).formatMoney(0) + '<br>' + date;
                return content;
            }
        });

        // XL Playmedia
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var graph = new Rickshaw.Graph({
            element: document.querySelector("#theChartXLPlaymedia"),
            width: 400,
            height: 200,
            renderer: 'area',
            stroke: true,
            series: [
                {
                    color: 'steelblue',
                    name: 'Revenue (IDR)',
                    data: {{ $dataLast7DaysXLPlaymedia }}
                }
            ]
        });

        graph.render();


        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            /*
            xFormatter: function (x) {
                var _o = new Date(x*1000);
                return _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
            },
            yFormatter: function (y) {return Math.floor(y).formatMoney(0);}
            */
            formatter: function (series, x, y) {
                var _o = new Date(x * 1000);
                var _y = _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
                var date = '<span class="date">' + _y + '</span>';
                var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                var content = swatch + series.name + ": " + Math.floor(y).formatMoney(0) + '<br>' + date;
                return content;
            }
        });

        //TSEL KMI
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var graph = new Rickshaw.Graph({
            element: document.querySelector("#theChartTSEL"),
            width: 400,
            height: 200,
            renderer: 'area',
            stroke: true,
            series: [
                {
                    color: 'steelblue',
                    name: 'Revenue (IDR)',
                    data: {{ $dataLast7DaysTSEL }}
                }
            ]
        });

        graph.render();


        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            /*
             xFormatter: function (x) {
             var _o = new Date(x*1000);
             return _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
             },
             yFormatter: function (y) {return Math.floor(y).formatMoney(0);}
             */
            formatter: function (series, x, y) {
                var _o = new Date(x * 1000);
                var _y = _o.getDate() + " " + MONTHS[_o.getMonth()] + " " + _o.getFullYear();
                var date = '<span class="date">' + _y + '</span>';
                var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                var content = swatch + series.name + ": " + Math.floor(y).formatMoney(0) + '<br>' + date;
                return content;
            }
        });

    </script>

    <script src="{{ asset('assets/js/highcharts.js') }}"></script>
    <script>
        $(function () {
            /* var data = {"name": "Days", "data": {{ $dataLast7DaysISAT }} }; */
            var data = {{ $dataLast7DaysPerSdcISAT }};
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart2ISAT',
                    type: 'area'
                },
                credits: {
                    /* enabled: false, */
                    text: "KMI - Revenue Report"
                },
                title: {
                    text: 'Total Last 30 Days'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: {{ $label7day }}
                },
                yAxis: {
                    title: {
                        /* text: 'Revenue (IDR)' */
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

        $(function () {
            /* var data = {"name": "Days", "data": {{ $dataLast7DaysISATPlaymedia }} }; */
            var data = {{ $dataLast7DaysPerSdcISATPlaymedia }};
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart2ISATPlaymedia',
                    type: 'area'
                },
                credits: {
                    /* enabled: false, */
                    text: "Playmedia - Revenue Report"
                },
                title: {
                    text: 'Total Last 30 Days'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: {{ $label7day }}
                },
                yAxis: {
                    title: {
                        /* text: 'Revenue (IDR)' */
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

        $(function () {
            /* var data = {"name": "Days", "data": {{ $dataLast7DaysXL }} }; */
            var data = {{ $dataLast7DaysPerSdcXL }};
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart2XL',
                    type: 'area'
                },
                credits: {
                    /* enabled: false, */
                    text: "KMI - Revenue Report"
                },
                title: {
                    text: 'Total Last 30 Days'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: {{ $label7day }}
                },
                yAxis: {
                    title: {
                        /* text: 'Revenue (IDR)' */
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

        $(function () {
            /* var data = {"name": "Days", "data": {{ $dataLast7DaysXLPlaymedia }} }; */
            var data = {{ $dataLast7DaysPerSdcXLPlaymedia }};
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart2XLPlaymedia',
                    type: 'area'
                },
                credits: {
                    /* enabled: false, */
                    text: "Playmedia - Revenue Report"
                },
                title: {
                    text: 'Total Last 30 Days'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: {{ $label7day }}
                },
                yAxis: {
                    title: {
                        /* text: 'Revenue (IDR)' */
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

        $(function () {
            /* var data = {"name": "Days", "data": {{ $dataLast7DaysTSEL }} }; */
            var data = {{ $dataLast7DaysPerSdcTSEL }};
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart2TSEL',
                    type: 'area'
                },
                credits: {
                    /* enabled: false, */
                    text: "KMI - Revenue Report"
                },
                title: {
                    text: 'Total Last 30 Days'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: {{ $label7day }}
                },
                yAxis: {
                    title: {
                        /* text: 'Revenue (IDR)' */
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

    </script>


    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}"
            type="text/javascript"></script>

    <!--
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/d3.v2.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js') }}"></script>
    -->

    <script src="{{ asset('assets/plugins/jquery-sparkline/jquery-sparkline.js') }}"></script>
    <script src="{{ asset('assets/plugins/skycons/skycons.js') }}"></script>
    <script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-polymaps/polymaps.min.js') }}" type="text/javascript"></script>


    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/chat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/dashboard_v2.js') }}" type="text/javascript"></script> -->


    <script type="text/javascript">
        $(document).ready(function () {
            $(".live-tile,.flip-list").liveTile();
        });
    </script>


</body>
</html>
