<?php
/**
 * This is the main for Manage Service List page
 *
 */
?>
<div class="row">
    <div class="grid simple">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Manage <span class="semi-bold">Service</span></h3>
                    <p>Service is a set of context which is applied in the Gateway. A service can have 0 or more application</p>
                    <p class="selectGrup">
                        <select id="sdcSelect" style="width:40%;" name="sdc">
                            <optgroup label="Choose SDC">
                                @foreach ($sdc as $each_sdc)
                                <option value="{{ $each_sdc->name }}" {{ (isset($givenSdc) && ($each_sdc->name == $givenSdc)) ? "selected='1'" : "" }}>{{ $each_sdc->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                        <select id="telcoSelect" style="width:40%;" name="telcoId">
                            <optgroup label="Choose Telco">
                                @foreach ($telco as $each_telco)
                                <option value="{{ $each_telco->id }}" {{ (isset($telcoId) && ($each_telco->id == $telcoId)) ? "selected='1'" : "" }}>{{ $each_telco->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                        <div class="dataTables_filter searchGrup" style="float:left;">
                            <form method="GET">
                            <label>Search Keyword
                                <input id="idSearchKeywordText" class="input-medium" type="text" aria-controls="example"></input>
                            </label>
                            </form>
                        </div>
                        
                    </p>
                    <br/>

                    <div class="row" style="clear:both; margin-top:50px;">
                        <div class="col-md-6">
                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewListService"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               style="margin-right: 30px;"
                               data-colorpicker-guid="8">View</a>

                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="createNewService"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               style="margin-right: 30px;"
                               data-colorpicker-guid="8">Add Service</a>

                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="manageServiceGroup"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               data-colorpicker-guid="8">Service Group</a>

                        </div>
                        
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="theData"></div>


