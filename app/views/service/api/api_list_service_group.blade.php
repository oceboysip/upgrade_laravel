@if (isset($the_data))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>Service <span class="semi-bold">Group</span></h4>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>

        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($the_data) < 1)
                <tr class="even gradeA">
                    <td colspan=5> No data </td>
                </tr> 
            @else    
                @foreach ($the_data as $each_data)
                    <tr class="even gradeA">
                        <td class="center">{{ $each_data->id }}</td>
                        <td class="center">{{ $each_data->name }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            </table>

            <!--
            <br/>
            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="createNewServiceGroup"
               class="btn btn-primary my-colorpicker-control" href="#"
               style="margin-right: 30px;"
               data-colorpicker-guid="8">Create New</a>
            -->

        </div>

        <!-- <div class="grid-body" id="theOfCreateServiceGroup"></div> -->
        <div id="theOfCreateServiceGroup"></div>
    </div>

</div>
@endif

<script type="text/javascript">
    $("#createNewServiceGroup").click(function () {
        $.get("{{ url('/api/create_service_group') }}",
            {ajax: 'true'}, function (data) {
                $("#theData").html('');
                $("#theData").html(data);
            }).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while calling service group! Please try again later or contact system Administrator')
                }
            })
        ;
    });
</script>
