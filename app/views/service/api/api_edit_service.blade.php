<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <br>
    <h4 id="myModalLabel" class="semi-bold">Edit Service {{ $service->name }}</h4>

    <p class="no-margin">Carefully review the values</p>
    <br>
</div>
<div class="modal-body">
    <form id="myform" method="POST">
        <input type="hidden" name="serviceId" value="{{ $serviceId }}">
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Name</label><br/>
                    <div class="controls">
                        <div class="controls">
                            <input id="idTelcoSid" style="width:60%;" value="{{ $service->name }}" placeholder="Service Name"
                                   type="text" class="form-control" name="serviceName">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Business Delegate URL</label><br/>
                    <span class="help">Node that process business for given service</span>
                    <div class="controls">
                        <input id="idEuPrice" value="{{ $service->url }}" placeholder="Business Delegate URL"
                               type="text" class="form-control" name="url">
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Service Group</label><br/>
                    <div class="controls">
                        <select id="idServiceGroup" style="width:80%;" name="serviceGroupId">
                            <optgroup label="Choose Group">
                                @foreach ($serviceGroup as $each_service_group)
                                <option value="{{ $each_service_group->id }}">{{ $each_service_group->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Status Active</label><br/>
                    <div class="controls">
                        <select id="idChargingDisabled" style="width:40%;" name="isActive">
                            <option value="1" {{ $service->is_active ? "selected='1'" : "" }}>Yes</option>
                            <option value="0" {{ $service->is_active ? "" : "selected='1'" }}>No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="waitCommit" class="row form-row"></div>
    <div id="idErrorMessage" class="row form-row"></div>
    <div id="idAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
    <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">
    var doneEdit = false;
    $("#idChargingDisabled").select2();
    $("#idServiceGroup").select2();

    $("#btnDismiss").click(function () {
        if (doneEdit) {
            window.location = "{{ url('/admin/manage_service') }}?telcoId="
                + $("#telcoSelect").val() + "&givenSdc=" + $("#sdcSelect").val();
        }
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled", "disabled");
        $.post("{{ url('/api/edit_service') }}", $("#myform").serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success&nbsp;edit service {{ $service->name }} </div>');
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while edit service! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });
</script>
