@if (isset($the_data))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            @if (isset($telcoName) && isset($sdc))
            <h4>List of Service <span class="semi-bold">{{ $telcoName }} - {{ $sdc }}</span></h4>
            @else
            <h4>List of Service</h4>
            @endif
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>


        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <th>Name</th>
                <th>Telco - SDC</th>
                <th>Business Delegate URL</th>
                <th>Group Name</th>
                <th>Active</th>
                <th>Keywords</th>
                <th> </th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($the_data) < 1)
                <tr class="even gradeA">
                    <td colspan=5> No data </td>
                </tr> 
            @else    
                @foreach ($the_data as $each_data)
                    <tr class="even gradeA">
                        <!-- <td>{{ $each_data->id }}</td> -->
                        <td title="Service Id: {{ $each_data->id }}">{{ $each_data->name }}</td>
                        <td class="center">{{ $each_data->telco_name }} - {{ $each_data->sdc }}</td>
                        <td class="center">{{ $each_data->url }}</td>
                        <td class="center">{{ $each_data->group_name }}</td>
                        @if ($each_data->is_active)
                        <td class="center"><span class="label label-success">Yes</span></td>
                        @else
                        <td class="center"><span class="label label-important">No</span></td>
                        @endif
                        <td class="center">
                            <button serviceId="{{ $each_data->id }}" serviceName="{{ $each_data->name }}"
                                    sdc="{{ $each_data->sdc }}" telcoId="{{ $each_data->telcoId }}"
                                    class="btn btn-small btn-white btn-cons classBtnManageKeywords"
                                    type="button" data-toggle="modal" data-target="#myModalKeywords">Manage</button>
                        </td>
                        <td class="center">
                            <button serviceId="{{ $each_data->id }}" class="btn btn-small btn-white btn-cons btnEdit"
                                    type="button" data-toggle="modal" data-target="#myModal">Edit Service</button>
                        </td>
                    </tr>                    
                @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalContent"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalKeywords" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalKeywordsContent"></div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(".btnEdit").click(function() {
        $.get("{{ url('/api/edit_service') }}" + "?serviceId=" + this.getAttribute('serviceId'),
            {ajax: 'true'},
            function (data) {
                $("#theModalContent").html('');
                $("#theModalContent").html(data);
            }
        );
    });

    $(".classBtnManageKeywords").on('click',function() {
        viewServiceKeywordList(this.getAttribute('serviceName'), this.getAttribute('serviceId'),
            this.getAttribute('sdc'), this.getAttribute('telcoId')
        );
    });


    function viewServiceKeywordList(that) {
        viewServiceKeywordList(that.getAttribute('serviceName'), that.getAttribute('serviceId'));
    }
    function viewServiceKeywordList(serviceName, serviceId, sdc, telcoId) {
        //var serviceName = that.getAttribute('serviceName');
        $.get("{{ url('/api/get_service_keywords') }}" + "?serviceId=" + /* that.getAttribute('serviceId') */ serviceId +
            "&serviceName=" + encodeURIComponent(/*that.getAttribute('serviceName') */ serviceName) +
            "&sdc=" + sdc + "&telcoId=" + telcoId,
            {ajax: 'true'},
            function (data) {
                $("#theModalKeywordsContent").html('');
                $("#theModalKeywordsContent").html(data);
            }
        ).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while get service keywords! Please try again later or contact system Administrator')
                }
            })
        ;
    }
</script>
@endif
