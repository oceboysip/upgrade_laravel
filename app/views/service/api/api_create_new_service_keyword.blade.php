<div class="modal-body">
    <button type="button" class="close" id="btnThisCreateClose">×</button><br/>
    <form id="myformKeyword" method="POST">
        <h4>Add New Service Keyword</h4><br/>
        <input type="hidden" name="sdc" value="{{ $sdc }}">
        <input type="hidden" id="idModelTelcoId" name="telcoId" value="{{ $telcoId }}">
        <input type="hidden" id="idModelServiceId" name="serviceId" value="{{ $serviceId }}">
        <div class="form-group">
            <label class="form-label">Keyword</label><br/>
            <div class="controls" style="width:90%;">
                <input type="text" class="form-control" name="keyword" id="idKeyword">
                <label class="error" style="display: none" for="idName" id="errorName">Keyword cannot be empty</label>
            </div>
        </div>
        <br/>
        <button id="btnSaveNewKeyword" class="btn btn-warning">Save</button>
        <button id="btnCloseNewKeywordWindow" class="btn btn-warning pull-left" style="display: none">Close</button>
        <div id="waitCommitKeyword" class="row form-row"></div>
        <div id="idErrorMessageKeyword" class="row form-row"></div>
        <div id="idAlertSuccessKeyword" class="row form-row"></div>
    </form>
</div>

<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var doneEdit = false;
    $("#btnThisCreateClose").click(function () {
            $("#theNewServiceKeyword").html('');
        }
    );
    $("#btnCloseNewKeywordWindow").click(function () {
            if (doneEdit) {
                viewServiceKeywordList('',{{ $serviceId }}, '{{ $sdc }}', {{ $telcoId }});
                return false;
            }
        }
    );

    //var doneEdit = false;
    $("#myform").validate({
        rules: {
            keyword: {required: true}
        }
    });
    $("#btnSaveNewKeyword").click(function () {
        $("#waitCommitKeyword").html('<div class="col-md-8 col-sm-8 col-xs-8"><br/>Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        if ('undefined' == $("#idModelTelcoId ").val() ) $("#idModelTelcoId ").val();

        $("#btnSaveNewKeyword").attr("disabled", "disabled");
        $.post("{{ url('/api/create_new_service_keyword') }}", $("#myformKeyword").serialize(), function (data) {
                $("#waitCommitKeyword").html('');
                if (data['Error']) {
                    $("#idErrorMessageKeyword").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#btnSaveNewKeyword").hide();
                    $("#btnCloseNewKeywordWindow").show();
                    $("#idAlertSuccessKeyword").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success:&nbsp;New service keyword ' + $('[name="keyword"]').val() + ' has been added<br/></div>');
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idErrorMessageKeyword").html(
                        '<div class="alert alert-error">Error while creating new service keyword! ' +
                            'Please try again later or contact system Administrator</div>')
                }
            })
            .always(function () {
                //alert("finished");
            });

        //to prevent Browser from submitting the real FORM post
        return false;
        //alert("{{ url('/api/create_new_service_keyword') }}");
    });
</script>
