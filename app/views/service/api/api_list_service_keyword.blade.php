<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><br/>
    <h5 id="myModalLabel" class="semi-bold">List of keywords for {{ $serviceName }}</h5>
</div>
<div class="modal-body">
    <form id="myform" method="POST">
        <!-- <input type="hidden" name="serviceId" value="12312"> -->
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label"></label><br/>
                    <div class="controls">
                        <ul>
                        @foreach ($the_data as $each_data)
                            <li>{{ $each_data->keyword }}
                                @if($each_data->is_default)
                                (*default)
                                @endif
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="waitCommit" class="row form-row"></div>
    <div id="idErrorMessage" class="row form-row"></div>
    <div id="idAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
    <button type="button" id="btnAddKeyword" class="btn btn-primary pull-left">Add Keyword</button>
    <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<div class="row" id="theNewServiceKeyword"></div>

<script type="text/javascript">
    $("#btnAddKeyword").click(function() {viewNewServiceKeywordList();});

    function viewNewServiceKeywordList() {
        $.get("{{ url('/api/create_new_service_keyword') }}" + "?serviceId={{ $serviceId }}&sdc={{ $sdc }}&telcoId={{ $telcoId }}",
            {ajax: 'true'},
            function (data) {
                $("#theNewServiceKeyword").html('');
                $("#theNewServiceKeyword").html(data);
            }
        ).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while viewing new service keyword! Please try again later or contact system Administrator')
                }
            })
        ;
    }
</script>
