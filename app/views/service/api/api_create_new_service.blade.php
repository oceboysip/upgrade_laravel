@if (isset($the_data))
@endif
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST">
            <h4>Add New Service for <b>{{ $telcoName }} - {{ $sdc }}</b></h4><br/>
            <input type="hidden" name="sdc" value="{{ $sdc }}">
            <input type="hidden" name="telcoId" value="{{ $telcoId }}">
            <input type="hidden" name="telcoName" value="{{ $telcoName }}">
            <input type="hidden" name="serviceGroupName" id="idServiceGroupName">
            <input type="hidden" name="servicePrefix" id="idServicePrefixName" value="{{ $telcoName }}-{{ $sdc }}">

            <div class="form-group">
                <label class="form-label">Name</label><br/>
                <span class="help">Service Name. Recommended Format: [Telco]-[SDC] [Name of Service]. i.e: ATT-2134 SMS Daily</span>
                <div class="controls" style="width:90%;">
                    <input type="text" class="form-control" name="name" id="idName" value="{{ $telcoName }}-{{ $sdc }} ">
                    <label class="error" style="display: none" for="idName" id="errorName">Name is required</label>
                </div>
            </div>
            <table style="width:90%;">
                <tr>
                    <td>
                        <div class="form-group">
                            <label class="form-label">Business Delegate URL</label><br/>
                            <span class="help">The URL that where the business resides</span>

                            <div class="controls">
                                <input id="idUrl" style="width:60%;" type="text" class="form-control" name="url" value="{{ $businessDelegateUrl }}">
                            </div>
                        </div>
                    </td>
                    <!--
                    <td style="padding-left: 30px;">
                        <div class="form-group">
                            <label class="form-label">Our Price</label><br/>
                            <span class="help">The price for Content Provider (in IDR)</span>
                            <div class="controls">
                                <input id="idCpPrice" style="width:60%;" type="text" class="form-control" name="cpPrice">
                            </div>
                        </div>
                    </td>
                    -->
                </tr>
            </table>

            <div class="form-group">
                <label class="form-label">Service Group</label><br/>
                <span class="help">Set group for this service (if any)</span>
                <div class="controls">
                    <select id="idServiceGroup" style="width:40%;" name="serviceGroupId">
                        @foreach ($serviceGroup as $each_group)
                        <option value="{{ $each_group->id }}">{{ $each_group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <br/>
            <br/>
            <button id="btnSave" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Save</button>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <br>
                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                            <h4 id="myModalLabel" class="semi-bold">Summary new Service for {{ $telcoName }} - {{ $sdc }}</h4>
                            <p class="no-margin">Please review the value again</p>
                            <br>
                        </div>
                        <div class="modal-body">
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-name" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-url" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-serviceGroup" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div id="waitCommit" class="row form-row"></div>
                            <div id="idErrorMessage" class="row form-row"></div>
                            <div id="idAlertSuccess" class="row form-row"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var doneEdit = false;
    $("#idServiceGroup").select2()
        .on("change", function () {
            $("#idServiceGroupName").val($("#idServiceGroup option:selected").text());
        });

    $("#myform").validate({
        rules: {
            name: {required: true},
            url: {required: true}
        }
    });

    $("#btnDismiss").click(function () {
        if (doneEdit) {
            window.location = "{{ url('/admin/manage_service') }}?telcoId="
                + $("#telcoSelect").val() + "&givenSdc=" + $("#sdcSelect").val();
        }
    });

    $("#btnSave").click(function() {
        var nameService = $("#idName").val();
        //var

        if (nameService == '' ||
            nameService.trim() == $("#idServicePrefixName").val().trim())  {
        //if (nameService == '') {
            $("#errorName").show();
            return false;
        } else {
            $("#id-lbl-name").html('Name: <b>' + nameService + '</b>');
            $("#id-lbl-url").html('Business Delegate URL: <br/><b>' + $('[name="url"]').val() + '</b>');
            $("#id-lbl-serviceGroup").html('Service Group: <b>' + $('#idServiceGroupName').val() + '</b>');
        }
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
                                '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled","disabled");
        $.post("{{ url('/api/create_new_service') }}", $( "#myform" ).serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success:&nbsp;The new service ' + $('[name="name"]').val() + ' has been added</div>');
                }
            })
            .done(function () {
                 //alert("second success");
             })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while creates new service! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                 //alert("finished");
            });
    });

    $("#idServiceGroupName").val($("#idServiceGroup option:selected").text());
</script>
