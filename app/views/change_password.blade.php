@extends('change_password-layout')

@section('content')
<br/>
<form id="myform" method="POST">
    <input type="hidden" name="userId" value="1">

    <div class="row form-row">
        <div class="col-md-10">
            <div class="form-group">
                <div class="controls">
                    <label class="form-label">Old Password</label>
                    <input id="idOldPassword" style="width:60%;"
                           type="password" class="form-control" name="oldPassword">
                </div>
            </div>
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-10">
            <div class="form-group">
                <div class="controls">
                    <label class="form-label">New Password</label><br/>
                    <input id="idNewPassword" style="width:60%;"
                           type="password" class="form-control" name="newPassword">
                </div>
            </div>
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-10">
            <div class="form-group">
                <div class="controls">
                    <label class="form-label">Confirm New Password</label><br/>
                    <input id="idConfirmPassword" style="width:60%;"
                               type="password" class="form-control" name="confirmPassword">
                </div>
            </div>
        </div>
    </div>
    <br/>
    <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="idUpdatePassword"
       class="btn btn-primary my-colorpicker-control" href="#"
       style="margin-right: 30px;"
       data-colorpicker-guid="8">Update</a>
    <br/><br/>
</form>
<div id="waitCommit" class="row form-row"></div>
<div id="idErrorMessage" class="row form-row"></div>
<div id="idAlertSuccess" class="row form-row"></div>

@stop
