<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>@yield('title.header')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{ url('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ url('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->
    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="error-body no-top">
<div class="container">
    <div class="row login-container column-seperation">
        <div class="col-md-4 col-md-offset-1">
            <h3 class="center-text">@yield("title.label")</h3>
            <!--p class="center-text">@yield("system.label")<br>
            </p-->
            <br/>
            <!-- <img src="assets/img/trias-logo-EWF-small.png" class="pull-left"/> -->
            <img src="@yield('logo.path')" class="center-block"/>

        </div>
        <div class="col-md-5 "><br>
            <!-- <form id="login-form" class="login-form" action="" method="post"> -->
            {{ Form::open(array('url' => '/login', 'class' => 'login-form', 'method' => 'post')) }}
                @if (Session::has('notice'))
                <div class="alert alert-danger alert-dismissable" style="font-size: 12px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    {{ Session::get('notice') }}
                    <?php
                        //This is a hack since we failed to remove the session in 'controller'
                        Session::forget('notice');
                    ?>
                </div>
                @endif
                @if (Session::has('message_error'))
                <div class="alert alert-danger alert-dismissable" style="font-size: 12px;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    {{ Session::get('message_error') }}
                </div>
                @endif

                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-label">Username</label>

                        <div class="controls">
                            <div class="input-with-icon  right">
                                <i class=""></i>
                                {{ Form::text('username', '', array('class' => 'form-control', 'required')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-label">Password</label>
                        <span class="help"></span>
                        <div class="controls">
                            <div class="input-with-icon  right">
                                <i class=""></i>
                                {{ Form::password('password', array('class' => 'form-control', 'required')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="control-group  col-md-10">
                        <div class="checkbox checkbox check-success">
                            <input type="checkbox" id="checkbox1" name="remember_me" value="1">
                            <label for="checkbox1">Keep me reminded </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
                    </div>
                </div>
            <!-- </form> -->
            {{ Form::close() }}

        </div>


    </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
<script src="{{ url('assets/plugins/jquery-1.8.3.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/js/login.js') }}" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<!-- END CORE TEMPLATE JS -->
</body>
</html>
