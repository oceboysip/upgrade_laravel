@extends('logins.layout')

@section('content')
    Users!
@stop

@section('title.header')
    KMI Webtools 
@stop

@section('system.label')
    .Triceré Messaging Appliance.
@stop

@section('title.label')
    Sign in to Dashboard
@stop

@section('logo.path')
    assets/img/logo-login-black.png
@stop
