<?php
/**
 * Sidebar list
 * User: pandupradhana
 * Date: 4/20/14
 * Time: 12:33 AM
 * To change this template use File | Settings | File Templates.
 */


/**
 * ----------
 * Subscriber
 * ----------
 *
 */
?>
@if (isset($traffic))
    @if (Request::is('traffic/*'))
<li class="active open">
    @else
<li class="">
    @endif
    <a href="#"> <i class="icon-custom-chart"></i>
        <span class="title">Traffic</span> <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        @foreach ($traffic as $traffic_menu)
        @if (isset($traffic_menu->sub))
        <li><a href="#"><span class="arrow "></span>
                {{ $traffic_menu->title }}
            </a>
            <ul class="sub-menu">
                @foreach ($traffic_menu->sub as $traffic_sub_menu)
                    @if (Request::is($traffic_sub_menu->module . '/' . $traffic_sub_menu->controller))
                <li class="active">
                    @else
                <li>
                    @endif
                    <a href="{{ route($traffic_sub_menu->module . '/' . $traffic_sub_menu->controller) }}">{{ $traffic_sub_menu->title }}</a>
                </li>
                @endforeach
            </ul>
        </li>
        @else
                @if(Request::is($traffic_menu->module . '/' . $traffic_menu->controller . '*'))
            <li class="active">
                @else
            <li>
                @endif
            <a href="{{ route($traffic_menu->module . '/' . $traffic_menu->controller) }}">
                {{ $traffic_menu->title }}
            </a>
        </li>
        @endif
        @endforeach
    </ul>
</li>
@endif


<?php
/**
 * -------
 * Content
 * -------
 *
 */
?>
@if (isset($content))
    @if (Request::is('content/*'))
<li class="active open">
    @else
<li class="">
    @endif
    <a href="javascript:;"> <i class="icon-custom-portlets"></i><span class="title">Content</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        @foreach ($content as $content_menu)
        @if (isset($content_menu->sub))
        <li><a href="#"><span class="arrow "></span>
                {{ $traffic_menu->title }}
            </a>
            <ul class="sub-menu">
                @foreach ($content_menu->sub as $content_sub_menu)
                    @if(Request::is( $content_sub_menu->module . '/' . $content_sub_menu->controller . '*'))
                <li class="active">
                    @else
                <li>
                    @endif
                    <a href="{{ route($content_sub_menu->module . '/' . $content_sub_menu->controller )}}">{{ $content_sub_menu->title }}</a>
                </li>
                @endforeach
            </ul>
        </li>
        @else
                @if(Request::is( $content_menu->module . '/' . $content_menu->controller))
            <li class="active">
                @else
            <li>
                @endif
            <a href="{{ route($content_menu->module . '/' . $content_menu->controller )}}">
                {{ $content_menu->title }}
            </a>
        </li>
        @endif
        @endforeach
    </ul>
</li>
@endif

<?php
/**
 * ----------
 * Subscriber
 * ----------
 *
 */
?>
@if (isset($subscriber))
    @if (Request::is('subscriber/*'))
<li class="active open">
    @else
<li class="">
    @endif
    <a href="javascript:;"> <i class="icon-custom-ui"></i><span class="title">Subscriber</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        @foreach ($subscriber as $subscriber_menu)
        @if (isset($subscriber_menu->sub))
        <li><a href="#"><span class="arrow "></span>
                {{ $subscriber_menu->title }}
            </a>
            <ul class="sub-menu">
                @foreach ($subscriber_menu->sub as $subscriber_sub_menu)
                    @if(Request::is( $subscriber_sub_menu->module . '/' . $subscriber_sub_menu->controller . '*'))
                <li class="active">
                    @else
                <li>
                    @endif
                    <a href="{{ route($subscriber_sub_menu->module . '/' . $subscriber_sub_menu->controller) }}">{{ $subscriber_sub_menu->title }}</a>
                </li>
                @endforeach
            </ul>
        </li>
        @else
                @if(Request::is( $subscriber_menu->module . '/' . $subscriber_menu->controller . '*'))
            <li class="active">
                @else
            <li>
                @endif
            <a href="{{ route($subscriber_menu->module . '/' . $subscriber_menu->controller) }}">
                {{ $subscriber_menu->title }}
            </a>
        </li>
        @endif
        @endforeach
    </ul>
</li>
@endif

<?php
/**
 * ----------
 * NODESMS
 * ----------
 *
 */
?>
@if (isset($nodesms))
    @if (Request::is('nodesms/*'))
<li class="active open">
    @else
<li class="">
    @endif
    <a href="javascript:;"> <i class="icon-custom-ui"></i><span class="title">NodeSMS</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        @foreach ($nodesms as $key => $nodesms_menu)
        @if (isset($nodesms_menu->sub))
        <li><a href="#"><span class="arrow "></span>
                {{ $nodesms_menu->title }}
            </a>
            <ul class="sub-menu">
                @foreach ($nodesms_menu->sub as $nodesms_sub_menu)
                    @if(Request::is( $nodesms_sub_menu->module . '/' . $nodesms_sub_menu->controller . '*'))
                <li class="active">
                    @else
                <li>
                    @endif
                    <a href="{{ route($nodesms_sub_menu->module . '/' . $nodesms_sub_menu->controller) }}">{{ $nodesms_sub_menu->title }}</a>
                </li>
                @endforeach
            </ul>
        </li>
        @else
                @if(Request::is( $nodesms_menu->module . '/' . $nodesms_menu->controller . '*'))
            <li class="active">
                @else
            <li>
                @endif
            <a href="{{ url($nodesms_menu->module . '/' . $nodesms_menu->controller) }}">
                {{ $nodesms_menu->title }}
            </a>
        </li>
        @endif
        @endforeach
    </ul>
</li>
@endif

<?php
/**
 * ----------
 * Monitoring
 * ----------
 *
 */
?>
@if (isset($monitoring))
    @if (Request::is('monitoring/*'))
<li class="active open">
    @else
<li class="">
    @endif
    <a href="javascript:;"> <i class="icon-custom-feedbacks"></i><span class="title">Monitoring</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        @foreach ($monitoring as $monitoring_menu)
        @if (isset($monitoring_menu->sub))
        <li><a href="#"><span class="arrow "></span>
                {{ $monitoring_menu->title }}
            </a>
            <ul class="sub-menu">
                @foreach ($monitoring_menu->sub as $monitoring_sub_menu)
                    @if(Request::is( $monitoring_sub_menu->module . '/' . $monitoring_sub_menu->controller . '*'))
                <li class="active">
                    @else
                <li>
                    @endif
                    <a href="{{ route($monitoring_sub_menu->module . '/' . $monitoring_sub_menu->controller) }}">{{ $monitoring_sub_menu->title }}</a>
                </li>
                @endforeach
            </ul>
        </li>
        @else
                @if(Request::is( $monitoring_menu->module . '/' . $monitoring_menu->controller . '*'))
            <li class="active">
                @else
            <li>
                @endif
            <a href="{{ route($monitoring_menu->module . '/' . $monitoring_menu->controller) }}">
                {{ $monitoring_menu->title }}
            </a></li>
        @endif
        @endforeach
    </ul>
</li>
@endif


<?php
/**
 * -----
 * Admin
 * -----
 *
 */
?>
@if (isset($admin))
    @if (Request::is('admin/*'))
<li class="active open">
    @else
<li class="">
    @endif
    <a href="javascript:;"> <i class="icon-custom-settings"></i><span class="title">Admin</span>
        <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        @foreach ($admin as $admin_menu)
        @if (isset($admin_menu->sub))
        <li><a href="#"><span class="arrow "></span>
                {{ $admin_menu->title }}
            </a>
            <ul class="sub-menu">
                @foreach ($admin_menu->sub as $admin_sub_menu)
                    @if(Request::is( $admin_sub_menu->module . '/' . $admin_sub_menu->controller . '*'))
                <li class="active">
                    @else
                <li>
                    @endif
                    <a href="{{ route($admin_sub_menu->module . '/' . $admin_sub_menu->controller) }}">{{ $admin_sub_menu->title }}</a>
                </li>
                @endforeach
            </ul>
        </li>
        @else
                @if(Request::is( $admin_menu->module . '/' . $admin_menu->controller . '*'))
            <li class="active">
                @else
            <li>
                @endif
            <a href="{{ route($admin_menu->module . '/' . $admin_menu->controller) }}">
                {{ $admin_menu->title }}
            </a>
        </li>
        @endif
        @endforeach
    </ul>
</li>
@endif
