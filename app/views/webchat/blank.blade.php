<!DOCTYPE html>
<html lang="en">
<head>
    <title>TEST - KEPO WEB CHAT</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link type="text/css" rel="stylesheet" media="screen" href="css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" media="screen" href="css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" media="screen" href="css/theme.css" />
    
    <script src="src/website.js"></script>
    <![if gte IE 11]>
        <link type="text/css" rel="stylesheet" media="screen" href="https://cdn.conversejs.org/3.3.1/css/converse.min.css" />
        <script src="dist/converse.js"></script>
    <![endif]>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">


</body>

<script>
    converse.initialize({
        auto_reconnect: true,
        message_archiving: 'always',
        message_carbon: true,
        forward_messages: false,
        show_controlbox_by_default: true,
        show_message_load_animation: false,
        hide_muc_server: false,
        play_sounds: true,
        allow_registration: false,
        allow_non_roster_messaging: true,
        auto_reconnect: true,
        auto_login: true,
        jid: '{{ \Auth::user()->kepoid }}@chat.kmi.mobi',
        debug: false,
        use_otr_by_default: false,
        visible_toolbar_buttons:
        {
            call: false,
            clear: false,
            emoticons: false,
            toggle_participants: false
        },
    });
    
</script>
</html>



