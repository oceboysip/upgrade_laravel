<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@yield("header.title","KMI Webtools")</title>

    <!-- BEGIN CORE CSS FRAMEWORK -->

    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->


    <!-- Webchat TEST -->
    <!-- <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/> -->
    <link href="{{ asset('assets/css/theme_kmi.css') }}" rel="stylesheet" type="text/css"/> 
    <script src="{{ asset('assets/js/website_kmi.js') }}" type="text/javascript"></script> 
    <link href="{{ asset('assets/css/converse_kmi.css') }}" rel="stylesheet" type="text/css"/>
    <!-- <script src="{{ asset('assets/js/converse_kmi.js') }}" type="text/javascript"></script> -->

</head>

<!-- BEGIN BODY -->
<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

<!-- BEGIN HEADER -->
@include("main_top_navigation-layout")
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">

    @include("sidebar-layout")

    <!-- BEGIN PAGE CONTAINER-->


    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here</div>
        </div>

        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title">
            </div>

            @yield("main_content")

            <div id="theContent"></div>

        </div>
    </div>

    <!-- END MAIN PAGE -->

</div>

<!-- END CONTAINER -->

</body>
<script>
        converse.initialize({
            auto_reconnect: true,
            message_archiving: 'always',
            message_carbon: true,
            forward_messages: false,
            show_controlbox_by_default: true,
            show_message_load_animation: false,
            hide_muc_server: false,
            play_sounds: true,
            allow_registration: false,
            allow_non_roster_messaging: true,
            auto_reconnect: true,
            auto_login: true,
            jid: '{{ \Auth::user()->kepoid }}@chat.kmi.mobi',
            debug: false,
            use_otr_by_default: false,
            visible_toolbar_buttons:
            {
                call: false,
                clear: false,
                emoticons: false,
                toggle_participants: false
            },
        });
</script>
</html>
