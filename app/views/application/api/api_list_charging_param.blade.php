<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <br>
    <h4 id="myModalLabel" class="semi-bold">Tariff Settings for {{ $applicationName }}</h4>
    <p class="no-margin">Carefully review the values</p>
    <br>
</div>
<div class="modal-body">
    <form id="myform" method="POST">
        <input type="hidden" name="applicationId" value="{{ $applicationId }}">
        <input type="hidden" name="telcoId" value="{{ $telcoId }}">
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">General Pull</label><br/>
                    <span class="help">Use during pull (Standard Pull / Subscription Type Service)</span>
                    <div class="controls">
                        <div class="controls">
                            <select id="idPull" style="width:80%;" name="pull">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    @if (isset($chargingParam) && $each_charging->code == $chargingParam->pull)
                                    <option value="{{ $each_charging->code }}" selected="selected">{{$each_charging->code}}</option>
                                    @else
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Failed Pull</label><br/>
                    <span class="help">Use during failed processing pull (Standard Pull / Subscription Type Service)</span>
                    <div class="controls">
                        <div class="controls">
                            <select id="idPullError" style="width:80%;" name="pullError">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    @if (isset($chargingParam) && $each_charging->code == $chargingParam->pull_error)
                                    <option value="{{ $each_charging->code }}" selected="selected">{{$each_charging->code}}</option>
                                    @else
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Success Registration</label><br/>
                    <span class="help">Use during successfull registration (Subscription Type Service)</span>
                    <div class="controls">
                        <div class="controls">
                            <select id="idReg" style="width:80%;" name="reg">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    @if (isset($chargingParam) && $each_charging->code == $chargingParam->reg)
                                    <option value="{{ $each_charging->code }}" selected="selected">{{$each_charging->code }}</option>
                                    @else
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Failed Registration</label><br/>
                    <span class="help">Use during failed registration (Subscription Type Service)</span>
                    <div class="controls">
                        <div class="controls">
                            <select id="idRegError" style="width:80%;" name="regError">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    @if (isset($chargingParam) && $each_charging->code == $chargingParam->reg_error)
                                    <option value="{{ $each_charging->code }}" selected="selected">{{$each_charging->code
                                        }}
                                    </option>
                                    @else
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Success Unregistration</label><br/>
                    <span class="help">Use during success Unregistration (Subscription Type Service)</span>
                    <div class="controls">
                        <div class="controls">
                            <select id="idUnreg" style="width:80%;" name="unreg">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    @if (isset($chargingParam) && $each_charging->code == $chargingParam->unreg)
                                    <option value="{{ $each_charging->code }}" selected="selected">{{$each_charging->code}}</option>
                                    @else
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Failed Unregistration</label><br/>
                    <span class="help">Use during failed Unregistration (Subscription Type Service)</span>
                    <div class="controls">
                        <div class="controls">
                            <select id="idUnregError" style="width:80%;" name="unregError">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    @if (isset($chargingParam) && $each_charging->code == $chargingParam->unreg)
                                    <option value="{{ $each_charging->code }}" selected="selected">{{$each_charging->code}}</option>
                                    @else
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endif
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="waitCommit" class="row form-row"></div>
    <div id="idErrorMessage" class="row form-row"></div>
    <div id="idAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
    <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">
    var doneEdit = false;
    $("#idPull").select2();
    $("#idPullError").select2();
    $("#idReg").select2();
    $("#idRegError").select2();
    $("#idUnreg").select2();
    $("#idUnregError").select2();

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled", "disabled");
        $.post("{{ url('/api/save_charging_param') }}", $("#myform").serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success&nbsp;edit business application {{ $applicationName }} </div>');
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while edit application tariff! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });
</script>
