@if (isset($the_data))
@endif
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST">
            <h4>Add New Business Application for <b>{{ $telcoName }} - {{ $sdc }}</b></h4><br/>
            <input type="hidden" name="sdc" value="{{ $sdc }}">
            <input type="hidden" name="telcoId" value="{{ $telcoId }}">
            <input type="hidden" name="telcoName" value="{{ $telcoName }}">
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Gateway Service</label><br/>
                        <div class="controls">
                            <select id="idServiceId" style="width:80%;" name="serviceId">
                                <optgroup label="Choose Group">
                                    @foreach ($allService as $each_service)
                                    <option value="{{ $each_service->id }}">{{ $each_service->name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Name</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input id="idApplicationName" style="width:60%;"
                                       placeholder="Application Name" type="text" class="form-control" name="applicationName">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Default Header</label><br/>
                        <span class="help">Default header that will be included in every message</span>
                        <div class="controls">
                            <input id="idDefaultHeader" placeholder="Default Header. i.e: HUMOR"
                                   type="text" class="form-control" name="defaultHeader">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Type</label><br/>
                        <div class="controls">
                            <select id="idApplicationType" style="width:80%;" name="applicationType">
                                @foreach ($applicationType as $each_type)
                                <option value="{{ $each_type->id }}">{{ $each_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Default Tariff</label><br/>
                        <div class="controls">
                            <select id="idChargingParam" style="width:80%;" name="defaultChargingParam">
                                <optgroup label="Choose Group">
                                    @foreach ($allCharging as $each_charging)
                                    <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="form-label">Delivery Interval (Days)</label><br/>
                        <div class="controls">
                            <select id="idPushDayInterval" style="width:80%;" name="pushDayInterval">
                                @for ($i = 0; $i < 8; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <button id="btnSave" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Save</button>

            <!-- The Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <br>
                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                            <h4 id="myModalLabel" class="semi-bold">Summary new application for {{ $telcoName }} - {{ $sdc }}</h4>
                            <p class="no-margin">Please review the value again</p>
                            <br>
                        </div>
                        <div class="modal-body">
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-name" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-service" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-header" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-type" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-default_tariff" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-interval" class="form-label"></label><br/>
                                </div>
                            </div>

                            <div id="waitCommit" class="row form-row"></div>
                            <div id="idErrorMessage" class="row form-row"></div>
                            <div id="idAlertSuccess" class="row form-row"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var doneEdit = false;
    $("#idApplicationType").select2();
    $("#idServiceId").select2();
    $("#idChargingParam").select2();
    $("#idPushDayInterval").select2();

    $("#idServiceId").select2()
        .on("change", function () {
            $("#idApplicationName").val($("#idServiceId option:selected").text());
        });

    $("#myform").validate({
        rules: {
            name: {required: true}
        }
    });

    $("#btnDismiss").click(function () {
        if (doneEdit) {
            window.location = "{{ url('/admin/manage_application') }}?telcoId="
                + $("#telcoSelect").val() + "&givenSdc=" + $("#sdcSelect").val();
        }
    });

    $("#btnSave").click(function() {
        $("#id-lbl-name").html('Name: <b>' + $('[name="applicationName"]').val() + '</b>');
        $("#id-lbl-service").html('Gateway Service: <b>' + $("#idServiceId option:selected").text() + '</b>');
        $("#id-lbl-header").html('Default Header: <b>' + $('[name="defaultHeader"]').val() + '</b><br/>');
        $("#id-lbl-type").html('Application Type: <b>' + $("#idApplicationType option:selected").text() + '</b>');
        $("#id-lbl-default_tariff").html('Default Tariff: <b>' + $("#idChargingParam option:selected").text() + '</b>');
        $("#id-lbl-interval").html('Delivery Interval: <b>' + $("#idPushDayInterval option:selected").text() + ' day(s) </b>');
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
                                '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled","disabled");
        $.post("{{ url('/api/create_new_application') }}", $( "#myform" ).serialize(),
            function (data) {
                $("#btnCommitChanges").removeAttr('disabled');
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success:&nbsp;The new application ' + $('[name="applicationName"]').val() + ' has been added</div>');
                }
            })
            .done(function () {
                $("#btnCommitChanges").removeAttr('disabled');
                 //alert("second success");
             })
            .fail(function () {
                $("#btnCommitChanges").removeAttr('disabled');
                $("#idAlertSuccess").html(
                    'Error while creating new application! Please try again later or contact system Administrator')
             })
            .always(function () {
                $("#btnCommitChanges").removeAttr('disabled');
                 //alert("finished");
            })
            .fail(function (jqXHR, textStatus) {
                $("#btnCommitChanges").removeAttr('disabled');
                if (jqXHR.status == 401) window.location = "{{ url('/login') }}";
            })
        ;
    });

    $("#idApplicationName").val($("#idServiceId option:selected").text());
</script>
