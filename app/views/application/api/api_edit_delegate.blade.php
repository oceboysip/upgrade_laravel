<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<br>
	<h4 id="myModalLabel" class="semi-bold">Edit Businqeess Delegate for {{ $application->name }}</h4>
	<p class="no-margin">Carefully review the values</p>
	<br>
</div>
<div class="modal-body">
	<form id="myform" method="POST">
		<input type="hidden" name="applicationId" value="{{ $applicationId }}">
		<div class="row form-row">
			<div class="col-md-10">
				<div class="form-group">
					<label class="form-label">Default Header</label><br/>
					<div class="controls">
						<div class="controls">
							@if($applicationDelegate)
							<input id="idDefaultHeader" style="width:60%;" value="{{ $applicationDelegate->default_header }}" placeholder="Default Header"
								   type="text" class="form-control" name="defaultHeader">
							@else
							<input id="idDefaultHeader" style="width:60%;" value="{{ $application->default_header }}" placeholder="Default Header. i.e: HEADER"
								   type="text" class="form-control" name="defaultHeader">

							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-label">Pull</label><br/>
					<span class="help">URL called during pull requests</span>
					<div class="controls">
						@if($applicationDelegate)
						<input id="idPull" value="{{ $applicationDelegate->pull_delegate }}" placeholder="Pull Delegate. i.e: http://127.0.0.1:9900/mts/service/any/pull.jsp"
							   type="text" class="form-control" name="pullDelegate">
						@else
						<input id="idPull" placeholder="Pull Delegate. i.e: http://127.0.0.1:9900/mts/service/any/pull.jsp"
							   type="text" class="form-control" name="pullDelegate">
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row form-row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-label">Registration Done</label><br/>
					<span class="help">URL called during successful registration</span>
					<div class="controls">
						@if($applicationDelegate)
						<input id="idRegDone" value="{{ $applicationDelegate->reg_done_delegate }}" placeholder="Registration Success Delegate. i.e: http://127.0.0.1:9900/mts/service/any/reg_done.jsp"
							   type="text" class="form-control" name="regDoneDelegate">
						@else
						<input id="idRegDone" placeholder="Registration Success Delegate. i.e: http://127.0.0.1:9900/mts/service/any/reg_done.jsp"
							   type="text" class="form-control" name="regDoneDelegate">

						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row form-row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-label">Registration Error</label><br/>
					<span class="help">URL called during error registration</span>
					<div class="controls">
						@if($applicationDelegate)
						<input id="idRegError" value="{{ $applicationDelegate->reg_error_delegate }}" placeholder="Registration Error Delegate. i.e: http://127.0.0.1:9900/mts/service/any/reg_done_error.jsp"
							   type="text" class="form-control" name="regErrorDelegate">
						@else
						<input id="idRegError" placeholder="Registration Error Delegate. i.e: http://127.0.0.1:9900/mts/service/any/reg_done_error.jsp"
							   type="text" class="form-control" name="regErrorDelegate">

						@endif
					</div>
				</div>
			</div>
		</div>

		<div class="row form-row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-label">Unregistration Done</label><br/>
					<span class="help">URL called during success unregistration</span>
					<div class="controls">
						@if($applicationDelegate)
						<input id="idUnregDone" value="{{ $applicationDelegate->unreg_done_delegate }}" placeholder="Unregistration Success Delegate. i.e: http://127.0.0.1:9900/mts/service/any/unreg_done.jsp"
							   type="text" class="form-control" name="unregDoneDelegate">
						@else
						<input id="idUnregDone" placeholder="Unregistration Success Delegate. i.e: http://127.0.0.1:9900/mts/service/any/unreg_done.jsp"
							   type="text" class="form-control" name="unregDoneDelegate">
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row form-row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-label">Unregistration Error</label><br/>
					<span class="help">URL called during error unregistration</span>
					<div class="controls">
						@if($applicationDelegate)
						<input id="idUnregError" value="{{ $applicationDelegate->unreg_error_delegate }}" placeholder="Unregistration Error Delegate. i.e: http://127.0.0.1:9900/mts/service/any/unreg_done_error.jsp"
							   type="text" class="form-control" name="unregErrorDelegate">
						@else
						<input id="idUnregError" placeholder="Unregistration Error Delegate. i.e: http://127.0.0.1:9900/mts/service/any/unreg_done_error.jsp"
							   type="text" class="form-control" name="unregErrorDelegate">
						@endif
					</div>
				</div>
			</div>
		</div>

		<div class="row form-row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-label">Formatter</label><br/>
					<span class="help">URL called during sending daily push, to format the content</span>
					<div class="controls">
						@if($applicationDelegate)
						<input id="idFormatter" value="{{ $applicationDelegate->formatter_delegate }}" placeholder="Formatter Delegate. i.e: http://127.0.0.1:9900/mts/service/any/formatter.jsp"
							   type="text" class="form-control" name="formatterDelegate">
						@else
						<input id="idFormatter" placeholder="Formatter Delegate. i.e: http://127.0.0.1:9900/mts/service/any/formatter.jsp"
							   type="text" class="form-control" name="formatterDelegate">
						@endif
					</div>
				</div>
			</div>
		</div>


	</form>
	<div id="waitDelegateCommit" class="row form-row"></div>
	<div id="idDelegateErrorMessage" class="row form-row"></div>
	<div id="idDelegateAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
	<button type="button" id="btnDelegateDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" id="btnDelegateCommitChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">
	var doneEdit = false;

	$("#btnDelegateDismiss").click(function () {
		if (doneEdit) {
			window.location = "{{ url('/admin/manage_application') }}?telcoId={{ $telcoId }} " +
								"&givenSdc={{ $sdc }}";
		}
	});

	$("#btnDelegateCommitChanges").click(function () {
		$("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
		'<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

		$("#btnDelegateCommitChanges").attr("disabled", "disabled");
		$.post("{{ url('/api/edit_application_delegate') }}", $("#myform").serialize(),
			function (data) {
				$("#waitDelegateCommit").html('');
				if (data['Error']) {
					$("#idDelegateErrorMessage").html(
						'<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
						'Error while saving to database: ' + data['Message'] + '</div>');
				} else {
					doneEdit = true;
					$("#idDelegateAlertSuccess").html(
						'<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
						'Success&nbsp;edit business application {{ $application->name }} </div>');
				}
			})
			.done(function () {
				//alert("second success");
			})
			.fail(function (jqXHR, textStatus) {
				if (jqXHR.status == 401) {
					window.location = "{{ url('/login') }}";
				}
				else {
					$("#idDelegateAlertSuccess").html(
						'Error while edit application! Please try again later or contact system Administrator')
				}
			})
			.always(function () {
				//alert("finished");
			});
	});
</script>
