<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <br>
    <h4 id="myModalLabel" class="semi-bold">Edit Businness Application {{ $application->name }}</h4>
    <p class="no-margin">Carefully review the values</p>
    <br>
</div>
<div class="modal-body">
    <form id="myform" method="POST">
        <input type="hidden" name="applicationId" value="{{ $applicationId }}">
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Name</label><br/>
                    <div class="controls">
                        <div class="controls">
                            <input id="idApplicationName" style="width:60%;" value="{{ $application->name }}" placeholder="Application Name"
                                   type="text" class="form-control" name="applicationName">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Default Header</label><br/>
                    <span class="help">Default header that will be included in every message</span>
                    <div class="controls">
                        <input id="idDefaultHeader" value="{{ $application->default_header }}" placeholder="Default Header. i.e: HUMOR"
                               type="text" class="form-control" name="defaultHeader">
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Gateway Service</label><br/>
                    <div class="controls">
                        <select id="idServiceId" style="width:80%;" name="serviceId">
                            <optgroup label="Choose Group">
                                @foreach ($allService as $each_service)
                                    @if ($each_service->id == $application->service_id)
                                    <option value="{{ $each_service->id }}" selected="selected">{{ $each_service->name }}</option>
                                    @else
                                    <option value="{{ $each_service->id }}">{{ $each_service->name }}</option>
                                    @endif
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Type</label><br/>
                    <div class="controls">
                        <select id="idApplicationType" style="width:80%;" name="applicationType">
                            @foreach ($applicationType as $each_type)
                            @if ($each_type->id == $application->application_type_id)
                            <option value="{{ $each_type->id }}" selected="selected">{{ $each_type->name }}</option>
                            @else
                            <option value="{{ $each_type->id }}">{{ $each_type->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Default Tariff</label><br/>
                    <div class="controls">
                        <select id="idChargingParam" style="width:80%;" name="defaultChargingParam">
                            <optgroup label="Choose Group">
                                @foreach ($allCharging as $each_charging)
                                @if ($each_charging->code == $application->default_charging_param)
                                <option value="{{ $each_charging->code }}" selected="selected">{{ $each_charging->code }}</option>
                                @else
                                <option value="{{ $each_charging->code }}">{{ $each_charging->code }}</option>
                                @endif
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Delivery Interval (Days)</label><br/>
                    <div class="controls">
                        <select id="idPushDayInterval" style="width:80%;" name="pushDayInterval">
                            @for ($i = 0; $i < 8; $i++)
                            @if ($i == $application->push_day_interval)
                            <option value="{{ $i }}" selected="selected">{{ $i }}</option>
                            @else
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endif
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Active</label><br/>
                    <div class="controls">
                        <select id="idApplicationDisabled" style="width:40%;" name="isActive">
                            <option value="1" {{ $application->is_active ? "selected='1'" : "" }}>Yes</option>
                            <option value="0" {{ $application->is_active ? "" : "selected='1'" }}>No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="waitCommit" class="row form-row"></div>
    <div id="idErrorMessage" class="row form-row"></div>
    <div id="idAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
    <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">
    var doneEdit = false;
    $("#idApplicationDisabled").select2();
    $("#idChargingParam").select2();
    $("#idServiceId").select2();
    $("#idPushDayInterval").select2();
    $("#idApplicationType").select2();

    $("#btnDismiss").click(function () {
        if (doneEdit) {
            window.location = "{{ url('/admin/manage_application') }}?telcoId="
                + $("#telcoSelect").val() + "&givenSdc=" + $("#sdcSelect").val();
        }
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled", "disabled");
        $.post("{{ url('/api/edit_application') }}", $("#myform").serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success&nbsp;edit business application {{ $application->name }} </div>');
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while edit application! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });
</script>
