@if (isset($the_data))
<div class="span12">
    <div class="grid simple">
        <div class="grid-title">
            @if (isset($telcoName) && isset($sdc))
            <h4>List of Business Application on <span class="semi-bold">{{ $telcoName }} - {{ $sdc }}</span></h4>
            @else
            <h4>List of Business Application</h4>
            @endif
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>


        <div class="grid-body">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-hover table-bordered table-responsive datatable" id="example2">
            <thead>
            <tr>
                <th data-class="expand">Name</th>
                <th data-class="expand">Gateway Service</th>
                <th>Type</th>
                <th>Default Tariff</th>
                <th>Active</th>
                <th>Default Header</th>
                <th>Delivery Interval</th>
                <th data-hide='phone,tablet'>Context Tariff</th>
                <th data-hide='phone,tablet'>Delegates</th>
                <th data-hide='phone,tablet'>Date Created</th>
                <th data-hide='phone,tablet'>Date Modified</th>
                <th> </th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($the_data) < 1)
                <tr class="even gradeA">
                    <td colspan=14> No data </td>
                </tr> 
            @else    
                @foreach ($the_data as $each_data)
                    <tr class="even gradeA">
                        <!-- <td>{{ $each_data->id }}</td> -->
                        <td title="Application Id: {{ $each_data->id }}">{{ $each_data->name }}</td>
                        <td class="center">{{ $each_data->service_name }}</td>
                        <td class="center">{{ $each_data->application_type }}</td>
                        <td class="center">{{ $each_data->default_charging_param }}</td>
                        <!--
                        <td class="center">Evaluator {{ $each_data->id }}</td>
                        <td class="center">Route Eval {{ $each_data->id }}</td>
                        <td class="center">Simple</td>
                        <td class="center">12TRX34p TCP:8080</td>
                        -->
                        @if ($each_data->is_active)
                        <td class="center"><span class="label label-success">Yes</span></td>
                        @else
                        <td class="center"><span class="label label-important">No</span></td>
                        @endif
                        <td class="center">{{ $each_data->default_header }}</td>
                        <td class="center">{{ $each_data->push_day_interval }} day(s)</td>
                        <td class="center">
                            <div class="grid manageCharging simple no-border">
                                <div class="grid-title no-border descriptive clickable" applicationId="{{ $each_data->id }}">
                                    <div class="actions">
                                        See Tariff&nbsp;&nbsp;&nbsp;<a class="view" href="javascript:;"><i class="fa fa-search"></i></a>
                                    </div>
                                    <br/>
                                    <button applicationId="{{ $each_data->id }}"
                                            applicationName="{{ $each_data->name }}"
                                            sdc="{{ $each_data->sdc }}" telcoId="{{ $each_data->telcoId }}"
                                            class="btn btn-small btn-white btn-cons classBtnManageParam"
                                            type="button" data-toggle="modal" data-target="#myModalParam">Manage</button>
                                </div>
                                <div class="grid-body no-border" style="display:none">
                                    <div class="post">
                                        <div class="info-wrapper">
                                            <div class="info" id="ctx-{{ $each_data->id }}"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </td>
                        <td class="center">
                            <button applicationId="{{ $each_data->id }}" sdc="{{ $each_data->sdc }}" telcoId="{{ $each_data->telcoId }}"
                                    class="btn btn-small btn-white btn-cons btnManageDelegate"
                                    type="button" data-toggle="modal" data-target="#myModalDelegate">Edit</button>
                        </td>
                        <td class="center">{{ $each_data->dtcreated }}</td>
                        <td class="center">{{ $each_data->dtmodified }}</td>
                        <td class="center">
                            <button applicationId="{{ $each_data->id }}" sdc="{{ $each_data->sdc }}" telcoId="{{ $each_data->telcoId }}"
                                    class="btn btn-small btn-white btn-cons btnEdit"
                                    type="button" data-toggle="modal" data-target="#myModal">Edit</button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalContent"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalParam" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalParamContent"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalDelegate" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalDelegateContent"></div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(".btnEdit").click(function() {
        $.get("{{ url('/api/edit_application') }}" +
            "?applicationId=" + this.getAttribute('applicationId') + "&sdc=" + this.getAttribute('sdc') +"&telcoId=" + this.getAttribute('telcoId'),
            {ajax: 'true'},
            function (data) {
                $("#theModalContent").html('');
                $("#theModalContent").html(data);
            }
        );
    });

    $(".btnManageDelegate").click(function() {
        $.get("{{ url('/api/edit_application_delegate') }}" +
            "?applicationId=" + this.getAttribute('applicationId') + "&sdc=" + this.getAttribute('sdc') +"&telcoId=" + this.getAttribute('telcoId'),
            {ajax: 'true'},
            function (data) {
                $("#theModalDelegateContent").html('');
                $("#theModalDelegateContent").html(data);
            }
        );
    });

    $(".classBtnManageParam").click(function() {
        viewApplicationTariffList(this.getAttribute('applicationName'), this.getAttribute('applicationId'),
                                this.getAttribute('sdc'), this.getAttribute('telcoId')
        );
    });

    function viewApplicationTariffList(applicationName, applicationId, sdc, telcoId) {
        $.get("{{ url('/api/get_application_charging_param') }}" + "?applicationId=" + applicationId +
            "&applicationName=" + encodeURIComponent(applicationName) + "&sdc=" + sdc + "&telcoId=" + telcoId,
            {ajax: 'true'},
            function (data) {
                $("#theModalParamContent").html('');
                $("#theModalParamContent").html(data);
            }
        ).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while getting application charging param! Please try again later or contact system Administrator')
                }
            })
        ;
    }

    $('.manageCharging .clickable').on('click', function () {
        var el = jQuery(this).parents(".manageCharging").children(".grid-body");
        //console.log(el);
        if (!el.is(":visible") ) {
            var applicationId = this.getAttribute('applicationId');
            $("#ctx-" + applicationId).html('<div class="span12"><div class="col-md-8 col-sm-8 col-xs-8">' +
                'Loading...<i class="icon-spinner icon-spin icon-large icon-2x"></i></div></div>');
            $.getJSON("{{ url('/api/get_charging_param_for_application') }}" + "?applicationId=" + applicationId,
                {ajax: 'true'},
                function (data) {
                    var str = '';
                    $.each(data, function (i, item) {
                        str += 'Reg: <b>' + item.reg + '</b><br/>';
                        str += 'Reg Error: <b>' + item.reg_error + '</b><br/>';
                        str += 'Unreg: <b>' + item.unreg + '</b><br/>';
                        str += 'Unreg Error: <b>' + item.unreg_error + '</b><br/>';
                        str += 'Pull: <b>' + item.pull + '</b><br/>';
                        str += 'Pull Error: <b>' + item.pull_error + '</b><br/>';
                    });
                    $("#ctx-" + applicationId).html('');
                    $("#ctx-" + applicationId).html(str);
                }
            ).fail(function (jqXHR, textStatus) {
                    if (jqXHR.status == 401) {
                        window.location = "{{ url('/login') }}";
                    }
                    else {
                        $("#idAlertSuccess").html(
                            'Error while getting application tariff! Please try again later or contact system Administrator')
                    }
                })
            ;
        }
        el.slideToggle(200);
    });
</script>
@endif
