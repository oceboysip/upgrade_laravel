<?php
/**
 * This is the main for Manage Service List page
 *
 */
?>
<div class="row">
    <div class="grid simple">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Manage Business <span class="semi-bold">Application</span></h3>
                    <p>Manage application which apply to a service</p>
                    <p>
                        <select id="sdcSelect" style="width:20%;" name="sdc">
                            <optgroup label="Choose SDC">
                                @foreach ($sdc as $each_sdc)
                                <option value="{{ $each_sdc->name }}" {{ (isset($givenSdc) && ($each_sdc->name == $givenSdc)) ? "selected='1'" : "" }}>{{ $each_sdc->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                        <select id="telcoSelect" style="width:20%;" name="telcoId">
                            <optgroup label="Choose Telco">
                                @foreach ($telco as $each_telco)
                                <option value="{{ $each_telco->id }}" {{ (isset($telcoId) && ($each_telco->id == $telcoId)) ? "selected='1'" : "" }}>{{ $each_telco->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </p>
                    <br/>
                    <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewListApplication"
                       class="btn btn-primary my-colorpicker-control" href="#"
                       style="margin-right: 30px;"
                       data-colorpicker-guid="8">View</a>

                    <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="createNewApplication"
                       class="btn btn-primary my-colorpicker-control" href="#"
                       style="margin-right: 30px;"
                       data-colorpicker-guid="8">Add New Application</a>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="theData"></div>
