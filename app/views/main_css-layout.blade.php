<?php
/**
 * List of all CSS related stuff
 *
 */
?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="PhynxMessaging Appliance - Dashboard" name="description"/>
    <meta content="Phynx" name="author"/>
    <link href="{{ asset('assets/plugins/jquery-polymaps/style.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/shape-hover/css/demo.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/shape-hover/css/component.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/owl-carousel/owl.theme.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <!-- Not using it now
    <link rel="stylesheet" href="assets/plugins/jquery-ricksaw-chart/css/rickshaw.css" type="text/css" media="screen">
    -->
    <link href="{{ asset('assets/plugins/jquery-isotope/isotope.css') }}" rel="stylesheet" type="text/css"/>

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/magic_space.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/tiles_responsive.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->