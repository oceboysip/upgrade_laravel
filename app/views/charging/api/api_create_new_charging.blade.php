@if (isset($the_data))
@endif
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST">
            <h4>Add New Tariff for <b>{{ $telcoName }} - {{ $sdc }}</b></h4><br/>
            <!--
            <table>
                <tr>
                    <td>
                        <div class="form-group">
                            <label class="form-label">SDC</label>
                            <div class="controls"><input type="text" class="form-control" disabled="disabled"
                                                         value="{{ $sdc }}"></div>
                        </div>
                    </td>
                    <td style="padding-left: 30px;">
                        <div class="form-group">
                            <label class="form-label">Telco</label>
                            <div class="controls">
                                <div class="controls">
                                    <input type="text" class="form-control" disabled="disabled" value="{{ $telcoName }}">
                                    <input type="hidden" value="{{ $telcoId }}">
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            -->
            <input type="hidden" name="sdc" value="{{ $sdc }}">
            <input type="hidden" name="telcoId" value="{{ $telcoId }}">
            <input type="hidden" name="telcoName" value="{{ $telcoName }}">
            <input type="hidden" name="chargingCode">

            <div class="form-group">
                <label class="form-label">MT Type</label>
                <span class="help">Type of Mobile Terminated</span>
                <div class="controls">
                    <select id="mtTypeSelect" style="width:20%;" name="mtType">
                        <option value="PUSH">PUSH</option>
                        <option value="PULL">PULL</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Telco SID</label>
                <span class="help">Telco Service Identifier</span>
                <div class="controls" style="width:30%;">
                    <input type="text" class="form-control" name="telcoSid" id="idTelcoSid">
                    <label class="error" style="display: none" for="idTelcoSid" id="errorTelcoSid">Telco SID is required</label>
                </div>
            </div>
            <table style="width:90%;">
                <tr>
                    <td>
                        <div class="form-group">
                            <label class="form-label">End User Price</label><br/>
                            <span class="help">The price user is charged (in IDR)</span>

                            <div class="controls">
                                <input id="idEuPrice" style="width:60%;" type="text" class="form-control" name="euPrice">
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 30px;">
                        <div class="form-group">
                            <label class="form-label">Our Price</label><br/>
                            <span class="help">The price for Content Provider (in IDR)</span>
                            <div class="controls">
                                <input id="idCpPrice" style="width:60%;" type="text" class="form-control" name="cpPrice">
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 30px;">
                        <div class="form-group">
                            <label class="form-label">Bearer Cost</label><br/>
                            <span class="help">Bearer Cost (in IDR)</span>
                            <div class="controls">
                                <input id="idCostBearer" style="width:60%;" type="text" class="form-control" name="costBearer">
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <textarea id="text-description" name="description"
                      placeholder="Description of this charging" class="span12" cols="100" rows="10"></textarea>

            <br/>
            <br/>
            <button id="btnSave" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Save</button>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <br>
                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                            <h4 id="myModalLabel" class="semi-bold">Summary new tariff for {{ $telcoName }} - {{ $sdc }}</h4>
                            <p class="no-margin">Please review the value again</p>
                            <br>
                        </div>
                        <div class="modal-body">
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-telcosid" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-code" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-eu_price" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-cp_price" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-cb_price" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-descr" class="form-label"></label><br/>
                                </div>
                            </div>
                            <div id="waitCommit" class="row form-row"></div>
                            <div id="idErrorMessage" class="row form-row"></div>
                            <div id="idAlertSuccess" class="row form-row"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    function refillDescription() {
        var _v = ($("#mtTypeSelect").val() == 'PUSH') ? "Push" : "Pull";
        $("#text-description").text(_v + " IDR. " + ($("#idCpPrice").val() ? $("#idCpPrice").val() : 0));
        $("#id-lbl-telcosid").html($("#idTelcoSid").val());
    }
    $("#mtTypeSelect").select2()
        .on("change", function () {refillDescription();});
    $("#idCpPrice")
        .on("focusout", function () {refillDescription();})
        .on("change", function () {refillDescription();});
    $("#myform").validate({
        rules: {
            telcoSid: {required: true},
            eu_price: {required: true,number: true},
            cp_price: {required: true, number: true}
        }
    });

    $("#btnSave").click(function() {
        var telcoSid = $("#idTelcoSid").val();
        //alert(telcoSid);
        //if ($("#idTelcoSid").isEmpty) {
        if (telcoSid == '') {
            $("#errorTelcoSid").show();
            return false;
        } else {
            $("#id-lbl-telcosid").html('Telco SID: <b>' + telcoSid + '</b>');
            var gCode = $('[name="sdc"]').val() + $('[name="mtType"]').val() + $('[name="cpPrice"]').val();
            $('[name="chargingCode"]').val(gCode);
            $("#id-lbl-code").html('Generated Code: <b>' + gCode + '</b>');
            if ($('[name="euPrice"]').val() == '') $('[name="euPrice"]').val('0');
            $("#id-lbl-eu_price").html('End User Price: <b>IDR. ' + $('[name="euPrice"]').val() + '</b>');
            if ($('[name="cpPrice"]').val() == '') $('[name="cpPrice"]').val('0');
            $("#id-lbl-cp_price").html('Price for Content Provider: <b>IDR. ' + $('[name="cpPrice"]').val() + '</b>');
            if ($('[name="costBearer"]').val() == '') $('[name="costBearer"]').val('0');
            $("#id-lbl-cb_price").html('Cost Bearer Price: <b>IDR. ' + $('[name="costBearer"]').val() + '</b>');
            $("#id-lbl-descr").html($('[name="description"]').val());

        }
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
                                '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled","disabled");
        $.post("{{ url('/api/create_new_charging') }}", $( "#myform" ).serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success:&nbsp;The new charging ' + $('[name="chargingCode"]').val() + ' has been added</div>');
                }
            })
            .done(function () {
                 //alert("second success");
             })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while creating new charging! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                 //alert("finished");
            });
    });

    refillDescription();
</script>
