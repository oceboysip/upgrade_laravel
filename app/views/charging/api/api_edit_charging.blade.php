<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <br>
    <h4 id="myModalLabel" class="semi-bold">Edit Tariff for {{ $telco }} - {{ $chargingCode }} </h4>

    <p class="no-margin">Review the values</p>
    <br>
</div>
<div class="modal-body">
    <form id="myform" method="POST">
        <input type="hidden" name="chargingId" value="{{ $chargingId }}">
        <input type="hidden" name="chargingCode" value="{{ $chargingCode }}">

        <div class="row form-row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="form-label">Telco SID</label><br/>
                    <span class="help">The SID that tight between MT and related charging flag. Make sure you know what you are doing</span>
                    <div class="controls">
                        <div class="controls">
                            <input id="idTelcoSid" style="width:40%;" value="{{ $charging->telco_sid }}" placeholder="Telco SID"
                                   type="text" class="form-control" name="telcoSid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">End User Price</label><br/>
                    <span class="help">The price user is charged (in IDR)</span>
                    <div class="controls">
                        <input id="idEuPrice" style="width:60%;" value="{{ $charging->eu_price }}" placeholder="EU Price"
                               type="text" class="form-control" name="euPrice">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Content Provider Price</label><br/>
                    <span class="help">CP Price (in IDR)</span>
                    <div class="controls">
                        <input id="idEuPrice" style="width:60%;" value="{{ $charging->cp_price }}" placeholder="CP Price"
                               type="text" class="form-control" name="cpPrice">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Cost Bearer</label><br/>
                    <span class="help">(in IDR)</span>
                    <div class="controls">
                        <input id="idEuPrice" style="width:60%;" value="{{ $charging->cost_bearer }}" placeholder="Cost Bearer"
                               type="text" class="form-control" name="costBearer">
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="form-label">Status Active</label><br/>
                    <div class="controls">
                        <select id="idChargingDisabled" style="width:40%;" name="isActive">
                            <option value="1" {{ $charging->is_active ? "selected='1'" : "" }}>Yes</option>
                            <option value="0" {{ $charging->is_active ? "" : "selected='1'" }}>No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-10">
                <textarea id="text-description" name="description"
                  placeholder="Description of this charging" class="span10" cols="70" rows="4">{{ $charging->descr }}</textarea>
            </div>
        </div>
    </form>
    <div id="waitCommit" class="row form-row"></div>
    <div id="idErrorMessage" class="row form-row"></div>
    <div id="idAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
    <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">
    var doneEdit = false;
    $("#idChargingDisabled").select2();

    $("#btnDismiss").click(function () {
        if (doneEdit) {
            window.location = "{{ url('/admin/manage_charging') }}?telcoId="
                + $("#telcoSelect").val() + "&givenSdc=" + $("#sdcSelect").val();
        }
    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
            '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled", "disabled");
        $.post("{{ url('/api/edit_charging') }}", $("#myform").serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success&nbsp;edit charging ' + $('[name="chargingCode"]').val() + '{{ $telco }} </div>');
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while edit charging! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });
</script>
