@if (isset($the_data))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>List charging <span class="semi-bold">Result</span></h4>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <!-- <a href="#grid-config" data-toggle="modal" class="config"></a> -->
                <a href="javascript:;" class="reload"></a>
                <!-- <a href="javascript:;" class="remove"></a> -->
            </div>
        </div>


        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <!-- <th>Charging ID</th> -->
                <th>Code</th>
                <th>End User Price (IDR)</th>
                <th>CP Price (IDR)</th>
                <th>Telco SID</th>
                <th>Cost Bearer (IDR)</th>
                <th>Active</th>
                <th>Description</th>
                <th> </th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($the_data) < 1)) 
                <tr class="even gradeA">
                    <td colspan=7> No data </td>
                </tr> 
            @else    
                @foreach ($the_data as $each_data)
                    <tr class="even gradeA">
                        <!-- <td>{{ $each_data->id }}</td> -->
                        <td title="Charging Id: {{ $each_data->id }}">{{ $each_data->code }}</td>
                        <td class="center">{{ $each_data->eu_price }}</td>
                        <td class="center">{{ $each_data->cp_price }}</td>
                        <td>{{ $each_data->telco_sid }}</td>
                        <td class="center">{{ $each_data->cost_bearer }}</td>
                        @if ($each_data->is_active)
                        <td class="center"><span class="label label-success">Yes</span></td>
                        @else
                        <td class="center"><span class="label label-important">No</span></td>
                        @endif
                        <td class="center">{{ $each_data->descr }}</td>
                        <td class="center">
                            <button chargingId="{{ $each_data->id }}" chargingCode="{{ $each_data->code }}"
                                    class="btn btn-small btn-white btn-cons btnEdit"
                                    type="button" data-toggle="modal" data-target="#myModal">Edit</button>
                        </td>
                    </tr>                    
                @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalContent"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".btnEdit").click(function() {
        $.get("{{ url('/api/edit_charging') }}" + "?chargingId=" + this.getAttribute('chargingId')
            + "&code=" + this.getAttribute('chargingCode'),
            {ajax: 'true'},
            function (data) {
                $("#theModalContent").html('');
                $("#theModalContent").html(data);
            }
        ).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while calling edit charging! Please try again later or contact system Administrator')
                }
            })
        ;
    });
</script>

@endif
