<!-- BEGIN SIDEBAR -->
<div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper" id="main-menu-wrapper">
        <!-- END MINI-PROFILE -->
        <!-- BEGIN SIDEBAR MENU -->
        <p class="menu-title">
            <!--
            <span class="pull-right">
                <a href="javascript:;"><i class="fa fa-refresh"></i></a>
            </span>
            -->
        </p>
        <ul>
            <li class="start active open ">
                <a href="{{ route('main') }}">
                    <i class="icon-custom-home"></i><span class="title">Dashboard</span>
                    <!-- <span class="selected"></span> <span class="arrow open"></span> -->
                </a>
                <!--
                <ul class="sub-menu">
                    <li><a href="dashboard_v1.html"> Dashboard v1 </a></li>
                    <li class="active"><a href="index.html "> Dashboard v2 <span
                            class=" label label-info pull-right m-r-30">NEW</span></a></li>
                </ul>
                -->
            </li>

            @include("sidebar_list-layout")

            <!-- End of 'main' menus -->

        </ul>

        <div class="clearfix"></div>

        <!-- END SIDEBAR MENU -->
    </div>
</div>

<!-- End of top sidebar -->

<!--div class="footer-widget">
    <!--
    <div class="progress transparent progress-small no-radius no-margin">
        <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar"></div>
    </div>
    -->
    <!--div class="pull-right">
        <!--
        <div class="details-status">
            <span data-animation-duration="560" data-value="86" class="animate-number"></span>%
        </div>
        -->
        <!--a href="{{ route('logout') }}"><i class="fa fa-power-off"></i></a>
    </div>
</div-->

<!-- END SIDEBAR -->
