@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    @if (\Session::has('err'))
                        <div class="alert alert-error">
                            <p>{{ \Session::get('err') }}</p>
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif

                    <h3>ABUSE  <span class="semi-bold">Chat</span></h3>
                </div>
            </div>
        @if (isset($results))
        <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            {{ $results->links() }}
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 10%">Date</th>
                                    <th style="width: 15%">Pelapor</th>
                                    <th style="width: 15%">Terlapor</th>
                                    <!-- <th style="width: 10%">Category</th> -->
                                    <th style="width: 45%">Reason</th>
                                    <th style="text-align:center; width: 5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    <tr>
                                        <td>{{ $data->created_at }}</td>
                                        <td>{{ $data->pelapor_name }}</td>
                                        <td>{{ $data->terlapor_name }}</td>
                                        <!-- <td>{{ $data->category }}</td> -->
                                        <td>{{ $data->reason }}</td>
                                       
                                        <td>
                                            <a href="{{url('monitoring/abuse/detail/'.$data->id.'?ref='.$ref)}}" class="btn btn-small btn-success">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $results->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>

    </div>
@stop

