@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    @if (\Session::has('err'))
                        <br>
                        <div class="alert alert-error">
                            <p>{{ \Session::get('err') }}</p>
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <br>
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif

                    <h3>Avatar Moderation  </h3>
                    <form>
                        Name:<br/>
                        <input type="text" name="searchName" id="searchName" placeholder="Search...">

                        <br/>
                        </p>

                        <button type="submit" name="btnDate" id="btnDate" class="btn btn-success">View</button>
                    </form>
                </div>
            </div>
        </div>

        @if (isset($results))
        <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            {{ $results->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 15%">Name</th>
                                    <th style="width: 15%">User Status</th>
                                    <th style="width: 35%">Avatar</th>
                                    <th style="width: 5%">Active</th>
                                    <th style="text-align:center; width: 5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    <tr>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->user_status }}</td>
                                        <td>
                                            <a href="https://img-mon.kepo.app/p/{{ $data->avatar }}_640x640.jpg" target="_blank"><img src="https://img-mon.kepo.app/p/{{ $data->avatar }}_640x640.jpg" height="150" width="150"></a>
                                        </td>
                                        <td>
                                            {{ ($data->n_status <= 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-default">Non-Active</span>') }}
                                        </td>
                                        <td>
                                            <a href="{{url('monitoring/abuse/avatar/del/'.$data->id.'?ref='.$ref)}}" class="btn btn-small btn-warning">
                                                Delete Avatar
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $results->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>

    </div>
@stop

