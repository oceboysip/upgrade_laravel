@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    @if (\Session::has('err'))
                        <br>
                        <div class="alert alert-error">
                            <p>{{ \Session::get('err') }}</p>
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <br>
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif

                    <h3>Rejected Avatar</h3>
                    <form>
                        Name:<br/>
                        <input type="text" name="searchName" id="searchName" placeholder="Search...">

                        <br/>
                        </p>

                        <button type="submit" name="btnDate" id="btnDate" class="btn btn-success">View</button>
                    </form>
                </div>
            </div>
        </div>

        @if (isset($results))
        <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            {{ $results->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 15%">MSISDN</th>
                                    <th style="width: 15%">ID</th>
                                    <th style="width: 25%">User Name</th>
                                    <th style="width: 10%">Avatar</th>
                                    <th style="width: 10%">Status</th>
                                    <th style="text-align:center; width: 35%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    <tr>
                                        <td>{{ $data->msisdn }}</td>
                                        <td>{{ $data->user_id }}</td>
                                        <td>{{ $data->user_name }}</td>
                                        <td>
                                            <a href="{{ $data->user_avatar }}" target="_blank"><img src="{{ $data->user_avatar }}" height="50" width="50"></a>
                                        </td>
                                        <td>
                                            {{ ($data->n_status <= 1 ? '<span class="label label-success">New</span>' : '<span class="label label-default">Reviewed</span>') }}
                                            
                                        </td>
                                        <td>
                                            @if ($data->n_status == 1)
                                            <a href="{{url('/monitoring/abuse/reject/'.$data->id.'/approve?ref='.$ref)}}" class="btn btn-small btn-danger">
                                                Approve
                                            </a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('/monitoring/abuse/reject/'.$data->id.'/close?ref='.$ref)}}" class="btn btn-small btn-success">
                                                DONE!
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $results->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>

    </div>
@stop

