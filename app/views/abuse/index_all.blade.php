@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    @if (\Session::has('err'))
                        <br>
                        <div class="alert alert-error">
                            <p>{{ \Session::get('err') }}</p>
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <br>
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif

                    <h3>Kepo  <span class="semi-bold">Report {{ $range }}</span></h3>
                    <form>

                        Date Range:<br/>
                        <input type="text" name="dtStart" id="dtStart" class="theDate">
                        &nbsp; - &nbsp;
                        <input type="text" name="dtEnd" id="dtEnd" class="theDate">

                        <br/>
                        <br/>

                        Search (by name):<br/>
                        <input type="text" name="searchName" id="searchName">

                        <br/>
                        </p>

                        <button type="submit" name="btnDate" id="btnDate" class="btn btn-success">View</button>
                    </form>
                </div>
            </div>
        </div>

        @if (isset($results))
        <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            {{ $results->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 10%">Date</th>
                                    <th style="width: 15%">Pelapor</th>
                                    <th style="width: 15%">Terlapor</th>
                                    <th style="width: 5%">Category</th>
                                    <th style="width: 45%">Reason</th>
                                    <th style="width: 5%">Status</th>
                                    <th style="text-align:center; width: 5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    <tr>
                                        <td>{{ $data->created_at }}</td>
                                        <td>{{ $data->pelapor_name }}</td>
                                        <td>{{ $data->terlapor_name }}</td>
                                        <td>
                                            @if($data->category <= 3)
                                                Gambar/Avatar
                                            @elseif($data->category <= 6)
                                                User/Behavior/Chat
                                            @else
                                                Umum/Content
                                            @endif
                                        </td>
                                        <td>{{ $data->reason }}</td>
                                        <td>
                                            {{ ($data->n_status <= 1 ? '<span class="label label-warning">New</span>' : '<span class="label label-success">Closed</span>') }}
                                        </td>
                                        <td>
                                            <a href="{{url('monitoring/abuse/detail/'.$data->id.'?ref='.$ref)}}" class="btn btn-small btn-success">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $results->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>

    </div>
@stop

