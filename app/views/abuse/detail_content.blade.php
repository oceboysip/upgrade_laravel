@extends('content.manage_content-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
    @if (\Session::has('err'))
        <div class="alert alert-error">
            <p>{{ \Session::get('err') }}</p>
        </div>
    @endif

        <form id="myform" method="POST" action="{{url('/monitoring/abuse/process/'.$id.'?ref='.$ref)}}">
            <h4>Detail Remote Content <small><span class="semi-bold">{{ $created_at }}</span></small></h4><br/>
            <div class="row">
            
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label"><b>Pelapor</b></label><br/>
                        <div class="controls">
                            <div class="controls">
                                ID:<br><label class="form-label">{{ $pelapor_id }}</label><br/>
                            </div>
                            <div class="controls">
                                User:<br><label class="form-label">{{ $pelapor_name }}</label><br/>
                            </div>
                            <div class="controls">
                                Status:<br><label class="form-label">{{ $pelapor_status }}</label><br/>
                            </div>
                            <div class="controls">
                                Avatar:<br>
                                
                                <a href="{{ $pelapor_avatar }}" target="_blank"><img src="{{ $pelapor_avatar }}" height="200" width="200"></a><br/>
                            </div>
                        </div>
                    </div>
                </div>

                @if($terlapor_id)
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label"><b>Terlapor</b></label><br/>
                        <div class="controls">
                            <div class="controls">
                                ID:<br><label class="form-label">{{ $terlapor_id }}</label><br/>
                            </div>
                            <div class="controls">
                                User:<br><label class="form-label">{{ $terlapor_name }}</label><br/>
                            </div>
                            <div class="controls">
                                Status:<br><label class="form-label">{{ $terlapor_status }}</label><br/>
                            </div>
                            <div class="controls">
                                Avatar:<br>
                                @if ($terlapor_avatar)
                                    <a href="{{ $terlapor_avatar }}" target="_blank"><img src="{{ $terlapor_avatar }}" height="200" width="200"></a><br/>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endif


                <div class="col-md-6">
                    <div class="form-group">
                        <br><br>
                        <label class="form-label"><b>Reported Content</b></label><br/>
                        <div class="controls">
                            <div class="controls">
                                Reason:<br><label class="form-label">{{ $reason }}</label><br/>
                            </div>
                            <div class="controls">
                                @if ($chat_image_url)
                                <a href="{{ $chat_image_url }}" target="_blank"><img src="{{ $chat_image_url }}" height="200" width="200"></a><br/>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        Action notes:<br>
                        <input style="width:80%;" type="text" class="" name="tindak_lanjut" id="tindak_lanjut" >
                    </div>
                </div>
            </div>


            <br/>
            <br/>
                <a href="{{ url($ref) }}" class="btn btn-warning">Back</a>
                @if($terlapor_id)
                    @if ($n_status == 1)
                        <button type="submit" name="btnDelete" id="btnDelete" value="btnDelete" class="btn btn-danger">Delete User</button>
                        <button type="submit" name="btnBlacklist" id="btnBlacklist" value="btnBlacklist" class="btn btn-danger">Blacklist User</button>
                        <button type="submit" name="btnDeleteProfile" id="btnDeleteProfile" value="btnDeleteProfile" class="btn btn-danger">Delete Avatar</button>
                        <button type="submit" name="btnJustClose" id="btnJustClose" value="btnJustClose" class="btn btn-primary">Done</button>
                    @else
                        <button type="submit" name="btnUnDelete" id="btnUnDelete" value="btnUnDelete" class="btn btn-success">Un-Delete User</button>
                        <button type="submit" name="btnUnBlacklist" id="btnUnBlacklist" value="btnUnBlacklist" class="btn btn-success">Un-Blacklist User</button>
                    @endif
                @else
                    <button type="submit" name="btnJustClose" id="btnJustClose" value="btnJustClose" class="btn btn-danger">Close Report</button>
                @endif
            </div>
        </form>
    </div>
</div>
@stop
