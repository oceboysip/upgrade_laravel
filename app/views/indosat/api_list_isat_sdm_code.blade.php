@if (isset($the_data))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>List of SDM Code</h4>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>

        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <th>SDC</th>
                <th>Service</th>
                <th>Keyword</th>
                <th>Code</th>
                <th> </th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($the_data) < 1)
                <tr class="even gradeA">
                    <td colspan=5> No data </td>
                </tr> 
            @else    
                @foreach ($the_data as $each_data)
                    <tr class="even gradeA">
                        <td>{{ $each_data->sdc }}</td>
                        <td class="center">{{ $each_data->name }}</td>
                        <td class="center">{{ $each_data->keyword }}</td>
                        <td class="center"><b>{{ $each_data->code }}</b></td>
                        <td class="center">
                            <button serviceId="{{ $each_data->service_id }}" keyword="{{ $each_data->keyword }}" code="{{ $each_data->code }}" sdc="{{ $each_data->sdc }}" class="btn btn-small btn-white btn-cons btnRemove"
                                    type="button">Remove</button>
                        </td>
                    </tr>                    
                @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
</div>


<form id="idForm">
    <input type="hidden" id="formFieldServiceId" name="serviceId"/>
    <input type="hidden" id="formFieldSdc" name="sdc"/>
    <input type="hidden" id="formFieldKeyword" name="keyword"/>
    <input type="hidden" id="formFieldCode" name="code"/>
</form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalContent"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalKeywords" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalKeywordsContent"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".btnRemove").click(function() {
        $("#idErrorMessageKeyword").html('');
        $("#idAlertSuccessKeyword").html('');

        $("#formFieldServiceId").val($(this).attr('serviceId'));
        $("#formFieldSdc").val($(this).attr('sdc'));
        $("#formFieldKeyword").val($(this).attr('keyword'));
        $("#formFieldCode").val($(this).attr('code'));

        $.post("{{ url('/api/isat/remove_sdm_code') }}",
            $("#idForm").serialize(),
            function (data) {
                if (data['Error']) {
                    $("#idErrorMessageKeyword").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                        'Error while removing SDM Code: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#btnSaveNewKeyword").hide();
                    $("#btnCloseNewKeywordWindow").show();
                    $("#idAlertSuccessKeyword").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                        'Success:&nbsp;remove SDM Code<br/></div>');


                    getSdmCodeList($("#sdcSelect").val(), $("#serviceSelect").val());
                }

            }
        ).fail(function (jqXHR, textStatus) {
            if (jqXHR.status == 401) {
                window.location = "{{ url('/login') }}";
            }
            else {
                $("#idErrorMessageKeyword").html(
                    '<div class="alert alert-error">Error while creating new SDM code! ' +
                    'Please try again later or contact system Administrator</div>')
            }
        });
    });
</script>
@endif
