<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@yield("header.title","KMI Webtools")</title>

    <!-- BEGIN CORE CSS FRAMEWORK -->

    <link href="{{ asset('assets/plugins/jquery-polymaps/style.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('assets/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/jquery-datatable/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrap-checkbox/css/bootstrap-checkbox.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/shape-hover/css/demo.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/shape-hover/css/component.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/owl-carousel/owl.carousel.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/owl-carousel/owl.theme.css') }}"/>

    <link href="{{ asset('assets/plugins/jquery-polymaps/style.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css"/>

    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/magic_space.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/tiles_responsive.css') }}" rel="stylesheet" type="text/css"/>
    <!-- <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/> -->

    <!-- END CSS TEMPLATE -->


</head>

<!-- BEGIN BODY -->
<body class="">

<!-- BEGIN HEADER -->
@include("main_top_navigation-layout")
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">

    @include("sidebar-layout")

    <!-- BEGIN PAGE CONTAINER-->


    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here</div>
        </div>

        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title">
            </div>

            <!-- THIS IS THE CONTENT -->

			<div class="row">
				<div class="grid simple">
					<div class="grid-body no-border">
						<div class="row">
							<div class="col-md-10">
								<h3>Manage Indosat <span class="semi-bold">SDM Code</span></h3>
								<p>Manage sdm code for Indosat Services</p>
								<form id="myformKeyword" method="POST">
									<div class="row">
										<div class="col-md-4">
											SDC: <select id="sdcSelect" style="width:100%;" name="sdc">
												<optgroup label="Choose SDC">
													@foreach ($sdc as $each_sdc)
													<option value="{{ $each_sdc->name }}" {{ (isset($givenSdc) && ($each_sdc->name == $givenSdc)) ? "selected='1'" : "" }}>{{ $each_sdc->name }}</option>
													@endforeach
												</optgroup>
											</select>
										</div>
										<div class="col-md-6">
											&nbsp;
											Service: <select id="serviceSelect" style="width:100%" name="serviceId">
												<optgroup label="Choose SDC First">
													@if(isset($service))
													@foreach ($service as $each_service)
													<option value="{{ $service->id }}" {{ (isset($givenServiceId) && ($service->id == $givenServiceId)) ? "selected='1'" : "" }}>{{ $service->name }}</option>
													@endforeach
													@endif
												</optgroup>
											</select>
										</div>
									</div>
									<br/>
									<div class="row">
										<div class="col-md-4">
											Keyword: <select id="keywordSelect" style="width:100%" name="keyword">
												<optgroup label="Choose Keyword">
													@if(isset($service))
													@foreach ($service as $each_service)
													<option value="{{ $service->id }}" {{ (isset($givenServiceId) && ($service->id == $givenServiceId)) ? "selected='1'" : "" }}>{{ $service->name }}</option>
													@endforeach
													@endif
												</optgroup>
											</select>
										</div>
										<div class="col-md-6">
											SDM Code:<br/><input id="idCodeText" type="text" name="sdmCode" placeholder="The SDM Code" />
										</div>
									</div>
									<br/>
									<div class="row">
										<div class="col-md-6">
											<a data-color="rgb(255, 255, 255)" data-color-format="hex" id="addNewSdmCode"
											   class="btn btn-primary" href="#"
											   style="margin-right: 30px;">Add Code</a>
										</div>
									</div>

									<div id="waitCommitKeyword" class="row form-row"></div>
									<div id="idErrorMessageKeyword" class="row form-row"></div>
									<div id="idAlertSuccessKeyword" class="row form-row"></div>

									<br>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row" id="theData">

			</div>

            <!-- THIS IS THE END OF CONTENT -->


        </div>
    </div>

    <!-- END MAIN PAGE -->

</div>

<!-- END CONTAINER -->

@include("main_js-layout")

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN CORE TEMPLATE JS -->

<script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('assets/plugins/jquery-datatable/extra/js/TableTools.min.js') }}" type="text/javascript" ></script>

<script type="text/javascript" src="{{ asset('assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/datatables-responsive/js/lodash.min.js') }}"></script>

<script type="text/javascript">

    $("#sdcSelect").select2({placeholder: "Choose SDC"});
	$("#sdcSelect").change(function() {getServiceList($("#sdcSelect").val());});

	$("#serviceSelect").select2({placeholder: "Choose Service"});
	$("#serviceSelect").change(function() {getServiceList($("#sdcSelect").val());});

	//Set rightaway
	getServiceList($("#sdcSelect").val());

	function getServiceList(choosenSdc) {
		$.getJSON("{{ url('api/get_list_active_service/2') }}/" + choosenSdc,
			{ajax: 'true'}, function (data) {
				$("#serviceSelect").html('');
				$("#serviceSelect").append('<optgroup label="Choose Service">');
				var firstData = null;
				$.each(data, function () {
					if (!firstData) {
						firstData = this.id;
					}
					$("#serviceSelect").append('<option value="' + this.id + '">' + this.name + '</option>')
				})
				$("#serviceSelect").append('</optgroup>');
				$("#serviceSelect").select2('val', firstData);

				getKeywordList(choosenSdc, $("#serviceSelect").val())
			});
	}

	function getKeywordList(choosenSdc, serviceId) {
		$.getJSON("{{ url('api/get_list_active_keyword/2') }}/" + choosenSdc + "/" + serviceId,
			{ajax: 'true'}, function (data) {
				$("#keywordSelect").html('');
				$("#keywordSelect").append('<optgroup label="Choose Keyword">');
				var firstData = null;
				$.each(data, function () {
					if (!firstData) {
						firstData = this.keyword;
					}
					$("#keywordSelect").append('<option value="' + this.keyword + '">' + this.keyword + '</option>')
				})
				$("#keywordSelect").append('</optgroup>');
				$("#keywordSelect").select2('val', firstData);

				getSdmCodeList(choosenSdc, serviceId);
			});
	}


	function getSdmCodeList(choosenSdc, serviceId) {
		$.get("{{ url('/api/isat/get_sdm_code_list') }}/" + choosenSdc + "/" + serviceId,
			{ajax: 'true'}, function (data) {
				$("#theData").html('');
				$("#theData").append(data);
			});
	}


	$("#addNewSdmCode").click(function () {
		$("#waitCommitKeyword").html('<div class="col-md-8 col-sm-8 col-xs-8"><br/>Saving data ... ' +
			'<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

		$("#idErrorMessageKeyword").html('');
		$("#idAlertSuccessKeyword").html('');

		$(this).attr("disabled", "disabled");
		$.post("{{ url('/api/isat/create_new_sdm_code') }}", $("#myformKeyword").serialize(), function (data) {
			$("#waitCommitKeyword").html('');
			$("#addNewSdmCode").removeAttr("disabled");
			$("#idCodeText").val("");

			if (data['Error']) {
				$("#idErrorMessageKeyword").html(
					'<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
					'Error while saving to database: ' + data['Message'] + '</div>');
			} else {
				doneEdit = true;
				$("#btnSaveNewKeyword").hide();
				$("#btnCloseNewKeywordWindow").show();
				$("#idAlertSuccessKeyword").html(
					'<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
					'Success:&nbsp;New SDM Code' + $('[name="sdmCode"]').val() + ' has been added<br/></div>');


				getSdmCodeList($("#sdcSelect").val(), $("#serviceSelect").val());
			}
		})
			.done(function () {
				//alert("second success");
			})
			.fail(function (jqXHR, textStatus) {
				if (jqXHR.status == 401) {
					window.location = "{{ url('/login') }}";
				}
				else {
					$("#idErrorMessageKeyword").html(
						'<div class="alert alert-error">Error while creating new SDM code! ' +
						'Please try again later or contact system Administrator</div>')
				}
			})
			.always(function () {
				//alert("finished");
			});

		//to prevent Browser from submitting the real FORM post
		return false;
		//alert("{{ url('/api/create_new_service_keyword') }}");
	});
</script>



</body>
</html>
