<?php
/**
 * This is the main for Home page
 * Currently being set as dummy. FIX THIS
 *
 * User: pandupradhana
 * Date: 4/20/14
 * Time: 12:36 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="page-content">
    <div class="content">
        <div class="page-title"></div>

        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">

            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                <div class="panel-heading"><h3>Indosat KMI Report</h3></div>
                <!-- <div class="col-md-7 b-grey b-r no-padding col-sm-8" style="height:520px"> -->
                <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                    <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                        <h5 class="text-success bold inline">Today's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
							@if ($todayRevISAT < $yesterdayRevISAT):
							<i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
							@else
							<i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
							@endif
                            <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevISAT) }}</h1>
                        </div>
                    </div>
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                    <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                        <h5 class="text-success bold inline">Yesterday's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
							@if ($yesterdayRevISAT < $beforeYesterdayRevISAT):
							<i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
							   style="vertical-align: super;"></i>
							@else
							<i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
							@endif
							<h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevISAT) }}</h2>
                        </div>
                    </div>                    
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                    <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                        <h5 class="text-success bold inline">This Months's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
							@if ($lastMonthRevISAT < $thisMonthRevISAT):
							<i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
							   style="vertical-align: super;"></i>
							@else
							<i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
							@endif
							<h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevISAT) }}</h2>
                        </div>
                    </div>
                </div>
				<div class="overlayer bottom-left fullwidth">
					<div class="overlayer-wrapper">
						<div class="" id="theChart2ISAT" style="width:100%;height:220px;"></div>
					</div>
				</div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

        <div class="row" style="display:none;">
			<div class="col-md-6 col-vlg-4">

				<div id="chart_container fullwidth" style="height: 220px;">
					<div id="theChartISAT"></div>
					<div id="legend_container">
						<div id="smoother" title="Smoothing"></div>
						<div id="legend"></div>
					</div>
					<div id="slider"></div>
				</div>
			</div>


			<div class="col-md-6 col-vlg-8">
				<div class="tiles white text-center">
					<h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
						{{ date('l') }}
					</h2>
					<div class="tiles-title blend m-b-5">Today</div>
					<div class="clearfix"></div>
				</div>                
			</div>

        </div>

    </div>

        <div class="clearfix">&nbsp;</div>

        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">

            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                <div class="panel-heading"><h3>Indosat Playmedia Report</h3></div>
                <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                        <h5 class="text-success bold inline">Today's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($todayRevISATPlaymedia < $yesterdayRevISATPlaymedia):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevISATPlaymedia) }}</h1>
                        </div>
                    </div>
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                        <h5 class="text-success bold inline">Yesterday's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($yesterdayRevISATPlaymedia < $beforeYesterdayRevISATPlaymedia):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                               style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevISATPlaymedia) }}</h2>
                        </div>
                    </div>                    
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                        <h5 class="text-success bold inline">This Months's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($lastMonthRevISATPlaymedia < $thisMonthRevISATPlaymedia):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                               style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevISATPlaymedia) }}</h2>
                        </div>
                    </div>
                </div>
                <div class="overlayer bottom-left fullwidth">
                    <div class="overlayer-wrapper">
                        <div class="" id="theChart2ISATPlaymedia" style="width:100%;height:220px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

        <div class="row" style="display:none;">
            <div class="col-md-6 col-vlg-4">

                <div id="chart_container fullwidth" style="height: 220px;">
                    <div id="theChartISATPlaymedia"></div>
                    <div id="legend_container">
                        <div id="smoother" title="Smoothing"></div>
                        <div id="legend"></div>
                    </div>
                    <div id="slider"></div>
                </div>
            </div>


            <div class="col-md-6 col-vlg-8">
                <div class="tiles white text-center">
                    <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                        {{ date('l') }}
                    </h2>
                    <div class="tiles-title blend m-b-5">Today</div>
                    <div class="clearfix"></div>
                </div>                
            </div>

        </div>

    </div>

        <div class="clearfix">&nbsp;</div>
            <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                    <div class="panel-heading"> <h3>XL KMI Report</h3> </div>
                    <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Today's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($todayRevXL < $yesterdayRevXL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevXL) }}</h1>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Yesterday's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($yesterdayRevXL < $beforeYesterdayRevXL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevXL) }}</h2>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">This Months's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($lastMonthRevXL < $thisMonthRevXL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevXL) }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="overlayer bottom-left fullwidth">
                        <div class="overlayer-wrapper">
                            <div class="" id="theChart2XL" style="width:100%;height:220px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END DASHBOARD TILES -->


            <div class="row" style="display:none;">
                <div class="col-md-6 col-vlg-4">

                    <div id="chart_container fullwidth" style="height: 220px;">
                        <div id="theChartXL"></div>
                        <div id="legend_container">
                            <div id="smoother" title="Smoothing"></div>
                            <div id="legend"></div>
                        </div>
                        <div id="slider"></div>
                    </div>
                </div>


                <div class="col-md-6 col-vlg-8">
                    <div class="tiles white text-center">
                        <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                            {{ date('l') }}
                        </h2>
                        <div class="tiles-title blend m-b-5">Today</div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
            
        </div>

            <div class="clearfix">&nbsp;</div>

            <!-- BEGIN DASHBOARD TILES -->
            <div class="row">

                <div class="col-md-12">
                    <div class="row tiles-container tiles white">
                    <div class="panel-heading"><h3>XL Playmedia Report</h3></div>
                    <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <h5 class="text-success bold inline">Today's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($todayRevXLPlaymedia < $yesterdayRevXLPlaymedia):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @else
                                <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevXLPlaymedia) }}</h1>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <h5 class="text-success bold inline">Yesterday's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($yesterdayRevXLPlaymedia < $beforeYesterdayRevXLPlaymedia):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevXLPlaymedia) }}</h2>
                            </div>
                        </div>                    
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <h5 class="text-success bold inline">This Months's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($lastMonthRevXLPlaymedia < $thisMonthRevXLPlaymedia):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevXLPlaymedia) }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="overlayer bottom-left fullwidth">
                        <div class="overlayer-wrapper">
                            <div class="" id="theChart2XLPlaymedia" style="width:100%;height:220px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END DASHBOARD TILES -->

            <div class="row" style="display:none;">
                <div class="col-md-6 col-vlg-4">

                    <div id="chart_container fullwidth" style="height: 220px;">
                        <div id="theChartISATPlaymedia"></div>
                        <div id="legend_container">
                            <div id="smoother" title="Smoothing"></div>
                            <div id="legend"></div>
                        </div>
                        <div id="slider"></div>
                    </div>
                </div>


                <div class="col-md-6 col-vlg-8">
                    <div class="tiles white text-center">
                        <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                            {{ date('l') }}
                        </h2>
                        <div class="tiles-title blend m-b-5">Today</div>
                        <div class="clearfix"></div>
                    </div>                
                </div>

            </div>

        </div>

        <div class="clearfix">&nbsp;</div>
        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                    <div class="panel-heading"> <h3>Telkomsel KMI Report</h3> </div>
                    <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Today's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($todayRevTSEL < $yesterdayRevTSEL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevTSEL) }}</h1>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Yesterday's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($yesterdayRevTSEL < $beforeYesterdayRevTSEL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevTSEL) }}</h2>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">This Months's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($lastMonthRevTSEL < $thisMonthRevTSEL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevTSEL) }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="overlayer bottom-left fullwidth">
                        <div class="overlayer-wrapper">
                            <div class="" id="theChart2TSEL" style="width:100%;height:220px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END DASHBOARD TILES -->


            <div class="row" style="display:none;">
                <div class="col-md-6 col-vlg-4">

                    <div id="chart_container fullwidth" style="height: 220px;">
                        <div id="theChartTSEL"></div>
                        <div id="legend_container">
                            <div id="smoother" title="Smoothing"></div>
                            <div id="legend"></div>
                        </div>
                        <div id="slider"></div>
                    </div>
                </div>


                <div class="col-md-6 col-vlg-8">
                    <div class="tiles white text-center">
                        <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                            {{ date('l') }}
                        </h2>
                        <div class="tiles-title blend m-b-5">Today</div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

<div class="clearfix">&nbsp;</div>

<!--
<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's MODEL PA3 - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
           
 <div class="clearfix">&nbsp;</div>
-->
<!--@if($username != 'inong')-->

<!--
<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's DISKON GPA1C - POSTPAID - Indosat</h5>
		<br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc1) }}</h1>
                </div>
	    </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg1) }}</h1>
                </div>
	    </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa1) }}</h1>
                </div>
	    </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv1, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
-->

<!--
<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's DISKON GPA1D - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc2) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg2) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa2) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv2, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
-->

<div class="clearfix">&nbsp;</div>


<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's MODEL PM1 - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc4) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg4) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa4) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv4, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix">&nbsp;</div>

<!--
<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's MODEL PM1 - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv5, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
-->

<!--
<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's MODEL PM4 - Ultimate - PREPAID - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv5, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
-->

<!--
<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's GAME PM1 - POSTPAID - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc4) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg4) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa4) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv4, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">&nbsp;</div>


<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's GAME PM2 - PREPAID - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv5, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix">&nbsp;</div>
-->

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's CB B - Telkomsel</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc3) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg3) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa3) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv3, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's CHATBC HPR - XL</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc2) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg2) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa2) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv2, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's LIVEBC HPR - XL</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc6) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg6) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa6) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv6, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's JODOH MB1 - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa5) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv5, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's MODEL GS2 - Indosat</h5>
        <br>
                <h5 class="text-black bold inline m-l-10">Sent</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_bc6) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">REG</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($total_reg6) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">COA, IDR</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa6) }}</h1>
                </div>
        </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Conversion %</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ number_format($coa_conv6, 2) }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
-->

<div class="clearfix">&nbsp;</div>

<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's AUTO UNREG (MODEM 1)</h5>
                <br>
                <h5 class="text-black bold inline m-l-10">Total Calls</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ @$TodaysTotalCalls }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min Calls</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinCalls }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min Calls CallerId</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinCallsCallerId }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Total Sms</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $TodaysTotalSms }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min SMS </h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinSms }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's AUTO UNREG (MODEM 2)</h5>
                <br>
                <h5 class="text-black bold inline m-l-10">Total Calls</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ @$TodaysTotalCalls_2 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min Calls</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinCalls_2 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min Calls CallerId</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinCallsCallerId_2 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Total Sms</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $TodaysTotalSms_2 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min SMS </h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinSms_2 }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">&nbsp;</div>

<div class="col-md-12">
    <div class="row tiles-container tiles white">
        <div class="col-md-12 no-padding col-sm-12" style="height:100%">
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-success bold inline">Today's AUTO UNREG (MODEM 3)</h5>
                <br>
                <h5 class="text-black bold inline m-l-10">Total Calls</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ @$TodaysTotalCalls_3 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min Calls</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinCalls_3 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min Calls CallerId</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinCallsCallerId_3 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Total Sms</h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $TodaysTotalSms_3 }}</h1>
                </div>
            </div>
            <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                <h5 class="text-black bold inline m-l-10">Last 30min SMS </h5>
                <div class="">
                    <h1 class="text-error bold inline no-margin">{{ $Last30MinSms_3 }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
@endif
</div>
