<?php
/**
 * This is the main for Home page
 * Currently being set as dummy. FIX THIS
 *
 * User: pandupradhana
 * Date: 4/20/14
 * Time: 12:36 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="page-content">
    <div class="content">
        <div class="page-title"></div>

        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">

            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                <div class="panel-heading"><h3>Indosat KMI Report</h3></div>
                <!-- <div class="col-md-7 b-grey b-r no-padding col-sm-8" style="height:520px"> -->
                <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                    <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                        <h5 class="text-success bold inline">Today's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($todayRevISAT < $yesterdayRevISAT):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevISAT) }}</h1>
                        </div>
                    </div>
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                    <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                        <h5 class="text-success bold inline">Yesterday's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($yesterdayRevISAT < $beforeYesterdayRevISAT):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                               style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevISAT) }}</h2>
                        </div>
                    </div>                    
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                    <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                        <h5 class="text-success bold inline">This Months's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($lastMonthRevISAT < $thisMonthRevISAT):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                               style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevISAT) }}</h2>
                        </div>
                    </div>
                </div>
                <div class="overlayer bottom-left fullwidth">
                    <div class="overlayer-wrapper">
                        <div class="" id="theChart2" style="width:100%;height:220px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

        <div class="row" style="display:none;">
            <div class="col-md-6 col-vlg-4">

                <div id="chart_container fullwidth" style="height: 220px;">
                    <div id="theChart"></div>
                    <div id="legend_container">
                        <div id="smoother" title="Smoothing"></div>
                        <div id="legend"></div>
                    </div>
                    <div id="slider"></div>
                </div>
            </div>


            <div class="col-md-6 col-vlg-8">
                <div class="tiles white text-center">
                    <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                        {{ date('l') }}
                    </h2>
                    <div class="tiles-title blend m-b-5">Today</div>
                    <div class="clearfix"></div>
                </div>                
            </div>

        </div>

    </div>

        <div class="clearfix">&nbsp;</div>

        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">

            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                <div class="panel-heading"><h3>Indosat Playmedia Report</h3></div>
                <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                        <h5 class="text-success bold inline">Today's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($todayRevISATPlaymedia < $yesterdayRevISATPlaymedia):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevISATPlaymedia) }}</h1>
                        </div>
                    </div>
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                        <h5 class="text-success bold inline">Yesterday's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($yesterdayRevISATPlaymedia < $beforeYesterdayRevISATPlaymedia):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                               style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevISATPlaymedia) }}</h2>
                        </div>
                    </div>                    
                    <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                        <h5 class="text-success bold inline">This Months's</h5>
                        <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                        <div class="">
                            @if ($lastMonthRevISATPlaymedia < $thisMonthRevISATPlaymedia):
                            <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                               style="vertical-align: super;"></i>
                            @else
                            <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                            @endif
                            <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevISATPlaymedia) }}</h2>
                        </div>
                    </div>
                </div>
                <div class="overlayer bottom-left fullwidth">
                    <div class="overlayer-wrapper">
                        <div class="" id="theChart2Playmedia" style="width:100%;height:220px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END DASHBOARD TILES -->

        <div class="row" style="display:none;">
            <div class="col-md-6 col-vlg-4">

                <div id="chart_container fullwidth" style="height: 220px;">
                    <div id="theChartISATPlaymedia"></div>
                    <div id="legend_container">
                        <div id="smoother" title="Smoothing"></div>
                        <div id="legend"></div>
                    </div>
                    <div id="slider"></div>
                </div>
            </div>


            <div class="col-md-6 col-vlg-8">
                <div class="tiles white text-center">
                    <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                        {{ date('l') }}
                    </h2>
                    <div class="tiles-title blend m-b-5">Today</div>
                    <div class="clearfix"></div>
                </div>                
            </div>

        </div>

    </div>

        <div class="clearfix">&nbsp;</div>
            <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                    <div class="panel-heading"> <h3>XL Report</h3> </div>
                    <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Today's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($todayRevXL < $yesterdayRevXL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevXL) }}</h1>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Yesterday's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($yesterdayRevXL < $beforeYesterdayRevXL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevXL) }}</h2>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">This Months's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($lastMonthRevXL < $thisMonthRevXL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevXL) }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="overlayer bottom-left fullwidth">
                        <div class="overlayer-wrapper">
                            <div class="" id="theChart2XL" style="width:100%;height:220px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END DASHBOARD TILES -->


            <div class="row" style="display:none;">
                <div class="col-md-6 col-vlg-4">

                    <div id="chart_container fullwidth" style="height: 220px;">
                        <div id="theChartXL"></div>
                        <div id="legend_container">
                            <div id="smoother" title="Smoothing"></div>
                            <div id="legend"></div>
                        </div>
                        <div id="slider"></div>
                    </div>
                </div>


                <div class="col-md-6 col-vlg-8">
                    <div class="tiles white text-center">
                        <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                            {{ date('l') }}
                        </h2>
                        <div class="tiles-title blend m-b-5">Today</div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
            
        </div>    

        <div class="clearfix">&nbsp;</div>
        <!-- BEGIN DASHBOARD TILES -->
        <div class="row">
            <div class="col-md-12">
                <div class="row tiles-container tiles white">
                    <div class="panel-heading"> <h3>Telkomsel Report</h3> </div>
                    <div class="col-md-12 no-padding col-sm-12" style="height:520px">
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Today's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($todayRevTSEL < $yesterdayRevTSEL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h1 class="text-error bold inline no-margin"> IDR {{ number_format($todayRevTSEL) }}</h1>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">Yesterday's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($yesterdayRevTSEL < $beforeYesterdayRevTSEL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($yesterdayRevTSEL) }}</h2>
                            </div>
                        </div>
                        <div class="p-l-20 p-r-20 p-b-10 p-t-10 b-b b-grey">
                            <!-- <div class="p-l-20 p-r-20 p-b-10 p-t-10"> -->
                            <h5 class="text-success bold inline">This Months's</h5>
                            <h5 class="text-black bold inline m-l-10">Revenue (Net)</h5>
                            <div class="">
                                @if ($lastMonthRevTSEL < $thisMonthRevTSEL):
                                <i class="fa fa-sort-asc fa-2x text-error inline p-b-10 p-r-10"
                                   style="vertical-align: super;"></i>
                                @else
                                    <i class="fa fa-sort-desc fa-2x inline p-b-10 p-r-10" style="vertical-align: super;"></i>
                                @endif
                                <h2 class="text-error bold inline no-margin"> IDR {{ number_format($thisMonthRevTSEL) }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="overlayer bottom-left fullwidth">
                        <div class="overlayer-wrapper">
                            <div class="" id="theChart2TSEL" style="width:100%;height:220px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END DASHBOARD TILES -->


            <div class="row" style="display:none;">
                <div class="col-md-6 col-vlg-4">

                    <div id="chart_container fullwidth" style="height: 220px;">
                        <div id="theChartTSEL"></div>
                        <div id="legend_container">
                            <div id="smoother" title="Smoothing"></div>
                            <div id="legend"></div>
                        </div>
                        <div id="slider"></div>
                    </div>
                </div>


                <div class="col-md-6 col-vlg-8">
                    <div class="tiles white text-center">
                        <h2 class="semi-bold text-error weather-widget-big-text no-margin p-t-20 p-b-10">
                            {{ date('l') }}
                        </h2>
                        <div class="tiles-title blend m-b-5">Today</div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

<div class="clearfix">&nbsp;</div>

@if($username != 'inong')

<div class="clearfix">&nbsp;</div>
@endif
</div>
