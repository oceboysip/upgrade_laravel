@if (isset($list))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>List of Users</h4>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>


        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <th>Username</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Active</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($list) < 1)
                <tr class="even gradeA">
                    <td colspan=5> No data </td>
                </tr> 
            @else    
                @foreach ($list as $each_data)
                    <tr class="even gradeA">
                        <!-- <td>{{ $each_data->id }}</td> -->
                        <td title="User Id: {{ $each_data->id }}">{{ $each_data->username }}</td>
                        <td class="center">{{ $each_data->fullname }}</td>
                        <td class="center">{{ $each_data->email }}</td>
                        <td class="center">{{ $each_data->role_name }}</td>
                        @if ($each_data->is_active)
                        <td class="center"><span class="label label-success">Yes</span></td>
                        @else
                        <td class="center"><span class="label label-important">No</span></td>
                        @endif
                        <td class="center">
                            <button userId="{{ $each_data->id }}" class="btn btn-small btn-white btn-cons btnEditUser"
                                    type="button" data-toggle="modal" data-target="#myEditUserModal">Edit User</button>
                        </td>
                    </tr>                    
                @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myEditUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalUserEditContent"></div>
        </div>
    </div>
</div>


<script type="text/javascript">
	$(".btnEditUser").click(function() {
		$.get("{{ url('/api/get_user') }}" + "/" + this.getAttribute('userId'),
			{ajax: 'true'},
			function (data) {
				$("#theModalUserEditContent").html('');
				$("#theModalUserEditContent").html(data);
			}
		).fail(function (jqXHR, textStatus) {
				if (jqXHR.status == 401) {
					window.location = "{{ url('/login') }}";
				}
				else {
					$("#idAlertSuccess").html(
						'Error while get roles! Please try again later or contact system Administrator')
				}
			})
		;
	});

</script>

@endif
