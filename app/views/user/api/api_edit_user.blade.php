@if($user)
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<br>
	<h4 id="myModalLabel" class="semi-bold">Edit User: {{ $user->fullname }}</h4>

	<p class="no-margin">Carefully review the values</p>
	<br>
</div>
<div class="modal-body">
	<form id="myEditUserform" method="POST">
		<input type="hidden" name="userId" value="{{ $user->id }}">

		<div class="form-group">
			<label class="form-label">Name</label><br/>
			<span class="help">Set the full name for this user</span>

			<div class="controls" style="width:90%;">
				<input type="text" class="form-control" name="name" id="idName" value="{{ $user->fullname }}">
				<label class="error" style="display: none" for="idName" id="errorName">Name is required</label>
			</div>
		</div>
		<div class="form-group">
			<label class="form-label">Username</label><br/>
			<span class="help">Set the username use for login</span>

			<div class="controls" style="width:90%;">
				{{ $user->username}}
				<label class="error" style="display: none" for="idUserName" id="errorUsername">Username is
					required</label>
			</div>
		</div>
		<div class="form-group">
			<label class="form-label">Email</label><br/>

			<div class="controls" style="width:90%;">
				<input type="text" class="form-control" name="email" id="idEmail" value="{{ $user->email }}">
				<label class="error" style="display: none" for="idEmail" id="errorEmail">Email is required</label>
			</div>
		</div>
		<div class="form-group">
			<label class="form-label">Role</label><br/>
			<span class="help">Set group for this service (if any)</span>

			<div class="controls">
				<select id="idRoleId" style="width:40%;" name="roleId">
					@foreach ($roles as $each_role)
						@if($each_role->id == $user->role_id)
						<option value="{{ $each_role->id }}" selected="selected">{{ $each_role->role_name }}</option>
						@else
						<option value="{{ $each_role->id }}">{{ $each_role->role_name }}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="form-label">Active</label><br/>
			<div class="controls">
				<select id="idUserDisabled" style="width:40%;" name="isActive">
					<option value="1"
					{{ $user->is_active ? "selected='1'" : "" }}>Yes</option>
					<option value="0"
					{{ $user->is_active ? "" : "selected='1'" }}>No</option>
				</select>
			</div>
		</div>
	</form>
	<div id="waitUserEditCommit" class="row form-row"></div>
	<div id="idUserEditErrorMessage" class="row form-row"></div>
	<div id="idUserEditAlertSuccess" class="row form-row"></div>
</div>
<div class="modal-footer">
	<button type="button" id="btnUserEditDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" id="btnCommitUserEditChanges" class="btn btn-primary">Commit changes</button>
</div>


<script type="text/javascript">
	var doneEdit = false;
	$("#idRoleId").select2();
	$("#idUserDisabled").select2();

	$("#btnUserEditDismiss").click(function () {
		if (doneEdit) {window.location = "{{ url('/admin/manage_user') }}";}
	});

	$("#btnCommitUserEditChanges").click(function () {
		$("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
			'<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

		$("#btnCommitUserEditChanges").attr("disabled", "disabled");
		$.post("{{ url('/api/edit_user') }}", $("#myEditUserform").serialize(),
			function (data) {
				$("#waitUserEditCommit").html('');
				if (data['Error']) {
					$("#idUserEditErrorMessage").html(
						'<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
							'Error while saving to database: ' + data['Message'] + '</div>');
				} else {
					doneEdit = true;
					$("#idUserEditAlertSuccess").html(
						'<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
							'Success&nbsp;edit user {{ $user->fullname }} </div>');
				}
			})
			.done(function () {
				//alert("second success");
			})
			.fail(function (jqXHR, textStatus) {
				if (jqXHR.status == 401) {
					window.location = "{{ url('/login') }}";
				}
				else {
					$("#idUserEditAlertSuccess").html(
						'Error while edit user! Please try again later or contact system Administrator')
				}
			})
			.always(function () {
				//alert("finished");
			});
	});
</script>
@endif
