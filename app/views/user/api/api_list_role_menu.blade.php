@if (isset($list))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <br>
            <h4>List of Menu granted for {{ $roleName }}</h4>
        </div>

        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <th>Menu</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($list) < 1)
                <tr class="even gradeA">
                    <td colspan=5> No data </td>
                </tr> 
            @else    
                @foreach ($list as $each_data)
                    <tr class="even gradeA">
                        <!-- <td>{{ $each_data->id }}</td> -->
                        <td title="Role Id: {{ $each_data->id }}">{{ $each_data->title }}</td>
                        <td class="center">
                            <button roleId="{{ $each_data->id }}" menuId="{{ $each_data->menu_id }}"
                                    class="btn btn-small btn-white btn-cons classBtnRemoveMenu"
                                    type="button">Remove</button>
                        </td>
                    </tr>                    
                @endforeach
            @endif
            </tbody>
            </table>

            @if (isset($otherMenu))
                <br/>
                Non-granted Menu:
                <input type="hidden" name="roleId" value="{{ $roleId }}" />
                <select id="idSelectMenuId" style="width:60%;" name="menuId">
                    <optgroup label="Choose Menu">
                        @foreach ($otherMenu as $each_menu)
                        <option value="{{ $each_menu->id }}">{{ $each_menu->title }}</option>
                        @endforeach
                    </optgroup>
                </select>
                <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="addNewMenu"
                   class="btn btn-primary my-colorpicker-control" href="#"
                   style="margin-right: 30px;"
                   data-colorpicker-guid="8">Add</a>


            <div id="waitCommit" class="row form-row"></div>
                <div id="idErrorMessage" class="row form-row"></div>
                <div id="idAlertSuccess" class="row form-row"></div>

            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#idSelectMenuId").select2();
    $(".classBtnRemoveMenu").click(function () {
        var roleId = this.getAttribute('roleId');
        // + "?roleId=" + this.getAttribute('roleId')
        $.post("{{ url('/api/remove_menu_from_role') }}",
            {roleId: this.getAttribute('roleId'), menuId: this.getAttribute('menuId')},
            function (data) {
                $("#theModalMenu").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while removing from database: ' + data['Message'] + '</div>');
                } else {
                    /*
                     //doneEdit = true;
                     $("#idAlertSuccess").html(
                     '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                     'Success:&nbsp;The new service ' + $('[name="name"]').val() + ' has been added</div>');
                     */
                    //RELOAD
                    $.get("{{ url('/api/get_role_menu') }}" + "?roleId=" + roleId,
                        {ajax: 'true'},
                        function (data) {
                            $("#theModalContent").html('');
                            $("#theModalContent").html(data);
                        }
                    );
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while remove menu from role! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });


    $("#addNewMenu").click(function () {
        var menuId = $("[name='menuId']").val();
        var roleId = $("[name='roleId']").val();
        $.post("{{ url('/api/save_menu_role') }}",
            {roleId: roleId, menuId: menuId},
            function (data) {
                $("#theModalMenu").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while adding to database: ' + data['Message'] + '</div>');
                } else {
                    /*
                     //doneEdit = true;
                     $("#idAlertSuccess").html(
                     '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                     'Success:&nbsp;The new service ' + $('[name="name"]').val() + ' has been added</div>');
                     */
                    //RELOAD
                    $.get("{{ url('/api/get_role_menu') }}" + "?roleId=" + roleId,
                        {ajax: 'true'},
                        function (data) {
                            $("#theModalContent").html('');
                            $("#theModalContent").html(data);
                        }
                    );
                }
            })
            .done(function () {
                //alert("second success");
            })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while save new menu for role! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                //alert("finished");
            });
    });
</script>
@endif
