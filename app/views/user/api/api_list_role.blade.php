@if (isset($list))
<div class="span12">
    <div class="grid simple ">
        <div class="grid-title">
            <h4>List of Role</h4>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>

        <div class="grid-body ">
            <table class="table table-hover table-bordered" id="example2">
            <thead>
            <tr>
                <th>Role</th>
                <th>Rights</th>
            </tr>
            </thead>
            <tbody>
            @if (sizeof($list) < 1)
                <tr class="even gradeA">
                    <td colspan=5> No data </td>
                </tr> 
            @else    
                @foreach ($list as $each_data)
                    <tr class="even gradeA">
                        <!-- <td>{{ $each_data->id }}</td> -->
                        <td title="Role Id: {{ $each_data->id }}">{{ $each_data->role_name }}</td>
                        <td class="center">
                            <button roleId="{{ $each_data->id }}" class="btn btn-small btn-white btn-cons classBtnManageMenu"
                                    type="button" data-toggle="modal" data-target="#myModal">See Menu</button>
                        </td>
                    </tr>                    
                @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="theModalContent"></div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(".classBtnManageMenu").click(function() {
        $.get("{{ url('/api/get_role_menu') }}" + "?roleId=" + this.getAttribute('roleId'),
            {ajax: 'true'},
            function (data) {
                $("#theModalContent").html('');
                $("#theModalContent").html(data);
            }
        ).fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while get roles! Please try again later or contact system Administrator')
                }
            })
        ;
    });

</script>
@endif
