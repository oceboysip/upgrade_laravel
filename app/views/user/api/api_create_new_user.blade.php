@if (isset($the_data))
@endif
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
        <form id="myform" method="POST">
            <h4><b>Add New User</b></h4><br/>
            <div class="form-group">
                <label class="form-label">Name</label><br/>
                <span class="help">Set the full name for this user</span>
                <div class="controls" style="width:90%;">
                    <input type="text" class="form-control" name="name" id="idName">
                    <label class="error" style="display: none" for="idName" id="errorName">Name is required</label>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Username</label><br/>
                <span class="help">Set the username use for login</span>
                <div class="controls" style="width:90%;">
                    <input type="text" class="form-control" name="username" id="idUserName">
                    <label class="error" style="display: none" for="idUserName" id="errorUsername">Username is required</label>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Password</label><br/>
                <span class="help">Set Password</span>
                <div class="controls" style="width:90%;">
                    <input type="password" class="form-control" name="password" id="idPassword">
                    <label class="error" style="display: none" for="idPassword" id="errorPassword">Password is required</label>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Confirm Password</label><br/>
                <div class="controls" style="width:90%;">
                    <input type="password" class="form-control" name="confirmPassword" id="idConfirmPassword">
                    <label class="error" style="display: none" for="idConfirmPassword" id="errorConfirmPassword">Password does not match / Confirm Password is required</label>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Email</label><br/>
                <div class="controls" style="width:90%;">
                    <input type="text" class="form-control" name="email" id="idEmail">
                    <label class="error" style="display: none" for="idEmail" id="errorEmail">Email is required</label>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Role</label><br/>
                <span class="help">Set group for this service (if any)</span>
                <div class="controls">
                    <select id="idRoleId" style="width:40%;" name="roleId">
                        @foreach ($roles as $each_role)
                        <option value="{{ $each_role->id }}">{{ $each_role->role_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <br/>
            <br/>
            <button id="btnSave" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Save</button>
            <br/>
            <br/>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <br>
                            <!-- <i class="icon-credit-card icon-7x"></i> -->
                            <h4 id="myModalLabel" class="semi-bold">Summary of new user</h4>
                            <p class="no-margin">Please review the value again</p>
                            <br>
                        </div>
                        <div class="modal-body">
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-name" class="form-label"></label>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-username" class="form-label"></label>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-email" class="form-label"></label>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-8">
                                    <label id="id-lbl-role" class="form-label"></label>
                                </div>
                            </div>

                            <div id="waitCommit" class="row form-row"></div>
                            <div id="idErrorMessage" class="row form-row"></div>
                            <div id="idAlertSuccess" class="row form-row"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnDismiss" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="btnCommitChanges" class="btn btn-primary">Commit changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    </div>
</div>

<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var doneEdit = false;
    $("#idRoleId").select2();
    $("#myform").validate({
        rules: {
            name: {required: true},
            username: {required: true},
            email: {required: true},
            password: {required: true},
            confirmPassword: {required: true}
        }
    });

    $("#btnDismiss").click(function () {
        if (doneEdit) {
            $.get("{{ url('/api/get_list_user') }}",
                {ajax: 'true'}, function (data) {
                    $("#theData").html('');
                    $("#theData").html(data);
            });
        }
    });

    $("#btnSave").click(function() {
        if ($("#idPassword").val().trim() != $("#idConfirmPassword").val().trim()) {
            $("#errorConfirmPassword").show();
            return false;
        }

        $("#id-lbl-name").html('Name: <b>' + $('[name="name"]').val() + '</b>');
        $("#id-lbl-username").html('Username: <b>' + $('[name="username"]').val() + '</b>');
        $("#id-lbl-email").html('Email: <b>' + $('[name="email"]').val() + '</b>');
        $("#id-lbl-role").html('Role: <b>' + $("#idRoleId option:selected").text() + '</b>');

    });

    $("#btnCommitChanges").click(function () {
        $("#waitCommit").html('<div class="col-md-8 col-sm-8 col-xs-8">Saving data ... ' +
                                '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');

        $("#btnCommitChanges").attr("disabled","disabled");
        $.post("{{ url('/api/create_new_user') }}", $( "#myform" ).serialize(),
            function (data) {
                $("#waitCommit").html('');
                if (data['Error']) {
                    $("#idErrorMessage").html(
                        '<div class="alert alert-error"><button class="close" data-dismiss="alert"></button>' +
                            'Error while saving to database: ' + data['Message'] + '</div>');
                } else {
                    doneEdit = true;
                    $("#idAlertSuccess").html(
                        '<div class="alert alert-success"><button class="close" data-dismiss="alert"></button>' +
                            'Success:&nbsp;The new user ' + $('[name="name"]').val() + ' has been added</div>');
                }
            })
            .done(function () {
                 //alert("second success");
             })
            .fail(function (jqXHR, textStatus) {
                if (jqXHR.status == 401) {
                    window.location = "{{ url('/login') }}";
                }
                else {
                    $("#idAlertSuccess").html(
                        'Error while create new user! Please try again later or contact system Administrator')
                }
            })
            .always(function () {
                 //alert("finished");
            });
    });

</script>
