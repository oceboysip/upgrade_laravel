<?php
/**
 * This is the main for Manage Service List page
 *
 */
?>
<div class="row">
    <div class="grid simple">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>Manage <span class="semi-bold">Webtools User</span></h3>
                    <p>Set how user login to dashboard</p>
                    <div class="row">
                        <div class="col-md-6">
                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewListUser"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               style="margin-right: 30px;"
                               data-colorpicker-guid="8">View User</a>

                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="createNewUser"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               style="margin-right: 30px;"
                               data-colorpicker-guid="8">Add User</a>

                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="manageRole"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               data-colorpicker-guid="8">Manage Role</a>

                        </div>
                        <!--
                        <div class="col-md-6 dataTables_filter">
                            <form method="GET">
                            <label>Search User
                                <input id="idSearchUserText" class="input-medium" type="text" aria-controls="example"></input>
                            </label>
                            </form>
                        </div>
                        -->
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row tableOverflow" id="theData"></div>


