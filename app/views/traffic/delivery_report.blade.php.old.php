@extends('content.manage_content-layout')
@include("main_js-layout")

@section('main_content')
    <div class="row">
        <div class="grid simple" style="width:2000px;">
            <form id="myTrafficForm" method="POST">
                <div class="grid-body no-border">
                    <div class="row">
                        <div class="col-md-10">
                            <h3>Delivery <span class="semi-bold">Report</span></h3>
                            <p>
                                <select id="sdcSelect" style="width:20%;" name="sdc">
                                    <optgroup label="Choose SDC">
                                        @foreach ($sdc as $each_sdc)
                                            <option value="{{ $each_sdc->name }}">{{ $each_sdc->name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </p>

                            Date Range:<br/>
                            <input type="text" name="dtStart" id="dateStart" class="theDate">
                            <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
                            &nbsp; - &nbsp;
                            <input type="text" name="dtEnd" id="toDate" class="theDate">
                            <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>

                            <br/>
                            <br/>
                            <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewDeliveryReport"
                               class="btn btn-primary my-colorpicker-control" href="#"
                               data-colorpicker-guid="8">View</a>

                        </div>
                    </div>
                </div>
            </form>
            <div id="waitQuery" class="row form-row"></div>
        </div>
    </div>

    <div class="row" id="theData"></div>
    <script type="text/javascript">

        $("#viewDeliveryReport").click(function(){
            $("#waitQuery").html('<div class="col-md-8 col-sm-8 col-xs-8">Querying ... ' + '<i class="icon-spinner icon-spin icon-large icon-2x"></i></div>');
            $("#viewDeliveryReport").attr("disabled", "disabled");

            $.post("{{url('/traffic/delivery_report')}}", $("#myTrafficForm").serialize(),
                    function (data) {
                        $("#viewDeliveryReport").removeAttr("disabled");
                        $("#waitQuery").html('');
                        $("#theData").html('');
                        $("#theData").html(data);
                    }
                )
                .done(function () {
                    $("#viewDeliveryReport").removeAttr("disabled");
                })
                .fail(function (jqXHR, textStatus) {
                    $("#viewDeliveryReport").removeAttr("disabled");
                    if (jqXHR.status == 401) {
                        window.location = "{{url('/login')}}}";
                    }
                    else {
                        $("#idQueryAlertSuccess").html(
                                'Error while edit service! Please try again later or contact system Administrator')
                    }
                })
                .always(function () {
                    //alert("finished");
                });
        });

    </script>
@stop
