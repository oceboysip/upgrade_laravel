@if (isset($the_dataOri))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Push Original - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_DELIVRD</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_505</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3101</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <!--
	                    <th>dr_3306</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1375</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3103</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                	-->
	                    <th>dr_3320</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1031</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3309</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1001</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_2208</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_EXPIRED</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1004</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_UNDELIV</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3108</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_511</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_512</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_514</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_OTHERS</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataOri) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataOri as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_DELIVRD}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_DELIVRD_p}}</i></td>
								<td>{{$each_data->dr_505}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_505_p}}</i></td>
								<td>{{$each_data->dr_3101}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3101_p}}</i></td>
								<!--<td>{{$each_data->dr_3306}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3306_p}}</i></td>
								<td>{{$each_data->dr_1375}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1375_p}}</i></td>
								<td>{{$each_data->dr_3103}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3103_p}}</i></td>
								-->
								<td>{{$each_data->dr_3320}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3320_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_1031}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1031_p}}</i></td>
								<td>{{$each_data->dr_3309}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3309_p}}</i></td>
								<td>{{$each_data->dr_1001}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1001_p}}</i></td>
								<td>{{$each_data->dr_2208}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2208_p}}</i></td>
								<td>{{$each_data->dr_EXPIRED}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_EXPIRED_p}}</i></td>
								<td>{{$each_data->dr_1004}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1004_p}}</i></td>
								<td>{{$each_data->dr_UNDELIV}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_UNDELIV_p}}</i></td>
								<td>{{$each_data->dr_3108}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3108_p}}</i></td>
								<td>{{$each_data->dr_511}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_511_p}}</i></td>
								<td>{{$each_data->dr_512}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_512_p}}</i></td>
								<td>{{$each_data->dr_514}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_514_p}}</i></td>
								<td>{{$each_data->dr_OTHERS}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_OTHERS_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataRetry))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Retry - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_DELIVRD</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_505</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3101</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <!--<th>dr_3306</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1375</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3103</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    -->
	                    <th>dr_3320</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1031</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3309</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1001</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_2208</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_EXPIRED</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1004</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_UNDELIV</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3108</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_511</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_512</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_514</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_OTHERS</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataRetry) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataRetry as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_DELIVRD}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_DELIVRD_p}}</i></td>
								<td>{{$each_data->dr_505}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_505_p}}</i></td>
								<td>{{$each_data->dr_3101}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3101_p}}</i></td>
								<!--<td>{{$each_data->dr_3306}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3306_p}}</i></td>
								<td>{{$each_data->dr_1375}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1375_p}}</i></td>
								<td>{{$each_data->dr_3103}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3103_p}}</i></td>
								-->
								<td>{{$each_data->dr_3320}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3320_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_1031}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1031_p}}</i></td>
								<td>{{$each_data->dr_3309}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3309_p}}</i></td>
								<td>{{$each_data->dr_1001}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1001_p}}</i></td>
								<td>{{$each_data->dr_2208}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2208_p}}</i></td>
								<td>{{$each_data->dr_EXPIRED}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_EXPIRED_p}}</i></td>
								<td>{{$each_data->dr_1004}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1004_p}}</i></td>
								<td>{{$each_data->dr_UNDELIV}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_UNDELIV_p}}</i></td>
								<td>{{$each_data->dr_3108}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3108_p}}</i></td>
								<td>{{$each_data->dr_511}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_511_p}}</i></td>
								<td>{{$each_data->dr_512}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_512_p}}</i></td>
								<td>{{$each_data->dr_514}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_514_p}}</i></td>
								<td>{{$each_data->dr_OTHERS}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_OTHERS_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataPush1))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Push #1 - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_DELIVRD</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_505</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3101</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <!--<th>dr_3306</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1375</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3103</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    -->
	                    <th>dr_3320</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1031</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3309</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1001</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_2208</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_EXPIRED</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1004</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_UNDELIV</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3108</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_511</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_512</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_514</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_OTHERS</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataPush1) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataPush1 as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_DELIVRD}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_DELIVRD_p}}</i></td>
								<td>{{$each_data->dr_505}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_505_p}}</i></td>
								<td>{{$each_data->dr_3101}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3101_p}}</i></td>
								<!--<td>{{$each_data->dr_3306}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3306_p}}</i></td>
								<td>{{$each_data->dr_1375}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1375_p}}</i></td>
								<td>{{$each_data->dr_3103}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3103_p}}</i></td>
								-->
								<td>{{$each_data->dr_3320}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3320_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_1031}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1031_p}}</i></td>
								<td>{{$each_data->dr_3309}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3309_p}}</i></td>
								<td>{{$each_data->dr_1001}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1001_p}}</i></td>
								<td>{{$each_data->dr_2208}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2208_p}}</i></td>
								<td>{{$each_data->dr_EXPIRED}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_EXPIRED_p}}</i></td>
								<td>{{$each_data->dr_1004}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1004_p}}</i></td>
								<td>{{$each_data->dr_UNDELIV}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_UNDELIV_p}}</i></td>
								<td>{{$each_data->dr_3108}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3108_p}}</i></td>
								<td>{{$each_data->dr_511}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_511_p}}</i></td>
								<td>{{$each_data->dr_512}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_512_p}}</i></td>
								<td>{{$each_data->dr_514}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_514_p}}</i></td>
								<td>{{$each_data->dr_OTHERS}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_OTHERS_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataAll))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - All - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_DELIVRD</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_505</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3101</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <!--<th>dr_3306</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1375</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3103</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    -->
	                    <th>dr_3320</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1031</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3309</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1001</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_2208</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_EXPIRED</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1004</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_UNDELIV</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3108</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_511</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_512</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_514</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_OTHERS</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataAll) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataAll as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_DELIVRD}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_DELIVRD_p}}</i></td>
								<td>{{$each_data->dr_505}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_505_p}}</i></td>
								<td>{{$each_data->dr_3101}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3101_p}}</i></td>
								<!--<td>{{$each_data->dr_3306}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3306_p}}</i></td>
								<td>{{$each_data->dr_1375}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1375_p}}</i></td>
								<td>{{$each_data->dr_3103}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3103_p}}</i></td>
								-->
								<td>{{$each_data->dr_3320}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3320_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_1031}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1031_p}}</i></td>
								<td>{{$each_data->dr_3309}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3309_p}}</i></td>
								<td>{{$each_data->dr_1001}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1001_p}}</i></td>
								<td>{{$each_data->dr_2208}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2208_p}}</i></td>
								<td>{{$each_data->dr_EXPIRED}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_EXPIRED_p}}</i></td>
								<td>{{$each_data->dr_1004}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1004_p}}</i></td>
								<td>{{$each_data->dr_UNDELIV}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_UNDELIV_p}}</i></td>
								<td>{{$each_data->dr_3108}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3108_p}}</i></td>
								<td>{{$each_data->dr_511}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_511_p}}</i></td>
								<td>{{$each_data->dr_512}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_512_p}}</i></td>
								<td>{{$each_data->dr_514}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_514_p}}</i></td>
								<td>{{$each_data->dr_OTHERS}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_OTHERS_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataOther))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Others - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_DELIVRD</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_505</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3101</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <!--<th>dr_3306</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1375</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3103</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    -->
	                    <th>dr_3320</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1031</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3309</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1001</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_2208</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_EXPIRED</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_1004</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_UNDELIV</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3108</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_511</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_512</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_514</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_OTHERS</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataOther) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataOther as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_DELIVRD}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_DELIVRD_p}}</i></td>
								<td>{{$each_data->dr_505}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_505_p}}</i></td>
								<td>{{$each_data->dr_3101}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3101_p}}</i></td>
								<!--<td>{{$each_data->dr_3306}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3306_p}}</i></td>
								<td>{{$each_data->dr_1375}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1375_p}}</i></td>
								<td>{{$each_data->dr_3103}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3103_p}}</i></td>
								-->
								<td>{{$each_data->dr_3320}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3320_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_1031}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1031_p}}</i></td>
								<td>{{$each_data->dr_3309}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3309_p}}</i></td>
								<td>{{$each_data->dr_1001}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1001_p}}</i></td>
								<td>{{$each_data->dr_2208}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2208_p}}</i></td>
								<td>{{$each_data->dr_EXPIRED}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_EXPIRED_p}}</i></td>
								<td>{{$each_data->dr_1004}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_1004_p}}</i></td>
								<td>{{$each_data->dr_UNDELIV}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_UNDELIV_p}}</i></td>
								<td>{{$each_data->dr_3108}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3108_p}}</i></td>
								<td>{{$each_data->dr_511}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_511_p}}</i></td>
								<td>{{$each_data->dr_512}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_512_p}}</i></td>
								<td>{{$each_data->dr_514}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_514_p}}</i></td>
								<td>{{$each_data->dr_OTHERS}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_OTHERS_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<style>
	.closeBelow {
		cursor: pointer;
	}
</style>
<script>
	$(".closeBelow").click(function () {
		var dt = this.getAttribute('date');
		if ($(".hm-" + dt).is(":visible") ) $(".hm-" + dt).hide();
		else {$(".hm-" + dt).show();}
	});
	$(".hideme").hide();
</script>

@endif
