<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>COA Report of <span class="semi-bold">{{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<table class="table table-hover table-bordered" id="example2">
				<thead>
				<tr>
					<th>Date</th>
					<th>Total Broadcast</th>
					<th>Total REG</th>
                    <th>COA(idr)</th>
                    <th>Conversion Rate(%)</th>
                </tr>
				</thead>
				<tbody>
					@foreach ($total_reg as $datetime => $each_data)
                        <?php
                                if($each_data == 0)
                                {
                                        $coa = 0;
                                        $conv = 0;
                                }
                                else
                                {
                                	//$koef = 65.40813176;
                                	$koef = 61;
                                	if($telcoId == '1') {
                                		$koef = 29;
                                	} elseif($telcoId == '3') {
                                        $koef = 35;
                                    }
                                    $coa = round(@$total_bc[$datetime]*$koef/$each_data);
                                    if(@$total_bc[$datetime]==0){
                                        $conv = 0;
                                    }else{
                                        $conv = round($each_data/@$total_bc[$datetime]*100, 2);
                                    }
                                }
                        ?>
                        <tr class="even gradeA">
                            <td>{{$datetime}}</td>
                            <td>{{intval(@$total_bc[$datetime])}}</td>
                            <td>{{@$each_data}}</td>
                            <td>{{$coa}}</td>
                            <td>{{$conv}} %</td>
                        </tr>

					@endforeach
                </tbody>
			</table>
		</div>
	</div>
</div>


<style>
	.closeBelow {
		cursor: pointer;
	}
</style>
<script>
	$(".closeBelow").click(function () {
		var dt = this.getAttribute('date');
		if ($(".hm-" + dt).is(":visible") ) $(".hm-" + dt).hide();
		else {$(".hm-" + dt).show();}
	});
	$(".hideme").hide();
</script>
