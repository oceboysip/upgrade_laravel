@if (isset($the_dataOri))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Push Original - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_2</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_4</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_5</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_6</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_8</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_96</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_97</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_99</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataOri) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataOri as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_2}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2_p}}</i></td>
								<td>{{$each_data->dr_3}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3_p}}</i></td>
								<td>{{$each_data->dr_4}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_4_p}}</i></td>
								<td>{{$each_data->dr_5}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_5_p}}</i></td>
								<td>{{$each_data->dr_6}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_6_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_8}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_8_p}}</i></td>
								<td>{{$each_data->dr_96}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_96_p}}</i></td>
								<td>{{$each_data->dr_97}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_97_p}}</i></td>
								<td>{{$each_data->dr_99}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_99_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataRetry))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Retry - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_2</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_4</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_5</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_6</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_8</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_96</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_97</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_99</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataRetry) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataRetry as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_2}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2_p}}</i></td>
								<td>{{$each_data->dr_3}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3_p}}</i></td>
								<td>{{$each_data->dr_4}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_4_p}}</i></td>
								<td>{{$each_data->dr_5}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_5_p}}</i></td>
								<td>{{$each_data->dr_6}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_6_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_8}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_8_p}}</i></td>
								<td>{{$each_data->dr_96}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_96_p}}</i></td>
								<td>{{$each_data->dr_97}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_97_p}}</i></td>
								<td>{{$each_data->dr_99}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_99_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataPush1))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Push #1 - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_2</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_4</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_5</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_6</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_8</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_96</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_97</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_99</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataPush1) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataPush1 as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_2}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2_p}}</i></td>
								<td>{{$each_data->dr_3}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3_p}}</i></td>
								<td>{{$each_data->dr_4}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_4_p}}</i></td>
								<td>{{$each_data->dr_5}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_5_p}}</i></td>
								<td>{{$each_data->dr_6}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_6_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_8}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_8_p}}</i></td>
								<td>{{$each_data->dr_96}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_96_p}}</i></td>
								<td>{{$each_data->dr_97}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_97_p}}</i></td>
								<td>{{$each_data->dr_99}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_99_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataAll))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - ALL - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_2</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_4</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_5</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_6</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_8</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_96</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_97</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_99</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataAll) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataAll as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_2}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2_p}}</i></td>
								<td>{{$each_data->dr_3}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3_p}}</i></td>
								<td>{{$each_data->dr_4}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_4_p}}</i></td>
								<td>{{$each_data->dr_5}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_5_p}}</i></td>
								<td>{{$each_data->dr_6}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_6_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_8}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_8_p}}</i></td>
								<td>{{$each_data->dr_96}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_96_p}}</i></td>
								<td>{{$each_data->dr_97}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_97_p}}</i></td>
								<td>{{$each_data->dr_99}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_99_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

@if (isset($the_dataOther))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Delivery Report of <span class="semi-bold"> - Others - {{ $sdcSelect }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<div style="overflow:auto; width:100%;">
				<table class="table table-hover table-bordered" id="example2">
					<thead>
					<tr>
						<th>Date</th>
						<th>Telco SID</th>
						<th>dr_2</th>
						<th style="background:#E9CFFF;">percent</th>
	                    <th>dr_3</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_4</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_5</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_6</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_7</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_8</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_96</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_97</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>dr_99</th>
	                    <th style="background:#E9CFFF;">percent</th>
	                    <th>total</th>
	                </tr>
					</thead>
					<tbody>
					@if (sizeof($the_dataOther) < 1)
					<tr class="even gradeA">
						<td colspan=11> No data</td>
					</tr>
					@else
					<?php $rowOpen = false; ?>
						@foreach ($the_dataOther as $each_data)
	                        <tr class="even gradeA" style="text-align:right;">
	                            <td>{{$each_data->dtreport}}</td>
								<td>{{$each_data->telco_sid}}</td>
								<td>{{$each_data->dr_2}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_2_p}}</i></td>
								<td>{{$each_data->dr_3}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_3_p}}</i></td>
								<td>{{$each_data->dr_4}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_4_p}}</i></td>
								<td>{{$each_data->dr_5}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_5_p}}</i></td>
								<td>{{$each_data->dr_6}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_6_p}}</i></td>
								<td>{{$each_data->dr_7}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_7_p}}</i></td>
								<td>{{$each_data->dr_8}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_8_p}}</i></td>
								<td>{{$each_data->dr_96}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_96_p}}</i></td>
								<td>{{$each_data->dr_97}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_97_p}}</i></td>
								<td>{{$each_data->dr_99}}</td>
								<td style="background-color:#ecf0f2;"><i style="font-size:12px;">{{$each_data->dr_99_p}}</i></td>
								<td>{{$each_data->total}}</td>
	                            
	                        </tr>

						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<style>
	.closeBelow {
		cursor: pointer;
	}
</style>
<script>
	$(".closeBelow").click(function () {
		var dt = this.getAttribute('date');
		if ($(".hm-" + dt).is(":visible") ) $(".hm-" + dt).hide();
		else {$(".hm-" + dt).show();}
	});
	$(".hideme").hide();
</script>

@endif
