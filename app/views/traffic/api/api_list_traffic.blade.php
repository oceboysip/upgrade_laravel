@if (isset($the_data))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Traffic of <span class="semi-bold">{{ $serviceName }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<table class="table table-hover-custom table-bordered-custom" id="example2">
				<thead>
				<tr>
					<th>DATE</th>
					<th colspan="3" style="text-align:center;">TOTAL</th>
					<th colspan="5" style="text-align:center;">ORIGINAL</th>
					<th colspan="3" style="text-align:center;">RETRY</th>
					<th colspan="5" style="text-align:center;">PUSH #1</th>
					<th>&nbsp;</th>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<th>Sent</th>
					<th>Delivered</th>
					<th>%</th>
					<th>Sent</th>
					<th>Ori Sent</th>
					<th>Delivered</th>
					<th>All %</th>
					<th>Ori %</th>
					<th>Sent</th>
					<th>Delivered</th>
					<th>%</th>
					<th>Sent</th>
					<th>Ori Sent</th>
					<th>Delivered</th>
					<th>All %</th>
					<th>Ori %</th>
					<th>Total Revenue (IDR)</th>
				</tr>

				</thead>
				<tbody>
				<?php $tS = 0;$tSuc = 0;$tOr = 0;$tOrRe = 0;$tOrSuc = 0;$tR = 0;$tRSuc = 0;$tG = 0; $tN = 0; $dt= ''; $tmS=0;$tmSuc=0;$tmR=0;$tmRSuc=0;$tmG=0;$tmN=0; $tp1 = 0; $tpo1 = 0; $tsp1 = 0;  ?>
				@if (sizeof($the_data) < 1)
				<tr class="even gradeA">
					<td colspan=11> No data</td>
				</tr>
				@else
				<?php $rowOpen = false; ?>
					@foreach ($the_data as $each_data)
						@if ($dt == $each_data->dtstat)

				<tr class="even gradeA hideme hm-{{ $each_data->dtstat }}">
					<td>-&nbsp;&nbsp;&nbsp;&nbsp;<b>SID: {{ $each_data->telco_sid }}</b> (IDR. {{ number_format($each_data->cp_price) }})</td>
					<td>{{ number_format($each_data->total_sent) }}</td>
					<td>{{ number_format($each_data->total_success) }}</td>
                    <td>{{ number_format($each_data->total_success/$each_data->total_sent*100, 2) }} %</td>
					<td>{{ number_format($each_data->total_original) }}</td>
					<td>{{ number_format($each_data->total_original_real) }}</td>
					<td>{{ number_format($each_data->total_success_ori) }}</td>
					<?php
                        if($tOr == 0){
                            $percentage_ori = 0;
                        }else{
                        	$percentage_ori = round($tOrSuc/$tOr*100, 2);
                        }
                        if($each_data->total_original_real == 0){
							$percentage_ori_real = 0;
						}else{
							$percentage_ori_real = round($tOrSuc/$each_data->total_original_real*100, 2);
						}
                    ?>
					<td>{{$percentage_ori}} %</td>
					<td>{{$percentage_ori_real}} %</td>
					<td>{{ number_format($each_data->total_retry) }}</td>
					<td>{{ number_format($each_data->total_success_retry) }}</td>                    
                    <?php
                        if($tR == 0){
                            $percentage_retry = 0;
                        }else{
                        	$percentage_retry = round($tRSuc/$tR*100, 2);
                        }
                    ?>
					<td>{{$percentage_retry}} %</td>
					<td>{{ number_format($each_data->total_push1) }}</td>
					<td>{{ number_format($each_data->total_push1_original) }}</td>
					<td>{{ number_format($each_data->total_success_push1) }}</td>
					<?php
                        if($each_data->total_push1 == 0){
                            $percentage_push1 = 0;
                        }else{
                        	$percentage_push1 = round($each_data->total_success_push1/$each_data->total_push1*100, 2);
                        }
                        if($each_data->total_push1_original == 0){
                            $percentage_push1ori = 0;
                        }else{
                        	$percentage_push1ori = round($each_data->total_success_push1/$each_data->total_push1_original*100, 2);
                        }
                    ?>
					<td>{{$percentage_push1}} %</td>
					<td>{{$percentage_push1ori}} %</td>
					<td>{{ number_format($each_data->total_gross) }}</td>

					<!--<td>{{ number_format($each_data->total_net) }}</td>-->
				</tr>
							<?php
								$tmS += $each_data->total_sent;
								$tmSuc += $each_data->total_success;
								// $tmOr += $each_data->total_original;
								// $tmOrSuc += $each_data->total_success_ori;
								$tmR += $each_data->total_retry;
								$tmRSuc += $each_data->total_success_retry;
								$tmG += $each_data->total_gross;
								$tmN += $each_data->total_net;
							?>
						@else

				<tr class="even gradeA">
					<td><a class="closeBelow" date="{{ $each_data->dtstat }}">{{ $each_data->dtstat }}</a></td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tS']) }}</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tSuc']) }}</td>
                    <td>{{ number_format($the_data_per_date[$each_data->dtstat]['tSuc']/$the_data_per_date[$each_data->dtstat]['tS']*100, 2) }} %</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tOr']) }}</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tOrRe']) }}</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tOrSuc']) }}</td>
					<?php
                        if($the_data_per_date[$each_data->dtstat]['tOr'] == 0){
                            $percentage_ori = 0;
                        }else{
                        	$percentage_ori = round($the_data_per_date[$each_data->dtstat]['tOrSuc']/
                        								$the_data_per_date[$each_data->dtstat]['tOr']*100, 2);
                        }
                        if($the_data_per_date[$each_data->dtstat]['tOrRe'] == 0){
                            $percentage_ori_real = 0;
                        }else{
                        	$percentage_ori_real = round($the_data_per_date[$each_data->dtstat]['tOrSuc']/
                        								$the_data_per_date[$each_data->dtstat]['tOrRe']*100, 2);
                        }
                    ?>
					<td>{{$percentage_ori}} %</td>
					<td>{{$percentage_ori_real}} %</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tR']) }}</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tRSuc']) }}</td>
                    <?php
                        if($the_data_per_date[$each_data->dtstat]['tR'] == 0){
                            $percentage_retry = 0;
                        }else{
                        	$percentage_retry = round($the_data_per_date[$each_data->dtstat]['tRSuc']/
                        								$the_data_per_date[$each_data->dtstat]['tR']*100, 2);
                        }
                    ?>
					<td>{{$percentage_retry}} %</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tp1']) }}</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tpo1']) }}</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tsp1']) }}</td>
					<?php
                        if($the_data_per_date[$each_data->dtstat]['tp1'] == 0){
                            $percentage_tp1 = 0;
                        }else{
                        	$percentage_tp1 = round($the_data_per_date[$each_data->dtstat]['tsp1']/
                        								$the_data_per_date[$each_data->dtstat]['tp1']*100, 2);
                        }
                        if($the_data_per_date[$each_data->dtstat]['tpo1'] == 0){
                            $percentage_tpo1 = 0;
                        }else{
                        	$percentage_tpo1 = round($the_data_per_date[$each_data->dtstat]['tsp1']/
                        								$the_data_per_date[$each_data->dtstat]['tpo1']*100, 2);
                        }
                    ?>
					<td>{{$percentage_tp1}}%</td>
					<td>{{$percentage_tpo1}}%</td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tG']) }}</td>
                    
                    <!--<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tS']) }}</td>-->
				<tr class="even gradeA hideme hm-{{ $each_data->dtstat }}">
					<td>- &nbsp;&nbsp;&nbsp;&nbsp;<b>SID: {{ $each_data->telco_sid }}</b> (IDR. {{number_format($each_data->cp_price) }})
					</td>
					<td>{{ number_format($each_data->total_sent) }}</td>
					<td>{{ number_format($each_data->total_success) }}</td>
                    <td>{{ number_format($each_data->total_success/$each_data->total_sent*100, 2) }} %</td>
					<td>{{ number_format($each_data->total_original) }}</td>
					<td>{{ number_format($each_data->total_original_real) }}</td>
					<td>{{ number_format($each_data->total_success_ori) }}</td>
					<?php
                        if($tOr == 0){
                            $percentage_ori = 0;
                        }else{
                        	$percentage_ori = round($tOrSuc/$tOr*100, 2);
                        }
                        if($tOrRe == 0){
                            $percentage_ori_real = 0;
                        }else{
                        	$percentage_ori_real = round($tOrSuc/$tOrRe*100, 2);
                        }
                    ?>
                    <td>{{$percentage_ori}} %</td>
                    <td>{{$percentage_ori_real}} %</td>
					<td>{{ number_format($each_data->total_retry) }}</td>
					<td>{{ number_format($each_data->total_success_retry) }}</td>
                    <?php
                        if($tR == 0){
                            $percentage_retry = 0;
                        }else{
                        	$percentage_retry = round($tRSuc/$tR*100, 2);
                        }
                    ?>
					<td>{{$percentage_retry}} %</td>
					<td>{{ number_format($each_data->total_push1) }}</td>
					<td>{{ number_format($each_data->total_push1_original) }}</td>
					<td>{{ number_format($each_data->total_success_push1) }}</td>
					<?php
                        if($each_data->total_push1 == 0){
                            $percentage_push1 = 0;
                        }else{
                        	$percentage_push1 = round($each_data->total_success_push1/$each_data->total_push1*100, 2);
                        }
                        if($each_data->total_push1_original == 0){
                            $percentage_push1ori = 0;
                        }else{
                        	$percentage_push1ori = round($each_data->total_success_push1/$each_data->total_push1_original*100, 2);
                        }
                    ?>
					<td>{{$percentage_push1}} %</td>
					<td>{{$percentage_push1ori}} %</td>
					<td>{{ number_format($each_data->total_gross) }}</td>

                    <!--<td>{{ number_format($each_data->total_net) }}</td>-->
				</tr>

						@endif

						<?php

						$tS += $each_data->total_sent;
						$tSuc += $each_data->total_success;
						$tOr += $each_data->total_original;
						$tOrRe += $each_data->total_original_real;
						$tOrSuc += $each_data->total_success_ori;
						$tR += $each_data->total_retry;
						$tRSuc += $each_data->total_success_retry;
						$tG += $each_data->total_gross;
						$tN += $each_data->total_net;
						$dt = $each_data->dtstat;
						$tp1 += $each_data->total_push1;
						$tpo1 += $each_data->total_push1_original;
						$tsp1 += $each_data->total_success_push1;
						?>
					@endforeach

					<tr class="even gradeA">
						<td><b>TOTAL (IDR)</b></td>
						<td>{{ number_format($tS) }}</td>
						<td>{{ number_format($tSuc) }}</td>
                        <td>{{ number_format($tSuc/$tS*100, 2) }} %</td>
						<td>{{ number_format($tOr) }}</td>
						<td>{{ number_format($tOrRe) }}</td>
						<td>{{ number_format($tOrSuc) }}</td>
						<?php
	                        if($tOr == 0){
	                            $percentage_ori = 0;
	                        }else{
	                        	$percentage_ori = round($tOrSuc/$tOr*100, 2);
	                        }
	                        if($tOrRe == 0){
	                            $percentage_ori_real = 0;
	                        }else{
	                        	$percentage_ori_real = round($tOrSuc/$tOrRe*100, 2);
	                        }
	                    ?>
						<td>{{$percentage_ori}} %</td>
						<td>{{$percentage_ori_real}} %</td>
						<td>{{ number_format($tR) }}</td>
						<td>{{ number_format($tRSuc) }}</td>
                        <?php
	                        if($tR == 0){
	                            $percentage_retry = 0;
	                        }else{
	                        	$percentage_retry = round($tRSuc/$tR*100, 2);
	                        }
	                    ?>
						<td>{{$percentage_retry}} %</td>
						<td>{{$tp1}}</td>
						<td>{{$tpo1}}</td>
						<td>{{$tsp1}}</td>
						<?php
	                        if($tp1 == 0){
	                            $percentage_p1 = 0;
	                        }else{
	                        	$percentage_p1 = round($tsp1/$tp1*100, 2);
	                        }
	                        if($tpo1 == 0){
	                            $percentage_po1 = 0;
	                        }else{
	                        	$percentage_po1 = round($tsp1/$tpo1*100, 2);
	                        }
	                    ?>
						<td>{{$percentage_p1}}%</td>
						<td>{{$percentage_po1}}%</td>
						<td>{{ number_format($tG) }}</td>

						<!--<td>{{ number_format($tN) }}</td>-->
					</tr>
				@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="myModalEditContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div id="theModalContent"></div>
		</div>
	</div>
</div>


<style>
	.closeBelow {
		cursor: pointer;
	}
</style>
<script>
	$(".closeBelow").click(function () {
		var dt = this.getAttribute('date');
		if ($(".hm-" + dt).is(":visible") ) $(".hm-" + dt).hide();
		else {$(".hm-" + dt).show();}
	});
	$(".hideme").hide();
</script>

@endif
