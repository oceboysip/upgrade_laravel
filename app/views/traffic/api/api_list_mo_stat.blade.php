@if (isset($the_data))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>MO statistic of <span class="semi-bold">{{ $serviceName }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<table class="table table-hover table-bordered" id="example2">
				<thead>
				<tr>
					<th>Date</th>
					<th>Service / Detail</th>
					<th>Total</th>
				</tr>
				</thead>
				<tbody>
				<?php $tS = 0;$tSuc = 0;$tG = 0; $tN = 0; $dt= ''; $tmS=0;$tmSuc=0;$tmG=0; $tmN=0;  ?>
				@if (sizeof($the_data) < 1)
				<tr class="even gradeA">
					<td colspan=10> No data</td>
				</tr>
				@else
				<?php $rowOpen = false; ?>
					@foreach ($the_data as $each_data)
						@if ($dt == $each_data->dtstat)

				<tr class="even gradeA hideme hm-{{ $each_data->dtstat }}">
					<td></td>
					<td>- &nbsp;&nbsp;&nbsp;&nbsp; {{ $each_data->name }}: <b>{{ $each_data->keyword }}</b></td>
					<td>{{ number_format($each_data->total) }}</td>
				</tr>
							<?php
								$tmS += $each_data->total;
							?>
						@else

				<tr class="even gradeA">
					<td><a class="closeBelow" date="{{ $each_data->dtstat }}">{{ $each_data->dtstat }}</a></td>
					<td> ... </td>
					<td>{{ number_format($the_data_per_date[$each_data->dtstat]['tS']) }}</td>
				</tr>
				<tr class="even gradeA hideme hm-{{ $each_data->dtstat }}">
					<td></td>
					<td>- &nbsp;&nbsp;&nbsp;&nbsp; {{ $each_data->name }}: <b>{{ $each_data->keyword }}</b></td>
					<td>{{ number_format($each_data->total) }}</td>
				</tr>

						@endif

						<?php

						$tS += $each_data->total;
						$dt = $each_data->dtstat;

						?>
					@endforeach

					<tr class="even gradeA">
						<td><b>TOTAL</b></td>
						<td></td>
						<td><b>{{ number_format($tS) }}</b></td>
					</tr>
				@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="myModalEditContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div id="theModalContent"></div>
		</div>
	</div>
</div>


<style>
	.closeBelow {
		cursor: pointer;
	}
</style>
<script>
	$(".closeBelow").click(function () {
		var dt = this.getAttribute('date');
		if ($(".hm-" + dt).is(":visible") ) $(".hm-" + dt).hide();
		else {$(".hm-" + dt).show();}
	});
	$(".hideme").hide();
</script>

@endif
