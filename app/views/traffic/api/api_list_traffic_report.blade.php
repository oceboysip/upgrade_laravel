@if (isset($the_data))
<div class="span12">
	<div class="grid simple ">
		<div class="grid-title">
			<h4>Traffic of <span class="semi-bold">{{ $serviceName }}  ({{ $dtStart }} to {{ $dtEnd }})</span></h4>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="grid-body ">
			<table class="table table-hover table-bordered" id="example2">
				<thead>
				<tr>
					<th>Date</th>
					<th>Success Original</th>
					<th>Original Sent</th>
					<th>Success Retry</th>
					<th>Retry Sent</th>
					<th>Success Push 1</th>
                    <th>Push 1 Sent</th>
					<th>Success Retry Push 1</th>
                    <th>Retry Sent Push 1</th>
				</tr>
				</thead>
				<tbody>
				<?php $tS = 0;$tSuc = 0;$tR = 0;$tRSuc = 0;$tG = 0; $tN = 0; $dt= ''; $tmS=0;$tmSuc=0;$tmR=0;$tmRSuc=0;$tmG=0;$tmN=0;  ?>
				@if (sizeof($the_data) < 1)
				<tr class="even gradeA">
					<td colspan=11> No data</td>
				</tr>
				@else
					@foreach ($the_data as $each_data)
                        <tr class="even gradeA">
                            <td>{{$each_data->dtreport}}</td>
                            <td>{{$each_data->success_original}}</td>
                            <td>{{$each_data->original_sent}}</td>
                            <td>{{$each_data->success_retry}}</td>
                            <td>{{$each_data->retry_sent}}</td>
                            <td>{{$each_data->success_push_1}}</td>
                            <td>{{$each_data->push_1_sent}}</td>
                            <td>{{$each_data->success_retry_push_1}}</td>
                            <td>{{$each_data->retry_sent_push_1}}</td>
                        </tr>
					@endforeach
				@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="myModalEditContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div id="theModalContent"></div>
		</div>
	</div>
</div>


<style>
	.closeBelow {
		cursor: pointer;
	}
</style>
<script>
	$(".closeBelow").click(function () {
		var dt = this.getAttribute('date');
		if ($(".hm-" + dt).is(":visible") ) $(".hm-" + dt).hide();
		else {$(".hm-" + dt).show();}
	});
	$(".hideme").hide();
</script>

@endif
