<?php
/**
 * This is the main for MO Stat page
 *
 */
?>
<div class="row">
    <div class="grid simple">
		<form id="myTrafficForm" method="POST">
        <div class="grid-body no-border">
            <div class="row">
                <div class="col-md-10">
                    <h3>MO <span class="semi-bold">Statistic</span></h3>
                    <p>
                        <select id="sdcSelect" style="width:20%;" name="sdc">
                            <optgroup label="Choose SDC">
                                @foreach ($sdc as $each_sdc)
                                <option value="{{ $each_sdc->name }}">{{ $each_sdc->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                        <select id="telcoSelect" style="width:20%;" name="telcoId">
                            <optgroup label="Choose Telco">
                                @foreach ($telco as $each_telco)
                                <option value="{{ $each_telco->id }}">{{ $each_telco->name }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </p>
                    <p>
                        <select id="serviceSelect" style="width:50%" name="serviceId">
							<optgroup label="Choose Telco and SDC First">
                                @if(isset($service))
									<option value="0"> -- All --</option>
									@foreach ($service as $each_service)
                                    <option value="{{ $service->id }}">{{ $service->name }}</option>
                                    @endforeach
                                @endif
                            </optgroup>
                        </select>
                    </p>

                    Date Range:<br/>
                    <input type="text" name="dtStart" id="dateStart" class="theDate">
                    <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
                    &nbsp; - &nbsp;
                    <input type="text" name="dtEnd" id="toDate" class="theDate">
                    <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
					<br/>
                    <br/>
					Keyword Breakdown: <input type="checkbox" name="keywordBreakdown"/><br/>

					<br/>
                    <a data-color="rgb(255, 255, 255)" data-color-format="hex" id="viewMoStatReport"
                       class="btn btn-primary my-colorpicker-control" href="#"
                       data-colorpicker-guid="8">View</a>

                </div>
            </div>
        </div>
		</form>
		<div id="waitQuery" class="row form-row"></div>
	</div>
</div>

<div class="row" id="theData"></div>
