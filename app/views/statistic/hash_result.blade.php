@extends('statistic.manage_content_refresh')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>{{ $judul }} <span class="semi-bold">Trending Topic</span></h3>
                        <a id="addContentBtn" class="btn btn-success" href="{{url('/monitoring/trending')}}">All</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/trending/3')}}">Kepo Only</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/trending/4')}}">iLive Only</a>
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th >No.</th>
                                    <th >Topic</th>
                                    <th ></th>
                                    <th >&Sigma;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $idx => $data)
                                    <tr>
                                        <td>{{ ($idx+1) }}</td>
                                        <td>{{ $data['hash_tag'] }}</td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" style="width:{{ ($data['jumlah']/$jumlah)*100 }}%;">
                                                    <span class="sr-only">60% Complete</span>
                                                </div>
                                            </div>
                                            {{ number_format(($data['jumlah']/$jumlah)*100,2) }}%
                                        </td>
                                        <td>{{ $data['jumlah'] }}</td>
                                        
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

