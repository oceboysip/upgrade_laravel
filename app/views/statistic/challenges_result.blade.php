@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <h3>KEPO-iLIVE <span class="semi-bold">Challenges</span></h3>
                </div>
                <div class="row form-row"> 
                    <form id="myform" method="POST" role="form" >
                        <div class="form-group">
                                <input placeholder="keyword.." type="text" class="" name="keyword" id="keyword">
                                <input placeholder="date start.." style="width:120px" type="text" class="theDate" name="start_date">
                                <input placeholder="date end.." style="width:120px" type="text" class=" theDate" name="end_date">
                                <button type="submit" id="btnSearch" class="btn btn-warning">Search</button>
                        </div>
                    </form>
                </div>
            </div>
            
            <h4><span>Searching for: <b>{{ $keyword }}</b> [{{ $start }} - {{ $end }}]</span></h4>
            
            @if (sizeof($results) < 1)
                Not Found
            @else

                <div class="grid-body ">
                    <table class="table table-hover table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th >No.</th>
                            <th >User ID</th>
                            <th >MSISDN</th>
                            <th >Name</th>
                            <th >Tag</th>
                            <th >Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($results as $idx => $data)
                            <tr>
                                <td>{{ ($idx+1) }}</td>
                                <td>{{ $data['user_id'] }}</td>
                                <td>{{ $data['msisdn'] }}</td>
                                <td>{{ $data['name'] }}</td>
                                <td>{{ $data['hash_tag'] }}</td>
                                <td>{{ date('Y-m-d H:i:s',$data['created_at']->sec) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>

    </div>
@stop

