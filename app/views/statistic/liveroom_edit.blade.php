@extends('content.manage_content_simple-layout')

@section('main_content')
<div class="span12">
    <div class="col-md-8 col-sm-8 col-xs-8">
    @if (\Session::has('err'))
        <div class="alert alert-error">
            <p>{{ \Session::get('err') }}</p>
        </div>
    @endif

        <form id="myform" method="POST" action="{{url('/monitoring/liveroom/edit/'.$results["_id"])}}">
            <h4>Edit Artist</h4><br/>
            <div class="row form-row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="form-label">Group Name</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" placeholder="" type="text" class="" name="group_name" id="group_name"  value="{{ $results['group_name'] }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label">Group Description</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" placeholder="" type="text" class="" name="group_description" id="group_description"  value="{{ $results['group_description'] }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label">live_pic</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" placeholder="" type="text" class="" name="live_pic" id="live_pic"  value="{{ $results['live_pic'] }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label">stream_url</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" placeholder="" type="text" class="" name="stream_url" id="stream_url"  value="{{ $results['stream_url'] }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label">recorded_url</label><br/>
                        <div class="controls">
                            <div class="controls">
                                <input style="width:100%;" placeholder="" type="text" class="" name="recorded_url" id="recorded_url"  value="{{ $results['recorded_url'] }}">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
            <br/>
            <br/>
                <a href="{{ url('/monitoring/liveroom') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" id="btnSave" class="btn btn-warning">Save</button>

                &nbsp;&nbsp;&nbsp;
                @if ($results['is_live'])
                    <a href="{{ url('/monitoring/liveroom/status/0/'.$results["_id"]) }}" class="btn btn-info">Turn off this artist</a>
                @else
                    <a href="{{ url('/monitoring/liveroom/status/1/'.$results["_id"]) }}" class="btn btn-primary">Turn on this artist</a>
                @endif
            </div>
        </form>
    </div>
</div>
@stop
