@extends('content.manage_content-layout')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <h3>KEPO-iLIVE <span class="semi-bold">Challenges</span></h3>
                </div>
                <div class="row form-row"> 
                    <form id="myform" method="POST" role="form" >
                        <div class="form-group">
                                <input placeholder="keyword.." type="text" class="" name="keyword" id="keyword">
                                <input placeholder="date start.." style="width:120px" type="text" class="theDate" name="start_date">
                                <input placeholder="date end.." style="width:120px" type="text" class=" theDate" name="end_date">
                                <button type="submit" id="btnSearch" class="btn btn-warning">Search</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
@stop

