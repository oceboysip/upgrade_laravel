<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@yield("header.title","KMI Webtools")</title>

    <!-- BEGIN CORE CSS FRAMEWORK -->

    <link href="{{ asset('assets/plugins/jquery-polymaps/style.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/shape-hover/css/demo.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/shape-hover/css/component.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/owl-carousel/owl.theme.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <!-- Not using it now
    <link rel="stylesheet" href="assets/plugins/jquery-ricksaw-chart/css/rickshaw.css" type="text/css" media="screen">
    -->
    <link href="{{ asset('assets/plugins/jquery-isotope/isotope.css') }}" rel="stylesheet" type="text/css"/>

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/magic_space.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/tiles_responsive.css') }}" rel="stylesheet" type="text/css"/>

    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
</head>

<!-- BEGIN BODY -->
<body class="">

<!-- BEGIN HEADER -->
@include("main_top_navigation-layout")
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">

    @include("sidebar-layout")

    <!-- BEGIN PAGE CONTAINER-->


    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here</div>
        </div>

        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title">
            </div>

            @yield("main_content")

            <div id="theContent"></div>

        </div>
    </div>

    <!-- END MAIN PAGE -->

</div>

<!-- END CONTAINER -->

@include("main_js-layout")

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}"
            type="text/javascript"></script>

    <!--
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/d3.v2.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js') }}"></script>
    -->

    <script src="{{ asset('assets/plugins/jquery-sparkline/jquery-sparkline.js') }}"></script>
    <script src="{{ asset('assets/plugins/skycons/skycons.js') }}"></script>
    <script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-polymaps/polymaps.min.js') }}" type="text/javascript"></script>


    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/chat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/demo.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/js/dashboard_v2.js') }}" type="text/javascript"></script> -->

    <script type="text/javascript">

        $('.theDate').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

    </script>
@yield("js-additional")

</body>

</html>
