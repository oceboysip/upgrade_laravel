@extends('statistic.manage_content_refresh')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Liveroom - <span class="semi-bold">{{ $group_name }}</span></h3>
                        <form id="myform" method="POST" role="form" >
                            <a id="addContentBtn" class="btn btn-success" href="{{url('/monitoring/liveroom')}}">Back</a>&nbsp;&nbsp;&nbsp;

                            <input placeholder="date start.." style="width:120px" type="text" class="theDate" name="start_date">
                            <input placeholder="date end.." style="width:120px" type="text" class=" theDate" name="end_date">
                            <button type="submit" id="btnSearch" class="btn btn-warning">Change Periode</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" id="theChart1" style="width:100%;height:280px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" id="theChart2" style="width:100%;height:280px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" id="theChart3" style="width:100%;height:280px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" id="theChart4" style="width:100%;height:280px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js-additional')
<script src="{{ asset('assets/js/highcharts.js') }}"></script>
	<script>
        $(function () {
            var data = [
                {"name":"Send Love","data": {{ $heart_value }}  },
            ];
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart1',
                    type: 'area'
                },
                credits: {
                    text: "{{ $group_name }} - Kepo Love"
                },
                title: {
                    text: 'KEPO Love Activity'
                },
                subtitle: {
                    text: '{{ $label_desc }}'
                },
                xAxis: {
                    categories: {{ $heart_label }} },
                yAxis: {
                    title: {
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

		$(function () {
            var data = [
                {"name":"Chat","data": {{ $bubble_value }}  },
            ];
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart2',
                    type: 'area'
                },
                credits: {
                    text: "{{ $group_name }} - Kepo Chat"
                },
                title: {
                    text: 'KEPO Chat Activity'
                },
                subtitle: {
                    text: '{{ $label_desc }}'
                },
                xAxis: {
                    categories: {{ $bubble_label }} },
                yAxis: {
                    title: {
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });


        $(function () {
            var data = [
                {"name":"Send Love","data": {{ $iheart_value }}  },
            ];
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart3',
                    type: 'area'
                },
                credits: {
                    text: "{{ $group_name }} - iLive Love"
                },
                title: {
                    text: 'iLive Love Activity'
                },
                subtitle: {
                    text: '{{ $label_desc }}'
                },
                xAxis: {
                    categories: {{ $iheart_label }} },
                yAxis: {
                    title: {
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });

		$(function () {
            var data = [
                {"name":"Chat","data": {{ $ibubble_value }}  },
            ];
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'theChart4',
                    type: 'area'
                },
                credits: {
                    text: "{{ $group_name }} - iLive Chat"
                },
                title: {
                    text: 'iLive Chat Activity'
                },
                subtitle: {
                    text: '{{ $label_desc }}'
                },
                xAxis: {
                    categories: {{ $ibubble_label }} },
                yAxis: {
                    title: {
                        text: ''
                    },
                    plotLines: [
                        {
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }
                    ]
                },
                series: data
            });
        });
</script>
@stop