@extends('statistic.manage_content_refresh')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>KEPO-iLIVE <span class="semi-bold">Liveroom</span></h3>
                        @if (\Session::has('success'))
                          <p>
                          <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                          </div>
                        @endif
                        @if (\Session::has('err'))
                          <p>
                          <div class="alert alert-danger">
                            <p>{{ \Session::get('err') }}</p>
                          </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 45"></th>
                                    <th style="width: 30%">Group Name</th>
                                    <th style="width: 40%">Group INFO</th>
                                    <th style="width: 10%">Status</th>
                                    <th style="text-align:right; width: 5%">Member</th>
                                    <th style="text-align:center; width: 5%">Action</th>
                                    <th style="text-align:center; width: 5%">Live</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    <tr>
                                        <td><img src="{{ $data['group_avatar'] }}" alt="" class="img-circle" height="42" width="42"></td>
                                        <td><a href="{{url('monitoring/liveroom/edit/'.$data['_id'])}}" >{{ $data['group_name'] }}</a></td>
                                        <td>
                                            {{ $data['group_description'] }}<br>
                                            @if ($data['live_url'] <> "")
                                                <span class="glyphicon glyphicon-bullhorn"></span> 
                                                <a target="_blank" href="{{ $data['live_url'] }}">{{ $data['live_url'] }}</a>
                                            @endif
                                        </td>
                                        <td>
                                        @if ($data['is_live'])
                                            <span class="label label-success"> ENABLED </span>
                                        @else
                                            <span class="label label-danger"> DISABLED </span>
                                        @endif
                                        </td>
                                        
                                        <td align="right">{{ number_format($data['total_member'],0) }}</td>
                                        <td>
                                            <a href="{{url('monitoring/liveroom/'.$data['_id'])}}" class="btn btn-small btn-success">
                                                View Graph
                                            </a>
                                        </td>
                                        <td>
                                            @if ($data['is_broadcaster'] >= 1)
                                                <a href="#" class="label label-success" >
                                                    <span class="glyphicon glyphicon-bullhorn"> Online </span> 
                                                </a>

                                                <a href="{{ url('monitoring/liveroom/stop/'.$data['_id']) }}" class="btn btn-small btn-danger">
                                                    Force Stop
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

