@extends('monitoring.manage_content_refresh')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Mapping Server <span class="semi-bold">Servers</span></h3>
                        <!--
                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/monitoring/map')}}">Mapping Server</a>
                        -->
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Server</th>
                                    <th>CPU</th>
                                    <th>Disk IO</th>
                                    <th>Memory</th>
                                    <th>Fullest Disk</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    @if (($data->avg_cpu >= 80)||($data->io_disk >= 80)||($data->disk_used_pct >= 80))
                                        <tr class="danger">
                                    @elseif (($data->avg_cpu >= 70)||($data->io_disk >= 70)||($data->disk_used_pct >= 87))
                                        <tr class="warning">
                                    @else
                                        <tr class="success">
                                    @endif
                                        <td>{{$data->serverid}}</td>
                                        <td align="right">{{$data->avg_cpu}} %</td>
                                        <td align="right">{{$data->io_disk}} %</td>
                                        <td align="right">{{$data->disk_used_pct}} %<br><small>
                                            @if ($data->used_ram<1024)
                                                {{$data->used_ram}} MB
                                            @else
                                                {{intval($data->used_ram/1024)}} GB
                                            @endif / 
                                            @if ($data->total_ram<1024)
                                                {{$data->total_ram}} MB
                                            @else
                                                {{intval($data->total_ram/1024)}} GB
                                            @endif</small></td>
                                        <td align="right">{{$data->disk_used_pct}} %<br>
                                            <small>{{$data->disk_free}} GB</small>
                                        </td>
                                        <!--
                                        <td>
                                            <a href="{{url('monitoring/server/'.$data->serverid)}}">
                                                <span class="glyphicon glyphicon-cog"></span>
                                            </a>
                                        </td>
                                        -->
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

