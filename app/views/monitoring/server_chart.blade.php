@extends('monitoring.manage_content_refresh')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>Server - <span class="semi-bold">{{ $server_name }}</span> {{ $captions }}</h3>
                        <a id="addContentBtn" class="btn btn-success" href="{{url('/monitoring/server')}}">Back</a>&nbsp;&nbsp;&nbsp;
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/server/'.$serverid.'/1')}}">24 Hour</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/server/'.$serverid.'/2')}}">2 Days</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/server/'.$serverid.'/7')}}">7 Days</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/server/'.$serverid.'/14')}}">14 Days</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/server/'.$serverid.'/20')}}">20 Days</a>
                        <a id="addContentBtn" class="btn btn-primary" href="{{url('/monitoring/server/'.$serverid.'/30')}}">30 Days</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div id="chart_cpu" style="width:90%;height:150px;margin: 0 auto;"></div>
        </div>
        
        <div class="row">
            <div id="chart_ram" style="width:90%;height:150px;margin: 0 auto;"></div>
        </div>
        
        <div class="row">
            <div id="chart_disk" style="width:90%;height:150px;margin: 0 auto;"></div>
        </div>

        <div class="row">
            <div id="chart_io" style="width:90%;height:150px;margin: 0 auto;"></div>
        </div>
    </div>
@stop

@section('js-additional')
<script type="text/javascript">
function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),
		dd = ('0' + d.getDate()).slice(-2),
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),
		ampm = 'AM',
		time;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}

	time = yyyy + '-' + mm + '-' + dd + ', ' + hh + ':' + min;
		
	return time;
}

var data_cpu = {{ $flot_cpu }};
var options_cpu = {
    grid  : {
        borderWidth: 1,
        hoverable: true,
    },
    series: {
        lines: {
            show: true,
            lineWidth: 0.8,
            fill: true
        },
        points: {
			show: false
		},
        shadowSize: 0,
        color     : '#00c0ef'
    },
    xaxis: {
        mode: "time",
        timeformat: "%H:%M",
        twelveHourClock: true,
        show: true,
        min:{{ $cpu_time_min }},
        max:{{ $cpu_time_max }},
        tickFormatter: function (v, axis) {
            var date = new Date(v*1000);
            var hours = date.getHours();
            var minutes = "0" + date.getMinutes();
            var seconds = "0" + date.getSeconds();
            var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
            return formattedTime;
        }
    },
    yaxis: {
        min: 0,
        max: 100,        
        tickSize: 10,
        tickFormatter: function (v, axis) {
            if (v % 20 == 0) {
                return v + " %";
            } else {
                return "";
            }
        },
        show: true,
        axisLabel: "CPU usage",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 6
        
    }
};


var data_ram = {{ $flot_ram }};
var data_ram_used = {{ $flot_ram_used }};
var options_ram = {
    grid  : {
        borderWidth: 1,
        hoverable: true,
    },
    series: {
        lines: {
            show: true,
            lineWidth: 0.8,
            fill: true,
        },
        shadowSize: 0,
    },
    colors: ["#808fed", "#172684"],
    xaxis: {
        mode: "time",
        timeformat: "%H:%M",
        twelveHourClock: true,
        show: true,
        min:{{ $cpu_time_min }},
        max:{{ $cpu_time_max }},
        tickFormatter: function (v, axis) {
            var date = new Date(v*1000);
            var hours = date.getHours();
            var minutes = "0" + date.getMinutes();
            var seconds = "0" + date.getSeconds();
            var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
            return formattedTime;
        }
    },
    yaxis: {
        min: {{ $ram_usage_min }},
        max: {{ $ram_usage_max }},        
        tickSize: {{ $ram_tick }},
        tickFormatter: function (v, axis) {
            if (v % {{ $ram_pembagi }} == 0) {
                @if ($ram_satuan == " GB")
                return v/1024 + "{{ $ram_satuan }}";
                @else
                return v + "{{ $ram_satuan }}";
                @endif
            } else {
                return "";
            }
        },
        show: true,
        axisLabel: "RAM usage",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 6
        
    }
};


var data_disk = {{ $flot_disk }};
var data_disk_used = {{ $flot_disk_used }};
var options_disk = {
    grid  : {
        borderWidth: 1,
        hoverable: true,
    },
    series: {
        lines: {
            show: true,
            lineWidth: 0.8,
            fill: true,
        },
        shadowSize: 0,
    },
    colors: ["#c1703a", "#75401d"],
    xaxis: {
        mode: "time",
        timeformat: "%H:%M",
        twelveHourClock: true,
        show: true,
        min:{{ $cpu_time_min }},
        max:{{ $cpu_time_max }},
        tickFormatter: function (v, axis) {
            var date = new Date(v*1000);
            var hours = date.getHours();
            var minutes = "0" + date.getMinutes();
            var seconds = "0" + date.getSeconds();
            var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
            return formattedTime;
        }
    },
    yaxis: {
        min: {{ $disk_usage_min }},
        max: {{ $disk_usage_max }},        
        tickSize: {{ $disk_tick }},
        tickFormatter: function (v, axis) {
            if (v % {{ $disk_pembagi }} == 0) {
                return v + " GB";
            } else {
                return "";
            }
        },
        show: true,
        axisLabel: "Disk usage",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 6
        
    }
};
 

var data_io = {{ $flot_io }};
var options_io = {
    grid  : {
        borderWidth: 1,
        hoverable: true,
    },
    series: {
        lines: {
            show: true,
            lineWidth: 0.8,
            fill: true
        },
        shadowSize: 0,
        color     : '#47751d'
    },
    xaxis: {
        mode: "time",
        timeformat: "%H:%M",
        twelveHourClock: true,
        show: true,
        min:{{ $cpu_time_min }},
        max:{{ $cpu_time_max }},
        tickFormatter: function (v, axis) {
            var date = new Date(v*1000);
            var hours = date.getHours();
            var minutes = "0" + date.getMinutes();
            var seconds = "0" + date.getSeconds();
            var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
            return formattedTime;
        }
    },
    yaxis: {
        min: 0,
        max: 100,        
        tickSize: 10,
        tickFormatter: function (v, axis) {
            if (v % 20 == 0) {
                return v + " %";
            } else {
                return "";
            }
        },
        show: true,
        axisLabel: "Disk IO",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 6
        
    }
};

$(document).ready(function () {
    var dataset_cpu = [
        { label: "{{ $labels_cpu }}", data: data_cpu }
    ];
    $.plot($("#chart_cpu"), dataset_cpu, options_cpu);

    var dataset_ram = [
        { label: "{{ $labels_ram }}", data: data_ram },
        { label: "{{ $labels_ram_used }}", data: data_ram_used }
    ];
    $.plot($("#chart_ram"), dataset_ram, options_ram);
    
    var dataset_disk = [
        { label: "{{ $labels_disk }}", data: data_disk },
        { label: "{{ $labels_disk_used }}", data: data_disk_used }
    ];
    $.plot($("#chart_disk"), dataset_disk, options_disk);

    var dataset_io = [
        { label: "{{ $labels_io }}", data: data_io }
    ];
    $.plot($("#chart_io"), dataset_io, options_io);


    $("<div id='tooltip'></div>")
        .css({
			position: "absolute",
			display: "none",
			border: "1px solid #7f96ff",
			padding: "2px",
			"background-color": "#f4f6ff",
			opacity: 0.90
	    })
        .appendTo("body");

	$("#chart_cpu").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0].toFixed(2),
				y = item.datapoint[1].toFixed(2);
			$("#tooltip")
				.html(" " + item.series.label + " " +convertTimestamp(x) + "; " + y)
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(100);
		} else {
			$("#tooltip").hide();
		}
	});

    $("#chart_ram").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0].toFixed(2),
				y = item.datapoint[1].toFixed(2);
			$("#tooltip")
				.html(" " + item.series.label + " " +convertTimestamp(x) + "; " + y)
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(100);
		} else {
			$("#tooltip").hide();
		}
	});

    $("#chart_disk").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0].toFixed(2),
				y = item.datapoint[1].toFixed(2);
			$("#tooltip")
				.html(" " + item.series.label + " " +convertTimestamp(x) + "; " + y)
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(100);
		} else {
			$("#tooltip").hide();
		}
	});

    $("#chart_io").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0].toFixed(2),
				y = item.datapoint[1].toFixed(2);
			$("#tooltip")
				.html(" " + item.series.label + " " +convertTimestamp(x) + "; " + y)
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(100);
		} else {
			$("#tooltip").hide();
		}
	});
});
</script>
@stop