@extends('monitoring.manage_content_refresh')

@section('main_content')
    <div class="row">
        <div class="grid simple">
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3>KMI <span class="semi-bold">Servers</span></h3>
                        <!--
                        <a data-color="rgb(255, 255, 255)" id="addContentBtn"
                           class="btn btn-primary my-colorpicker-control" href="{{url('/monitoring/mapping')}}">Mapping Server</a>
                        -->
                    </div>
                </div>
            </div>
        </div>
        @if (isset($results))
            <div class="row">
                <div class="grid simple">
                    <div class="col-md-12">
                        <div class="grid-body ">
                            <table class="table table-hover table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 40%">Server</th>
                                    <th style="text-align:right; width: 15%">CPU</th>
                                    <th style="text-align:right; width: 15%">Disk IO</th>
                                    <th style="text-align:right; width: 15%">Memory</th>
                                    <th style="text-align:right; width: 15%">Fullest Disk</th>
                                    <th style="text-align:right; width: 15%">/ </th>
                                    <th style="text-align:right; width: 15%">/data</th>
                                    <th style="text-align:right; width: 15%">/ backup</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (sizeof($results) < 1)
                                    <tr class="">
                                        <td colspan=7> No data </td>
                                    </tr>
                                @else
                                    @foreach ($results as $data)
                                    @if (($data->cpu >= 80)||($data->io_disk >= 80)||($data->disk_used_pct >= 80)||($data->ram_pct >= 80))
                                        <tr class="merahseru">
                                    @elseif (($data->cpu >= 70)||($data->io_disk >= 70)||($data->disk_used_pct >= 70)||($data->ram_pct >= 70))
                                        <tr class="amber">
                                    @else
                                        <tr class="success">
                                    @endif
                                        <td>{{$data->server_name}}</td>
                                        <td align="right">{{ number_format($data->cpu,1) }} %</td>
                                        <td align="right">{{ number_format($data->io_disk,1) }} %</td>
                                        <td align="right">{{ number_format($data->ram_pct,1) }} %<br><small>
                                            @if ($data->used_ram<1024)
                                                {{$data->used_ram}} MB
                                            @else
                                                {{intval($data->used_ram/1024)}} GB
                                            @endif / 
                                            @if ($data->total_ram<1024)
                                                {{$data->total_ram}} MB
                                            @else
                                                {{intval($data->total_ram/1024)}} GB
                                            @endif</small></td>
                                        <td align="right">{{ number_format($data->disk_used_pct,1) }} %<br>
                                            <small>{{$data->disk_free}} GB</small>
                                        </td>

                                        <td align="right">{{ number_format($data->disk_slash_pct,1) }} %<br>
                                            <small>{{$data->disk_slash_free}} GB</small>
                                        </td>

                                        <td align="right">{{ number_format($data->disk_data_pct,1) }} %<br>
                                            <small>{{$data->disk_data_free}} GB</small>
                                        </td>

                                        <td align="right">{{ number_format($data->disk_backup_pct,1) }} %<br>
                                            <small>{{$data->disk_backup_free}} GB</small>
                                        </td>
                                        <td>
                                            <a href="{{url('monitoring/server/'.$data->serverid.'/1')}}">
                                                <span class="glyphicon glyphicon-picture"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

