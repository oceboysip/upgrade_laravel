<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="refresh" content="30">
    <title>@yield("header.title","KMI Webtools")</title>

    <!-- BEGIN CORE CSS FRAMEWORK -->

    <link href="{{ asset('assets/plugins/jquery-slider/css/jquery.sidr.light.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/jquery-datatable/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/boostrap-checkbox/css/bootstrap-checkbox.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->

    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('assets/plugins/bootstrap-select2/select2.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/custom-icon-set.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/magic_space.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/tiles_responsive.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->

    <link href="{{ asset('assets/css/c3.min.css') }}" rel="stylesheet" type="text/css"/>

    <style>
    .merahseru {
        background-color: #ec7063;
    }
    .amber {
        background-color: #fad7a0;
    }
    
    </style>
</head>

<!-- BEGIN BODY -->
<body class="">

<!-- BEGIN HEADER -->
@include("main_top_navigation-layout")
<!-- END HEADER -->


<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">

    @include("sidebar-layout")

    <!-- BEGIN PAGE CONTAINER-->


    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here</div>
        </div>

        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title">
            </div>

            @yield("main_content")

            <div id="theContent"></div>

        </div>
    </div>

    <!-- END MAIN PAGE -->

</div>

<!-- END CONTAINER -->

@include("main_js-layout")

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN CORE TEMPLATE JS -->

<script src="{{ asset('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/js/core.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-datatable/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.time.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.axislabels.js') }}" type="text/javascript" ></script>

@yield("js-additional")

</body>

</html>
