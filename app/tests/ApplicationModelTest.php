<?php

/**
 * Unit test for Application Model 
 *
 *
 */
class ApplicationModelTest extends TestCase {

	/**
	 * Test for all gets
	 *
	 * @return void
	 */
	public function testGets() {
		$applicationType = Application::getApplicationType(0);
		$this->assertNotNull($applicationType);
		$this->assertNotNull($applicationType[0]->name);
		//var_dump($applicationType);

	}


    /**
     * Test insert
     *
     */
    public function testInsertEdit(){
        $applicationId = Application::saveApplication("7777-Telco-Dummy-Pull", 190000, "7777PULL0",1, "7777",1,"DUMMY", 1);
        Application::editApplication($applicationId, "7777-Telco-Dummy-Pull","7777PULL1",1, "7777",1,"DUMMY", 1);


        Application::saveApplicationChargingParam($applicationId, 35, "7777PULL1", "7777PULL1",
                                                   "7777PULL1", "7777PULL1", "7777PULL1", "7777PULL1");
        Application::editApplicationChargingParam($applicationId, 35, "7777PULL1", "7777PULL1",
                                                   "7777PULL1", "7777PULL1", "7777PULL1", "7777PULL1");


        Application::saveApplicationDelegate($applicationId, "DUMMY",
                    "http://127.0.0.1/phynxma/xl_9191_default.php","http://127.0.0.1/phynxma/xl_9191_default.php",
                    "http://127.0.0.1/phynxma/xl_9191_default.php", "http://127.0.0.1/phynxma/xl_9191_default.php",
                    "http://127.0.0.1/phynxma/xl_9191_default.php", "http://127.0.0.1/phynxma/xl_9191_default.php"
        );

        Application::editApplicationDelegate($applicationId, "DUMMY",
            "http://127.0.0.1/phynxma/xl_9191_default.php", "http://127.0.0.1/phynxma/xl_9191_default.php",
            "http://127.0.0.1/phynxma/xl_9191_default.php", "http://127.0.0.1/phynxma/xl_9191_default.php",
            "http://127.0.0.1/phynxma/xl_9191_default.php", "http://127.0.0.1/phynxma/xl_9191_default.php"
        );

        Application::saveApplicationMessage($applicationId, 35,
            "Sukses REG", "Unreg Error","Sudah terdaftar","Unreg sukses", "Unreg Error", "Pull Standard", "Unreg Error"
        );

        Application::editApplicationMessage($applicationId, 35,
            "Sukses REG", "Unreg Error","Sudah terdaftar dalam layanan",
            "Unreg sukses", "Unreg Error", "Pull Standard", "Unreg Error"
        );


    }


}
