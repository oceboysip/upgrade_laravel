<?php

/**
 * Unit test for Charging Model 
 *
 *
 */
class ChargingModelTest extends TestCase {

	/**
	 * Get charging code 
	 *
	 * @return void
	 */
	public function testGetChargingCode() {
		$isExist = Charging::isChargingCodeExists("9191PULL0");
		$this->assertTrue($isExist);

		$isExist = Charging::isChargingCodeExists("909PULL0");
		$this->assertFalse($isExist);
	}



	/**
	 * Save Charging Code 
	 *
	 */
	public function testSaveChargingCode() {
		//$telcoId, $telcoSid,  $sdc, $cpPrice, $euPrice, $costBearer, $descr
		$theId = Charging::saveCharging(35, '12200000090082', '7777', 1100,1100,0,'Pull Rp. 1000');
		//echo "The inserted Id: " . $theId . PHP_EOL;
		$this->assertTrue($theId > 0);

		Charging::saveChargingRouting($theId, 35, '7777PULL1100');
	}



	/**
	 * Save Charging Code 
	 *
	 */
	public function testGetCharging() {
		$chargingInfo = Charging::getChargingById(21750);
		//var_dump($chargingInfo);
		$this->assertTrue(sizeof($chargingInfo) > 0);
	}


}
