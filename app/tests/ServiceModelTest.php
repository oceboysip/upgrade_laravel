<?php

/**
 * Unit test for Service Model 
 *
 *
 */
class ServiceModelTest extends TestCase {

	/**
	 * Test for all gets
	 *
	 * @return void
	 */
	public function testGet() {
        $service = GatewayService::isServiceExists("XL-9191 SMS Hoki", "9191", 3);
        $this->assertNotNull($service);
        $this->assertTrue(sizeof($service) > 0);

        $theService = GatewayService::getById($service[0]->id);
        $this->assertTrue(sizeof($theService) > 0);
        //var_dump($theService);

        $serviceKeyword = GatewayService::isServiceKeywordExists(1, "9191", "pull1",3);
        $this->assertNotNull($serviceKeyword);
        $this->assertTrue(sizeof($serviceKeyword) > 0);
        var_dump($serviceKeyword);

	}

    /**
     * Test insert
     *
     */
    public function testInsertEdit() {
        $serviceId = GatewayService::saveService("7777-Telco-Dummy-Pull","http://127.0.0.1:9900/mts/gateway/handler","7777", 35, 0);
        GatewayService::saveServiceKeyword($serviceId, "7777","dummy",35);

        GatewayService::editService($serviceId,"7777-Telco-Dummy-Pull", "http://127.0.0.1:9901/mts/gateway/handler",1);


        $serviceId = GatewayService::saveService("XL-9191 SMS Dudu","http://127.0.0.1:9900/mts/gateway/handler","9191", 3, 0);
        GatewayService::saveServiceKeyword($serviceId, "9191","dudu",3);

    }



}
