<?php

/**
 * General Unit Test
 *
 */
class GeneralTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 */
	public function testGeneral(){
		$this->assertNotNull(Config::get('reporting.database_config'));
		$this->assertEquals("cdb",Config::get('reporting.database_config'));
	}

}
