<?php

/**
 * Class SubscriberController
 *
 */
class SubscriberController extends BaseController {

    protected $montessori_db, $umb_app_db, $umb_microsite;
    public function __construct()
    {
        $this->montessori_db = Config::get('reporting.database_application');
        $this->umb_app_db = Config::get('reporting.database_umb');
        $this->umb_microsite = Config::get('reporting.database_umb_microsite');
    }
    /**
     * Show subscriber 'home' screen of user controller
     * @return mixed
     */
    public function home(){
        $data = $this->constructMenu();
        return View::make('subscriber.menu', $data);
    }

	/**
	 * Show registration report
	 * @return mixed
	 */
	public function showRegReport() {
		$data = $this->constructMenu();
		$data['sdc'] = MainServiceConfig::getAllSdc();
		$data['telco'] = MainServiceConfig::getAllTelco();

        $db = Config::get('reporting.database_application');
        $subscribers = DB::table("{$db}.subscriber as s")
            ->join("{$db}.application as a", 's.application_id', '=', 'a.id')
            ->groupBy('a.name')
            ->orderBy('total', 'desc')
            ->select(DB::raw('a.name, count(*) as total'))
            ->get();

        $data['subscriber_data'] = $subscribers;

        /*
        if(Auth::user()->username == "inong") {
            unset($data['sdc'][1]);
            unset($data['sdc'][2]);
            unset($data['sdc'][3]);
            unset($data['telco'][1]);
            unset($data['telco'][2]);

            for ($i=1 ; $i<=13 ; $i++) {
                unset($data['subscriber_data'][$i]);
            }
        }
        */
        
        return View::make('subscriber.reg_report', $data);
	}


	/**
     * Find subscriber
     */
    public function apiFind() {
        $msisdn = Input::get("msisdn");
        $db = Config::get('reporting.database_application');
        $dbConfig = Config::get('reporting.database_config');
//        $services = DB::table("{$db}.subscriber as s")
//                ->join("{$db}.application as a", 's.application_id', '=', 'a.id')
//                ->join("{$dbConfig}.telco as t", 's.telco_id', '=', 't.id')
//                ->where('s.msisdn', '=', $msisdn)
//                ->orderBy('s.dtregister','desc')
//                ->get(array('s.id as id', 's.msisdn', 's.dtregister', 's.reg_sms','t.name as telco_name','s.application_id', 'a.service_id',
//                    's.dtlast_push', 's.dtmodified','a.name as application_name','s.channel', 's.area_code_id', 's.is_active'));

        $reg_services = DB::select("select s.id as id, s.msisdn, s.dtregister, s.reg_sms,t.name as telco_name,s.application_id, a.service_id,
                                   s.dtlast_push, s.dtmodified,a.name as application_name,s.channel, s.area_code_id, s.is_active
                            from {$this->montessori_db}.subscriber s
                            LEFT JOIN {$this->montessori_db}.application a ON s.application_id = a.id
                            LEFT JOIN cdb.telco t ON s.telco_id = t.id
                            WHERE s.msisdn = '{$msisdn}'
                            ORDER BY s.dtregister DESC");
        $unreg_services = DB::select("select s.id as id, s.msisdn, s.dtregister, s.dtdeleted, s.reg_sms,t.name as telco_name,s.application_id, a.service_id,
                                           s.dtlast_push, s.dtmodified,a.name as application_name,s.channel, s.area_code_id, s.is_active
                                    from {$this->montessori_db}.deleted_subscriber s
                                    LEFT JOIN {$this->montessori_db}.application a ON s.application_id = a.id
                                    LEFT JOIN cdb.telco t ON s.telco_id = t.id
                                    WHERE s.msisdn = '{$msisdn}'
                                    ORDER BY s.dtregister DESC");

        $data = array();
        $data['msisdn'] = $msisdn;
        $data['reg_services'] = $reg_services;
        $data['unreg_services'] = $unreg_services;
        return View::make('subscriber.api.api_find_subscriber', $data);
    }


    /**
     * Unregister Subscriber!
     */
    public function apiUnregisterSubscriber() {
        $subscriberId = Input::get("subscriberId");
        $db = Config::get('reporting.database_application');

        $rs_subscriber = DB::select("SELECT msisdn,sdc,telco_id FROM `montessori`.subscriber WHERE id={$subscriberId}");
        $msisdn = $rs_subscriber[0]->msisdn;
        $sdc = $rs_subscriber[0]->sdc;
        $telco_id = $rs_subscriber[0]->telco_id;

        if ($sdc == "99858" && $telco_id == 2) {
            $text = 'Terima+kasih+telah+menghubungi+kami.+Nomor+kamu+akan+segera+kami+batalkan+dari+semua+layanan+99858.+Kalau+masih+ada+pertanyaan%2c+silakan+hub+kami+di+081510027073';
            $url = 'http://172.16.69.1:2035/isat/mtbroker.jsp?sdc=99858&msisdn='.$msisdn.'&text='.$text.'&telcoId=2&serviceId=2000&chargingCode=99858PUSH0&keyword=model&channel=sms&areaCodeId=0';
        } elseif ($sdc == "98858" && $telco_id == 2) {
            $text = 'Terima+kasih+telah+menghubungi+kami.+Nomor+kamu+akan+segera+kami+batalkan+dari+semua+layanan+99199.+Kalau+masih+ada+pertanyaan%2c+silakan+hub+kami+di+081510027073';
            //$url = 'http://172.16.69.1:2035/isat/mtbroker.jsp?sdc=99858&msisdn='.$msisdn.'&text='.$text.'&telcoId=2&serviceId=2000&chargingCode=99858PUSH0&keyword=model&channel=sms&areaCodeId=0';
            $url = 'http://bckapital.ddns.net:8044/smsthank_v3.php?msisdn='.$msisdn.'&sdc='.$sdc.'&telcoId='.$telco_id.'&text='.$text;
        } elseif ($sdc == "99199" && $telco_id == 2) {
            $text = 'Terima+kasih+telah+menghubungi+kami.+Nomor+kamu+akan+segera+kami+batalkan+dari+semua+layanan+99199.+Kalau+masih+ada+pertanyaan%2c+silakan+hub+kami+di+081510027073';
            $url = 'http://116.206.196.8:2015/isat/mtbroker.jsp?sdc=99199&msisdn='.$msisdn.'&text='.$text.'&telcoId=2&serviceId=90000&chargingCode=99199PUSH0&keyword=jodoh&channel=sms&areaCodeId=0';
            //$url = 'http://bckapital.ddns.net:8044/smsthank_v3.php?msisdn='.$msisdn.'&sdc='.$sdc.'&telcoId='.$telco_id.'&text='.$text;
        } elseif ($sdc == "99858" && $telco_id == 1) {
            $text = 'Terima+kasih+telah+menghubungi+kami.+Nomor+kamu+akan+segera+kami+batalkan+dari+semua+layanan+99858.+Kalau+masih+ada+pertanyaan%2c+silakan+hub+kami+di+08119899858';
            $url = 'http://bckapital.ddns.net:8044/smsthank_v3.php?msisdn='.$msisdn.'&sdc='.$sdc.'&telcoId='.$telco_id.'&text='.$text;
        } elseif ($sdc == "99858" && $telco_id == 3) {
            $text = 'Terima+kasih+telah+menghubungi+kami.+Nomor+kamu+akan+segera+kami+batalkan+dari+semua+layanan+99858.+Kalau+masih+ada+pertanyaan%2c+silakan+hub+kami+di+0818988585';
            $url = 'http://103.105.98.139:3000/xl/mtbroker.jsp?msisdn='.$msisdn.'&retryCount=0&channel=78&telcoId=3&serviceId=6000&chargingCode=99858PUSH0&keyword=chat&areaCodeId=0&contentId=125&dtregister=20180410112656&text='.$text;
        } elseif ($sdc == "99199" && $telco_id == 3) {
            $text = 'Terima+kasih+telah+menghubungi+kami.+Nomor+kamu+akan+segera+kami+batalkan+dari+semua+layanan+99858.+Kalau+masih+ada+pertanyaan%2c+silakan+hub+kami+di+0818988585';
            $url = 'http://103.105.98.139:3030/xl/mtbroker.jsp?msisdn='.$msisdn.'&retryCount=0&channel=155&telcoId=3&serviceId=4000&chargingCode=99199PUSH0&keyword=live&areaCodeId=0&contentId=0&text='.$text;
        }

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_exec($ch);
        curl_close($ch);

        try {
            DB::statement(DB::raw("CALL {$db}.p_unregisterSubscriber({$subscriberId},'2')"));

			ActivityLog::writeActiviy(
				Config::get('activity.SUBSCRIBER_UNREG'), '/api/do_unregister_subscriber',
				"Unreg subscriberId: {$subscriberId}"
			);

			/**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }



	public function apiGetRegReport($applicationId) {
		$data = array();
		$dateStart = Input::get("dtStart");
		$dateEnd = Input::get("dtEnd");

		$data['dtStart'] = $dateStart;
		$data['dtEnd'] = $dateEnd;

		$data['application'] = Application::getById($applicationId);

		/*
		echo ' --> ' . $applicationId . PHP_EOL;
		echo ' --> DtDtart: ' . $dateStart . PHP_EOL;
		echo ' --> DtEnd: ' . $dateEnd . PHP_EOL;
		*/

		$rawRegResult = Subscriber::getRegOnDate($dateStart, $dateEnd, $applicationId);

		/*
		//$rawUnregResult = Subscriber::getUnregOnDate($dateStart, $dateEnd, $applicationId);
		$perDate = array();

		$date_from = strtotime($dateStart);
		$date_to = strtotime($dateEnd);

		// Loop from the start date to end date and output all dates inbetween
		for ($i = $date_from; $i <= $date_to; $i += 86400) {
			//echo date("Y-m-d", $i) . '<br />';
			$dNow = date("Y-m-d", $i);
			$perDate
		}
		*/

		/*
		foreach ($rawResult as $raw) {
			if (!isset($perDate[$raw->dtstat])) {
				$perDate[$raw->dtstat]['tS'] = 0;
				$perDate[$raw->dtstat]['tSuc'] = 0;
				$perDate[$raw->dtstat]['tG'] = 0;
				$perDate[$raw->dtstat]['tN'] = 0;
			}
			$perDate[$raw->dtstat]['tS'] += $raw->total_sent;
			$perDate[$raw->dtstat]['tSuc'] += $raw->total_success;
			$perDate[$raw->dtstat]['tG'] += $raw->total_gross;
			$perDate[$raw->dtstat]['tN'] += $raw->total_net;
		}
		*/

		$data['the_data'] = $rawRegResult;

		return View::make('subscriber.api.api_list_reg_report', $data);

	}

    public function upload_msisdn()
    {
        $data = $this->constructMenu();
        return View::make('subscriber.upload_msisdn', $data);
    }

    public function upload_msisdn_act()
    {
        try{
            $file = $_FILES["files"];
            $openfile = fopen($file["tmp_name"], "r") or die("Unable to open file!");
            $readfile =  nl2br(fread($openfile,filesize($file["tmp_name"])));

            $array = explode('<br />', $readfile);

            $data = [];
            $i=0;
            foreach($array as $value)
            {
                $explode_string = explode(',', $value);
                $msisdn = trim($explode_string[0]);
                if(is_numeric($msisdn) || isset($explode_string[1]))
                {
                    $dtcomplaint = trim($explode_string[1]);
                    $data[] = [
                        'msisdn' => $msisdn,
                        'dtcomplaint' => $dtcomplaint,
                        'complaint_type' => 1
                    ];
                    $i++;
                }
            }

            $rs_insert = DB::table("trafficdb_isat_99858.msisdn_complaint")->insert(
                $data
            );
            if($rs_insert == 1) {
                $data = ['message' => 'Success, '.$i.' row affected'];
                echo json_encode($data);
            }
        }catch (Exception $e){
            $string_error = 'Terjadi Kesalahan
            '.$e->getMessage();
            $data = ['message' => $string_error];
            echo json_encode($data);
        }

    }

    public function apiGetRegPromo($applicationId)
    {
        $data = array();
        $dateStart = Input::get("dtStart");
        $dateEnd = Input::get("dtEnd");
        $keyword = Input::get("keyword");

        $data['dtStart'] = $dateStart;
        $data['dtEnd'] = $dateEnd;

        $data['application'] = Application::getById($applicationId);

        $rawRegResult = Subscriber::getRegPromoChannel($dateStart, $dateEnd, $keyword, $applicationId);

        $data['the_data'] = $rawRegResult;

        return View::make('subscriber.api.api_list_reg_report', $data);
    }

    public function promo_channel()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        return View::make('subscriber.promo_channel_option', $data);
    }

    public function apiGetKeywordPromo($applicationId)
    {
        $keyword = DB::select("SELECT keyword, CONCAT(channel_promo,' - ',keyword) as name
                               FROM {$this->montessori_db}.application_channel_promo
                               WHERE application_id = {$applicationId}");

        $data['keyword'] = $keyword;
        return View::make('subscriber.api.keyword_promo', $data);
    }

}
