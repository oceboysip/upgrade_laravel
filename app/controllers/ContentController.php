<?php

/**
 * Class ContentController
 *
 */
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;


class ContentController extends BaseController {


    protected $montessori_db, $umb_app_db, $umb_microsite, $monaco_db;
    public function __construct()
    {
        $this->montessori_db = Config::get('reporting.database_application');
        $this->umb_app_db = Config::get('reporting.database_umb');
        $this->umb_microsite = Config::get('reporting.database_umb_microsite');
        $this->monaco_db = Config::get('reporting.database_monaco');
    }
    /**
     * Show 'home' screen of pull content
     * @return mixed
     */
    public function pullContentMenu(){
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

		$telcoId = Input::get('telcoId');
		if ($telcoId) $data['telcoId'] = $telcoId;
		$givenSdc = Input::get('givenSdc');
		if ($givenSdc) $data['givenSdc'] = $givenSdc;
		$givenServiceId = Input::get('givenServiceId');
		if ($givenServiceId) $data['givenServiceId'] = $givenServiceId;
		$givenPage = Input::get('givenPage');
		if ($givenPage) $data['givenPage'] = $givenPage;

		return View::make('content.pull_content', $data);
    }

    public function upload()
    {
        $file = $_FILES["files"];
        $check = getimagesize($file["tmp_name"]);

        $target_dir = Config::get('app.video_dir');
        $url = Config::get('app.video_url');
        //check image apa bukan
        if($check)
        {
            $url = Config::get('app.image_url');
            $target_dir = Config::get('app.image_dir');
        }

        $extension = explode('.', $file['name']);
        $filename = date('Ymdhis').'.'.$extension[1];

        $target_file = $target_dir . $filename;


        if (move_uploaded_file($file["tmp_name"], $target_file))
        {
            $url_link = $url.$filename;
            $short = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 5);
            DB::insert("INSERT INTO {$this->montessori_db}.urls(url_link,url_short,url_ip,url_date)
                VALUES('{$url_link}', '{$short}', '{$_SERVER['REMOTE_ADDR']}', ".time().")");
            $data = ['data' => 'http://kmi.mobi/'.$short];

            echo json_encode($data);
        }
        else
        {
            echo null;
        }
    }

    public function upload_image_tinymce()
    {
        $file = $_FILES["files"];

        $url = Config::get('app.image_url');
        $target_dir = Config::get('app.image_dir');

        $extension = explode('.', $file['name']);
        $filename = date('Ymdhis').'.'.$extension[1];

        $target_file = $target_dir . $filename;

        if (move_uploaded_file($file["tmp_name"], $target_file))
        {
            $url_link = $url.$filename;
            $data = [
                        'data' => [
                                    'url' => $url_link,
                                    'filename' => $filename
                                    ]
                    ];

            echo json_encode($data);
        }
        else
        {
            echo null;
        }
    }

    /**
     * Show 'home' screen of push content  
     * @return mixed
     */
    public function pushContentMenu(){
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $data['shio'] = Shio::getShio();

		$telcoId = Input::get('telcoId');
		if ($telcoId) $data['telcoId'] = $telcoId;
		$givenSdc = Input::get('givenSdc');
		if ($givenSdc) $data['givenSdc'] = $givenSdc;
		$givenServiceId = Input::get('givenServiceId');
		if ($givenServiceId) $data['givenServiceId'] = $givenServiceId;
		$givenPage = Input::get('givenPage');
		if ($givenPage) $data['givenPage'] = $givenPage;

		return View::make('content.push_content', $data);
    }

    public function discount_content()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_diskon_category a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.application_diskon_category c ON
                                a.category_id = c.category_id 
                                WHERE b.application_id=2007 
                                AND b.dtexpired >= NOW()
                                GROUP BY b.id ORDER BY b.id DESC");

        $data['results'] = $results;

        return View::make('content.discount_content', $data);
    }

    public function content_umb()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = ['Discount', 'Loker', 'TebakSkor', 'Badword'];
        $data['results'] = $results;

        return View::make('content.content_umb', $data);
    }

    public function discount_content_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

        $data['categories'] = $results;

        return View::make('content.discount_content_add', $data);
    }

    public function loker()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT *,a.id as id FROM {$this->montessori_db}.content_loker a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.loker_subcategories c ON
                                a.subcategory_id = c.id
                                INNER JOIN {$this->montessori_db}.loker_categories d ON
                                c.loker_category_id = d.id WHERE b.application_id=7002 GROUP BY b.id ORDER BY a.id DESC");

        $data['results'] = $results;

        return View::make('content.loker', $data);
    }

    public function chatbola()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_cb WHERE n_status = 1 ORDER BY content_type ASC");

        $data['results'] = $results;

        return View::make('content.chatbola', $data);
    }

    public function chat()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_chat WHERE n_status = 1 ORDER BY content_type ASC");

        $data['results'] = $results;

        return View::make('content.chat', $data);
    }

    public function model()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_model_cnd WHERE n_status = 1 ORDER BY content_type ASC");

        $data['results'] = $results;

        return View::make('content.model', $data);
    }

    public function loker_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $categories = DB::select("SELECT * FROM {$this->montessori_db}.loker_categories LIMIT 10000");
        $subcategories = DB::select("SELECT * FROM {$this->montessori_db}.loker_subcategories LIMIT 10000");

        $results = DB::connection('umb-microsite')->select("SELECT * FROM {$this->montessori_db}.content_loker a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.loker_subcategories c ON
                                a.subcategory_id = c.id
                                INNER JOIN {$this->montessori_db}.loker_categories d ON
                                c.loker_category_id = d.id
                                INNER JOIN loker_detail e ON
                                a.loker_detail_id = e.id
                                WHERE a.id={$id} LIMIT 1");
        $data['results'] = $results[0];
        $data['categories'] = $categories;
        $data['subcategories'] = $subcategories;
        $data['id'] = $id;
        return View::make('content.loker_edit', $data);
    }

    public function chatbola_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_cb WHERE id = {$id}
                                LIMIT 1");
        $data['results'] = $results[0];
        $data['id'] = $id;
        return View::make('content.chatbola_edit', $data);
    }

    public function chat_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_chat WHERE id = {$id}
                                LIMIT 1");
        $data['results'] = $results[0];
        $data['id'] = $id;
        return View::make('content.chat_edit', $data);
    }

    public function model_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_model_cnd WHERE id = {$id}
                                LIMIT 1");
        $data['results'] = $results[0];
        $data['id'] = $id;
        return View::make('content.model_edit', $data);
    }

    public function loker_edit_act($id)
    {
        $text = Input::get('text');
        $category_id = Input::get('category_id');
        $subcategory_id = Input::get('subcategory_id');
        $job_title = addslashes(Input::get('job_title'));
        $description = addslashes(Input::get('description'));
        $dtscheduled = Input::get('dtscheduled');
        $dtexpired = Input::get('dtexpired');
        $sequence = Input::get('sequence');
        $loker_detail_id = Input::get('loker_detail_id');

        $rs_content_loker = DB::select("SELECT * FROM {$this->montessori_db}.content_loker WHERE id={$id}
                            LIMIT 1");
        $content_loker = $rs_content_loker[0];

        DB::update("UPDATE {$this->montessori_db}.content SET text='{$text}',
                            dtmodified=now(), dtscheduled='{$dtscheduled}', sequence='{$sequence}',
                            dtexpired='{$dtexpired}' WHERE id={$content_loker->content_id}");

        DB::connection('umb-microsite')->update("UPDATE loker_detail SET job_title='{$job_title}', content='{$description}',
                  updated_at=now() WHERE id={$content_loker->loker_detail_id}");

        DB::update("UPDATE {$this->montessori_db}.content_loker SET subcategory_id='{$subcategory_id}',
                    updated_at=now() WHERE id={$id}");

        //clone
        $rs_content = DB::select("SELECT * FROM {$this->montessori_db}.content WHERE id={$content_loker->content_id}
                            LIMIT 1");

        $content = $rs_content[0];

        DB::update("UPDATE {$this->montessori_db}.content SET text='{$text}',
                            dtmodified=now(), dtscheduled='{$dtscheduled}', sequence='{$sequence}',
                            dtexpired='{$dtexpired}' WHERE parent_id={$content->id}");

        $rs_clone= DB::select("SELECT * FROM {$this->montessori_db}.content WHERE parent_id={$content->parent_id}
                            LIMIT 1");
        $content_clone = $rs_clone[0];

        DB::update("UPDATE {$this->montessori_db}.content_loker SET subcategory_id='{$subcategory_id}',
                    updated_at=now() WHERE content_id={$content_clone->id}");

        return Redirect::to('/content/loker');
    }

    public function chatbola_edit_act($id)
    {
        $content_type = Input::get('content_type');
        $text = addslashes(Input::get('text'));
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');
        $id = Input::get('id');

        DB::update("UPDATE {$this->montessori_db}.content_cb SET content_type={$content_type}, 
                            `text`='{$text}', dtnext_push='{$dtnext_push}', sequence='{$sequence}'
                            WHERE id={$id}");

        return Redirect::to('/content/chatbola');
    }

    public function chat_edit_act($id)
    {
        $content_type = Input::get('content_type');
        $text = addslashes(Input::get('text'));
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');
        $id = Input::get('id');

        DB::update("UPDATE {$this->montessori_db}.content_chat SET content_type={$content_type}, 
                            `text`='{$text}', dtnext_push='{$dtnext_push}', sequence='{$sequence}'
                            WHERE id={$id}");

        return Redirect::to('/content/chat');
    }

    public function model_edit_act($id)
    {
        $content_type = Input::get('content_type');
        $text = addslashes(Input::get('text'));
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');
        $id = Input::get('id');

        DB::update("UPDATE {$this->montessori_db}.content_model_cnd SET content_type={$content_type}, 
                            `text`='{$text}', dtnext_push='{$dtnext_push}', sequence='{$sequence}'
                            WHERE id={$id}");

        return Redirect::to('/content/model');
    }

    public function loker_del($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content SET is_disabled=1,
                            dtmodified=now() WHERE id={$id}");
        return Redirect::to('/content/loker');
    }

    public function loker_enable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content SET is_disabled=0,
                            dtmodified=now() WHERE id={$id}");
        return Redirect::to('/content/loker');
    }

    public function chatbola_del($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_cb SET n_status=0 WHERE id={$id}");
        return Redirect::to('/content/chatbola');
    }

    public function chatbola_enable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_cb SET n_status=1  WHERE id={$id}");
        return Redirect::to('/content/chatbola');
    }

    public function chat_del($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_chat SET n_status=0 WHERE id={$id}");
        return Redirect::to('/content/chat');
    }

    public function chat_enable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_chat SET n_status=1  WHERE id={$id}");
        return Redirect::to('/content/chat');
    }

    public function model_del($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_model_cnd SET n_status=0 WHERE id={$id}");
        return Redirect::to('/content/model');
    }

    public function model_enable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_model_cnd SET n_status=1  WHERE id={$id}");
        return Redirect::to('/content/model');
    }

    public function loker_umb_del($id)
    {
        DB::connection('montessori')->update("UPDATE loker SET n_status=0,
                            updated_at=now() WHERE id={$id}");
        return Redirect::to('/content/loker_umb');
    }

    public function loker_umb_enable($id)
    {
        DB::connection('montessori')->update("UPDATE loker SET n_status=1,
                            updated_at=now() WHERE id={$id}");
        return Redirect::to('/content/loker_umb');
    }

    public function loker_umb()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT *, a.id as content_id, a.n_status as n_status FROM {$this->montessori_db}.loker a
                                INNER JOIN {$this->montessori_db}.loker_subcategories b ON
                                a.subcategory_id = b.id
                                INNER JOIN {$this->montessori_db}.loker_categories c ON
                                b.loker_category_id = c.id ORDER BY a.id DESC LIMIT 10000");

        $data['results'] = $results;

        return View::make('content.loker_umb', $data);
    }

    public function apiGetSubcategoryLoker($category_id)
    {
        $results = DB::select("SELECT * FROM {$this->montessori_db}.loker_subcategories WHERE loker_category_id={$category_id}
            LIMIT 10000");
        $data['results'] = $results;
        return View::make('content.api.loker_subcategories', $data);
    }

    public function loker_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.loker_categories LIMIT 10000");

        $data['categories'] = $results;

        return View::make('content.loker_add', $data);
    }

    public function chatbola_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        return View::make('content.chat_bola_add', $data);
    }

    public function chat_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        return View::make('content.chat_add', $data);
    }

    public function model_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        return View::make('content.model_add', $data);
    }

    public function discount_content_add_action()
    {
        $content_insert = [
                            'text' => Input::get('text'),
                            'dtscheduled' => Input::get('dtscheduled'),
                            'application_id' => 2007,
                            'sequence' => Input::get('sequence'),
                            'dtexpired' => Input::get('dtexpired')
                        ];
        $id = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_diskon_category = [
            'content_id' => $id,
            'category_id' => Input::get('category_id')
        ];

        DB::connection('montessori')->table('content_diskon_category')->insertGetId($content_diskon_category);

        $content_insert['application_id'] = 6005;
        $content_insert['parent_id'] = $id;

        $insert_id = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_diskon_category = [
            'content_id' => $insert_id,
            'category_id' => Input::get('category_id')
        ];

        DB::connection('montessori')->table('content_diskon_category')->insertGetId($content_diskon_category);

        return Redirect::to('/content/discount_content');
    }

    public function loker_add_action()
    {
        $category_id = Input::get('category_id');
        $subcategory_id = Input::get('subcategory_id');
        $title = addslashes(Input::get('title'));
        $description = addslashes(Input::get('description'));
        $dtscheduled = Input::get('dtscheduled');
        $dtexpired = Input::get('dtexpired');
        $sequence = Input::get('sequence');

        $content_insert = [
            'job_title' => $title,
            'content' => $description,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $id = DB::connection('umb-microsite')->table('loker_detail')->insertGetId($content_insert);

        $microsite_url = 'http://loker.kmi.mobi/?id='.$id;
        $shorten = $this->shorten_url($microsite_url);

        $data = [
            'shorten_url' => $shorten,
            'dtscheduled' => $dtscheduled,
            'dtexpired' => $dtexpired,
            'sequence' => $sequence,
            'category_id' => $category_id,
            'subcategory_id' => $subcategory_id,
            'loker_detail_id' => $id
        ];

        return View::make('content.loker_sms', $data);
    }

    public function chatbola_add_action()
    {
        $content_type = addslashes(Input::get('content_type'));
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        $content_insert = [
            'text' => $text,
            'content_type' => $content_type,
            'dtnext_push' => $dtnext_push,
            'sequence' => $sequence
        ];

        $id = DB::connection('montessori')->table('content_cb')->insertGetId($content_insert);

        return Redirect::to('/content/chatbola');
    }

    public function chat_add_action()
    {
        $content_type = addslashes(Input::get('content_type'));
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        $content_insert = [
            'text' => $text,
            'content_type' => $content_type,
            'dtnext_push' => $dtnext_push,
            'sequence' => $sequence
        ];

        $id = DB::connection('montessori')->table('content_chat')->insertGetId($content_insert);

        return Redirect::to('/content/chat');
    }

    public function model_add_action()
    {
        $content_type = addslashes(Input::get('content_type'));
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        $content_insert = [
            'text' => $text,
            'content_type' => $content_type,
            'dtnext_push' => $dtnext_push,
            'sequence' => $sequence
        ];

        $id = DB::connection('montessori')->table('content_model_cnd')->insertGetId($content_insert);

        return Redirect::to('/content/model');
    }

    public function loker_sms_add_action()
    {
        $text = Input::get('text');
        $category_id = Input::get('category_id');
        $subcategory_id = Input::get('subcategory_id');
        $title = Input::get('title');
        $description = Input::get('description');
        $dtscheduled = Input::get('dtscheduled');
        $dtexpired = Input::get('dtexpired');
        $sequence = Input::get('sequence');
        $loker_detail_id = Input::get('loker_detail_id');

        $content_insert = [
            'text' => $text,
            'dtscheduled' => $dtscheduled,
            'application_id' => 7002,
            'sequence' => $sequence,
            'dtexpired' => $dtexpired
        ];

        $content_id = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_loker_category = [
            'content_id' => $content_id,
            'subcategory_id' => $subcategory_id,
            'loker_detail_id' => $loker_detail_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        DB::connection('montessori')->table('content_loker')->insertGetId($content_loker_category);

        $content_insert['application_id'] = 6007;
        $content_insert['parent_id'] = $content_id;

        $last_insert_clone = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_loker_category['content_id'] = $last_insert_clone;
        DB::connection('montessori')->table('content_loker')->insertGetId($content_loker_category);


        return Redirect::to('/content/loker');

    }


    public function chatbola_sms_add_action()
    {
        $text = Input::get('text');
        $category_id = Input::get('category_id');
        $title = Input::get('title');
        $description = Input::get('description');
        $dtscheduled = Input::get('dtscheduled');
        $dtexpired = Input::get('dtexpired');
        $sequence = Input::get('sequence');
        $chatbola_detail_id = Input::get('chatbola_detail_id');

        $content_insert = [
            'text' => $text,
            'dtscheduled' => $dtscheduled,
            'application_id' => 8001,
            'sequence' => $sequence,
            'dtexpired' => $dtexpired
        ];

        $content_id = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_chatbola_category = [
            'content_id' => $content_id,
            'chatbola_detail_id' => $chatbola_detail_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        DB::connection('montessori')->table('content_chatbola')->insertGetId($content_chatbola_category);

//        $content_insert['application_id'] = 8001;
//        $content_insert['parent_id'] = $content_id;
//
//        $last_insert_clone = DB::connection('montessori')->table('content')->insertGetId($content_insert);
//
//        $content_loker_category['content_id'] = $last_insert_clone;
//        DB::connection('montessori')->table('content_loker')->insertGetId($content_loker_category);


        return Redirect::to('/content/chatbola');

    }

    public function chat_sms_add_action()
    {
        $text = Input::get('text');
        $category_id = Input::get('category_id');
        $title = Input::get('title');
        $description = Input::get('description');
        $dtscheduled = Input::get('dtscheduled');
        $dtexpired = Input::get('dtexpired');
        $sequence = Input::get('sequence');
        $chat_detail_id = Input::get('chat_detail_id');

        $content_insert = [
            'text' => $text,
            'dtscheduled' => $dtscheduled,
            'application_id' => 6000,
            'sequence' => $sequence,
            'dtexpired' => $dtexpired
        ];

        $content_id = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_chat_category = [
            'content_id' => $content_id,
            'chat_detail_id' => $chat_detail_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        DB::connection('montessori')->table('content_chat')->insertGetId($content_chat_category);

//        $content_insert['application_id'] = 8001;
//        $content_insert['parent_id'] = $content_id;
//
//        $last_insert_clone = DB::connection('montessori')->table('content')->insertGetId($content_insert);
//
//        $content_loker_category['content_id'] = $last_insert_clone;
//        DB::connection('montessori')->table('content_loker')->insertGetId($content_loker_category);


        return Redirect::to('/content/chat');

    }

    public function model_sms_add_action()
    {
        $text = Input::get('text');
        $category_id = Input::get('category_id');
        $title = Input::get('title');
        $description = Input::get('description');
        $dtscheduled = Input::get('dtscheduled');
        $dtexpired = Input::get('dtexpired');
        $sequence = Input::get('sequence');
        $chat_detail_id = Input::get('chat_detail_id');

        $content_insert = [
            'text' => $text,
            'dtscheduled' => $dtscheduled,
            'application_id' => 2000,
            'sequence' => $sequence,
            'dtexpired' => $dtexpired
        ];

        $content_id = DB::connection('montessori')->table('content')->insertGetId($content_insert);

        $content_chat_category = [
            'content_id' => $content_id,
            'chat_detail_id' => $chat_detail_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        DB::connection('montessori')->table('content_model_cnd')->insertGetId($content_chat_category);

//        $content_insert['application_id'] = 8001;
//        $content_insert['parent_id'] = $content_id;
//
//        $last_insert_clone = DB::connection('montessori')->table('content')->insertGetId($content_insert);
//
//        $content_loker_category['content_id'] = $last_insert_clone;
//        DB::connection('montessori')->table('content_loker')->insertGetId($content_loker_category);


        return Redirect::to('/content/model');

    }

    public function umb_promo()
    {
        $data = $this->constructMenu();
        
        $result = DB::connection('umb-app')->select("SELECT * FROM event_promo_coin ORDER BY id DESC LIMIT 10");
        $date_now = Carbon::now();
        $data['results'] = $result;

        return View::make('content.umb_promo', $data);
    }

    public function umb_promo_add()
    {
        $data = $this->constructMenu();
        $data['coin_config'] = Config::get('app.coin');

        return View::make('content.umb_promo_add', $data);
    }

    public function umb_promo_save()
    {
        $promo_name = Input::get('promo_name');
        $start_date = Input::get('start_date').' 00:00:00';
        $end_date = Input::get('end_date').' 23:59:59';
        $header_utama = Input::get('header_utama');
        $header_cnd = Input::get('header_cnd');
        $header_inbox = Input::get('header_inbox');
        $redeem_date = Input::get('redeem_date');
        $coin_reward = Input::get('coin_reward');
        $array_coin_reward = explode('#', $coin_reward);
        $game_config = Input::get('game_config');
        $raw_data = addslashes(serialize(Input::get()));

        DB::connection('umb-app')->select("INSERT INTO event_promo_coin(`promo_name`, `start_date`, 
                                `end_date`, `redeem_date`, `amount_coin`, `reward`, `data`) 
                                VALUES('{$promo_name}', '{$start_date}', '{$end_date}', '{$redeem_date}', 
                                {$array_coin_reward[0]}, '{$array_coin_reward[1]}', '{$raw_data}')");

        return Redirect::to('/content/umb_promo');
    }

    public function umb_promo_edit($id)
    {
        $data = $this->constructMenu();
        $result = DB::connection('umb-app')->select("SELECT * FROM event_promo_coin WHERE id={$id} LIMIT 1");
        //print_r($result);
        $rs_row = $result[0];
        // $data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $rs_row->data);
        // var_dump(unserialize($data));
        // exit();
        $row_data = unserialize($rs_row->data);
        $data['row_data'] = $row_data;
        $data['coin_reward'] = $rs_row->amount_coin.'#'.$rs_row->reward;
        $data['coin_config'] = $row_data['coin_config'];
        $data['id'] = $rs_row->id;

        return View::make('content.umb_promo_edit', $data);
    }

    public function umb_promo_edit_save()
    {
        $promo_name = Input::get('promo_name');
        $start_date = Input::get('start_date').' 00:00:00';
        $end_date = Input::get('end_date').' 23:59:59';
        $header_utama = Input::get('header_utama');
        $header_cnd = Input::get('header_cnd');
        $header_inbox = Input::get('header_inbox');
        $redeem_date = Input::get('redeem_date');
        $coin_reward = Input::get('coin_reward');
        $array_coin_reward = explode('#', $coin_reward);
        $game_config = Input::get('game_config');
        $raw_data = addslashes(serialize(Input::get()));

        $id = Input::get('id');

        DB::connection('umb-app')->select("UPDATE event_promo_coin SET `promo_name`='{$promo_name}', `start_date`='{$start_date}',
                                            `end_date`='{$end_date}', `redeem_date`='{$redeem_date}', `amount_coin`={$array_coin_reward[0]}, 
                                            `reward`='{$array_coin_reward[1]}', `data`='{$raw_data}', n_status=1 WHERE 
                                            id={$id}");

        return Redirect::to('/content/umb_promo');
    }

    public function umb_promo_stop($id)
    {
        $result = DB::connection('umb-app')->select("UPDATE event_promo_coin SET n_status = 4 WHERE id={$id}");
        return Redirect::to('/content/umb_promo');
    }

    public function umb_promo_start($id)
    {
        $result = DB::connection('umb-app')->select("UPDATE event_promo_coin SET n_status = 1 WHERE id={$id}");
        return Redirect::to('/content/umb_promo');
    }

    //umb promo indosat
    public function umb_promo_indosat()
    {
        $data = $this->constructMenu();
        
        $result = DB::connection('umb-app')->select("SELECT * FROM event_promo_coin_indosat ORDER BY id DESC LIMIT 10");
        $date_now = Carbon::now();
        $data['results'] = $result;

        return View::make('content.umb_promo_indosat', $data);
    }

    public function umb_promo_add_indosat()
    {
        $data = $this->constructMenu();
        $data['coin_config'] = Config::get('app.coin');

        return View::make('content.umb_promo_add_indosat', $data);
    }

    public function umb_promo_save_indosat()
    {
        $promo_name = Input::get('promo_name');
        $start_date = Input::get('start_date').' 00:00:00';
        $end_date = Input::get('end_date').' 23:59:59';
        $header_utama = Input::get('header_utama');
        $header_cnd = Input::get('header_cnd');
        $header_inbox = Input::get('header_inbox');
        $redeem_date = Input::get('redeem_date');
        $coin_reward = Input::get('coin_reward');
        $array_coin_reward = explode('#', $coin_reward);
        $game_config = Input::get('game_config');
        $raw_data = addslashes(serialize(Input::get()));

        DB::connection('umb-app')->select("INSERT INTO event_promo_coin_indosat(`promo_name`, `start_date`, 
                                `end_date`, `redeem_date`, `amount_coin`, `reward`, `data`) 
                                VALUES('{$promo_name}', '{$start_date}', '{$end_date}', '{$redeem_date}', 
                                {$array_coin_reward[0]}, '{$array_coin_reward[1]}', '{$raw_data}')");

        return Redirect::to('/content/umb_promo_indosat');
    }

    public function umb_promo_edit_indosat($id)
    {
        $data = $this->constructMenu();
        $result = DB::connection('umb-app')->select("SELECT * FROM event_promo_coin_indosat WHERE id={$id} LIMIT 1");
        //print_r($result);
        $rs_row = $result[0];
        // $data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $rs_row->data);
        // var_dump(unserialize($data));
        // exit();
        $row_data = unserialize($rs_row->data);
        $data['row_data'] = $row_data;
        $data['coin_reward'] = $rs_row->amount_coin.'#'.$rs_row->reward;
        $data['coin_config'] = $row_data['coin_config'];
        $data['id'] = $rs_row->id;

        return View::make('content.umb_promo_edit_indosat', $data);
    }

    public function umb_promo_edit_save_indosat()
    {
        $promo_name = Input::get('promo_name');
        $start_date = Input::get('start_date').' 00:00:00';
        $end_date = Input::get('end_date').' 23:59:59';
        $header_utama = Input::get('header_utama');
        $header_cnd = Input::get('header_cnd');
        $header_inbox = Input::get('header_inbox');
        $redeem_date = Input::get('redeem_date');
        $coin_reward = Input::get('coin_reward');
        $array_coin_reward = explode('#', $coin_reward);
        $game_config = Input::get('game_config');
        $raw_data = addslashes(serialize(Input::get()));

        $id = Input::get('id');

        DB::connection('umb-app')->select("UPDATE event_promo_coin_indosat SET `promo_name`='{$promo_name}', `start_date`='{$start_date}',
                                            `end_date`='{$end_date}', `redeem_date`='{$redeem_date}', `amount_coin`={$array_coin_reward[0]}, 
                                            `reward`='{$array_coin_reward[1]}', `data`='{$raw_data}', n_status=1 WHERE 
                                            id={$id}");

        return Redirect::to('/content/umb_promo_indosat');
    }

    public function umb_promo_stop_indosat($id)
    {
        $result = DB::connection('umb-app')->select("UPDATE event_promo_coin_indosat SET n_status = 4 WHERE id={$id}");
        return Redirect::to('/content/umb_promo_indosat');
    }

    public function umb_promo_start_indosat($id)
    {
        $result = DB::connection('umb-app')->select("UPDATE event_promo_coin_indosat SET n_status = 1 WHERE id={$id}");
        return Redirect::to('/content/umb_promo_indosat');
    }
    //eof umb promo indosat

    public function discount_content_edit($id)
    {
        $results = DB::select("SELECT *,b.id as content_id FROM {$this->montessori_db}.content_diskon_category a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.application_diskon_category c ON
                                a.category_id = c.category_id WHERE b.id = {$id}");

        $result = $results[0];

        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $category = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

        $data['categories'] = $category;
        $data['result'] = $result;

        return View::make('content.discount_content_edit', $data);
    }

    public function discount_content_edit_act($id)
    {

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content WHERE id = {$id}");
        $result = $results[0];
        $parent_id = $result->parent_id;

        $category_id = Input::get('category_id');
        $content_update = [
            'text' => Input::get('text'),
            'dtscheduled' => Input::get('dtscheduled'),
            'sequence' => Input::get('sequence'),
            'dtexpired' => Input::get('dtexpired')
        ];

        DB::connection('montessori')->table('content')
            ->where('id', $id)
            ->update($content_update);

        $content_diskon_category = [
            'category_id' => Input::get('category_id')
        ];

        DB::connection('montessori')->table('content_diskon_category')
            ->where('content_id', $id)
            ->update($content_diskon_category);

        //clone
        DB::connection('montessori')->table('content')
            ->where('id', $parent_id)
            ->update($content_update);

        DB::connection('montessori')->table('content_diskon_category')
            ->where('content_id', $parent_id)
            ->update($content_diskon_category);


        return Redirect::to('/content/discount_content');
    }

    public function discount_content_dell($id)
    {
        DB::select("UPDATE {$this->montessori_db}.content SET is_disabled = 1 WHERE id={$id}");
        return Redirect::to('/content/discount_content');
    }

    public function discount_content_enable($id)
    {
        DB::select("UPDATE {$this->montessori_db}.content SET is_disabled = 0 WHERE id={$id}");
        return Redirect::to('/content/discount_content');
    }
    public function discount_content_umb()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::connection('umb-app')->select("SELECT * FROM discounts
                                WHERE n_status = 1
                                ORDER BY id DESC");

        $categories = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

        $category = [];
        foreach($categories as $value)
        {
            $category[$value->category_id] = $value->category_name;
        }
        $data['category'] = $category;
        $data['results'] = $results;

        return View::make('content.discount_content_umb', $data);
    }

    public function discount_umb()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::connection('umb-app')->select("SELECT * FROM discounts
                                WHERE n_status = 1
                                ORDER BY id DESC");

        $categories = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

        $category = [];
        foreach($categories as $value)
        {
            $category[$value->category_id] = $value->category_name;
        }
        $data['category'] = $category;
        $data['results'] = $results;

        return View::make('content.discount_umb', $data);
    }

    public function discount_content_umb_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

        $data['categories'] = $results;

        return View::make('content.discount_content_umb_add', $data);
    }

    public function loker_umb_add()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $results = DB::select("SELECT * FROM {$this->montessori_db}.loker_categories LIMIT 10000");

        $data['categories'] = $results;

        return View::make('content.loker_umb_add', $data);
    }

    public function loker_umb_add_action()
    {
        $category_id = Input::get('category_id');
        $subcategory_id = Input::get('subcategory_id');
        $job_title = addslashes(Input::get('title'));
        $description = addslashes(Input::get('description'));
        $dtscheduled = Input::get('started_at');
        $dtexpired = Input::get('expired_at');
        $is_top_loker = Input::get('top_loker');

        $content_insert = [
            'job_title' => $job_title,
            'content' => $description,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $id = DB::connection('umb-microsite')->table('loker_detail')->insertGetId($content_insert);

        $microsite_url = 'http://loker.kmi.mobi/?id='.$id;
        $shorten_url = $this->shorten_url($microsite_url);

        DB::insert("INSERT INTO {$this->montessori_db}.loker (subcategory_id,loker_detail_id,job_title,
                  shorten_url, started_at,expired_at,is_top_loker,created_at,updated_at)
        VALUES('{$subcategory_id}', '{$id}', '{$job_title}', '{$shorten_url}',
        '{$dtscheduled}', '{$dtexpired}', '{$is_top_loker}', now(), now())");

        return Redirect::to('/content/loker_umb');
    }

    public function loker_umb_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();


        $rs_loker = DB::select("SELECT * FROM {$this->montessori_db}.loker a
                                INNER JOIN `{$this->umb_microsite}`.loker_detail b
                                ON a.loker_detail_id=b.id
                                INNER JOIN {$this->montessori_db}.loker_subcategories c
                                ON a.subcategory_id = c.id
                                INNER JOIN {$this->montessori_db}.loker_categories d
                                ON c.loker_category_id = d.id
                                WHERE a.id = {$id} LIMIT 1");

        $loker = $rs_loker[0];

        $categories = DB::select("SELECT * FROM {$this->montessori_db}.loker_categories LIMIT 10000");
        $subcategories = DB::select("SELECT * FROM {$this->montessori_db}.loker_subcategories
                          WHERE loker_category_id={$loker->loker_category_id} LIMIT 10000");

        $data['results'] = $loker;
        $data['categories'] = $categories;
        $data['subcategories'] = $subcategories;
        $data['id'] = $id;

        return View::make('content.loker_umb_edit', $data);
    }

    public function redem_coin_list()
    {
        $rs_redem = DB::connection('umb-app')->select();
    }

    public function loker_umb_edit_act($id)
    {
        $text = Input::get('text');
        $category_id = Input::get('category_id');
        $subcategory_id = Input::get('subcategory_id');
        $job_title = addslashes(Input::get('job_title'));
        $description = addslashes(Input::get('description'));
        $started_at = Input::get('started_at');
        $expired_at = Input::get('expired_at');
        $loker_detail_id = Input::get('loker_detail_id');
        $is_top_loker = Input::get('top_loker');

        $rs_content_loker = DB::connection('montessori')->select("SELECT * FROM loker WHERE id={$id}
                            LIMIT 1");
        $content_loker = $rs_content_loker[0];

        DB::connection('umb-microsite')->update("UPDATE loker_detail SET job_title='{$job_title}', content='{$description}',
                  updated_at=now() WHERE id={$content_loker->loker_detail_id}");

        DB::connection('montessori')->update("UPDATE loker SET job_title='{$job_title}',
                    started_at='{$started_at}', expired_at='{$expired_at}', is_top_loker='{$is_top_loker}',
                    updated_at=now() WHERE id={$id}");

        return Redirect::to('/content/loker_umb');
    }

    public function discount_content_umb_add_action()
    {
        $content_insert = [
            'discount_category_id' => Input::get('category_id'),
            'content' => Input::get('content'),
            'start_at' => Input::get('start_at'),
            'expired_at' => Input::get('expired_at'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $id = DB::connection('umb-app')->table('discounts')->insertGetId($content_insert);

        return Redirect::to('/content/discount_umb');
    }

    public function discount_content_umb_edit($id)
    {
        $rs_discount = DB::connection('umb-app')->select("SELECT *, id AS content_id FROM discounts WHERE id = {$id} AND n_status = 1");

        $result = $rs_discount[0];

        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $category = DB::connection('montessori')->select("SELECT * FROM application_diskon_category GROUP BY category_id");

        $data['categories'] = $category;
        $data['result'] = $result;

        return View::make('content.discount_content_umb_edit', $data);
    }

    public function discount_content_umb_edit_act($id)
    {
        $category_id = Input::get('category_id');
        $discount_update = [
            'content' => Input::get('content'),
            'discount_category_id' => $category_id,
            'start_at' => Input::get('start_at'),
            'expired_at' => Input::get('expired_at'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        DB::connection('umb-app')->table('discounts')
            ->where('id', $id)
            ->update($discount_update);

        return Redirect::to('/content/discount_content_umb');
    }

    public function discount_content_umb_del($id)
    {
        DB::connection('umb-app')->table('discounts')
            ->where('id', $id)
            ->update(['n_status' => 0]);

        return Redirect::to('/content/discount_content_umb');
    }
    /**
     * The 'API' of list of pull application.
     */
    public function apiGetListPullApplication($telcoId = 0, $sdc = ''){
        return json_encode(Application::getListApplication($telcoId, $sdc, 0));
    }


    /**
     * The 'API' of list of push application.
     */
    public function apiGetListPushApplication($telcoId = 0, $sdc = ''){
        $data = Application::getListApplication($telcoId, $sdc, 1);
        /*
        if(Auth::user()->username == 'inong') {
            for($i = 1; $i <= 6; $i++ ) {
                unset($data[$i]);
            }
        }
        */
        return json_encode($data);
    }

    public function apiGetListUmbContent($umbapp)
    {
        if($umbapp == 'Loker')
        {
            $results = DB::connection('montessori')->select("SELECT *, a.id AS id, a.n_status AS n_status FROM loker a
                                    INNER JOIN {$this->montessori_db}.loker_subcategories b ON a.subcategory_id=b.id
                                    INNER JOIN {$this->montessori_db}.loker_categories c ON b.loker_category_id = c.id
                                    ORDER BY a.id DESC LIMIT 10000;");

            $results['results'] = $results;

            return View::make('content.loker_content_umb', $results);

        }
        else if($umbapp == 'Discount')
        {
            $results = DB::connection('umb-app')->select("SELECT * FROM discounts
                                WHERE n_status = 1
                                ORDER BY id DESC");

            $categories = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

            $category = [];
            foreach($categories as $value)
            {
                $category[$value->category_id] = $value->category_name;
            }
            $data['category'] = $category;
            $data['results'] = $results;

            return View::make('content.discount_content_umb_ajax', $data);
        }
        else if($umbapp == 'TebakSkor')
        {
            $results = DB::connection('umb-app')->select("SELECT * FROM tebak_skor 
                    ORDER BY match_date DESC, gameid ASC 
                    LIMIT 0, 100");
            $results['results'] = $results;
            return View::make('content.tebakskor_content', $results);
        }
        else if($umbapp == 'Badword')
        {
            $results['results'] = DB::connection('umb-app')
                    ->table('bad_words')
                    ->select('id','keyword','regex_filter')
                    ->orderBy('keyword', 'asc')
                    ->get();
            return View::make('content.badword_content', $results);
        }
    }

    private function shorten_url($url)
    {
        $host = trim($this->get_host_without_tdl($url));
        $add_url_campaign = '';
        $separator = '';
        if ($host != 'loker.kmi.mobi')
        {
            if($host != 'bola.kmi.mobi')
            {
                $add_url_campaign = 'utm_source=super-diskon&utm_medium=sms&utm_campaign=super-diskon-sms-' . $host . '-info';
                $separator = '?';
                if(is_numeric(strpos($url, '?')))
                {
                    $separator = '&';
                }
            }
        }

        $final_url =  $url.$separator.$add_url_campaign;
        $short = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 5);
        DB::insert("INSERT INTO {$this->montessori_db}.urls(url_link,url_short,url_ip,url_date)
        VALUES('{$final_url}', '{$short}', '{$_SERVER['REMOTE_ADDR']}', ".time().")");

        return 'http://kmi.mobi/'.$short;
    }

    public function apiShortenUrl()
    {
        $url =  urldecode(Input::get('url'));
        $add_url_campaign = 'utm_source=super-diskon&utm_medium=sms&utm_campaign=super-diskon-sms-'.$this->get_host_without_tdl($url).'-info';
        $separator = '?';
        if(is_numeric(strpos($url, '?')))
        {
            $separator = '&';
        }

        $final_url =  $url.$separator.$add_url_campaign;
        $short = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 5);
        DB::insert("INSERT INTO {$this->montessori_db}.urls(url_link,url_short,url_ip,url_date)
        VALUES('{$final_url}', '{$short}', '{$_SERVER['REMOTE_ADDR']}', ".time().")");

        echo 'http://kmi.mobi/'.$short;
    }

    private function get_host_without_tdl($url)
    {
        $parse = parse_url($url);
        $string_array = explode('.', $parse['host']);

        if($string_array[0] == 'www')
        {
            return $string_array[1];
        }

        return $parse['host'];
    }


    /**
     * Get list of pull content
     * @param $applicationId
     * @return mixed
     */
    public function apiGetListPullContent($applicationId) {
        $data = array();
        $db = Config::get('reporting.database_application');
        $data['title'] = "Pull Content";
        $data['contentType'] = 0;
        $rec = Application::getById($applicationId);
        $data['application'] = $rec[0];
        $data['page'] = Input::get('page') ? Input::get('page') : 0;
        $data['applicationId'] = $applicationId;

        $data['the_data'] = DB::table("{$db}.pull_content")
            ->select('id','text','is_disabled','dtscheduled','sequence','is_new','dtexpired','charging_param','composite_type')
            ->where('application_id','=',$applicationId)
            ->orderBy('id', 'DESC')->paginate(10);
        /*
        $data['total'] = DB::table("{$db}.pull_content")
            ->where('application_id', '=', $applicationId)
            ->count();
        */
        return View::make('content.api.api_list_content', $data);
    }


    /**
     * Get list of push content
     * @param $applicationId
     * @return mixed
     */
    public function apiGetListPushContent($applicationId) {
        $data = array();
        $db = Config::get('reporting.database_application');
        $data['title'] = "Push Content";
        $data['contentType'] = 1;
        $rec = Application::getById($applicationId);
        $data['application'] = $rec[0];
        $data['page'] = Input::get('page') ? Input::get('page') : 0;
        $data['applicationId'] = $applicationId;
        $data['the_data'] = DB::table("{$db}.content")
            //->select('id','text','is_disabled','dtscheduled','sequence','is_new','dtexpired','charging_param','composite_type')
            ->select('id','text','is_disabled','dtscheduled','sequence','charging_param','composite_type','application_id')
            ->where('application_id','=',$applicationId)
            ->orderBy('id', 'DESC')->paginate(10);
        if($applicationId == '2007')
        {
            $data['the_data'] = DB::select("SELECT * FROM {$this->montessori_db}.content_diskon_category a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.application_diskon_category c ON
                                a.category_id = c.category_id 
                                WHERE b.application_id={$applicationId} 
                                AND b.dtexpired >= NOW()    
                                GROUP BY b.id ORDER BY b.id DESC");

            //$data['push_one'] = DB::select("SELECT * FROM {$this->montessori_db}.application_welcome_content
            //                          WHERE application_id=2007 ORDER BY id DESC");

	    //$data['push_one'] = DB::select("SELECT a.id,a.text,b.category_name,a.dtexpired FROM {$this->montessori_db}.application_welcome_content
            //                          a, {$this->montessori_db}.application_diskon_category b
            //                          WHERE a.keyword_id = b.category_id
            //                          AND a.application_id=2007
            //                          GROUP BY a.id
            //                          ORDER BY id DESC");

	    $data['push_one'] = DB::select("SELECT a.id,a.text,b.category_name,CONCAT('DISKON ',b.keyword) as keyword,a.dtexpired 
					FROM {$this->montessori_db}.application_welcome_content a, {$this->montessori_db}.application_diskon_category b
                                      	WHERE a.keyword_id = b.keyword_id 
					AND a.application_id=2007");

            return View::make('content.api.api_list_content_discount', $data);
        }
        else if($applicationId == '7002')
        {
            $results = DB::select("SELECT *,a.id as id FROM {$this->montessori_db}.content_loker a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.loker_subcategories c ON
                                a.subcategory_id = c.id
                                INNER JOIN {$this->montessori_db}.loker_categories d ON
                                c.loker_category_id = d.id
                                INNER JOIN `{$this->umb_microsite}`.loker_detail e ON
                                a.loker_detail_id = e.id WHERE b.application_id={$applicationId}  GROUP BY b.id ORDER BY a.id DESC");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id=7002");

            return View::make('content.api.api_list_content_loker', $data);
        }
        else if($applicationId == 6005)
        {
            $data['the_data'] = DB::select("SELECT * FROM {$this->montessori_db}.content_diskon_category a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.application_diskon_category c ON
                                a.category_id = c.category_id WHERE b.application_id={$applicationId} GROUP BY b.id ORDER BY b.id DESC");


            $data['push_one'] = DB::select("SELECT a.id,a.text,b.category_name,CONCAT('DISKON ',b.keyword) as keyword,a.dtexpired
					FROM {$this->montessori_db}.application_welcome_content a, {$this->montessori_db}.application_diskon_category b
                                      	WHERE a.keyword_id = b.keyword_id
					AND a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_xl_discount', $data);
        }
        else if($applicationId == 6007)
        {
            $results = DB::select("SELECT *,a.id as id FROM {$this->montessori_db}.content_loker a
                                INNER JOIN {$this->montessori_db}.content b ON a.content_id = b.id
                                INNER JOIN {$this->montessori_db}.loker_subcategories c ON
                                a.subcategory_id = c.id
                                INNER JOIN {$this->montessori_db}.loker_categories d ON
                                c.loker_category_id = d.id
                                INNER JOIN `{$this->umb_microsite}`.loker_detail e ON
                                a.loker_detail_id = e.id WHERE b.application_id={$applicationId}  GROUP BY b.id ORDER BY a.id DESC");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_xl_loker', $data);

        }
        else if($applicationId == '8001' )
        {
            $results = DB::select("SELECT * FROM {$this->montessori_db}.content_cb WHERE n_status = 1 ORDER BY content_type ASC LIMIT 20");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_tsel_chatbola', $data);
        }
        else if($applicationId == '6000' )
        {
            $results = DB::select("SELECT * FROM {$this->montessori_db}.content_chat WHERE n_status = 1 ORDER BY content_type ASC LIMIT 20");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_xl_chat', $data);
        }
        else if($applicationId == '2000' )
        {
            $results = DB::select("SELECT * FROM {$this->montessori_db}.content_model_cnd WHERE n_status = 1 ORDER BY content_type ASC LIMIT 20");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_isat_model', $data);
        }
        else if($applicationId == '2006' )
        {
            $results = DB::select("SELECT * FROM {$this->montessori_db}.content_model_cnd WHERE n_status = 1 ORDER BY content_type ASC LIMIT 20");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_isat_model', $data);
        }
        else if($applicationId == '9000')
        {
            $results = DB::select("SELECT * FROM {$this->montessori_db}.content_ilive_isat WHERE n_status = 1 ORDER BY content_type ASC");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_isat_ilive', $data);
        }
        else if($applicationId == '4000')
        {
            $results = DB::select("SELECT * FROM {$this->montessori_db}.content_ilive_xl WHERE n_status = 1 ORDER BY content_type ASC");

            $data['the_data'] = $results;

            $data['push_one'] = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.application_id={$applicationId}");

            return View::make('content.api.api_list_content_xl_ilive', $data);
        }

        return View::make('content.api.api_list_content', $data);
    }

    public function push_one($id)
    {
        $data = $this->constructMenu();
        //$rs_push = DB::select("SELECT * FROM {$this->montessori_db}.application_welcome_content
        //                              WHERE application_id=2007 AND id={$id}");
        $rs_push = DB::select("SELECT a.id,a.text,b.category_id,b.category_name,a.dtexpired
                        FROM {$this->montessori_db}.application_welcome_content a, {$this->montessori_db}.application_diskon_category b
                                            WHERE a.keyword_id = b.keyword_id
                                             AND a.id={$id}");

            //$rs_push = DB::select("SELECT a.id,a.text,b.category_id,b.category_name,a.dtexpired,a.keyword_id
            //                      FROM {$this->montessori_db}.application_welcome_content a, {$this->montessori_db}.application_diskon_category b
            //                                  WHERE a.keyword_id = b.keyword_id AND a.application_id=2007 AND a.id={$id}
            //                                  ORDER BY a.id DESC");

        $category = DB::select("SELECT * FROM {$this->montessori_db}.application_diskon_category GROUP BY category_id");

        $data['categories'] = $category;

        $data['result'] = $rs_push[0];

        return View::make('content.push_one_edit', $data);
    }

    public function push_one_action($id)
    {
        $content_update = [
            'text' => Input::get('text'),
            'dtmodified' => date('Y-m-d H:i:s'),
            'dtexpired' => Input::get('dtexpired')
        ];

        DB::connection('montessori')->table('application_welcome_content')
            ->where('id', $id)
            ->update($content_update);

        return Redirect::to('/content/push_content');
    }

    public function universal_push_one($id)
    {
        $data = $this->constructMenu();
        //$rs_push = DB::select("SELECT * FROM {$this->montessori_db}.application_welcome_content
        //                              WHERE application_id=2007 AND id={$id}");
        $rs_push = DB::select("SELECT a.id,a.text,a.dtexpired
                                    FROM montessori.application_welcome_content a
                                    WHERE a.id={$id}");


        $data['result'] = $rs_push[0];

        return View::make('content.loker_push_one_edit', $data);
    }

    public function universal_push_one_action($id)
    {
        $content_update = [
            'text' => Input::get('text'),
            'dtmodified' => date('Y-m-d H:i:s'),
            'dtexpired' => Input::get('dtexpired')
        ];

        DB::connection('montessori')->table('application_welcome_content')
            ->where('id', $id)
            ->update($content_update);

        return Redirect::to('/content/push_content');
    }

    /**
     * The 'API' of viewing add push content
     * @param $applicationId application id
     */
    public function apiAddPushContent($applicationId) {
        $data = array();
        $data['type'] = 1;
        $data['typeLabel'] = 'Push';
        $rec = Application::getById($applicationId);
        $data['application'] = $rec[0];
        $data['applicationId'] = $applicationId;
        return View::make('content.api.api_add_new_content', $data);
    }

    /**
     * The 'API' of viewing add pull content
     * @param $applicationId application id
     */
    public function apiAddPullContent($applicationId) {
        $data = array();
        $data['type'] = 0;
        $data['typeLabel'] = 'Pull';
        $rec = Application::getById($applicationId);
        $data['application'] = $rec[0];
        $data['applicationId'] = $applicationId;
        return View::make('content.api.api_add_new_content', $data);
    }


    /**
     * The 'API' save new push content
     */
    public function apiSaveNewPushContent($applicationId) {
        $content = $this->filterContent(Input::get('content'));
        $sequence = Input::get('sequence');
        $content_type = Input::get('content_type');
        try {
            if($applicationId == 9000)
            {
                //content_type_select
                //mengikuti struktur table, karena bukan eloquent, gunakan raw SQL
                if (Input::get('content_type_select') == 2)
                {
                    $sql = "INSERT INTO {$this->montessori_db}.content_ilive_isat(`content_type`,`text`,`zodiac`,`sequence`)
                        VALUES('".Input::get('content_type_select')."','".addslashes(Input::get('content'))."',LOWER('".Input::get('zodiac')."'),'".Input::get('sequence')."')";
                }
                elseif (Input::get('content_type_select') == 3)
                {
                    $sql = "INSERT INTO {$this->montessori_db}.content_ilive_isat(`content_type`,`text`,`url_image`,`sequence`)
                        VALUES('".Input::get('content_type_select')."','".addslashes(Input::get('content'))."','".Input::get('url_image')."','".Input::get('sequence')."')";
                }
                else
                {
                    $sql = "INSERT INTO {$this->montessori_db}.content_ilive_isat(`content_type`,`text`,`sequence`)
                        VALUES('".Input::get('content_type_select')."','".addslashes(Input::get('content'))."','".Input::get('sequence')."')";
                }
                DB::insert($sql);
            }
            elseif($applicationId == 4000)
            {
                //content_type_select
                //mengikuti struktur table, karena bukan eloquent, gunakan raw SQL
                if (Input::get('content_type_select') == 2)
                {
                    $sql = "INSERT INTO {$this->montessori_db}.content_ilive_xl(`content_type`,`text`,`zodiac`,`sequence`)
                        VALUES('".Input::get('content_type_select')."','".addslashes(Input::get('content'))."',LOWER('".Input::get('zodiac')."'),'".Input::get('sequence')."')";
                }
                elseif (Input::get('content_type_select') == 3)
                {
                    $sql = "INSERT INTO {$this->montessori_db}.content_ilive_xl(`content_type`,`text`,`url_image`,`sequence`)
                        VALUES('".Input::get('content_type_select')."','".addslashes(Input::get('content'))."','".Input::get('url_image')."','".Input::get('sequence')."')";
                }
                else
                {
                    $sql = "INSERT INTO {$this->montessori_db}.content_ilive_xl(`content_type`,`text`,`sequence`)
                        VALUES('".Input::get('content_type_select')."','".addslashes(Input::get('content'))."','".Input::get('sequence')."')";
                }
                DB::insert($sql);
            }
            else
            {
                Content::saveNewPushContent($applicationId, $content, $sequence, $content_type);
            }
            

			ActivityLog::writeActiviy(Config::get('activity.CONTENT_PUSH_CREATE'), '/api/create_new_push_content');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * The 'API' save new push content
     */
    public function apiSaveNewPullContent($applicationId) {
        $content = $this->filterContent(Input::get('content'));
        //Just a quick hack
        $content = str_replace('“','"', $content);
        $content = str_replace('”', '"', $content);
        $content = str_replace('‘', '"', $content);

        $sequence = Input::get('sequence');
        //$applicationId = Input::get('applicationId');
        try {
            Content::saveNewPullContent($applicationId, $content, $sequence);

			ActivityLog::writeActiviy(Config::get('activity.CONTENT_PULL_CREATE'), '/api/create_new_pull_content');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * The 'API' to view edit content
     */
    public function apiViewEditContent($contentId){
        $data = array();
        $data['contentId'] = $contentId;
        $data['contentType'] = Input::get("contentType");
        $data['typeLabel'] = Input::get("contentType") == 1 ? "Push" : "Pull";
        $rec = Input::get("contentType") == 1 ? Content::getPushContentById($contentId) : Content::getPullContentById($contentId);
        $data['content'] = $rec[0];
        $data['applicationId'] = Input::get("applicationId");
        return View::make('content.api.api_edit_content', $data);
    }


    /**
     * The 'API' to save edited content
     * @param $contentId content id
     */
    public function apiSaveEditContent($contentId) {
        $content = $this->filterContent(Input::get('content'));
//        var_dump($content);
        $sequence = Input::get('sequence');
        $isDisabled = Input::get('isDisabled');
        $content_type = Input::get('content_type');
        try {
            switch(Input::get("contentType")) {
                case 1:
                    Content::editPushContent($contentId, $content, $sequence, $isDisabled, $content_type);
					ActivityLog::writeActiviy(Config::get('activity.CONTENT_PUSH_EDIT'), '/api/edit_content');

                    break;
                default:
                    Content::editPullContent($contentId, $content, $sequence, $isDisabled);
					ActivityLog::writeActiviy(Config::get('activity.CONTENT_PULL_EDIT'), '/api/edit_content');

                    break;
            }
            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }

    public function get_traffic_for_subscriber()
    {
        $msisdn = Input::get('msisdn');
        $last_number = substr($msisdn, -1);
        $rs_data = DB::select("select * from (
                                            select a.dtsubmitted, a.text, b.eu_price, a.telco_trans_id,
                                            a.telco_sid, a.telco_dr_status
                                            from trafficdb_isat_99858.mt_request_2015_{$last_number} a, cdb.charging b
                                            where a.charging_id = b.id
                                            and a.msisdn = ?
                                            union all
                                            select a.dtsubmitted, a.text, b.eu_price, a.telco_trans_id, a.telco_sid,
                                            a.telco_dr_status from trafficdb_isat_99858.mt_request a, cdb.charging b
                                            where a.charging_id = b.id
                                            and a.msisdn = ?
                                        )
                                tab1
                                order by tab1.dtsubmitted DESC", array($msisdn, $msisdn));
        $data['rs_data'] = $rs_data;
        return View::make('subscriber.api.get_traffic_for_subscriber', $data);
    }

    public function coin_redeem()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $offset = abs(Input::get('offset'));
        $limit = 100;

        $where_sql = '';
        if(Auth::user()->role_id == 8) 
        {
            $where_sql = "WHERE b.provider = 't'";
        }

        $results = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at, IF(a.redeem_source = '2','UMB-PLAY',IF(a.redeem_source = '3','KEPO',IF(a.redeem_source = '4','ILIVE',IF(a.redeem_source = '5','SMSPULL-TSEL','UMB-KMI')))) AS redem_source FROM coin_redem a
                                INNER JOIN users b USING (msisdn) 
                                ".$where_sql."
                                ORDER BY a.id DESC LIMIT {$offset}, {$limit}");

        $next_offset = $offset+$limit;

        $next_results = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at, IF(a.redeem_source = '2','UMB-PLAY',IF(a.redeem_source = '3','KEPO',IF(a.redeem_source = '4','ILIVE',IF(a.redeem_source = '5','SMSPULL-TSEL','UMB-KMI')))) AS redem_source FROM coin_redem a
                                INNER JOIN users b USING (msisdn) 
                                ".$where_sql."
                                ORDER BY a.id DESC LIMIT {$next_offset}, {$limit}");

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/content/coin_redeem?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/content/coin_redeem?offset='.$next_offset);
        }

        $data['results'] = $results;

        return View::make('content.coin', $data);
    }

    //user telkomsel yang sudah redeem koin
    public function coin_redeem_telkomsel()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $offset = abs(Input::get('offset'));
        $limit = 10;

        if(Auth::user()->role_id == 8) 
        {
            $where_sql = "WHERE b.provider = 't'";
            $results = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at FROM coin_redem a
                                    INNER JOIN users b USING (msisdn) 
                                    ".$where_sql."
                                    ORDER BY a.id DESC LIMIT {$offset}, {$limit}");

            $next_offset = $offset+$limit;

            $next_results = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at FROM coin_redem a
                                    INNER JOIN users b USING (msisdn) 
                                    ".$where_sql."
                                    ORDER BY a.id DESC LIMIT {$next_offset}, {$limit}");
        } 
        else 
        {
            $results = [];
            $next_results = [];
        }

        foreach ($results as $key => $value) {
            $results[$key]->name = preg_replace_callback('!\w+!', 'filter_badwords', $value->name);
        }

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/content/coin_redeem_telkomsel?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/content/coin_redeem_telkomsel?offset='.$next_offset);
        }

        $data['results'] = $results;

        return View::make('content.coin-redeem-tsel', $data);
    }

    public function coin_redeem_telkomsel_date()
    {
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $results = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at FROM coin_redem a
                                INNER JOIN users b USING (msisdn)
                                WHERE b.provider = 't' AND DATE_FORMAT(a.created_at,'%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(a.created_at,'%Y-%m-%d') <= '{$end_date}'
                                ORDER BY a.id DESC");

        $data['results'] = $results;

        return View::make('content.coin_redem_date', $data);
    }

    public function coin_redeem_telkomsel_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $result = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at FROM coin_redem a
                                INNER JOIN users USING (msisdn)
                                WHERE a.id={$id} ");

        $data['status'] = 'Pending';
        if($result[0]->n_status == 2)
        {
            $data['status'] = 'Done';
        }

        $data['result'] = $result[0];
        return View::make('content.coin_redeem_edit_tsel', $data);
    }

    public function coin_redeem_telkomsel_update()
    {
        $id = Input::get('id');
        $n_status = Input::get('n_status');
        $update_n_status = 2;
        if($n_status == 2)
        {
            $update_n_status = 1;
        }
        $result = DB::connection('umb-app')->select("UPDATE coin_redem SET n_status = {$update_n_status}
                                WHERE id={$id}");

        if($n_status == 1)
        {
            $rs_subscriber = DB::connection('umb-app')->select("SELECT msisdn FROM coin_redem WHERE id={$id}");
            $msisdn = $rs_subscriber[0]->msisdn;
        }

        return Redirect::to('/content/coin_redeem_telkomsel');
    }

    //user telkomsel yang lebih dari 5000 koin
    public function coin_telkomsel()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $offset = abs(Input::get('offset'));
        $limit = 10;

        $results = DB::connection('umb-app')->select("SELECT a.id, a.msisdn, a.name, b.coins FROM users a INNER JOIN coins b USING(msisdn)
                                WHERE a.provider = 't' AND b.coins >= 5000 ORDER BY b.coins DESC 
                                LIMIT {$offset}, {$limit}");

        $next_offset = $offset+$limit;

        $next_results = DB::connection('umb-app')->select("SELECT a.id, a.msisdn, a.name, b.coins FROM users a INNER JOIN coins b USING(msisdn)
                                WHERE a.provider = 't' AND b.coins >= 5000 ORDER BY b.coins DESC 
                                LIMIT {$offset}, {$limit}");

        //filter bad words
        foreach ($results as $key => $value) {
            $results[$key]->name = preg_replace_callback('!\w+!', 'filter_badwords', $value->name);
        }

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/content/coin_telkomsel?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/content/coin_telkomsel?offset='.$next_offset);
        }

        $data['results'] = $results;

        return View::make('content.coin-telkomsel', $data);
    }

    public function coin_redem_date()
    {
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $results = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at, IF(a.redeem_source = '1','UMB-KMI',IF(a.redeem_source = '2','UMB-PLAY',IF(a.redeem_source = '3','KEPO','ILIVE'))) AS redem_source FROM coin_redem a
                                INNER JOIN users USING (msisdn)
                                WHERE DATE_FORMAT(a.created_at,'%Y-%m-%d') >= '{$start_date}' AND DATE_FORMAT(a.created_at,'%Y-%m-%d') <= '{$end_date}'
                                ORDER BY a.id DESC");

        $data['results'] = $results;

        return View::make('content.coin_redem_date', $data);
    }

    public function coin_redeem_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $result = DB::connection('umb-app')->select("SELECT *, a.id AS id, a.n_status AS n_status, a.created_at AS created_at, IF(a.redeem_source = '1','UMB-KMI',IF(a.redeem_source = '2','UMB-PLAY',IF(a.redeem_source = '3','KEPO','ILIVE'))) AS redem_source FROM coin_redem a
                                INNER JOIN users USING (msisdn)
                                WHERE a.id={$id} ");

        $data['status'] = 'Pending';
        if($result[0]->n_status == 2)
        {
            $data['status'] = 'Done';
        } 
        elseif($result[0]->n_status == 3)
        {
            $data['status'] = 'Cancel';  
        }

        $data['result'] = $result[0];
        return View::make('content.coin_redeem_edit', $data);
    }

    public function coin_redeem_update()
    {
        $id = Input::get('id');
        $n_status = Input::get('n_status');
        $update_n_status = 2;
        if($n_status == 2)
        {
            $update_n_status = 1;
        } 
        elseif($n_status == 3)
        {
            $update_n_status = 3;
        }
        
        $result = DB::connection('umb-app')->select("UPDATE coin_redem SET n_status = {$update_n_status}
                                WHERE id={$id}");

        if($n_status == 1)
        {
            $rs_subscriber = DB::connection('umb-app')->select("SELECT msisdn,amount FROM coin_redem WHERE id={$id}");
            $msisdn = $rs_subscriber[0]->msisdn;
            $amount = $rs_subscriber[0]->amount;

            $amount = substr($amount,1);

            $text = 'Selamat%2c+pulsa+Rp'.$amount.'+sdh+ditransfer+ke+nomor+Anda.+Kumpulkan+terus+koinnya.+Akses+selalu+*858%23.+Info%3a+081510027073';
            $url = 'http://172.16.69.1:2015/isat/mtbroker.jsp?sdc=99858&msisdn='.$msisdn.'&text='.$text.'&telcoId=2&serviceId=2000&chargingCode=99858PUSH0&keyword=model&channel=sms&areaCodeId=0';
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_exec($ch);
            curl_close($ch);
        }

        return Redirect::to('/content/coin_redeem');
    }

    public function cnd()
    {
        $limit = 10;
        $offset = abs(Input::get('offset'));

        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = DB::select("SELECT id,IF(telco_id=2,'INDOSAT','XL') as telco_id,IF(user_type=0,'NON MEMBER CND','MEMBER CND') as user_type,IF(content_type=0,'PAID','FREE') as content_type,`text`,sequence,dtnext_push,n_status FROM {$this->montessori_db}.content_cnd ORDER BY id DESC LIMIT {$offset}, {$limit}");
        $next_offset = $offset+$limit;

        $next_results = DB::select("SELECT id,IF(telco_id=2,'INDOSAT','XL') as telco_id,IF(user_type=0,'NON MEMBER CND','MEMBER CND') as user_type,IF(content_type=0,'PAID','FREE') as content_type,`text`,sequence,dtnext_push,n_status FROM {$this->montessori_db}.content_cnd ORDER BY id DESC LIMIT {$next_offset}, {$limit}");

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/content/cnd?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/content/cnd?offset='.$next_offset);
        }

        $data['results'] = $results;
        return View::make('content.content_cnd', $data);
    }

    public function cnd_save()
    {
        $telco_id = Input::get('telco_id');     
        $user_type = Input::get('user_type');
        $content_type = Input::get('content_type');
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        DB::insert("INSERT INTO {$this->montessori_db}.content_cnd (telco_id,user_type,content_type,text,sequence,dtnext_push)
        VALUES('{$telco_id}', '{$user_type}', '{$content_type}', '{$text}', '{$sequence}', '{$dtnext_push}')");

        return Redirect::to('/content/cnd');
    }

    public function cnd_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $result = DB::select("SELECT * FROM {$this->montessori_db}.content_cnd WHERE id={$id} LIMIT 1");

        $data['result'] = $result[0];

        return View::make('content.content_cnd_edit', $data);
    }

    public function cnd_update($id)
    {
        $telco_id = Input::get('telco_id');    
        $user_type = Input::get('user_type');
        $content_type = Input::get('content_type');
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        DB::update("UPDATE {$this->montessori_db}.content_cnd SET telco_id={$telco_id}, user_type={$user_type}, content_type={$content_type},
                    text='{$text}', dtnext_push='{$dtnext_push}', sequence={$sequence} WHERE id={$id}");

        return Redirect::to('/content/cnd');

    }

    public function cnd_del($id)
    {
        DB::insert("UPDATE {$this->montessori_db}.content_cnd SET n_status = 0 WHERE id={$id}");

        return Redirect::to('/content/cnd');
    }

    public function cnd_enable($id)
    {
        DB::insert("UPDATE {$this->montessori_db}.content_cnd SET n_status = 1 WHERE id={$id}");

        return Redirect::to('/content/cnd');
    }

    public function fb()
    {
        $limit = 10;
        $offset = abs(Input::get('offset'));

        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = DB::select("SELECT id,IF(telco_id=2,'INDOSAT','XL') as telco_id,IF(user_type=0,'NON MEMBER FB','MEMBER FB') as user_type,IF(content_type=0,'PAID','FREE') as content_type,`text`,sequence,dtnext_push,n_status FROM {$this->montessori_db}.content_fb ORDER BY id DESC LIMIT {$offset}, {$limit}");
        $next_offset = $offset+$limit;

        $next_results = DB::select("SELECT id,IF(telco_id=2,'INDOSAT','XL') as telco_id,IF(user_type=0,'NON MEMBER FB','MEMBER FB') as user_type,IF(content_type=0,'PAID','FREE') as content_type,`text`,sequence,dtnext_push,n_status FROM {$this->montessori_db}.content_fb ORDER BY id DESC LIMIT {$next_offset}, {$limit}");

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/content/fb?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/content/fb?offset='.$next_offset);
        }

        $data['results'] = $results;
        return View::make('content.content_fb', $data);
    }

    public function fb_save()
    {
        $telco_id = Input::get('telco_id');     
        $user_type = Input::get('user_type');
        $content_type = Input::get('content_type');
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        DB::insert("INSERT INTO {$this->montessori_db}.content_fb (telco_id,user_type,content_type,text,sequence,dtnext_push)
        VALUES('{$telco_id}', '{$user_type}', '{$content_type}', '{$text}', '{$sequence}', '{$dtnext_push}')");

        return Redirect::to('/content/fb');
    }

    public function fb_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $result = DB::select("SELECT * FROM {$this->montessori_db}.content_fb WHERE id={$id} LIMIT 1");

        $data['result'] = $result[0];

        return View::make('content.content_fb_edit', $data);
    }

    public function fb_update($id)
    {
        $telco_id = Input::get('telco_id');    
        $user_type = Input::get('user_type');
        $content_type = Input::get('content_type');
        $text = Input::get('text');
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');

        DB::update("UPDATE {$this->montessori_db}.content_fb SET telco_id={$telco_id}, user_type={$user_type}, content_type={$content_type},
                    text='{$text}', dtnext_push='{$dtnext_push}', sequence={$sequence} WHERE id={$id}");

        return Redirect::to('/content/fb');

    }

    public function fb_del($id)
    {
        DB::insert("UPDATE {$this->montessori_db}.content_fb SET n_status = 0 WHERE id={$id}");

        return Redirect::to('/content/fb');
    }

    public function fb_enable($id)
    {
        DB::insert("UPDATE {$this->montessori_db}.content_fb SET n_status = 1 WHERE id={$id}");

        return Redirect::to('/content/fb');
    }

    public function questionnaire()
    {
        $data = $this->constructMenu();
        $quest = DB::connection($this->umb_app_db)->select("SELECT * FROM `questionnaire` WHERE n_status = 1 LIMIT 1000");

        return View::make('content.questionnaire', $data);
    }

    public function faq()
    {
        $limit = 50;
        $offset = abs(Input::get('offset'));
        $data = $this->constructMenu();

        $faq_conversations = FaqConversation::with(array('faq' => function($query) {
            $query->where('user_id', '!=', 1)->orderBy('id', 'DESC');
        }, 'user'))->where('user_id', '!=', 1)->orderBy('updated_at', 'DESC')->take($limit)->skip($offset)->get();

        $next_offset = $offset+$limit;

        $next_results = FaqConversation::with(array('faq' => function($query) {
            $query->where('user_id', '!=', 1)->orderBy('id', 'DESC');
        }, 'user'))->where('user_id', '!=', 1)->orderBy('updated_at', 'DESC')->take($limit)->skip($next_offset)->get();

        $data['results'] = $faq_conversations;

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/subscriber/faq?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/subscriber/faq?offset='.$next_offset);
        }

        return View::make('content.faq', $data);
    }

    public function remove_faq($id)
    {
        FaqConversation::where('id', $id)->delete();
        Faq::where('faq_conversation_id', $id)->delete();
        
        return Redirect::to('/subscriber/faq');
    }

    public function faq_reply($id)
    {
        $data = $this->constructMenu();

        $faq_conversations = FaqConversation::with(array('faq' => function($query) {
            $query->orderBy('id', 'DESC');
        }, 'user'))->find($id);

        $data['user'] = $faq_conversations->user;
        $data['faqs'] = array_reverse($faq_conversations->faq->toArray());
        $data['results'] = $faq_conversations;

        return View::make('content.faq-reply', $data);
    }

    public function faq_reply_act($id)
    {
        $data = $this->constructMenu();
        $reply = Input::get('reply');

        $faq_conversation = FaqConversation::find($id);
        $faq_conversation->updated_at = date('Y-m-d H:i:s');
        $faq_conversation->save();

        $last_faq = Faq::where('faq_conversation_id', $id)->where('user_id' , '!=', 1)
                    ->orderBy('id', 'DESC')->first();

        $update_faq = Faq::find($last_faq->id);
        $update_faq->n_status = 1;
        $update_faq->save();

        $faq = new Faq;
        $faq->faq_conversation_id = $id;
        $faq->user_id = 1;
        $faq->to_user_id = $faq_conversation->user_id;
        $faq->faq = trim($reply);
        $faq->save();

        $faq_conversations = FaqConversation::with(array('faq' => function($query) {
            $query->orderBy('id', 'DESC');
        }, 'user'))->find($id);

        $data['user'] = $faq_conversations->user;
        $data['faqs'] = array_reverse($faq_conversations->faq->toArray());
        $data['results'] = $faq_conversations;
        $data['end_faq'] = end($data['faqs']);

        return View::make('content.faq-reply', $data);
    }

    /** Broadcast Push Notification KEPO **/
    public function pushKepo()
    {
        $limit = 10;
        $offset = abs(Input::get('offset'));

        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $results = DB::connection('monaco')->select("SELECT id,IF(push_type=0,'Push Notification','Announcement') as push_type,IF(app_type=0,'KEPO','iLIVE') as app_type,title,message,image,dtsubmitted,dtscheduled,n_status FROM content_pushnotif ORDER BY id ASC LIMIT {$offset}, {$limit}");
        $next_offset = $offset+$limit;

        $next_results = DB::connection('monaco')->select("SELECT id,IF(push_type=0,'Push Notification','Announcement') as push_type,IF(app_type=0,'KEPO','iLIVE') as app_type,title,message,image,dtsubmitted,dtscheduled,n_status FROM content_pushnotif ORDER BY id ASC LIMIT {$offset}, {$limit}");

        $data['prev_page'] = '';
        if($offset > 0)
        {
            $data['prev_page'] = url('/content/pushKepo?offset='.abs($limit-$offset));
        }
        $data['next_page'] = '';
        if(count($next_results) > 0)
        {
            $data['next_page'] = url('/content/pushKepo?offset='.$next_offset);
        }

        $data['results'] = $results;
        return View::make('content.content_pushKepo', $data);
    }

    public function pushKepo_save()
    {
        $push_type = Input::get('push_type');
        $app_type = Input::get('app_type');
        $title = Input::get('title');
        $message = Input::get('message');
        $image = Input::get('image');
        $dtsubmitted = Input::get('dtsubmitted');
        $dtscheduled = Input::get('dtscheduled');
        $content_queue = Input::get('content_queue');

        /*
        if($push_type == 0)
        {
            DB::connection('monaco')->insert("INSERT INTO content_pushnotif (push_type,title,message,image,dtsubmitted,dtscheduled,content_queue)
            VALUES('0', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}','{$content_queue}')");
            DB::connection('monaco')->insert("INSERT INTO push_notification (push_type,bc_type,fcm_id,title,message,image,dtsubmitted,dtscheduled)
            VALUES('0','1', '/topics/allDevices', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}')");
        } else {
            DB::connection('monaco')->insert("INSERT INTO content_pushnotif (push_type,title,message,image,dtsubmitted,dtscheduled,content_queue)
            VALUES('1', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}','{$content_queue}')");
        }
        */

        if($push_type == 0) {
            if($app_type == 0) {
                DB::connection('monaco')->insert("INSERT INTO content_pushnotif (push_type,app_type,title,message,image,dtsubmitted,dtscheduled,content_queue)
                VALUES('0', '0', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}','{$content_queue}')");
                DB::connection('monaco')->insert("INSERT INTO push_notification (push_type,bc_type,fcm_id,title,message,image,dtsubmitted,dtscheduled)
                VALUES('0','1', '/topics/allDevices', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}')");
            } else {
                DB::connection('monaco')->insert("INSERT INTO content_pushnotif (push_type,app_type,title,message,image,dtsubmitted,dtscheduled,content_queue)
                VALUES('0', '1', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}','{$content_queue}')");
                DB::connection('monaco')->insert("INSERT INTO push_notification (push_type,bc_type,fcm_id,title,message,image,dtsubmitted,dtscheduled)
                VALUES('0','1', '/topics/allDevicesILIVE', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}')");
            }
        } else {
            if($app_type == 0) {
                DB::connection('monaco')->insert("INSERT INTO content_pushnotif (push_type,app_type,title,message,image,dtsubmitted,dtscheduled,content_queue)
                VALUES('1', '0', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}','{$content_queue}')");
            } else {
                DB::connection('monaco')->insert("INSERT INTO content_pushnotif (push_type,app_type,title,message,image,dtsubmitted,dtscheduled,content_queue)
                VALUES('1', '1', '{$title}', '{$message}', '{$image}', NOW(), '{$dtscheduled}','{$content_queue}')");
            }
        }

           

        return Redirect::to('/content/pushKepo');
    }

    public function pushKepo_edit($id)
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        $result = DB::connection('monaco')->select("SELECT * FROM content_pushnotif WHERE id={$id} LIMIT 1");

        $data['result'] = $result[0];

        return View::make('content.content_pushKepo_edit', $data);
    }

    public function pushKepo_update($id)
    {
        $push_type = Input::get('push_type');
        $app_type = Input::get('app_type');
        $bc_type = Input::get('bc_type');    
        $fcm_id = Input::get('fcm_id');
        $title = Input::get('title');
        $message = Input::get('message');
        $image = Input::get('image');
        $dtscheduled = Input::get('dtscheduled');

        /*
        if($push_type == 0)
        {
            DB::connection('monaco')->update("UPDATE content_pushnotif SET push_type='0', title='{$title}',
                        message='{$message}', image='{$image}', dtscheduled='{$dtscheduled}' WHERE id={$id}");
        } else {
            DB::connection('monaco')->update("UPDATE content_pushnotif SET push_type='1', title='{$title}',
                        message='{$message}', image='{$image}', dtscheduled='{$dtscheduled}' WHERE id={$id}");
        }
        */

        if($push_type == 0) {
            if($app_type == 0) {
                DB::connection('monaco')->update("UPDATE content_pushnotif SET push_type='0', app_type='0', title='{$title}',
                            message='{$message}', image='{$image}', dtscheduled='{$dtscheduled}' WHERE id={$id}");
            } else {
                DB::connection('monaco')->update("UPDATE content_pushnotif SET push_type='0', app_type='1', title='{$title}',
                            message='{$message}', image='{$image}', dtscheduled='{$dtscheduled}' WHERE id={$id}");
            }
        } else {
            if($app_type == 0) {
                DB::connection('monaco')->update("UPDATE content_pushnotif SET push_type='1', app_type='0', title='{$title}',
                            message='{$message}', image='{$image}', dtscheduled='{$dtscheduled}' WHERE id={$id}");
            } else {
                DB::connection('monaco')->update("UPDATE content_pushnotif SET push_type='1', app_type='1', title='{$title}',
                            message='{$message}', image='{$image}', dtscheduled='{$dtscheduled}' WHERE id={$id}");
            }
        }


        return Redirect::to('/content/pushKepo');

    }

    public function pushKepo_del($id)
    {
        DB::connection('monaco')->update("UPDATE content_pushnotif SET n_status = 2 WHERE id={$id}");

        return Redirect::to('/content/pushKepo');
    }

    public function pushKepo_disable($id)
    {
        DB::connection('monaco')->update("UPDATE content_pushnotif SET n_status = 0 WHERE id={$id}");

        return Redirect::to('/content/pushKepo');
    }

    public function pushKepo_enable($id)
    {
        DB::connection('monaco')->update("UPDATE content_pushnotif SET n_status = 1 WHERE id={$id}");

        return Redirect::to('/content/pushKepo');
    }


    //SDC Content JODOH - iLive
    public function ilive_isat()
    {
        $data = $this->constructMenu();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_ilive_isat ORDER BY content_type ASC, `text` ASC");

        $data['results'] = $results;

        return View::make('content.ilive_isat', $data);
    }

    public function ilive_isat_add()
    {
        $data = $this->constructMenu();
        $data['shio'] = Shio::getShio();

        return View::make('content.ilive_isat_add', $data);
    }

    public function ilive_isat_add_action()
    {
        if ((empty(Input::get('text'))) || 
            (empty(Input::get('sequence'))) ||
            (empty(Input::get('content_type'))))
        {
            return Redirect::to('/content/ilive_isat/add')->with('err', 'Cannot empty.');
        }
        else
        {
            $tgl = empty(Input::get('dtnext_push')) ?  "NULL": "'".Input::get('dtnext_push')."'";
            //mengikuti struktur table, karena bukan eloquent, gunakan raw SQL
            if (Input::get('content_type') == 2)
            {
                $sql = "INSERT INTO {$this->montessori_db}.content_ilive_isat(`content_type`,`text`,`zodiac`,`sequence`,`dtnext_push`)
                    VALUES('".Input::get('content_type')."','".addslashes(Input::get('text'))."',LOWER('".Input::get('zodiac')."'),'".Input::get('sequence')."',".$tgl.")";
            }
            elseif (Input::get('content_type') == 3)
            {
                $sql = "INSERT INTO {$this->montessori_db}.content_ilive_isat(`content_type`,`text`,`url_image`,`sequence`,`dtnext_push`)
                    VALUES('".Input::get('content_type')."','".addslashes(Input::get('text'))."','".Input::get('url_image')."','".Input::get('sequence')."',".$tgl.")";
            }
            else
            {
                $sql = "INSERT INTO {$this->montessori_db}.content_ilive_isat(`content_type`,`text`,`sequence`,`dtnext_push`)
                    VALUES('".Input::get('content_type')."','".addslashes(Input::get('text'))."','".Input::get('sequence')."',".$tgl.")";
            }
            DB::insert($sql);
        
            return Redirect::to('/content/ilive_isat')->with('success', 'Data added [' . Input::get('text') . '].');
        }
    }

    public function ilive_isat_edit($id)
    {
        $data = $this->constructMenu();
        $data['shio'] = Shio::getShio();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_ilive_isat WHERE id = {$id}
                                LIMIT 1");
        $data['results'] = $results[0];
        $data['id'] = $id;
        return View::make('content.ilive_isat_edit', $data);
    }

    public function ilive_isat_edit_action($id)
    {
        $content_type = Input::get('content_type');
        $text = addslashes(Input::get('text'));
        $zodiac = addslashes(Input::get('zodiac'));
        $url_image = addslashes(Input::get('url_image'));
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');
        $id = Input::get('id');

        if ((Input::get('dtnext_push') == '0000-00-00') || (Input::get('dtnext_push') == '') )
        {
            $tgl = ",dtnext_push=NULL";
        }
        else
        {
            $tgl = ",dtnext_push='".Input::get('dtnext_push')."'";
        }
        
        if (Input::get('content_type') == 2)
        {
            $sql = "UPDATE {$this->montessori_db}.content_ilive_isat SET content_type={$content_type}, 
                        `text`='{$text}', zodiac=LOWER('{$zodiac}'), `sequence`='{$sequence}' $tgl
                        WHERE id={$id}";
        }
        elseif (Input::get('content_type') == 3)
        {
            $sql = "UPDATE {$this->montessori_db}.content_ilive_isat SET content_type={$content_type}, 
                        `text`='{$text}', url_image='{$url_image}', `sequence`='{$sequence}' $tgl
                        WHERE id={$id}";
         }
        else
        {
            $sql = "UPDATE {$this->montessori_db}.content_ilive_isat SET content_type={$content_type}, 
                        `text`='{$text}', `sequence`='{$sequence}' $tgl
                        WHERE id={$id}";
         }
        DB::update($sql);

        return Redirect::to('/content/ilive_isat')->with('success', 'Data updated [ID=' . $id . ', Type='.$content_type.', '.$text.'].');
    }

    public function ilive_isat_disable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_ilive_isat SET n_status=0 WHERE id={$id}");

        return Redirect::to('/content/ilive_isat')->with('success', 'Data disabled [ID=' . $id . '] success.');
    }

    public function ilive_isat_enable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_ilive_isat SET n_status=1 WHERE id={$id}");

        return Redirect::to('/content/ilive_isat')->with('success', 'Data enable [ID=' . $id . '] success.');
    }

    //SDC Content LIVE XL - iLive
    public function ilive_xl()
    {
        $data = $this->constructMenu();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_ilive_xl ORDER BY content_type ASC, `text` ASC");

        $data['results'] = $results;

        return View::make('content.ilive_xl', $data);
    }

    public function ilive_xl_add()
    {
        $data = $this->constructMenu();
        $data['shio'] = Shio::getShio();

        return View::make('content.ilive_xl_add', $data);
    }

    public function ilive_xl_add_action()
    {
        if ((empty(Input::get('text'))) || 
            (empty(Input::get('sequence'))) ||
            (empty(Input::get('content_type'))))
        {
            return Redirect::to('/content/ilive_xl/add')->with('err', 'Cannot empty.');
        }
        else
        {
            $tgl = empty(Input::get('dtnext_push')) ?  "NULL": "'".Input::get('dtnext_push')."'";
            //mengikuti struktur table, karena bukan eloquent, gunakan raw SQL
            if (Input::get('content_type') == 2)
            {
                $sql = "INSERT INTO {$this->montessori_db}.content_ilive_xl(`content_type`,`text`,`zodiac`,`sequence`,`dtnext_push`)
                    VALUES('".Input::get('content_type')."','".addslashes(Input::get('text'))."',LOWER('".Input::get('zodiac')."'),'".Input::get('sequence')."',".$tgl.")";
            }
            elseif (Input::get('content_type') == 3)
            {
                $sql = "INSERT INTO {$this->montessori_db}.content_ilive_xl(`content_type`,`text`,`url_image`,`sequence`,`dtnext_push`)
                    VALUES('".Input::get('content_type')."','".addslashes(Input::get('text'))."','".Input::get('url_image')."','".Input::get('sequence')."',".$tgl.")";
            }
            else
            {
                $sql = "INSERT INTO {$this->montessori_db}.content_ilive_xl(`content_type`,`text`,`sequence`,`dtnext_push`)
                    VALUES('".Input::get('content_type')."','".addslashes(Input::get('text'))."','".Input::get('sequence')."',".$tgl.")";
            }
            DB::insert($sql);
        
            return Redirect::to('/content/ilive_xl')->with('success', 'Data added [' . Input::get('text') . '].');
        }
    }

    public function ilive_xl_edit($id)
    {
        $data = $this->constructMenu();
        $data['shio'] = Shio::getShio();

        $results = DB::select("SELECT * FROM {$this->montessori_db}.content_ilive_xl WHERE id = {$id}
                                LIMIT 1");
        $data['results'] = $results[0];
        $data['id'] = $id;
        return View::make('content.ilive_xl_edit', $data);
    }

    public function ilive_xl_edit_action($id)
    {
        $content_type = Input::get('content_type');
        $text = addslashes(Input::get('text'));
        $zodiac = addslashes(Input::get('zodiac'));
        $url_image = addslashes(Input::get('url_image'));
        $dtnext_push = Input::get('dtnext_push');
        $sequence = Input::get('sequence');
        $id = Input::get('id');

        if ((Input::get('dtnext_push') == '0000-00-00') || (Input::get('dtnext_push') == '') )
        {
            $tgl = ",dtnext_push=NULL";
        }
        else
        {
            $tgl = ",dtnext_push='".Input::get('dtnext_push')."'";
        }
        
        if (Input::get('content_type') == 2)
        {
            $sql = "UPDATE {$this->montessori_db}.content_ilive_xl SET content_type={$content_type}, 
                        `text`='{$text}', zodiac=LOWER('{$zodiac}'), `sequence`='{$sequence}' $tgl
                        WHERE id={$id}";
        }
        elseif (Input::get('content_type') == 3)
        {
            $sql = "UPDATE {$this->montessori_db}.content_ilive_xl SET content_type={$content_type}, 
                        `text`='{$text}', url_image='{$url_image}', `sequence`='{$sequence}' $tgl
                        WHERE id={$id}";
         }
        else
        {
            $sql = "UPDATE {$this->montessori_db}.content_ilive_xl SET content_type={$content_type}, 
                        `text`='{$text}', `sequence`='{$sequence}' $tgl
                        WHERE id={$id}";
         }
        DB::update($sql);

        return Redirect::to('/content/ilive_xl')->with('success', 'Data updated [ID=' . $id . ', Type='.$content_type.', '.$text.'].');
    }

    public function ilive_xl_disable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_ilive_xl SET n_status=0 WHERE id={$id}");

        return Redirect::to('/content/ilive_xl')->with('success', 'Data disabled [ID=' . $id . '] success.');
    }

    public function ilive_xl_enable($id)
    {
        DB::update("UPDATE {$this->montessori_db}.content_ilive_xl SET n_status=1 WHERE id={$id}");

        return Redirect::to('/content/ilive_xl')->with('success', 'Data enable [ID=' . $id . '] success.');
    }

    //OK
    public function tebakskor()
    {
        $data = $this->constructMenu();
        $results = DB::connection($this->umb_app_db)
                        ->select("SELECT * FROM tebak_skor 
                                ORDER BY match_date DESC, gameid ASC 
                                LIMIT 0, 100");
        
        $data['results'] = $results;

        return View::make('content.tebakskor', $data);
    }

    //OK
    public function tebakskor_add()
    {
        $data = $this->constructMenu();
        return View::make('content.tebakskor_add', $data);
    }

    //OK
    public function tebakskor_add_save()
    {
        if ((empty(Input::get('club1'))) || 
            (empty(Input::get('club2'))) ||
            (empty(Input::get('match_date'))))
        {
            return Redirect::to('/content/tebakskor/add')->with('err', 'Cannot empty.');
        }
        else
        {
            $skor = [
                'club' => Input::get('club1') . '|' . Input::get('club2'),
                'match_date' => Input::get('match_date'),
            ];
    
            TebakSkor::create($skor);
            return Redirect::to('/content/tebakskor')->with('success', 
                'Match ' . Input::get('club1') . ' vs ' . Input::get('club2') . ' has been saved.');
        }
       
    }

    //OK
    public function tebakskor_edit($id)
    {
        $data = $this->constructMenu();
        $skor = TebakSkor::find($id);
        $hsl= empty($skor->score)?'-|-':$skor->score;
        $data['tebakskor'] = $skor;
        $data['club'] = explode('|',$skor->club);
        $data['score'] = explode('|',$hsl);
        $data['id'] = $id;

        return View::make('content.tebakskor_edit', $data);
    }

    //OK
    public function tebakskor_edit_save($id)
    {
        if ((empty(Input::get('club1'))) || 
            (empty(Input::get('club2'))) ||
            (empty(Input::get('match_date'))))
        {
            return Redirect::to('/content/tebakskor_edit/'.$id)->with('err', 'Cannot empty.');
        }
        else
        {
            $tebakskor = TebakSkor::find($id);
            $tebakskor->club = Input::get('club1') . '|' . Input::get('club2');
            $tebakskor->score = (Input::get('score1')) . '|' . (Input::get('score2'));
            $tebakskor->match_date = Input::get('match_date');
            $tebakskor->save();

            return Redirect::to('/content/tebakskor')->with('success', 
                'Update match ' . Input::get('club1') . ' vs ' . Input::get('club2') . ' '.
                '[' . Input::get('score1') . '-' . Input::get('score1') . '] has been saved.');
        }
    }

    //OK
    public function tebakskor_prepare($id)
    {
        $data = $this->constructMenu();
        $partisipan = TebakSkorAnswer::where('gameid', $id)->count();

        $skor = TebakSkor::find($id);
        $hsl= empty($skor->score)?'-|-':$skor->score;
        $data['tebakskor'] = $skor;
        $data['club'] = explode('|',$skor->club);
        $data['score'] = explode('|',$hsl);
        $data['partisipan'] = $partisipan;
        $data['id'] = $id;

        return View::make('content.tebakskor_process', $data);
    }

    //OK
    public function tebakskor_process($id)
    {
        $time_start = microtime(true);
        $data = $this->constructMenu();

        $coin = Config::get('app.coin');

        //$coin->tebakskor;
        //Set klub pemenang di table tebak_skor
        DB::connection($this->umb_app_db)
        ->statement(DB::raw("UPDATE tebak_skor SET 
                            winner_club_idx=IF(SUBSTRING_INDEX(score,'|',1)=SUBSTRING_INDEX(score,'|',-1) ,0,
                                        IF(SUBSTRING_INDEX(score,'|',1)>SUBSTRING_INDEX(score,'|',-1),1,2))
                                       WHERE gameid=".$id." AND winner_club_idx=-1"));

        //Memastikan isi field di table tebak_skor_answer.club_guest_idx <> -1
        DB::connection($this->umb_app_db)
            ->statement(DB::raw("UPDATE tebak_skor_answer SET 
                                club_guest_idx=IF(SUBSTRING_INDEX(score_guest,'|',1)=SUBSTRING_INDEX(score_guest,'|',-1) ,0,
                                            IF(SUBSTRING_INDEX(score_guest,'|',1)>SUBSTRING_INDEX(score_guest,'|',-1),1,2))
                                           WHERE gameid=".$id." AND club_guest_idx=-1"));

        //proses query matching
        //Default KOIN=0
        //Populate untuk KOIN 10 (hanya tebakan klub yang benar)
        DB::connection($this->umb_app_db)
            ->statement('UPDATE tebak_skor_answer
                        INNER JOIN tebak_skor ON 
                            tebak_skor_answer.gameid=tebak_skor.gameid AND 
                            tebak_skor_answer.score_guest<>tebak_skor.score AND 
                            tebak_skor_answer.club_guest_idx=tebak_skor.winner_club_idx AND 
                            tebak_skor.gameid='.$id.'
                        SET tebak_skor_answer.winner=1,tebak_skor_answer.koin_rewards='.$coin['tebakklub']);

        //Populate untuk KOIN 20 (tebakan skor & klub benar)
        DB::connection($this->umb_app_db)
            ->statement('UPDATE tebak_skor_answer
                        INNER JOIN tebak_skor ON 
                            tebak_skor_answer.gameid=tebak_skor.gameid AND 
                            tebak_skor_answer.score_guest=tebak_skor.score AND 
                            tebak_skor_answer.club_guest_idx=tebak_skor.winner_club_idx AND 
                            tebak_skor.gameid='.$id.'
                        SET tebak_skor_answer.winner=1,tebak_skor_answer.koin_rewards='.$coin['tebakskor']);

        $tepat = TebakSkorAnswer::where('gameid', $id)
                ->where('koin_rewards',$coin['tebakskor'])
                ->count();
        $sedang = TebakSkorAnswer::where('gameid', $id)
                ->where('koin_rewards',$coin['tebakklub'])
                ->count();
        $salah = TebakSkorAnswer::where('gameid', $id)
                ->where('koin_rewards',0)
                ->count();

        $skor = TebakSkor::find($id);
        $skor->processed = 1;
        $skor->save();


        $partisipan = TebakSkorAnswer::where('gameid', $id)->count();

        $skor = TebakSkor::find($id);
        $hsl= empty($skor->score)?'-|-':$skor->score;
        
        $data['tebakskor'] = $skor;
        $data['tepat'] = $tepat;
        $data['sedang'] = $sedang;
        $data['salah'] = $salah;
        $data['partisipan'] = $partisipan;
        $data['club'] = explode('|',$skor->club);
        $data['score'] = explode('|',$hsl);
        $data['id'] = $id;

        $time_end = microtime(true);
        $data['time'] = $time_end - $time_start;
        return View::make('content.tebakskor_result', $data);
    }

    //OK
    public function tebakskor_result($id)
    {
        $time_start = microtime(true);
        $data = $this->constructMenu();

        $coin = Config::get('app.coin');

        $tepat = TebakSkorAnswer::where('gameid', $id)
                ->where('koin_rewards',$coin['tebakskor'])
                ->count();
        $sedang = TebakSkorAnswer::where('gameid', $id)
                ->where('koin_rewards',$coin['tebakklub'])
                ->count();
        $salah = TebakSkorAnswer::where('gameid', $id)
                    ->where('koin_rewards',0)
                    ->count();


        $partisipan = TebakSkorAnswer::where('gameid', $id)->count();

        $skor = TebakSkor::find($id);
        $hsl= empty($skor->score)?'-|-':$skor->score;
        $data['tebakskor'] = $skor;
        $data['tepat'] = $tepat;
        $data['sedang'] = $sedang;
        $data['salah'] = $salah;
        $data['partisipan'] = $partisipan;
        $data['club'] = explode('|',$skor->club);
        $data['score'] = explode('|',$hsl);
        $data['id'] = $id;

        $time_end = microtime(true);
        $data['time'] = $time_end - $time_start;
        return View::make('content.tebakskor_result', $data);
    }

    //OK
    public function tebakskor_coin($id)
    {
        $data = $this->constructMenu();
        $partisipan = TebakSkorAnswer::where('gameid', $id)->count();

        $skor = TebakSkor::find($id);
        $hsl= empty($skor->score)?'-|-':$skor->score;
        $data['tebakskor'] = $skor;
        $data['club'] = explode('|',$skor->club);
        $data['score'] = explode('|',$hsl);
        $data['partisipan'] = $partisipan;
        $data['id'] = $id;

        return View::make('content.tebakskor_coin', $data);
    }

    //PENDING
    public function tebakskor_coin_process($id)
    {
        //ToDo: Kirim Coin ke MS
        //Waiting: API ke Microservice

        //ToDo: Update DB, SET expired=1
        //$skor = TebakSkor::find($id);
        //$club = explode('|',$skor->club);

        //$skor->expired = 1;
        //$skor->save();

        //return Redirect::to('/content/tebakskor')->with('success', 
        //    'Coin Sent! match <strong>' . $club[0] . ' vs ' . $club[1] . '</strong> has been saved.');
        return Redirect::to('/content/tebakskor')->with('err', 
            'Belum bisa kirim COIN ke Microservice, menunggu API key.');
    }

    //OK
    public function tebakskor_displayed($id)
    {
        $tebakskor = TebakSkor::find($id);
        $club = explode('|',$tebakskor->club);
        $tebakskor->displayed = ($tebakskor->displayed == 1) ? 0:1;
        $tebakskor->save();
        return Redirect::to('/content/tebakskor')->with('success', 
            'Update status, match <strong>' . $club[0] . ' vs ' . $club[1] . '</strong> has been saved.');
    }


    //Badwords Tools
    public function badword()
    {
        $data = $this->constructMenu();
        $data['results'] = DB::connection('umb-app')
                ->table('bad_words')
                ->select('id','keyword','regex_filter','exact')
                ->orderBy('keyword', 'asc')
                ->paginate(20);
        return View::make('content.badword', $data);
    }

    public function badword_search()
    {
        $data = $this->constructMenu();
        //$d='';
        //empty($req->qs) ? $d = '' : $d = $req->qs;
        //Log::critical($req->q);
        $data['results'] = DB::connection('umb-app')
                ->table('bad_words')
                ->select('id','keyword','regex_filter','exact')
                ->where('keyword','like','%'.Input::get('qs').'%')
                ->orderBy('keyword', 'asc')
                ->paginate(20);
        return View::make('content.badword', $data);
    }

    //Todo
    public function badword_generate($id)
    {
        $ref = Input::get('ref');

        $data = Badword::find($id);
        $keyword = $data->keyword;
        $data->regex_filter = $this->generate_regex($keyword,$data->exact);
        $data->save();

        return Redirect::to($ref)->with('success', 
            'Generate badword <strong>' . $keyword . '</strong> done.');
    }

    //Todo
    public function badword_add()
    {
        $data = $this->constructMenu();
        return View::make('content.badword_add', $data);
    }

    //Todo
    public function badword_add_save()
    {
        if (empty(Input::get('keyword')))
        {
            return Redirect::to('/content/badword/add')->with('err', 'Cannot empty.');
        }
        else
        {
            if (Input::get('exact',false))
            {
                //Cheklist nya aktif
                $tepat = 1;
            }
            else
            {
                $tepat = 0;
            }

            $regexresult = $this->generate_regex(Input::get('keyword'),$tepat);
            $data = [
                'keyword' => Input::get('keyword'),
                'exact' => $tepat,
                'regex_filter' => $regexresult,
            ];

            
            Badword::create($data);
            $this->regex_to_file();
            
            return Redirect::to('/content/badword')->with('success', 
                'Badword [' . Input::get('keyword') . '] has been saved.');
        }
       
    }

    //Todo
    public function badword_edit($id)
    {
        $data = $this->constructMenu();
        $bad = Badword::find($id);
        $data['keyword'] = $bad->keyword;
        $data['regex_filter'] = $bad->regex_filter;
        $data['exact'] = $bad->exact;
        $data['id'] = $bad->id;

        return View::make('content.badword_edit', $data);
    }

    public function badword_edit_save($id)
    {
        if (empty(Input::get('keyword')))
        {
            return Redirect::to('/content/badword_edit/'.$id)->with('err', 'Cannot empty.');
        }
        else
        {
            if (Input::get('exact',false))
            {
                //Cheklist nya aktif
                $tepat = 1;
                echo "value nya exact : " . Input::get('exact') . "<br>";
                $regexresult = $this->generate_regex(Input::get('keyword'),true);
                echo "di check bro..<br>";
            }
            else
            {
                //tidak di cjklist
                $tepat = 0;
                echo "value nya exact : " . Input::get('exact') . "<br>";
                $regexresult = $this->generate_regex(Input::get('keyword'),false);
                echo "kagak diapa-apain....<br>";
            }
            echo "keyword : " . Input::get('keyword') . "<br>";
            echo "hasil regext : " . $regexresult;
            
            
            $data = Badword::find($id);
            $data->keyword = Input::get('keyword');
            $data->exact = $tepat;
            $data->regex_filter = $regexresult;
            $data->save();

            //Todo: put regex to last entry in files
            $this->regex_to_file();

            return Redirect::to('/content/badword')->with('success', 
                'Update badword [' . Input::get('keyword') . '] has been saved.');
        }
    }

    //Todo
    public function badword_delete($id)
    {
        $data = Badword::find($id);
        $yangdihapus = $data->keyword;

        $data->delete();

        //Generate to file
        $this->regex_to_file();

        return Redirect::to('/content/badword')->with('success', 
            'Badword <strong>' . $yangdihapus . '</strong> has been deleted.');
    }

    //generate_regex('kata-katanya',1/0)
    //generate_regex('kata-katanya',[tetap huruf]/[sesuai yang lama])
    private function generate_regex($input,$keyword_exact)
    {
        $input = strtolower($input);

        $corpus_first = [
            'a' => '(([a|4|\-|\_|\.|\,]{1,})',
            'b' => '(([b|6|8|\-|\_|\.|\,]{1,})',
            'c' => '(([c|\-|\_|\.|\,]{1,})',
            'd' => '(([d|\-|\_|\.|\,]{1,})',
            'e' => '(([e|3|\-|\_|\.|\,]{1,})',
            'f' => '(([f|p|\-|\_|\.|\,]{1,})',
            'g' => '(([g|6|9|\-|\_|\.|\,]{1,})',
            'h' => '(([h|\-|\_|\.|\,]{1,})',
            'i' => '(([i|l|1|\-|\_|\.|\,]{1,})',
            'j' => '(([j|7|\-|\_|\.|\,]{1,})',
            'k' => '(([k|\-|\_|\.|\,]{1,})',
            'l' => '(([l|\-|\_|\.|\,]{1,})',
            'm' => '(([m|\-|\_|\.|\,]{1,})',
            'n' => '(([n|\-|\_|\.|\,]{1,})',
            'o' => '(([o|0|\-|\_|\.|\,]{1,})',
            'p' => '(([p|f|\-|\_|\.|\,]{1,})',
            'q' => '(([q|\-|\_|\.|\,]{1,})',
            'r' => '(([r|2|\-|\_|\.|\,]{1,})',
            's' => '(([s|5|\-|\_|\.|\,]{1,})',
            't' => '(([t|\-|\_|\.|\,]{1,})',
            'u' => '(([u|\-|\_|\.|\,]{1,})',
            'v' => '(([v|\-|\_|\.|\,]{1,})',
            'w' => '(([w|\-|\_|\.|\,]{1,})',
            'x' => '(([x|\-|\_|\.|\,]{1,})',
            'y' => '(([y|\-|\_|\.|\,]{1,})',
            'z' => '(([z|\-|\_|\.|\,]{1,})',
            '1' => '(([1|\-|\_|\.|\,]{1,})',
            '2' => '(([2|\-|\_|\.|\,]{1,})',
            '3' => '(([3|\-|\_|\.|\,]{1,})',
            '4' => '(([4|\-|\_|\.|\,]{1,})',
            '5' => '(([5|\-|\_|\.|\,]{1,})',
            '6' => '(([6|\-|\_|\.|\,]{1,})',
            '7' => '(([7|\-|\_|\.|\,]{1,})',
            '8' => '(([8|\-|\_|\.|\,]{1,})',
            '9' => '(([9|\-|\_|\.|\,]{1,})',
            '0' => '(([0|\-|\_|\.|\,]{1,})',
        ];

        $corpus_first_exact = [
            'a' => '((^[a|4|\-|\_|\.|\,]{1,})',
            'b' => '((^[b|6|8|\-|\_|\.|\,]{1,})',
            'c' => '((^[c|\-|\_|\.|\,]{1,})',
            'd' => '((^[d|\-|\_|\.|\,]{1,})',
            'e' => '((^[e|3|\-|\_|\.|\,]{1,})',
            'f' => '((^[f|p|\-|\_|\.|\,]{1,})',
            'g' => '((^[g|6|9|\-|\_|\.|\,]{1,})',
            'h' => '((^[h|\-|\_|\.|\,]{1,})',
            'i' => '((^[i|l|1|\-|\_|\.|\,]{1,})',
            'j' => '((^[j|7|\-|\_|\.|\,]{1,})',
            'k' => '((^[k|\-|\_|\.|\,]{1,})',
            'l' => '((^[l|\-|\_|\.|\,]{1,})',
            'm' => '((^[m|\-|\_|\.|\,]{1,})',
            'n' => '((^[n|\-|\_|\.|\,]{1,})',
            'o' => '((^[o|0|\-|\_|\.|\,]{1,})',
            'p' => '((^[p|f|\-|\_|\.|\,]{1,})',
            'q' => '((^[q|\-|\_|\.|\,]{1,})',
            'r' => '((^[r|2|\-|\_|\.|\,]{1,})',
            's' => '((^[s|5|\-|\_|\.|\,]{1,})',
            't' => '((^[t|\-|\_|\.|\,]{1,})',
            'u' => '((^[u|\-|\_|\.|\,]{1,})',
            'v' => '((^[v|\-|\_|\.|\,]{1,})',
            'w' => '((^[w|\-|\_|\.|\,]{1,})',
            'x' => '((^[x|\-|\_|\.|\,]{1,})',
            'y' => '((^[y|\-|\_|\.|\,]{1,})',
            'z' => '((^[z|\-|\_|\.|\,]{1,})',
            '1' => '((^[1|\-|\_|\.|\,]{1,})',
            '2' => '((^[2|\-|\_|\.|\,]{1,})',
            '3' => '((^[3|\-|\_|\.|\,]{1,})',
            '4' => '((^[4|\-|\_|\.|\,]{1,})',
            '5' => '((^[5|\-|\_|\.|\,]{1,})',
            '6' => '((^[6|\-|\_|\.|\,]{1,})',
            '7' => '((^[7|\-|\_|\.|\,]{1,})',
            '8' => '((^[8|\-|\_|\.|\,]{1,})',
            '9' => '((^[9|\-|\_|\.|\,]{1,})',
            '0' => '((^[0|\-|\_|\.|\,]{1,})',
        ];

        $corpus_last = [
            'a' => '([a|4]{1,}))',
            'b' => '([b|6|8]{1,}))',
            'c' => '([c]{1,}))',
            'd' => '([d]{1,}))',
            'e' => '([e]{1,}))',
            'f' => '([f|p]{1,}))',
            'g' => '([g|6|9]{1,}))',
            'h' => '([h]{1,}))',
            'i' => '([i|l|1]{1,}))',
            'j' => '([j|7]{1,}))',
            'k' => '([k]{1,}))',
            'l' => '([l]{1,}))',
            'm' => '([m]{1,}))',
            'n' => '([n]{1,}))',
            'o' => '([o|0]{1,}))',
            'p' => '([p|f]{1,}))',
            'q' => '([q]{1,}))',
            'r' => '([r|2]{1,}))',
            's' => '([s|5]{1,}))',
            't' => '([t]{1,}))',
            'u' => '([u]{1,}))',
            'v' => '([v]{1,}))',
            'w' => '([w]{1,}))',
            'x' => '([x]{1,}))',
            'y' => '([y]{1,}))',
            'z' => '([z]{1,}))',
            '1' => '([1]{1,}))',
            '2' => '([2]{1,}))',
            '3' => '([3]{1,}))',
            '4' => '([4]{1,}))',
            '5' => '([5]{1,}))',
            '6' => '([6]{1,}))',
            '7' => '([7]{1,}))',
            '8' => '([8]{1,}))',
            '9' => '([9]{1,}))',
            '0' => '([0]{1,}))',
        ];

        $corpus_last_exact = [
            'a' => '([a|4]{1,}))$',
            'b' => '([b|6|8]{1,}))$',
            'c' => '([c]{1,}))$',
            'd' => '([d]{1,}))$',
            'e' => '([e]{1,}))$',
            'f' => '([f|p]{1,}))$',
            'g' => '([g|6|9]{1,}))$',
            'h' => '([h]{1,}))$',
            'i' => '([i|l|1]{1,}))$',
            'j' => '([j|7]{1,}))$',
            'k' => '([k]{1,}))$',
            'l' => '([l]{1,}))$',
            'm' => '([m]{1,}))$',
            'n' => '([n]{1,}))$',
            'o' => '([o|0]{1,}))$',
            'p' => '([p|f]{1,}))$',
            'q' => '([q]{1,}))$',
            'r' => '([r|2]{1,}))$',
            's' => '([s|5]{1,}))$',
            't' => '([t]{1,}))$',
            'u' => '([u]{1,}))$',
            'v' => '([v]{1,}))$',
            'w' => '([w]{1,}))$',
            'x' => '([x]{1,}))$',
            'y' => '([y]{1,}))$',
            'z' => '([z]{1,}))$',
            '1' => '([1]{1,}))$',
            '2' => '([2]{1,}))$',
            '3' => '([3]{1,}))$',
            '4' => '([4]{1,}))$',
            '5' => '([5]{1,}))$',
            '6' => '([6]{1,}))$',
            '7' => '([7]{1,}))$',
            '8' => '([8]{1,}))$',
            '9' => '([9]{1,}))$',
            '0' => '([0]{1,}))$',
        ];

        $corpus = [
            'a' => '([a|4|\s|\-|\_|\.|\,]{1,})',
            'b' => '([b|6|8|\s|\-|\_|\.|\,]{1,})',
            'c' => '([c|\s|\-|\_|\.|\,]{1,})',
            'd' => '([d|\s|\-|\_|\.|\,]{1,})',
            'e' => '([e|3|\s|\-|\_|\.|\,]{1,})',
            'f' => '([f|p|\s|\-|\_|\.|\,]{1,})',
            'g' => '([g|6|9|\s|\-|\_|\.|\,]{1,})',
            'h' => '([h|\s|\-|\_|\.|\,]{1,})',
            'i' => '([i|l|1|\s|\-|\_|\.|\,]{1,})',
            'j' => '([j|7|\s|\-|\_|\.|\,]{1,})',
            'k' => '([k|\s|\-|\_|\.|\,]{1,})',
            'l' => '([l|\s|\-|\_|\.|\,]{1,})',
            'm' => '([m|\s|\-|\_|\.|\,]{1,})',
            'n' => '([n|\s|\-|\_|\.|\,]{1,})',
            'o' => '([o|0|\s|\-|\_|\.|\,]{1,})',
            'p' => '([p|f|\s|\-|\_|\.|\,]{1,})',
            'q' => '([q|\s|\-|\_|\.|\,]{1,})',
            'r' => '([r|2|\s|\-|\_|\.|\,]{1,})',
            's' => '([s|5|\s|\-|\_|\.|\,]{1,})',
            't' => '([t|\s|\-|\_|\.|\,]{1,})',
            'u' => '([u|\s|\-|\_|\.|\,]{1,})',
            'v' => '([v|\s|\-|\_|\.|\,]{1,})',
            'w' => '([w|\s|\-|\_|\.|\,]{1,})',
            'x' => '([x|\s|\-|\_|\.|\,]{1,})',
            'y' => '([y|\s|\-|\_|\.|\,]{1,})',
            'z' => '([z|\s|\-|\_|\.|\,]{1,})',
            '1' => '([1|\s|\-|\_|\.|\,]{1,})',
            '2' => '([2|\s|\-|\_|\.|\,]{1,})',
            '3' => '([3|\s|\-|\_|\.|\,]{1,})',
            '4' => '([4|\s|\-|\_|\.|\,]{1,})',
            '5' => '([5|\s|\-|\_|\.|\,]{1,})',
            '6' => '([6|\s|\-|\_|\.|\,]{1,})',
            '7' => '([7|\s|\-|\_|\.|\,]{1,})',
            '8' => '([8|\s|\-|\_|\.|\,]{1,})',
            '9' => '([9|\s|\-|\_|\.|\,]{1,})',
            '0' => '([0|\s|\-|\_|\.|\,]{1,})',
        ];

        $result = '';

        //$imin = strlen($input) - 1;
        //($keyword_exact <= 0 ?  $result .= $corpus_first_exact[$input[$i]] : $result .= $corpus_first[$input[$i]]);

        /*
        for ($i=0; $i<strlen($input); $i++)
        {
            if ($i==0) {
                if ($input == $keyword_exact) { //$keyword_exact ngambil dari table bad_words_exact
                    $result .= $corpus_first_exact[$input[$i]];
                } else {    
                    $result .= $corpus_first[$input[$i]];
                }
            } elseif ($i == (strlen($input) - 1)) {
                $result .= $corpus_last[$input[$i]];
            } else {    
                $result .= $corpus[$input[$i]];
            }
        }
        */

        //Kondisi kata-kata fleksibel (sesuai perhitungan terdahulu):
        if ($keyword_exact <= 0)
        {
            for ($i=0; $i<strlen($input); $i++)
            {
                if ($i==0) {
                    $result .= $corpus_first[$input[$i]];
                } elseif ($i == (strlen($input) - 1)) {
                    $result .= $corpus_last[$input[$i]];
                } else {    
                    $result .= $corpus[$input[$i]];
                }
            }
        }
        else
        {
            //Kondisi kata-harus tepat
            for ($i=0; $i<strlen($input); $i++)
            {
                if ($i==0) {
                    $result .= $corpus_first_exact[$input[$i]];
                } elseif ($i == (strlen($input) - 1)) {
                    $result .= $corpus_last_exact[$input[$i]];
                } else {    
                    $result .= $corpus[$input[$i]];
                }
            }
        }
        
        return $result;
    }

    //todo: write regex file
    public function regex_to_file()
    {
        //Jangan lupa set permission +write
        $filei = "/home/app/public_html/umb_v2/isat/config/badwords.php";
        $fileip = "/home/app/public_html/umb_v2/isat_playmedia/config/badwords.php";
        $filet = "/home/app/public_html/umb_v2/tsel/config/badwords.php";
        $filex = "/home/app/public_html/umb_v2/xl/config/badwords.php";
        $filexp = "/home/app/public_html/umb_v2/xl_playmedia/config/badwords.php";
        $filej = "/home/app/public_html/umb-microservice/chat/tools/regexp.js";
        $filei = "/home/app/public_html/umb_v2/isat/config/badwords.php";
        $filepa = "/home/app/public_html/umb/umb-app/api/badwords.php";

        //Tes dilaptop
        //$filei = "/Users/yayan/work/kmi/target_write_files/ibadwords.php";
        //$filet = "/Users/yayan/work/kmi/target_write_files/tbadwords.php";
        //$filex = "/Users/yayan/work/kmi/target_write_files/xbadwords.php";
        //$filej = "/Users/yayan/work/kmi/target_write_files/regexp.js";

        //Todo: Write to files
        $content = "";
        $results = DB::connection('umb-app')
                ->table('bad_words')
                ->select('regex_filter')
                ->where('regex_filter','<>','')
                ->orderBy('id', 'asc')
                ->get();
        foreach ($results as $data)
        {
            $content .= $data->regex_filter . "|";
        }
        $content = rtrim($content,'|');
        
        if (File::exists($filei))
        {
            file_put_contents($filei, "<?php\nreturn \"/(".$content.")/i\";");
            file_put_contents($fileip, "<?php\nreturn \"/(".$content.")/i\";");
            file_put_contents($filet, "<?php\nreturn \"/(".$content.")/i\";");
            file_put_contents($filex, "<?php\nreturn \"/(".$content.")/i\";");
            file_put_contents($filexp, "<?php\nreturn \"/(".$content.")/i\";");
            file_put_contents($filej, "module.exports = /(".$content.")/gi;");
            file_put_contents($filepa, "<?php\n\$regex=\"/(".$content.")/i\";");
        }
        

        //Todo: execute shell script
        //shell_exec dibatalkan, karena maslaah security, diganti cronjob checksum_badword.sh
        //shell_exec('/home/app/tools/sync/sync_umb-app-isat-v2.sh');
        //shell_exec('/home/app/tools/sync/sync_umb-app-tsel-v2.sh');
        //shell_exec('/home/app/tools/sync/sync_umb-app-xl-v2.sh');

        //shell_exec('/home/app/tools/sync/sync_umb-microservice-chat.sh');
    }

    public function regex_to_file_btn()
    {
        $this->regex_to_file();

        return Redirect::to('/content/badword')->with('success', 
            'Badword <strong>Generate to Files</strong> done.');
    }

    public function generate_regex_db()
    {
        //Todo: looping, generate all data
        $bads = Badword::all();
        foreach ($bads as $data)
        {
            $updt = Badword::find($data->id);
            $updt->regex_filter = $this->generate_regex($data->keyword,$data->exact);
            $updt->save();
        }

        $this->regex_to_file();

        return Redirect::to('/content/badword')->with('success', 
            'Regenerate REGEX DB done.');
    }


    //Jodoh
    public function jodoh()
    {
        $data = $this->constructMenu();
        $data['results'] = DB::connection('umb-app')
                ->table('jodoh')
                ->select('id','text','n_status')
                ->orderBy('n_status', 'asc')
                ->orderBy('text', 'asc')
                ->paginate(25);
        return View::make('content.jodoh', $data);
    }

    public function jodoh_search()
    {
        $data = $this->constructMenu();
        $data['results'] = DB::connection('umb-app')
                ->table('jodoh')
                ->select('id','text','n_status')
                ->where('text','like','%'.Input::get('qs').'%')
                ->orderBy('n_status', 'asc')
                ->orderBy('text', 'asc')
                ->paginate(25);
        return View::make('content.jodoh', $data);
    }

    public function jodoh_add()
    {
        $data = $this->constructMenu();
        return View::make('content.jodoh_add', $data);
    }

    public function jodoh_add_save()
    {
        if (empty(Input::get('text')))
        {
            return Redirect::to('/content/jodoh/add')->with('err', 'Cannot empty.');
        }
        else
        {   
            $data = [
                'text' => Input::get('text'),
            ];

            Jodoh::create($data);
            
            return Redirect::to('/content/jodoh')->with('success', 
                'Jodoh [' . Input::get('text') . '] has been saved.');
        }
       
    }

    public function jodoh_enable($id)
    {
        $data = Jodoh::find($id);
        $data->n_status = 1;
        $data->save();

        return Redirect::to('/content/jodoh')->with('success', 
            'Enable jodoh [' . $data->text . '].');
    }

    public function jodoh_delete($id)
    {
        $data = Jodoh::find($id);
        $data->n_status = 2;
        $data->save();

        return Redirect::to('/content/jodoh')->with('success', 
            'Delete jodoh [' . $data->text . '].');
    }

    public function jodoh_edit($id)
    {
        $data = $this->constructMenu();
        $jodoh = Jodoh::find($id);
        $data['text'] = $jodoh->text;
        $data['id'] = $jodoh->id;

        return View::make('content.jodoh_edit', $data);
    }

    public function jodoh_edit_save($id)
    {
        if (empty(Input::get('text')))
        {
            return Redirect::to('/content/jodoh_edit/'.$id)->with('err', 'Cannot empty.');
        }
        else
        {
            $data = Jodoh::find($id);
            $data->text = Input::get('text');
            $data->save();

            return Redirect::to('/content/jodoh')->with('success', 
                'Update jodoh [' . Input::get('text') . '] has been saved.');
        }
    }



    //Jodoh Prediksi
    public function jodohprediksi()
    {
        $data = $this->constructMenu();
        $data['results'] = DB::connection('umb-app')
                ->table('jodoh_prediksi')
                ->select('id','text','prediksi_start','prediksi_end','n_status')
                ->orderBy('n_status', 'asc')
                ->orderBy('prediksi_start', 'asc')
                ->paginate(25);
        return View::make('content.jodohprediksi', $data);
    }

    public function jodohprediksi_search()
    {
        $data = $this->constructMenu();
        $data['results'] = DB::connection('umb-app')
                ->table('jodoh_prediksi')
                ->select('id','text','prediksi_start','prediksi_end','n_status')
                ->where('text','like','%'.Input::get('qs').'%')
                ->orderBy('n_status', 'asc')
                ->orderBy('prediksi_start', 'asc')
                ->paginate(25);
        return View::make('content.jodohprediksi', $data);
    }

    public function jodohprediksi_add()
    {
        $data = $this->constructMenu();
        return View::make('content.jodohprediksi_add', $data);
    }

    public function jodohprediksi_add_save()
    {
        if (empty(Input::get('text')) && empty(Input::get('prediksi_start')) && empty(Input::get('prediksi_end')))
        {
            return Redirect::to('/content/jodohprediksi/add')->with('err', 'Cannot empty.');
        }
        else
        {   
            $data = [
                'text' => Input::get('text'),
                'prediksi_start' => intval(Input::get('prediksi_start')),
                'prediksi_end' => intval(Input::get('prediksi_end')),
            ];

            JodohPrediksi::create($data);
            
            return Redirect::to('/content/jodohprediksi')->with('success', 
                'Jodoh Prediksi [' . Input::get('text') . '] has been saved.');
        }
       
    }

    public function jodohprediksi_edit($id)
    {
        $data = $this->constructMenu();
        $jodoh = JodohPrediksi::find($id);
        $data['text'] = $jodoh->text;
        $data['prediksi_start'] = $jodoh->prediksi_start;
        $data['prediksi_end'] = $jodoh->prediksi_end;
        $data['id'] = $jodoh->id;

        return View::make('content.jodohprediksi_edit', $data);
    }

    public function jodohprediksi_edit_save($id)
    {
        if (empty(Input::get('text')) && empty(Input::get('prediksi_start')) && empty(Input::get('prediksi_end')))
        {
            return Redirect::to('/content/jodohprediksi_edit/'.$id)->with('err', 'Cannot empty.');
        }
        else
        {
            $data = JodohPrediksi::find($id);
            $data->text = Input::get('text');
            $data->prediksi_start = Input::get('prediksi_start');
            $data->prediksi_end = Input::get('prediksi_end');
            $data->save();

            return Redirect::to('/content/jodohprediksi')->with('success', 
                'Update jodoh prediksi [' . Input::get('text') . '] has been saved.');
        }
    }

    public function jodohprediksi_enable($id)
    {
        $data = JodohPrediksi::find($id);
        $data->n_status = 1;
        $data->save();

        return Redirect::to('/content/jodohprediksi')->with('success', 
            'Enable jodoh prediksi [' . $data->text . '].');
    }

    public function jodohprediksi_delete($id)
    {
        $data = JodohPrediksi::find($id);
        $data->n_status = 2;
        $data->save();

        return Redirect::to('/content/jodohprediksi')->with('success', 
            'Delete jodoh prediksi [' . $data->text . '].');
    }

    /**
     * To filter content, removes strange characters
     * @param $content a content
     * @return mixed
     */
    private function filterContent($content) {
        //_s = _s.replace('“','"'); //8220
        //_s = _s.replace('”','"'); //8221
        //_s = _s.replace('‘','"'); //8216
		$content = strip_tags($content);
        //Just a quick hack
        $content = str_replace('“', '"', $content);
        $content = str_replace('”', '"', $content);
        $content = str_replace('‘', '"', $content);
        return $content;

    }


}
