<?php

class BaseController extends Controller {

    /**
     * Construct menu put it in an array.
     *
     * To be consumable by the UI part
     *
     */
    protected function constructMenu() {
        $data = array();
        $menus = Menu::getMenuByRole();

        // print_r($menus);
        // exit();

        foreach ($menus as $menu) {
            //only for parents
            if ($menu->parent_id > 0) continue;

            if ($menu->module == 'traffic') {
                $data['traffic'] = $this->constructSubMenu($menus, $menu->id);
                //Log::warning($data['traffic']);
            } else if ($menu->module == 'content') {
                $data['content'] = $this->constructSubMenu($menus, $menu->id);
            } else if ($menu->module == 'subscriber') {
                $data['subscriber'] = $this->constructSubMenu($menus, $menu->id);
            } else if ($menu->module == 'monitoring') {
                $data['monitoring'] = $this->constructSubMenu($menus, $menu->id);
            } else if ($menu->module == 'admin') {
                $data['admin'] = $this->constructSubMenu($menus, $menu->id);
            } else if ($menu->module == 'nodesms') {
                $data['nodesms'] = $this->constructSubMenu($menus, $menu->id);
            }
        }

        /*
        if(Auth::user()->username == "inong") {
            unset($data['traffic']);
            unset($data['content']);
            unset($data['monitoring']);
            unset($data['admin']);
            unset($data['nodesms']);
            unset($data['subscriber'][0]);
            for($i = 2; $i <= 7; $i++ ) {
                unset($data['subscriber'][$i]);
            }
        }
        */

        return $data;
    }

    /**
     * construct sub menu
     *
     */
    protected function constructSubMenu($menus, $menuId)
    {
        $subMenus = array();
        foreach ($menus as $menu) {
            if ($menu->parent_id == $menuId) {
                $asub = $menu;
                $_asub = $this->constructSubMenu($menus, $asub->id);
                if (!empty($_asub)) $asub->sub = $_asub;
                $subMenus[] = $asub;
            }
        }
        return $subMenus;
    }


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
