<?php

/**
 * Class ContentController
 *
 */
use Symfony\Component\HttpFoundation\Request;


class StatisticController extends BaseController {
    protected $connection = 'mongodb';
    protected $reporting;

    public function __construct()
    {
        //
    }

    public function index(){
        $data = $this->constructMenu();
        //$chat_groups = DB::connection('mongodb')
        //            ->collection('chat_groups')
        //            ->where('is_default', 2)
        //            ->get();
        //$data['results'] = $chat_groups;

        $challenges_data = DB::connection('mongodb')->collection('chat_groups')->raw(function($collection) {
            return $collection->aggregate(
                [ '$match' => [ 
                    'is_default' => 2
                    ]
                ],
                [ '$lookup' => [ 
                    'from' => 'chat_group_lives',
                    'foreignField' => 'chat_group',
                    'localField' => '_id',
                    'as' => 'lives_lookup'
                    ]
                ],
                [
                    '$unwind' => '$lives_lookup'
                ],
                [ '$project' => [
                    '_id' => 1,
                    'group_name' => 1,
                    'group_description' => 1,
                    'group_avatar' => 1,
                    'total_member' => 1,
                    'last_active' => 1,
                    'is_live' => '$lives_lookup.is_live',
                    'live_pic' => '$lives_lookup.live_pic',
                    'live_url' => '$lives_lookup.live_url',
                    'is_broadcaster' => '$lives_lookup.is_broadcaster'
                    ]
                ]
            );
        });
        $data['results'] = $challenges_data['result'];

        return View::make('statistic.liveroom_list', $data);
    }

    public function stop($group_id)
    {
        //$url = "http://127.0.0.1:7069/v2/room/stop?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJtb2JpbGUgYXBwcyIsImtleSI6InJob2RjWWgwMW1GWUtub0wwRkZacHVjbXNJbkJJZWdrIiwiaXAiOiIiLCJtc2lzZG4iOiI2Mjg1NjQzMDU5MzY3IiwiZGV2aWNlX2lkIjoiYTRmYmU5MjBjNjI4ODczZCIsImlhdCI6MTUxOTc5ODMzNiwiZXhwIjoxNTgyOTEyMjQwfQ.teUofbSO5AGSAFpwBB-j-kvMaedGLDwc0dvaR0ToIfw&group_id=";
        //$url = "http://127.0.0.1:7069/v2/dev_ver?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJtb2JpbGUgYXBwcyIsImtleSI6InJob2RjWWgwMW1GWUtub0wwRkZacHVjbXNJbkJJZWdrIiwiaXAiOiIiLCJtc2lzZG4iOiI2Mjg1NjQzMDU5MzY3IiwiZGV2aWNlX2lkIjoiYTRmYmU5MjBjNjI4ODczZCIsImlhdCI6MTUxOTc5ODMzNiwiZXhwIjoxNTgyOTEyMjQwfQ.teUofbSO5AGSAFpwBB-j-kvMaedGLDwc0dvaR0ToIfw&group_id=";
        //$url = "https://mon-api.kmi.mobi/v2/room/stop?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJtb2JpbGUgYXBwcyIsImtleSI6InJob2RjWWgwMW1GWUtub0wwRkZacHVjbXNJbkJJZWdrIiwiaXAiOiIiLCJtc2lzZG4iOiI2Mjg1NjQzMDU5MzY3IiwiZGV2aWNlX2lkIjoiYTRmYmU5MjBjNjI4ODczZCIsImlhdCI6MTUxOTc5ODMzNiwiZXhwIjoxNTgyOTEyMjQwfQ.teUofbSO5AGSAFpwBB-j-kvMaedGLDwc0dvaR0ToIfw&group_id=";
        $url = "http://202.43.162.252:7001/v2/room/stop?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJtb2JpbGUgYXBwcyIsImtleSI6InJob2RjWWgwMW1GWUtub0wwRkZacHVjbXNJbkJJZWdrIiwiaXAiOiIiLCJtc2lzZG4iOiI2Mjg1NjQzMDU5MzY3IiwiZGV2aWNlX2lkIjoiYTRmYmU5MjBjNjI4ODczZCIsImlhdCI6MTUxOTc5ODMzNiwiZXhwIjoxNTgyOTEyMjQwfQ.teUofbSO5AGSAFpwBB-j-kvMaedGLDwc0dvaR0ToIfw&group_id=";
        //$url = "https://mon-api.kmi.mobi/v2/me?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJtb2JpbGUgYXBwcyIsImtleSI6InJob2RjWWgwMW1GWUtub0wwRkZacHVjbXNJbkJJZWdrIiwiaXAiOiIiLCJtc2lzZG4iOiI2Mjg1NjQzMDU5MzY3IiwiZGV2aWNlX2lkIjoiYTRmYmU5MjBjNjI4ODczZCIsImlhdCI6MTUxOTc5ODMzNiwiZXhwIjoxNTgyOTEyMjQwfQ.teUofbSO5AGSAFpwBB-j-kvMaedGLDwc0dvaR0ToIfw&group_id=";
        //$url = "https://mon-api.kmi.mobi/v2/dev_ver?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOiJtb2JpbGUgYXBwcyIsImtleSI6InJob2RjWWgwMW1GWUtub0wwRkZacHVjbXNJbkJJZWdrIiwiaXAiOiIiLCJtc2lzZG4iOiI2Mjg1NjQzMDU5MzY3IiwiZGV2aWNlX2lkIjoiYTRmYmU5MjBjNjI4ODczZCIsImlhdCI6MTUxOTc5ODMzNiwiZXhwIjoxNTgyOTEyMjQwfQ.teUofbSO5AGSAFpwBB-j-kvMaedGLDwc0dvaR0ToIfw&group_id=";

        $ch = curl_init($url.$group_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $response = json_decode(curl_exec ($ch));
        curl_close($ch);
        if ($response)
        {
            return Redirect::to('/monitoring/liveroom')->with('success', $response->message);
        }
        else
        {
            return Redirect::to('/monitoring/liveroom')->with('error', 'Error hit API..');
        }
        
    }

    public function chart($group_id)
    {
        try
        {
            $data = $this->constructMenu();

            //---------------------------------------------------------------------------------------------------------
            $newdate = date("Y-m-d", strtotime("-1 months"));
            $start = date("d M Y");
            $end = date("d M Y", strtotime("-1 months"));
            $data['label_desc'] = $end . " - " . $start;

            $group_data = DB::connection('mongodb')
                        ->collection('chat_groups')
                        ->find($group_id);

            $data['group_name'] = $group_data['group_name'];
            $data['group_description'] = $group_data['group_description'];
            
            
            // KEPO ---------------------------------------------------------------------------------------------------------
            $stats_heart = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($newdate))
                        ->where('activity_type',1)
                        ->where('user_source',3)
                        ->orderBy('date','asc')
                        ->get();
            $heart_data = "[";$heart_time = "[";
            foreach($stats_heart as $dt)
            {
                $heart_data .=  $dt['activity_count'] . ",";
                $heart_time .=  '"' . date('d M', $dt['date']->sec) . '",';
            }
            $data['heart_value'] = rtrim($heart_data,",") . "]";
            $data['heart_label'] = rtrim($heart_time,",") . "]";



            $stats_bubble = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($newdate))
                        ->where('activity_type',2)
                        ->where('user_source',3)
                        ->orderBy('date','asc')
                        ->get();
            $bubble_data = "[";$bubble_time = "[";
            foreach($stats_bubble as $dt)
            {
                $bubble_data .=  $dt['activity_count'] . ",";
                $bubble_time .=  '"' . date('d M', $dt['date']->sec) . '",';
            }
            $data['bubble_value'] = rtrim($bubble_data,",") . "]";
            $data['bubble_label'] = rtrim($bubble_time,",") . "]";


            // ILIVE ---------------------------------------------------------------------------------------------------------
            $istats_heart = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($newdate))
                        ->where('activity_type',1)
                        ->where('user_source',4)
                        ->orderBy('date','asc')
                        ->get();
            $iheart_data = "[";$iheart_time = "[";
            foreach($istats_heart as $idt)
            {
                $iheart_data .=  $idt['activity_count'] . ",";
                $iheart_time .=  '"' . date('d M', $idt['date']->sec) . '",';
            }
            $data['iheart_value'] = rtrim($iheart_data,",") . "]";
            $data['iheart_label'] = rtrim($iheart_time,",") . "]";



            $istats_bubble = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($newdate))
                        ->where('activity_type',2)
                        ->where('user_source',4)
                        ->orderBy('date','asc')
                        ->get();
            $ibubble_data = "[";$ibubble_time = "[";
            foreach($istats_bubble as $idt)
            {
                $ibubble_data .=  $idt['activity_count'] . ",";
                $ibubble_time .=  '"' . date('d M', $idt['date']->sec) . '",';
            }
            $data['ibubble_value'] = rtrim($ibubble_data,",") . "]";
            $data['ibubble_label'] = rtrim($ibubble_time,",") . "]";


            return View::make('statistic.group_chart', $data);
        }
        catch (MongoException $ex)
        {
            return Redirect::to('/monitoring/liveroom');
        }
        
    }

    public function chart_custom($group_id)
    {
        try
        {
            $data = $this->constructMenu();

            //---------------------------------------------------------------------------------------------------------
            $first = date("Y-m-d", strtotime(Input::get('start_date')));
            $last = date("Y-m-d", strtotime(Input::get('end_date')));
            $dt_end = date("d M Y", strtotime(Input::get('end_date')));
            $dt_start = date("d M Y", strtotime(Input::get('start_date')));
            $data['label_desc'] = $dt_start . " - " . $dt_end;

            $group_data = DB::connection('mongodb')
                        ->collection('chat_groups')
                        ->find($group_id);

            $data['group_name'] = $group_data['group_name'];
            $data['group_description'] = $group_data['group_description'];
            
            
            // KEPO ---------------------------------------------------------------------------------------------------------
            $stats_heart = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($first))
                        ->where("date","<=",new DateTime($last))
                        ->where('activity_type',1)
                        ->where('user_source',3)
                        ->orderBy('date','asc')
                        ->get();
            $heart_data = "[";$heart_time = "[";
            foreach($stats_heart as $dt)
            {
                $heart_data .=  $dt['activity_count'] . ",";
                $heart_time .=  '"' . date('d M', $dt['date']->sec) . '",';
            }
            $data['heart_value'] = rtrim($heart_data,",") . "]";
            $data['heart_label'] = rtrim($heart_time,",") . "]";



            $stats_bubble = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($first))
                        ->where("date","<=",new DateTime($last))
                        ->where('activity_type',2)
                        ->where('user_source',3)
                        ->orderBy('date','asc')
                        ->get();
            $bubble_data = "[";$bubble_time = "[";
            foreach($stats_bubble as $dt)
            {
                $bubble_data .=  $dt['activity_count'] . ",";
                $bubble_time .=  '"' . date('d M', $dt['date']->sec) . '",';
            }
            $data['bubble_value'] = rtrim($bubble_data,",") . "]";
            $data['bubble_label'] = rtrim($bubble_time,",") . "]";


            // ILIVE ---------------------------------------------------------------------------------------------------------
            $istats_heart = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($first))
                        ->where("date","<=",new DateTime($last))
                        ->where('activity_type',1)
                        ->where('user_source',4)
                        ->orderBy('date','asc')
                        ->get();
            $iheart_data = "[";$iheart_time = "[";
            foreach($istats_heart as $idt)
            {
                $iheart_data .=  $idt['activity_count'] . ",";
                $iheart_time .=  '"' . date('d M', $idt['date']->sec) . '",';
            }
            $data['iheart_value'] = rtrim($iheart_data,",") . "]";
            $data['iheart_label'] = rtrim($iheart_time,",") . "]";



            $istats_bubble = DB::connection('mongodb')->collection('rpt_statistics')
                        ->where( "chat_group",new MongoId($group_id))
                        ->where("date",">=",new DateTime($first))
                        ->where("date","<=",new DateTime($last))
                        ->where('activity_type',2)
                        ->where('user_source',4)
                        ->orderBy('date','asc')
                        ->get();
            $ibubble_data = "[";$ibubble_time = "[";
            foreach($istats_bubble as $idt)
            {
                $ibubble_data .=  $idt['activity_count'] . ",";
                $ibubble_time .=  '"' . date('d M', $idt['date']->sec) . '",';
            }
            $data['ibubble_value'] = rtrim($ibubble_data,",") . "]";
            $data['ibubble_label'] = rtrim($ibubble_time,",") . "]";


            return View::make('statistic.group_chart', $data);
        }
        catch (MongoException $ex)
        {
            return Redirect::to('/monitoring/liveroom');
        }
        
    }

    public function challenges()
    {
        $data = $this->constructMenu();
        $chat_groups = DB::connection('mongodb')
                    ->collection('chat_groups')
                    ->where('is_default', 2)
                    ->get();

        $data['results'] = $chat_groups;
        return View::make('statistic.challenges_index', $data);
    }

    public function challenges_result()
    {
        $data = $this->constructMenu();
        
        //---------------------------------------------------------------------------------------------------------
        empty(Input::get('start_date')) ?
            $searchdata['start'] = new MongoDate(strtotime(date("Y-m-d", strtotime("1970-01-01")))) :
            $searchdata['start'] = new MongoDate(strtotime(date("Y-m-d", strtotime(Input::get('start_date')))));
        empty(Input::get('end_date')) ?
            $searchdata['end'] = new MongoDate(strtotime(date("Y-m-d", strtotime("+1 days")))) :
            $searchdata['end'] = new MongoDate(strtotime(date("Y-m-d", strtotime(Input::get('end_date')))));
        
        $searchdata['keyword'] = Input::get('keyword');
        $challenges_data = DB::connection('mongodb')->collection('user_challenges')->raw(function($collection) use ($searchdata){
            return $collection->aggregate(
                [ '$match' => [ 
                    'hash_tag' => new MongoRegex('/'.$searchdata['keyword'].'/'),
                    'created_at' => [ 
                        '$gte' => $searchdata['start'],
                        '$lte' => $searchdata['end']
                     ]
                    ]
                ],
                [ '$lookup' => [ 
                    'from' => 'users',
                    'foreignField' => 'id',
                    'localField' => 'user_id',
                    'as' => 'user_lookup'
                    ]
                ],
                [
                    '$unwind' => '$user_lookup'
                ],
                [ '$project' => [
                    '_id' => 0,
                    'user_id' => 1,
                    'msisdn' => '$user_lookup.msisdn',
                    'name' => '$user_lookup.name',
                    'hash_tag' => 1,
                    'created_at' => 1,
                    ]
                ]
            );
        });
        $data['results'] = $challenges_data['result'];

        $data['keyword'] = Input::get('keyword');
        $data['start'] = Input::get('start_date');
        $data['end'] = Input::get('end_date');
        return View::make('statistic.challenges_result', $data);
    }

    public function hashtag_result($source = null)
    {
        try
        {
            $data = $this->constructMenu();
            $newdate = date("Y-m-d", strtotime("-1 months"));
            $start = new MongoDate(strtotime(date("Y-m-d", strtotime("-1 months"))));

            if ($source == 3 )
            {
                $totaltag = DB::connection('mongodb')->collection('user_challenges')->where('user_source',3)->where("created_at",">=",new DateTime($newdate))->count();
                $challenges_data = DB::connection('mongodb')->collection('user_challenges')->raw(function($collection) use ($start){
                    return $collection->aggregate(
                        [ '$match' => [ 
                                'hash_tag' => [ '$ne' => null ],
                                'user_source' => [ '$eq' => 3 ],
                                'created_at' => [ '$gt' => $start ] ]
                        ],
                        [ '$group' => [ 
                                '_id' => [ 'hash_tag' => '$hash_tag' ],
                                'COUNT(hash_tag)' => [ '$sum' => 1 ] ],    
                        ],
                        [ '$project' => [
                                '_id' => 0,
                                'hash_tag' => '$_id.hash_tag',
                                'jumlah' => '$COUNT(hash_tag)' ],
                        ],
                        [ '$sort' => [ 'jumlah' => -1 ] ]
                    );
                });
                $data['judul'] = 'KEPO';
                $data['jumlah'] = $totaltag;
                $data['results'] = $challenges_data['result'];
            }
            else if ($source == 4 )
            {
                $totaltag = DB::connection('mongodb')->collection('user_challenges')->where('user_source',4)->where("created_at",">=",new DateTime($newdate))->count();
                $challenges_data = DB::connection('mongodb')->collection('user_challenges')->raw(function($collection) use ($start){
                    return $collection->aggregate(
                        [ '$match' => [ 
                                'hash_tag' => [ '$ne' => null ],
                                'user_source' => [ '$eq' => 4 ],
                                'created_at' => [ '$gt' => $start ] ]
                        ],
                        [ '$group' => [ 
                                '_id' => [ 'hash_tag' => '$hash_tag' ],
                                'COUNT(hash_tag)' => [ '$sum' => 1 ] ],    
                        ],
                        [ '$project' => [
                                '_id' => 0,
                                'hash_tag' => '$_id.hash_tag',
                                'jumlah' => '$COUNT(hash_tag)' ],
                        ],
                        [ '$sort' => [ 'jumlah' => -1 ] ]
                    );
                });
                $data['judul'] = 'iLIVE';
                $data['jumlah'] = $totaltag;
                $data['results'] = $challenges_data['result'];
            }
            else
            {
                $totaltag = DB::connection('mongodb')->collection('user_challenges')->where("created_at",">=",new DateTime($newdate))->count();
                $challenges_data = DB::connection('mongodb')->collection('user_challenges')->raw(function($collection) use ($start){
                    return $collection->aggregate(
                        [ '$match' => [ 
                                'hash_tag' => ['$ne' => null ],
                                'created_at' => [ '$gt' => $start ] ]
                        ],
                        [ '$group' => [ 
                                '_id' => [ 'hash_tag' => '$hash_tag' ],
                                'COUNT(hash_tag)' => [ '$sum' => 1 ] ],    
                        ],
                        [ '$project' => [
                                '_id' => 0,
                                'hash_tag' => '$_id.hash_tag',
                                'jumlah' => '$COUNT(hash_tag)' ],
                        ],
                        [ '$sort' => [ 'jumlah' => -1 ] ]
                    );
                });
                $data['judul'] = 'KEPO-iLIVE';
                $data['jumlah'] = $totaltag;
                $data['results'] = $challenges_data['result'];
            }

            return View::make('statistic.hash_result', $data);
        }
        catch (MongoException $ex)
        {
            return Redirect::to('/monitoring/trending');
        }
        
    }

    public function edit_view($group_id)
    {
        $data = $this->constructMenu();

        $chat_groups = DB::connection('mongodb')->collection('chat_groups')->raw(function($collection) use ($group_id){
            return $collection->aggregate(
                [ '$match' => [ 
                    'is_default' => 2,
                    '_id' => new MongoId($group_id)
                    ]
                ],
                [ '$lookup' => [ 
                    'from' => 'chat_group_lives',
                    'foreignField' => 'chat_group',
                    'localField' => '_id',
                    'as' => 'lives_lookup'
                    ]
                ],
                [
                    '$unwind' => '$lives_lookup'
                ],
                [ '$project' => [
                    '_id' => 1,
                    'group_name' => 1,
                    'group_description' => 1,
                    'group_avatar' => 1,
                    'is_live' => '$lives_lookup.is_live',
                    'live_pic' => '$lives_lookup.live_pic',
                    'stream_url' => '$lives_lookup.stream_url',
                    'recorded_url' => '$lives_lookup.recorded_url'
                    ]
                ]
            );
        });

        $data['results'] = $chat_groups['result'][0];

        return View::make('statistic.liveroom_edit', $data);
    }

    public function toggle_status($status,$group_id)
    {
        if ($status == 0)
        {
            echo "Disable this<p>";
            $text = "Disable";
            $chat_group_lives = DB::connection('mongodb')
                        ->collection('chat_group_lives')
                        ->where('chat_group', new MongoId($group_id))
                        ->update(['is_live' => false], ['upsert' => false]);
        }
        else
        {
            echo "Enable this<p>";
            $text = "Enable";
            $chat_group_lives = DB::connection('mongodb')
                        ->collection('chat_group_lives')
                        ->where('chat_group', new MongoId($group_id))
                        ->update(['is_live' => true], ['upsert' => false]);
        }

        //echo $status . " - " . $group_id;
        return Redirect::to('/monitoring/liveroom')->with('success', $text . ' done.');
    }
    public function edit_process($group_id)
    {
        $data = [
            'group_name' => Input::get('group_name'),
            'group_description' => Input::get('group_description')
        ];
        $chat_groups = DB::connection('mongodb')
                    ->collection('chat_groups')
                    ->where('_id', new MongoId($group_id))
                    ->update($data, ['upsert' => false]);

        $data = [
            'live_pic' => Input::get('live_pic'),
            'stream_url' => Input::get('stream_url'),
            'recorded_url' => Input::get('recorded_url')
        ];
        $chat_group_lives = DB::connection('mongodb')
                    ->collection('chat_group_lives')
                    ->where('chat_group', new MongoId($group_id))
                    ->update($data, ['upsert' => false]);

                     
        return Redirect::to('/monitoring/liveroom')->with('success', 'Artist [' . Input::get('group_name') . '] has been updated.');
    }

    public function test_mongo()
    {
        
        $newdate = date("Y-m-d", strtotime("-10 days"));
        $start = new MongoDate(strtotime(date("Y-m-d", strtotime("-1 months"))));
        echo "$start<p>";
        echo "$newdate<pre>";

        $jumlah = DB::connection('mongodb')->collection('user_challenges')->where("created_at",">=",new DateTime($newdate))->count();
        echo "jumlah = $jumlah<p>";

        $challenges_data = DB::connection('mongodb')->collection('user_challenges')->raw(function($collection) use ($start){
            return $collection->aggregate(
                [
                    '$match' => 
                    [ 
                        'hash_tag' => ['$ne' => null ],
                        'created_at' => [ '$gt' => $start ]
                    ]
                ],
                [
                    '$group' => 
                    [ 
                        '_id' => [ 'hash_tag' => '$hash_tag' ],
                        'COUNT(hash_tag)' => [ '$sum' => 1 ]
                    ],    
                ],
                [
                    '$project' => 
                    [
                        '_id' => 0,
                        'hash_tag' => '$_id.hash_tag',
                        'jumlah' => '$COUNT(hash_tag)'
                    ],
                ],
                [
                    '$sort' => [ 'jumlah' => -1 ]
                ]
            );
        });
        print_r($challenges_data);
    }
}
