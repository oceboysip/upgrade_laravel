<?php

/**
 * Class ApiController
 *
 * Responsible for handling the General / Common AJAX requests
 *
 */
class ApiController extends Controller {

    /**
     * Get list of active services
     *
     * @param int|\telco $telcoId telco Id
     * @param \sdc|string $sdc sdc
     * @return string in JSON
     */
    public function getListOfActiveService($telcoId = 0, $sdc = '') {
        return json_encode(MainServiceConfig::getActiveService($telcoId, $sdc));
    }


    /**
     * Get list of service keyword
     * @param int|\string $telcoId telco Id
     * @param int|\string $serviceId service Id
     * @param \sdc|string $sdc sdc
     * @return string in JSON
     */
    public function getListOfKeyword($telcoId = 0, $sdc = '', $serviceId = 0) {
        return json_encode(MainServiceConfig::getActiveKeywords($telcoId, $sdc,$serviceId));
    }
}

