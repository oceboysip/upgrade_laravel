<?php

class TrafficController extends BaseController {

    public function showRevenueReport() {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        //$data['services'] = MainServiceConfig::getAllTelco();
        return View::make('traffic.revenue_report',$data);
    }


	public function showMoStat() {
		$data = $this->constructMenu();
		$data['sdc'] = MainServiceConfig::getAllSdc();
		$data['telco'] = MainServiceConfig::getAllTelco();
		//$data['services'] = MainServiceConfig::getAllTelco();
		return View::make('traffic.mo_stat', $data);
	}


	/* --- End View Part --- */

	/**
	 * The 'API' of getting report detail
	 *
	 * @param $telcoId telco id
	 * @param $sdc sdc
	 * @param $serviceId service id
	 * @return mixed
	 */
	public function apiGetRevenueReport($telcoId, $sdc, $serviceId) {
		$data = array();
		$dateStart = Input::get("dtStart");
		$dateEnd = Input::get("dtEnd");
        $keyword = Input::get("keyword");

		$qTelco = MainServiceConfig::getTelcoById($telcoId);
		$data['telcoName'] = $qTelco[0]->name;
		$data['sdc'] = $sdc;
		$data['dtStart'] = $dateStart;
		$data['dtEnd'] = $dateEnd;
		$data['serviceName'] = "All";
		if ($serviceId > 0) {
			$qService = GatewayService::getById($serviceId);
			$data['serviceName'] = $qService[0]->name;
		}

		$rawResult = Reporting::getRevenueOnDateBetween($dateStart, $dateEnd, $telcoId, $serviceId, $sdc, $keyword);

		$data['the_data'] = $rawResult;

		$perDate = array();
		foreach ($rawResult as $raw) {
			if (!isset($perDate[$raw->dtstat])) {
				$perDate[$raw->dtstat]['tS'] = 0;
				$perDate[$raw->dtstat]['tSuc'] = 0;
                $perDate[$raw->dtstat]['tOr'] = 0;
                $perDate[$raw->dtstat]['tOrRe'] = 0;
                $perDate[$raw->dtstat]['tOrSuc'] = 0;
				$perDate[$raw->dtstat]['tR'] = 0;
				$perDate[$raw->dtstat]['tRSuc'] = 0;
				$perDate[$raw->dtstat]['tG'] = 0;
				$perDate[$raw->dtstat]['tN'] = 0;
                $perDate[$raw->dtstat]['tp1'] = 0;
                $perDate[$raw->dtstat]['tpo1'] = 0;
                $perDate[$raw->dtstat]['tsp1'] = 0;
			}
			$perDate[$raw->dtstat]['tS'] += $raw->total_sent;
			$perDate[$raw->dtstat]['tSuc'] += $raw->total_success;
            $perDate[$raw->dtstat]['tOr'] += $raw->total_original;
            $perDate[$raw->dtstat]['tOrRe'] += $raw->total_original_real;
            $perDate[$raw->dtstat]['tOrSuc'] += $raw->total_success_ori;
			$perDate[$raw->dtstat]['tR'] += $raw->total_retry;
			$perDate[$raw->dtstat]['tRSuc'] += $raw->total_success_retry;
			$perDate[$raw->dtstat]['tG'] += $raw->total_gross;
			$perDate[$raw->dtstat]['tN'] += $raw->total_net;
            $perDate[$raw->dtstat]['tp1'] += $raw->total_push1;
            $perDate[$raw->dtstat]['tpo1'] += $raw->total_push1_original;
            $perDate[$raw->dtstat]['tsp1'] += $raw->total_success_push1;
		}

		$data['the_data_per_date'] = $perDate;
		return View::make('traffic.api.api_list_traffic', $data);
	}


    public function apiGetTrafficReport($telcoId, $sdc, $serviceId) {
        $data = array();
        $dateStart = Input::get("dtStart");
        $dateEnd = Input::get("dtEnd");
        $keyword = Input::get("keyword");

        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        $data['sdc'] = $sdc;
        $data['dtStart'] = $dateStart;
        $data['dtEnd'] = $dateEnd;
        $data['serviceName'] = "All";
        if ($serviceId > 0) {
            $qService = GatewayService::getById($serviceId);
            $data['serviceName'] = $qService[0]->name;
        }

        $rawResult = Reporting::getTrafficOnDateBetween($dateStart, $dateEnd, $telcoId, $serviceId, $sdc, $keyword);
        $data['the_data'] = $rawResult;

        return View::make('traffic.api.api_list_traffic_report', $data);
    }

    public function traffic_report()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        //$data['services'] = MainServiceConfig::getAllTelco();
        return View::make('traffic.traffic_report',$data);
    }


	/**
	 * The 'API' of MO stat detail
	 *
	 * @param $telcoId
	 * @param $sdc
	 * @param $serviceId
	 * @return mixed
	 */
	public function apiGetMoStat($telcoId, $sdc, $serviceId) {
		$data = array();
		$dateStart = Input::get("dtStart");
		$dateEnd = Input::get("dtEnd");

		$qTelco = MainServiceConfig::getTelcoById($telcoId);
		$data['telcoName'] = $qTelco[0]->name;
		$data['sdc'] = $sdc;
		$data['dtStart'] = $dateStart;
		$data['dtEnd'] = $dateEnd;
		$data['serviceName'] = "All";
		if ($serviceId > 0) {
			$qService = GatewayService::getById($serviceId);
			$data['serviceName'] = $qService[0]->name;
		}

		$useKeyword = 0;
		if (Input::get("keywordBreakdown")) {$useKeyword = 1;}

		$rawResult = Reporting::getMoStatOnDate($dateStart, $dateEnd, $telcoId, $serviceId, $sdc, $useKeyword);
		$data['the_data'] = $rawResult;
		$data['useKeyword'] = $useKeyword;

		$perDate = array();
		foreach ($rawResult as $raw) {
			if (!isset($perDate[$raw->dtstat])) {
				$perDate[$raw->dtstat]['tS'] = 0;
				/*
				$perDate[$raw->dtstat]['tSuc'] = 0;
				$perDate[$raw->dtstat]['tG'] = 0;
				$perDate[$raw->dtstat]['tN'] = 0;
				*/
			}
			$perDate[$raw->dtstat]['tS'] += $raw->total;
			/*
			$perDate[$raw->dtstat]['tSuc'] += $raw->total_success;
			$perDate[$raw->dtstat]['tG'] += $raw->total_gross;
			$perDate[$raw->dtstat]['tN'] += $raw->total_net;
			*/
		}

		$data['the_data_per_date'] = $perDate;
		return View::make('traffic.api.api_list_mo_stat', $data);
	}

    public function delivery_report()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        //$data['services'] = MainServiceConfig::getAllTelco();
        return View::make('traffic.delivery_report', $data);
    }

    public function delivery_report_action()
    {
        $post_data = Input::get();
        $dr2 = "concat(round(((dr_2*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_2_p";
        $dr3 = "concat(round(((dr_3*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_3_p";
        $dr4 = "concat(round(((dr_4*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_4_p";
        $dr5 = "concat(round(((dr_5*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_5_p";
        $dr6 = "concat(round(((dr_6*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_6_p";
        $dr7 = "concat(round(((dr_7*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_7_p";
        $dr8 = "concat(round(((dr_8*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_8_p";
        $dr96 = "concat(round(((dr_96*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_96_p";
        $dr97 = "concat(round(((dr_97*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_97_p";
        $dr99 = "concat(round(((dr_99*100)/(dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99)),2),'%') as dr_99_p";
        $t = "format((dr_2 + dr_3 + dr_4 + dr_5 + dr_6 + dr_7 + dr_8 + dr_96 + dr_97 + dr_99),0) as total";

        $dr2All = "concat(round(((sum(dr_2)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_2_p";
        $dr3All = "concat(round(((sum(dr_3)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_3_p";
        $dr4All = "concat(round(((sum(dr_4)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_4_p";
        $dr5All = "concat(round(((sum(dr_5)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_5_p";
        $dr6All = "concat(round(((sum(dr_6)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_6_p";
        $dr7All = "concat(round(((sum(dr_7)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_7_p";
        $dr8All = "concat(round(((sum(dr_8)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_8_p";
        $dr96All = "concat(round(((sum(dr_96)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_96_p";
        $dr97All = "concat(round(((sum(dr_97)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_97_p";
        $dr99All = "concat(round(((sum(dr_99)*100)/(sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99))),2),'%') as dr_99_p";
        $tAll = "format((sum(dr_2) + sum(dr_3) + sum(dr_4) + sum(dr_5) + sum(dr_6) + sum(dr_7) + sum(dr_8) + sum(dr_96) + sum(dr_97) + sum(dr_99)),0) as total";

        $resultsOri = DB::select("SELECT dtreport, telco_sid, format(dr_2,0) as dr_2, $dr2, format(dr_3,0) as dr_3, $dr3, format(dr_4,0) as dr_4, $dr4, format(dr_5,0)as dr_5, $dr5, format(dr_6,0) as dr_6, $dr6, format(dr_7,0) as dr_7, $dr7, format(dr_8,0) as dr_8, $dr8, format(dr_96,0) as dr_96, $dr96, format(dr_97,0)as dr_97, $dr97, format(dr_99,0) as dr_99, $dr99, $t
                              FROM traffic_rpt.traffic_dr_summary_isat
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                	AND telco_sid in ('99858336008003','99199197008001')
                                    AND mt_type = '1'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsRetry = DB::select("SELECT dtreport, telco_sid, format(dr_2,0) as dr_2, $dr2, format(dr_3,0) as dr_3, $dr3, format(dr_4,0) as dr_4, $dr4, format(dr_5,0)as dr_5, $dr5, format(dr_6,0) as dr_6, $dr6, format(dr_7,0) as dr_7, $dr7, format(dr_8,0) as dr_8, $dr8, format(dr_96,0) as dr_96, $dr96, format(dr_97,0)as dr_97, $dr97, format(dr_99,0) as dr_99, $dr99, $t
                              FROM traffic_rpt.traffic_dr_summary_isat
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('99858336008003','99199197008001')
                                    AND mt_type = '2'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsPush1 = DB::select("SELECT dtreport, telco_sid, format(dr_2,0) as dr_2, $dr2, format(dr_3,0) as dr_3, $dr3, format(dr_4,0) as dr_4, $dr4, format(dr_5,0)as dr_5, $dr5, format(dr_6,0) as dr_6, $dr6, format(dr_7,0) as dr_7, $dr7, format(dr_8,0) as dr_8, $dr8, format(dr_96,0) as dr_96, $dr96, format(dr_97,0)as dr_97, $dr97, format(dr_99,0) as dr_99, $dr99, $t
                              FROM traffic_rpt.traffic_dr_summary_isat
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('99858336008003','99199197008001')
                                    AND mt_type = '3'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsAll = DB::select("SELECT dtreport, telco_sid, format(sum(dr_2),0) as dr_2, $dr2All, format(sum(dr_3),0) as dr_3, $dr3All, format(sum(dr_4),0) as dr_4, $dr4All, format(sum(dr_5),0)as dr_5, $dr5All, format(sum(dr_6),0) as dr_6, $dr6All, format(sum(dr_7),0) as dr_7, $dr7All, format(sum(dr_8),0) as dr_8, $dr8All, format(sum(dr_96),0) as dr_96, $dr96All, format(sum(dr_97),0)as dr_97, $dr97All, format(sum(dr_99),0) as dr_99, $dr99All, $tAll
                              FROM traffic_rpt.traffic_dr_summary_isat
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('99858336008003','99199197008001')
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                    GROUP BY dtreport, telco_sid
                                ORDER BY dtreport DESC");

        $resultsOther = DB::select("SELECT dtreport, telco_sid, format(dr_2,0) as dr_2, $dr2, format(dr_3,0) as dr_3, $dr3, format(dr_4,0) as dr_4, $dr4, format(dr_5,0)as dr_5, $dr5, format(dr_6,0) as dr_6, $dr6, format(dr_7,0) as dr_7, $dr7, format(dr_8,0) as dr_8, $dr8, format(dr_96,0) as dr_96, $dr96, format(dr_97,0)as dr_97, $dr97, format(dr_99,0) as dr_99, $dr99, $t
                              FROM traffic_rpt.traffic_dr_summary_isat
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('99858336008003','99199197008001')
                                    AND mt_type = '4'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $data['the_dataOri'] = $resultsOri;
        $data['the_dataRetry'] = $resultsRetry;
        $data['the_dataPush1'] = $resultsPush1;
        $data['the_dataAll'] = $resultsAll;
        $data['the_dataOther'] = $resultsOther;
        $data['sdcSelect'] = $post_data['sdc'];
        $data['dtStart'] = $post_data['dtStart'];
        $data['dtEnd'] = $post_data['dtEnd'];
        return View::make('traffic.api.delivery_report_api', $data);
    }

    public function xl_delivery_report()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        //$data['services'] = MainServiceConfig::getAllTelco();
        return View::make('traffic.xl_delivery_report', $data);
    }

    public function xl_delivery_report_action()
    {
        $post_data = Input::get();
        $xDELIVRD = "concat(round(((dr_DELIVRD*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_DELIVRD_p";
        $x505 = "concat(round(((dr_505*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_505_p";
        $x3101 = "concat(round(((dr_3101*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_3101_p";
        $x3306 = "concat(round(((dr_3306*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_3306_p";
        $x1375 = "concat(round(((dr_1375*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_1375_p";
        $x3103 = "concat(round(((dr_3103*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_3103_p";
        $x3320 = "concat(round(((dr_3320*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_3320_p";
        $x7 = "concat(round(((dr_7*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_7_p";
        $x1031 = "concat(round(((dr_1031*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_1031_p";
        $x3309 = "concat(round(((dr_3309*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_3309_p";
        $x1001 = "concat(round(((dr_1001*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_1001_p";
        $x2208 = "concat(round(((dr_2208*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_2208_p";
        $xEXPIRED = "concat(round(((dr_EXPIRED*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_EXPIRED_p";
        $x1004 = "concat(round(((dr_1004*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_1004_p";
        $xUNDELIV = "concat(round(((dr_UNDELIV*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_UNDELIV_p";
        $x3108 = "concat(round(((dr_3108*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_3108_p";
        $x511 = "concat(round(((dr_511*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_511_p";
        $x512 = "concat(round(((dr_512*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_512_p";
        $x514 = "concat(round(((dr_514*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_514_p";
        $xOTHERS = "concat(round(((dr_OTHERS*100)/(dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS)),2),'%') as dr_OTHERS_p";
        $xt = "format((dr_DELIVRD + dr_505 + dr_3101 + dr_3306 + dr_1375 + dr_3103 + dr_3320 + dr_7 + dr_1031 + dr_3309 + dr_1001 + dr_2208 + dr_EXPIRED + dr_1004 + dr_UNDELIV + dr_3108 + dr_511 + dr_512 + dr_514 + dr_OTHERS),0) as total";

        $xDELIVRDAll = "concat(round(((sum(dr_DELIVRD)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_DELIVRD_p";
        $x505All = "concat(round(((sum(dr_505)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_505_p";
        $x3101All = "concat(round(((sum(dr_3101)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_3101_p";
        $x3306All = "concat(round(((sum(dr_3306)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_3306_p";
        $x1375All = "concat(round(((sum(dr_1375)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_1375_p";
        $x3103All = "concat(round(((sum(dr_3103)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_3103_p";
        $x3320All = "concat(round(((sum(dr_3320)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_3320_p";
        $x7All = "concat(round(((sum(dr_7)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_7_p";
        $x1031All = "concat(round(((sum(dr_1031)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_1031_p";
        $x3309All = "concat(round(((sum(dr_3309)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_3309_p";
        $x1001All = "concat(round(((sum(dr_1001)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_1001_p";
        $x2208All = "concat(round(((sum(dr_2208)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_2208_p";
        $xEXPIREDAll = "concat(round(((sum(dr_EXPIRED)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_EXPIRED_p";
        $x1004All = "concat(round(((sum(dr_1004)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_1004_p";
        $xUNDELIVAll = "concat(round(((sum(dr_UNDELIV)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_UNDELIV_p";
        $x3108All = "concat(round(((sum(dr_3108)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_3108_p";
        $x511All = "concat(round(((sum(dr_511)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_511_p";
        $x512All = "concat(round(((sum(dr_512)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_512_p";
        $x514All = "concat(round(((sum(dr_514)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_514_p";
        $xOTHERSAll = "concat(round(((sum(dr_OTHERS)*100)/(sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS))),2),'%') as dr_OTHERS_p";
        $xtAll = "format((sum(dr_DELIVRD) + sum(dr_505) + sum(dr_3101) + sum(dr_3306) + sum(dr_1375) + sum(dr_3103) + sum(dr_3320) + sum(dr_7) + sum(dr_1031) + sum(dr_3309) + sum(dr_1001) + sum(dr_2208) + sum(dr_EXPIRED) + sum(dr_1004) + sum(dr_UNDELIV) + sum(dr_3108) + sum(dr_511) + sum(dr_512) + sum(dr_514) + sum(dr_OTHERS)),0) as total";


        $resultsOri = DB::select("SELECT dtreport, telco_sid, format(dr_DELIVRD,0) as dr_DELIVRD, $xDELIVRD, format(dr_505,0) as dr_505, $x505, format(dr_3101,0) as dr_3101, $x3101, format(dr_3306,0)as dr_3306, $x3306, format(dr_1375,0) as dr_1375, $x1375, format(dr_3103,0) as dr_3103, $x3103, format(dr_3320,0) as dr_3320, $x3320, format(dr_7,0) as dr_7, $x7, format(dr_1031,0)as dr_1031, $x1031, format(dr_3309,0) as dr_3309, $x3309, format(dr_1001,0) as dr_1001, $x1001, format(dr_2208,0) as dr_2208, $x2208, format(dr_EXPIRED,0) as dr_EXPIRED, $xEXPIRED, format(dr_1004,0) as dr_1004, $x1004, format(dr_UNDELIV,0) as dr_UNDELIV, $xUNDELIV,format(dr_3108,0) as dr_3108, $x3108,format(dr_511,0) as dr_511, $x511,format(dr_512,0) as dr_512, $x512,format(dr_514,0) as dr_514, $x514,format(dr_OTHERS,0) as dr_OTHERS, $xOTHERS, $xt
                              FROM traffic_rpt.traffic_dr_summary_xl
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                	AND telco_sid in ('9985806','9919906')
                                    AND mt_type = '1'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");
        
        $resultsRetry = DB::select("SELECT dtreport, telco_sid, format(dr_DELIVRD,0) as dr_DELIVRD, $xDELIVRD, format(dr_505,0) as dr_505, $x505, format(dr_3101,0) as dr_3101, $x3101, format(dr_3306,0)as dr_3306, $x3306, format(dr_1375,0) as dr_1375, $x1375, format(dr_3103,0) as dr_3103, $x3103, format(dr_3320,0) as dr_3320, $x3320, format(dr_7,0) as dr_7, $x7, format(dr_1031,0)as dr_1031, $x1031, format(dr_3309,0) as dr_3309, $x3309, format(dr_1001,0) as dr_1001, $x1001, format(dr_2208,0) as dr_2208, $x2208, format(dr_EXPIRED,0) as dr_EXPIRED, $xEXPIRED, format(dr_1004,0) as dr_1004, $x1004, format(dr_UNDELIV,0) as dr_UNDELIV, $xUNDELIV,format(dr_3108,0) as dr_3108, $x3108,format(dr_511,0) as dr_511, $x511,format(dr_512,0) as dr_512, $x512,format(dr_514,0) as dr_514, $x514,format(dr_OTHERS,0) as dr_OTHERS, $xOTHERS, $xt
                              FROM traffic_rpt.traffic_dr_summary_xl
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('9985806','9919906')
                                    AND mt_type = '2'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");
        
        $resultsPush1 = DB::select("SELECT dtreport, telco_sid, format(dr_DELIVRD,0) as dr_DELIVRD, $xDELIVRD, format(dr_505,0) as dr_505, $x505, format(dr_3101,0) as dr_3101, $x3101, format(dr_3306,0)as dr_3306, $x3306, format(dr_1375,0) as dr_1375, $x1375, format(dr_3103,0) as dr_3103, $x3103, format(dr_3320,0) as dr_3320, $x3320, format(dr_7,0) as dr_7, $x7, format(dr_1031,0)as dr_1031, $x1031, format(dr_3309,0) as dr_3309, $x3309, format(dr_1001,0) as dr_1001, $x1001, format(dr_2208,0) as dr_2208, $x2208, format(dr_EXPIRED,0) as dr_EXPIRED, $xEXPIRED, format(dr_1004,0) as dr_1004, $x1004, format(dr_UNDELIV,0) as dr_UNDELIV, $xUNDELIV,format(dr_3108,0) as dr_3108, $x3108,format(dr_511,0) as dr_511, $x511,format(dr_512,0) as dr_512, $x512,format(dr_514,0) as dr_514, $x514,format(dr_OTHERS,0) as dr_OTHERS, $xOTHERS, $xt
                              FROM traffic_rpt.traffic_dr_summary_xl
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('9985806','9919906')
                                    AND mt_type = '3'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsAll = DB::select("SELECT dtreport, telco_sid, format(sum(dr_DELIVRD),0) as dr_DELIVRD, $xDELIVRDAll, format(sum(dr_505),0) as dr_505, $x505All, format(sum(dr_3101),0) as dr_3101, $x3101All, format(sum(dr_3306),0)as dr_3306, $x3306All, format(sum(dr_1375),0) as dr_1375, $x1375All, format(sum(dr_3103),0) as dr_3103, $x3103All, format(sum(dr_3320),0) as dr_3320, $x3320All, format(sum(dr_7),0) as dr_7, $x7All, format(sum(dr_1031),0)as dr_1031, $x1031All, format(sum(dr_3309),0) as dr_3309, $x3309All, format(sum(dr_1001),0) as dr_1001, $x1001All, format(sum(dr_2208),0) as dr_2208, $x2208All, format(sum(dr_EXPIRED),0) as dr_EXPIRED, $xEXPIREDAll, format(sum(dr_1004),0) as dr_1004, $x1004All, format(sum(dr_UNDELIV),0) as dr_UNDELIV, $xUNDELIVAll,format(sum(dr_3108),0) as dr_3108, $x3108All,format(sum(dr_511),0) as dr_511, $x511All,format(sum(dr_512),0) as dr_512, $x512All,format(sum(dr_514),0) as dr_514, $x514All,format(sum(dr_OTHERS),0) as dr_OTHERS, $xOTHERSAll, $xtAll
                              FROM traffic_rpt.traffic_dr_summary_xl
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('9985806','9919906')
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                    GROUP BY dtreport, telco_sid
                                ORDER BY dtreport DESC");

        $resultsOther = DB::select("SELECT dtreport, telco_sid, format(dr_DELIVRD,0) as dr_DELIVRD, $xDELIVRD, format(dr_505,0) as dr_505, $x505, format(dr_3101,0) as dr_3101, $x3101, format(dr_3306,0)as dr_3306, $x3306, format(dr_1375,0) as dr_1375, $x1375, format(dr_3103,0) as dr_3103, $x3103, format(dr_3320,0) as dr_3320, $x3320, format(dr_7,0) as dr_7, $x7, format(dr_1031,0)as dr_1031, $x1031, format(dr_3309,0) as dr_3309, $x3309, format(dr_1001,0) as dr_1001, $x1001, format(dr_2208,0) as dr_2208, $x2208, format(dr_EXPIRED,0) as dr_EXPIRED, $xEXPIRED, format(dr_1004,0) as dr_1004, $x1004, format(dr_UNDELIV,0) as dr_UNDELIV, $xUNDELIV,format(dr_3108,0) as dr_3108, $x3108,format(dr_511,0) as dr_511, $x511,format(dr_512,0) as dr_512, $x512,format(dr_514,0) as dr_514, $x514,format(dr_OTHERS,0) as dr_OTHERS, $xOTHERS, $xt
                              FROM traffic_rpt.traffic_dr_summary_xl
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid in ('9985806','9919906')
                                    AND mt_type = '4'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $data['the_dataOri'] = $resultsOri;
        $data['the_dataRetry'] = $resultsRetry;
        $data['the_dataPush1'] = $resultsPush1;
        $data['the_dataAll'] = $resultsAll;
        $data['the_dataOther'] = $resultsOther;
        $data['sdcSelect'] = $post_data['sdc'];
        $data['dtStart'] = $post_data['dtStart'];
        $data['dtEnd'] = $post_data['dtEnd'];
        return View::make('traffic.api.xl_delivery_report_api', $data);
    }


    public function tsel_delivery_report()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        //$data['services'] = MainServiceConfig::getAllTelco();
        return View::make('traffic.tsel_delivery_report', $data);
    }

    public function tsel_delivery_report_action()
    {
        $post_data = Input::get();
        $x10 = "concat(round(((`dr_1:0`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_10_p";
        $x321 = "concat(round(((`dr_3:21`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_321_p";
        $x327 = "concat(round(((`dr_3:27`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_327_p";
        $x51 = "concat(round(((`dr_5:1`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_51_p";
        $x52 = "concat(round(((`dr_5:2`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_52_p";
        $x6 = "concat(round(((`dr_6`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_6_p";
        $xOthers = "concat(round(((`dr_others`*100)/(`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`)),2),'%') as dr_others_p";
        $xt = "format((`dr_1:0` + `dr_3:21` + `dr_3:27` + `dr_5:1` + `dr_5:2` + `dr_6` + `dr_Others`),0) as total";

        $x10All = "concat(round(((sum(`dr_1:0`)*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_10_p";
        $x321All = "concat(round(((`dr_3:21`*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_321_p";
        $x327All = "concat(round(((`dr_3:27`*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_327_p";
        $x51All = "concat(round(((`dr_5:1`*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_51_p";
        $x52All = "concat(round(((`dr_5:2`*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_52_p";
        $x6All = "concat(round(((`dr_6`*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_6_p";
        $xOthersAll = "concat(round(((`dr_others`*100)/(sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`))),2),'%') as dr_others_p";
        $xtAll = "format((sum(`dr_1:0`) + sum(`dr_3:21`) + sum(`dr_3:27`) + sum(`dr_5:1`) + sum(`dr_5:2`) + sum(`dr_6`) + sum(`dr_Others`)),0) as total";

        $resultsOri = DB::select("SELECT dtreport, telco_sid, format(`dr_1:0`,0) as `dr_10`, $x10, format(`dr_3:21`,0) as `dr_321`, $x321, format(`dr_3:27`,0) as `dr_327`, $x327, format(`dr_5:1`,0)as `dr_51`, $x51, format(`dr_5:2`,0) as `dr_52`, $x52, format(`dr_6`,0) as `dr_6`, $x6, format(`dr_others`,0) as `dr_others`, $xOthers, $xt
                              FROM traffic_rpt.traffic_dr_summary_tsel
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid = 'TXTGENRVASCB_Subs'
                                    AND mt_type = '1'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsRetry = DB::select("SELECT dtreport, telco_sid, format(`dr_1:0`,0) as `dr_10`, $x10, format(`dr_3:21`,0) as `dr_321`, $x321, format(`dr_3:27`,0) as `dr_327`, $x327, format(`dr_5:1`,0)as `dr_51`, $x51, format(`dr_5:2`,0) as `dr_52`, $x52, format(`dr_6`,0) as `dr_6`, $x6, format(`dr_others`,0) as `dr_others`, $xOthers, $xt
                              FROM traffic_rpt.traffic_dr_summary_tsel
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid = 'TXTGENRVASCB_Subs'
                                    AND mt_type = '2'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsPush1 = DB::select("SELECT dtreport, telco_sid, format(`dr_1:0`,0) as `dr_10`, $x10, format(`dr_3:21`,0) as `dr_321`, $x321, format(`dr_3:27`,0) as `dr_327`, $x327, format(`dr_5:1`,0)as `dr_51`, $x51, format(`dr_5:2`,0) as `dr_52`, $x52, format(`dr_6`,0) as `dr_6`, $x6, format(`dr_others`,0) as `dr_others`, $xOthers, $xt
                              FROM traffic_rpt.traffic_dr_summary_tsel
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid = 'TXTGENRVASCB_Subs'
                                    AND mt_type = '3'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $resultsAll = DB::select("SELECT dtreport, telco_sid, format(sum(`dr_1:0`),0) as `dr_10`, $x10All, format(sum(`dr_3:21`),0) as `dr_321`, $x321All, format(sum(`dr_3:27`),0) as `dr_327`, $x327All, format(sum(`dr_5:1`),0)as `dr_51`, $x51All, format(sum(`dr_5:2`),0) as `dr_52`, $x52All, format(sum(`dr_6`),0) as `dr_6`, $x6All, format(sum(`dr_others`),0) as `dr_others`, $xOthersAll, $xtAll
                              FROM traffic_rpt.traffic_dr_summary_tsel
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid = 'TXTGENRVASCB_Subs'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                    GROUP BY dtreport, telco_sid
                                ORDER BY dtreport DESC");

        $resultsOther = DB::select("SELECT dtreport, telco_sid, format(`dr_1:0`,0) as `dr_10`, $x10, format(`dr_3:21`,0) as `dr_321`, $x321, format(`dr_3:27`,0) as `dr_327`, $x327, format(`dr_5:1`,0)as `dr_51`, $x51, format(`dr_5:2`,0) as `dr_52`, $x52, format(`dr_6`,0) as `dr_6`, $x6, format(`dr_others`,0) as `dr_others`, $xOthers, $xt
                              FROM traffic_rpt.traffic_dr_summary_tsel
                                WHERE telco_sdc = '{$post_data['sdc']}'
                                    AND telco_sid = 'TXTGENRVASCB_Subs'
                                    AND mt_type = '4'
                                    AND dtreport between '{$post_data['dtStart']}' AND '{$post_data['dtEnd']}'
                                ORDER BY dtreport DESC");

        $data['the_dataOri'] = $resultsOri;
        $data['the_dataRetry'] = $resultsRetry;
        $data['the_dataPush1'] = $resultsPush1;
        $data['the_dataAll'] = $resultsAll;
        $data['the_dataOther'] = $resultsOther;
        $data['sdcSelect'] = $post_data['sdc'];
        $data['dtStart'] = $post_data['dtStart'];
        $data['dtEnd'] = $post_data['dtEnd'];
        return View::make('traffic.api.tsel_delivery_report_api', $data);
    }


    public function coa_report()
    {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();

        //$role_array = array(15,16);
        //if ( in_array(Auth::user()->id, $role_array) ) {
        //    return View::make('traffic.coa_report', $data);
        //}
        return View::make('traffic.coa_report',$data);

        //return View::make('content.not_admin_dashboard', $data);
    }

    public function coa_report_act($telcoId, $sdc, $serviceId)
    {
        $post_data = Input::get();

        $rs_total_bc = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = '{$post_data['keyword']}'
                                AND dtreport >= '{$post_data['dtStart']} 00:00:00'
                                and dtreport <= '{$post_data['dtEnd']} 23:59:59'");

        $rs_total_reg = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total from (
							       select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
							       where dtregister >= '{$post_data['dtStart']} 00:00:00'
							       and dtregister <= '{$post_data['dtEnd']} 23:59:59'
							       and reg_sms = 'REG {$post_data['keyword']}'
							       union all
							       select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
							       where dtregister >= '{$post_data['dtStart']} 00:00:00'
							       and dtregister <= '{$post_data['dtEnd']} 23:59:59'
							       and reg_sms = 'REG {$post_data['keyword']}'
							       order by date desc
							       ) a
							group by datetime");

        $total_bc = [];
        foreach($rs_total_bc as $value)
        {
            $total_bc[@$value->dtreport] = @$value->total_sent;
        }

        $total_reg = [];
        foreach($rs_total_reg as $value2)
        {
            $total_reg[@$value2->datetime] = @$value2->total;
        }

        $data['total_reg'] = $total_reg;
        $data['total_bc'] = $total_bc;
        $data['sdcSelect'] = $post_data['sdc'];
        $data['dtStart'] = $post_data['dtStart'];
        $data['dtEnd'] = $post_data['dtEnd'];
        $data['telcoId'] = $telcoId;
        return View::make('traffic.api.coa_report_api', $data);
    }


}
