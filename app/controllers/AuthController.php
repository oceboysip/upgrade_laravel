<?php

/**
 * Class AuthController
 *
 * Responsible for handling authentication
 *
 * @author Pandu
 * @since April 17, 2014
 *
 */
class AuthController extends BaseController {

    /**
     * The layout that should be used for responses.
     */
    //protected $layout = 'layouts.master';
    
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct() {
        //currently nothing
    }


	/**
	 * Show login form.
	 *
	 * @return Response
	 */
	public function showLogin() {
		$v = View::make('logins.mainview');
        /*
        if (Session::has('notice')) {
            Session::forget('notice');
        }
        */
        return $v;
	}


	/**
	 * Do authenticate user login
	 *
	 * @return Response
	 */
	public function doLogin() {
        $rules = array(
            'username'    => 'required',
            'password'    => 'Required|AlphaNum|Between:4,20'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
        	Session::flash('message_error', 'Please provide a valid username/password');
            return Redirect::to('login')->withErrors($validator);
        }

        /**
         * NEVER ever do the hashing here
         */
        $pass = Input::get('password');
        $user = array(
            'username' => Input::get('username'),
            'password' => $pass,
            'is_active' => 1
        );

		// flag for remember me features
        $remember_me = Input::get('remember_me');

        /*
         * This is a 'hack' mechanism for not creating user from Laravel framework 'Eloquent
         *
         */
        $old_password = md5(Input::get('password'));
        $checkPassNoEloquent = User::where('password', '=', $old_password)->first();

        // if exist, create new password hash & replace old password
        if($checkPassNoEloquent) {
            Log::warning('Found the password which is NOT to eluoquent');
            $checkPassNoEloquent->PASSWORD = Hash::make($pass);
            $checkPassNoEloquent->save();
        }

        /* DEBUG
        Log::warning('SATU::: ' . $user['username']);
        Log::warning('DUA::: ' . $user['password']);
        $userGet = User::find('admin');
        Log::warning('TIGA::: ' . $userGet);
        */

        // Attempt to login
        if (Auth::attempt($user, $remember_me == "1" ? true : false)) {

			ActivityLog::writeActiviy(Config::get('activity.USER_LOGIN'),'login');

            if(Auth::user()->role_id == 1) {
                return Redirect::to('/admin');
            } else if(Auth::user()->role_id > 1){
				/**
				 * Currently all goes to admin
				 * FIX THIS
				 */
				return Redirect::to('/admin');
                //return Redirect::to('/menu');
            }

        }

        // Authentication failure! lets go back to the login page.
        return Redirect::route('login')
            ->with('message_error', '[Authentication failed] Your username/password combination was incorrect.')
            ->withInput();
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function doLogout(){

		ActivityLog::writeActiviy(Config::get('activity.USER_LOGOUT'), 'logout');

		Auth::logout();
        $cookie = Cookie::forget('authenticated');
        Session::put('notice', 'You have been successfully logged out.');
        return Redirect::to('/login')->withCookie($cookie);
	}


	/**
	 * Check current user login status.
	 *
	 * @return Response
	 */
	public function checkLogin(){
		//
        if (Auth::check()) {
    		return Response::json(['authenticated' => TRUE], 200);
		} else {
			return Response::json(['authenticated' => FALSE], 401);
		}
	}

	/**
	 * Get current user resource.
	 *
	 * @return Response
	 */
	public function currentUser() {
		//
        if (Auth::check()) {
			$current_user = array(
				'id' =>  Auth::user()->id,
				'fullname' =>  Auth::user()->fullname,
				'username' =>  Auth::user()->username,
				'email' =>  Auth::user()->email
			);
    		return Response::json(['current' => $current_user], 200);
		} else {
			return Response::json(['authenticated' => FALSE], 401);
		}
	}
    
    /**
	 * Show the form for forgot password.
	 *
	 * @return Response
	 */
	public function forgotPassword() {
		$this->layout->content = View::make('forgot_password');
	}

	/**
	 * Send the password resest link email to user.
	 *
	 * @return Response
	 */
    public function remindPassword() {
        $credentials = array('email' => Input::get('email'));
        return Password::remind($credentials, function($message, $user) {
            	$message->subject('DNA - Your Password Reminder');
        });
    }
    
    /**
	 * Get the token to reset password.
	 *
	 * @param  string  $token
	 * @return Response
	 */
    /*
    public function resetPassword($token) {
        $this->layout->content = View::make('reset_password')->with('token', $token);
    }
    */


    /**
     * View change password
     */
    public function viewChangePassword() {
        $data = $this->constructMenu();
        return View::make('change_password', $data);
    }


    /**
     * The 'API' call of saving the changed password
     */
    public function apiSaveChangePassword() {

		ActivityLog::writeActiviy(Config::get('activity.USER_CHANGE_ATTEMPT_PASSWORD'), 'logout');

        $oldPass = Input::get("oldPassword");
        $newPassw = Input::get("newPassword");
        $confirmPass = Input::get("confirmPassword");
        try {
            $hashedOldPass = Hash::make($oldPass);
            /*
            echo $hashedOldPass;
            $rec = DB::table('dashboard_user')
                        ->where("username",'=', Auth::user()->username)
                        ->where("password", '=', $hashedOldPass)
                        ->get(array("id"))
                    ;

            */

            $rec = DB::table('dashboard_user')
                ->where("username",'=', Auth::user()->username)
                ->get(array("password"));

            if (!Hash::check($oldPass, $rec[0]->password)) {
                // The passwords not match...
                return Response::json(array('Error' => '1', 'Message' => 'Old password does not match'));
            }

            /*
            if (sizeof($rec) < 1) {
                return Response::json(array('Error' => '1', 'Message' => 'Old password does not match'));
            }
            */

            if ($newPassw != $confirmPass) {
                return Response::json(array('Error' => '1', 'Message' => 'New password does not match'));
            }

            //$checkPassNoEloquent = User::where('password', '=', $old_password)->first();

            /**
             * Update the real password here
             */
            DB::table('dashboard_user')
                ->where("username",'=', Auth::user()->username)
                ->update(array("password" => Hash::make($newPassw)));


			ActivityLog::writeActiviy(Config::get('activity.USER_CHANGE_PASSWORD'), 'logout');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }

    }

}
