<?php

/**
 * Class ContentController
 *
 */

use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;


class WebchatController extends BaseController {

    public function __construct()
    {
        //
    }

    public function index()
    {
		return View::make('webchat.blank');
    }
    
}
