<?php

/**
 * Controller that responsible for the Business Application 
 *
 *
 */
class BusinessApplicationController extends BaseController {

    /**
     * Home for Business application page
     * @return mixed
     */
    public function home() {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $telcoId = Input::get('telcoId');
        if ($telcoId) $data['telcoId'] = $telcoId;
        $givenSdc = Input::get('givenSdc');
        if ($givenSdc) $data['givenSdc'] = $givenSdc;
        return View::make('application.list_application',$data);
    }


    /**
     * The 'API' of list of application.
     */
    public function apiGetListApplication(){
        $data = array();
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        $data['the_data'] = Application::getListApplication($telcoId,$sdc);
        return View::make('application.api.api_list_application', $data);
    }


    /**
     * The 'API' of list of application charging param .
     */
    public function apiGetListChargignParam(){
        $data = array();
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        $data['the_data'] = Application::getListApplication($telcoId, $sdc);
        return View::make('application.api.api_list_application_charging', $data);
    }


    /**
     * View of Create new application
     */
    public function apiViewCreateNewApplication() {
        $telcoId = Input::get('telcoId');
        $sdc = Input::get('sdc');
        $data = array();
        $data['telcoId'] = $telcoId;
        $data['sdc'] = $sdc;
        $serviceIds = array();
        $existingApplication = Application::getListApplication($telcoId, $sdc);
        foreach ($existingApplication as $singleApp) {
            $serviceIds[] = $singleApp->service_id;
        }
        $data['allService'] = GatewayService::getListOfService($telcoId, $sdc);
        //$data['allService'] = GatewayService::getListOfSimpleServiceExcept($telcoId, $sdc,$serviceIds);
        $data['allCharging'] = Charging::getListOfCharging($telcoId, $sdc);
        $data['applicationType'] = Application::getApplicationType();
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        return View::make('application.api.api_create_new_application', $data);
    }


    /**
     * The 'API' of saving the new application
     */
    public function apiSaveNewApplication() {
        try {
            Application::saveApplication(Input::get("applicationName"),Input::get("serviceId"),Input::get("defaultChargingParam"),
                Input::get("applicationType"),Input::get("sdc"),1,Input::get("defaultHeader"),Input::get("pushDayInterval"));

			ActivityLog::writeActiviy(Config::get('activity.BA_CREATE'), '/api/create_new_application');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }

    }



    /**
     * View Edit Application
     * @return mixed
     */
    public function apiViewEditApplication() {
        $applicationId = Input::get('applicationId');
        $telcoId = Input::get('telcoId');
        $sdc = Input::get('sdc');
        $data = array();
        $data['applicationId'] = $applicationId;
        $data['telcoId'] = $telcoId;
        $data['sdc'] = $sdc;
        $data['allService'] = GatewayService::getListOfService($telcoId, $sdc);
        $data['allCharging'] = Charging::getListOfCharging($telcoId, $sdc);
        $application = Application::getById($applicationId);
        $data['application'] = $application[0];
        $data['applicationType'] = Application::getApplicationType();
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telco'] = $qTelco[0]->name;
        return View::make('application.api.api_edit_application', $data);
    }


    /**
     * The 'API' of edit application delegate
     */
    public function apiViewEditApplicationDelegate(){
        $applicationId = Input::get('applicationId');
        $telcoId = Input::get('telcoId');
        $sdc = Input::get('sdc');
        $data = array();
        $data['applicationId'] = $applicationId;
        $data['telcoId'] = $telcoId;
        $data['sdc'] = $sdc;
        //$data['allService'] = GatewayService::getListOfService($telcoId, $sdc);
        //$data['allCharging'] = Charging::getListOfCharging($telcoId, $sdc);
        $application = Application::getById($applicationId);
        $data['application'] = $application[0];
        //$data['applicationType'] = Application::getApplicationType();
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telco'] = $qTelco[0]->name;

        $_appDelegate = Application::getListApplicationDelegate($applicationId);
        if($_appDelegate) {
            $_appDelegate = $_appDelegate[0];
        }
        $data['applicationDelegate'] = $_appDelegate;
        return View::make('application.api.api_edit_delegate', $data);
    }



    /**
     * The 'API' save editted application
     */
    public function apiSaveEditApplication() {
        $applicationId = Input::get('applicationId');
        $applicationName = Input::get('applicationName');
        $defaultHeader = Input::get('defaultHeader');
        //Current implement NOT changing the service_id since its being CRITICAL
        $serviceId = Input::get('serviceId');
        $defaultChargingParam = Input::get('defaultChargingParam');
        $applicationType = Input::get('applicationType');
        $pushDayInterval = Input::get('pushDayInterval');
        $isActive = Input::get('isActive');
        try {
            Application::editApplication($applicationId, $applicationName,
                                        $defaultChargingParam, $applicationType, $isActive, $defaultHeader, $pushDayInterval);

			ActivityLog::writeActiviy(Config::get('activity.BA_EDIT'), '/api/edit_application');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * The 'API' save editted application delegate
     */
    public function apiSaveEditApplicationDelegate() {
        $applicationId = Input::get('applicationId');
        $defaultHeader = Input::get('defaultHeader');
        $pullDelegate = Input::get('pullDelegate');
        $regDoneDelegate = Input::get('regDoneDelegate');
        $regErrorDelegate = Input::get('regErrorDelegate');
        $unregDoneDelegate = Input::get('unregDoneDelegate');
        $unregErrorDelegate = Input::get('unregErrorDelegate');
        $formatterDelegate = Input::get('formatterDelegate');

        try {
            if(Application::isApplicationDelegateExists($applicationId)) {
                Application::editApplicationDelegate($applicationId, $defaultHeader,
                    $regDoneDelegate, $regErrorDelegate, $unregDoneDelegate, $unregErrorDelegate,
                    $formatterDelegate, $pullDelegate);
            } else {
                Application::saveApplicationDelegate($applicationId, $defaultHeader,
                    $regDoneDelegate, $regErrorDelegate, $unregDoneDelegate, $unregErrorDelegate,
                    $formatterDelegate, $pullDelegate);
            }


            ActivityLog::writeActiviy(Config::get('activity.BA_DELEGATE_EDIT'), '/api/edit_application_delegate');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }



    /**
     * The 'API' view list of charging for given application
     *
     */
    public function apiGetListApplicationCharging() {
        $data = array();
        $applicationId = Input::get('applicationId');
        $applicationName = Input::get('applicationName');
        $telcoId = Input::get('telcoId');
        $sdc = Input::get('sdc');
        $chargingParams = Application::getApplicationChargingParam($applicationId);
        if (sizeof($chargingParams) > 0) {
            $data['chargingParam'] = $chargingParams[0];
        }
        $data['telcoId'] = $telcoId;
        $data['sdc'] = $sdc;
        $data['allCharging'] = Charging::getListOfCharging($telcoId, $sdc);
        $data['applicationId'] = $applicationId;
        $data['applicationName'] = $applicationName;
        return View::make('application.api.api_list_charging_param', $data);
    }


    /**
     * The 'API' get charging for given application
     *
     */
    public function apiGetApplicationCharging() {
        return json_encode(Application::getApplicationChargingParam(Input::get('applicationId')));
    }


    /**
     * The 'API' to save application charging settings
     */
    public function apiSaveEditApplicationCharging() {
        $applicationId = Input::get('applicationId');
        $telcoId = Input::get('telcoId');
        try {
            if (Application::isApplicationChargingParamExists($telcoId, $applicationId)) {
                Application::editApplicationChargingParam($applicationId, $telcoId,
                    Input::get("reg"), Input::get("regError"), Input::get("unreg"), Input::get("unregError"),
                    Input::get("pull"), Input::get("pullError")
                );

            } else {
                Application::saveApplicationChargingParam($applicationId, $telcoId,
                        Input::get("reg"), Input::get("regError"),Input::get("unreg"), Input::get("unregError"),
                        Input::get("pull"), Input::get("pullError")
                );
            }
            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }



}
