<?php

/**
 * Class ContentController
 *
 */
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

class AbuseController extends BaseController {
    
    protected $montessori_db;

    public function __construct()
    {
        $this->montessori_db = Config::get('reporting.database_application');
    }

    public function redirect()
    {
        return Redirect::to('/monitoring/abuse/user');

    }

    public function user(){
        
        $limitpage = 20;
        $data = $this->constructMenu();
        //if ((!empty(Input::get('dtStart'))) && (!empty(Input::get('dtEnd'))))
        if ((Input::get('dtStart') <> "") && (Input::get('dtEnd') <> ""))
            
        {
            $akhir = date('Y-m-d', strtotime("+1 day", strtotime(Input::get('dtEnd'))));
            //echo "+1 = " . $akhir;

            $data['range'] = "[".Input::get('dtStart') ."  -  ".Input::get('dtEnd')."] ".Input::get('searchName');
            
            $data['results'] = DB::connection('montessori')
                ->table('kepo_report_user')
                ->select('*')
                ->whereDate('created_at','>=',Input::get('dtStart'))
                ->whereDate('created_at','<',$akhir)
                ->whereRaw("(pelapor_name like '%".Input::get('searchName')."%' or terlapor_name like '%".Input::get('searchName')."%')")
                ->orderBy('created_at', 'desc')
                ->orderBy('n_status', 'asc')
                ->paginate($limitpage);
        }
        else
        {
            //echo "xxxxxx";
            $data['range'] = " (".Input::get('searchName').")";
            $data['results'] = DB::connection('montessori')
                ->table('kepo_report_user')
                ->select('*')
                ->whereDate('created_at','>=',Carbon::today()->subDays(30))
                ->whereRaw("(pelapor_name like '%".Input::get('searchName')."%' or terlapor_name like '%".Input::get('searchName')."%')")
                ->orderBy('created_at', 'desc')
                ->orderBy('n_status', 'asc')
                ->paginate($limitpage);
        }

        $data['ref'] = \Request::getRequestUri();
        return View::make('abuse.index_all', $data);
		
    }

    public function detail_user($id)
    {
        $data = $this->constructMenu();
        
        
        $abuse = KepoReportUser::find($id);

        if ($abuse)
        {
            $data['id'] = $abuse->id;
            $data['pelapor_id'] = $abuse->pelapor_id;
            $data['pelapor_name'] = $abuse->pelapor_name;
            $data['pelapor_status'] = $abuse->pelapor_status;
            $data['pelapor_avatar'] = $abuse->pelapor_avatar;

            $data['terlapor_id'] = $abuse->terlapor_id;
            $data['terlapor_name'] = $abuse->terlapor_name;
            $data['terlapor_status'] = $abuse->terlapor_status;
            $data['terlapor_avatar'] = $abuse->terlapor_avatar;

            $data['category'] = $abuse->category;
            $data['chat_image_url'] = $abuse->chat_image_url;
            $data['reason'] = $abuse->reason;
            $data['quote'] = $abuse->quote;
            $data['tindak_lanjut'] = $abuse->tindak_lanjut;
            $data['n_status'] = $abuse->n_status;
            $data['created_at'] = $abuse->created_at;
            $data['updated_at'] = $abuse->updated_at;
            $data['ref'] = Input::get('ref');
            $data['refdet'] = \Request::getRequestUri();

            if ($abuse->category  <= 3)
            {
                return View::make('abuse.detail_user', $data);
            }
            elseif ($abuse->category  < 10)
            {
                return View::make('abuse.detail_chat', $data);
            }
            else
            {
                return View::make('abuse.detail_content', $data);
            }
            
        }
        else
        {
            return Redirect::to('/monitoring/abuse/user')->with('err', 'Data not found.');
        }
    }

    public function process($id)
    {
        /*
         * curl "http://103.58.100.37/appgw/internal/deleteUserKepo.php?msisdn=6285810908909&type=1" 
         * 1 = Delete User 
         * 2 = Blacklist user 
         * 3 = Unblacklist user 
         * 4 = undelete user
         * 
         * curl "http://103.58.100.37/appgw/internal/deleteProfilePict.php?msisdn=6285810908909"
         */
        
        $ret = 'err';
        $message = "";
        $debug = "";
        $url = "";

        $ref =  Input::get('ref');
        $refdetail =  Input::get('refdetail');

        $abuse = KepoReportUser::find($id);

        //Untuk pelaporan single (tidak ada orang/terlapor yang dilaporkan)
        if (Input::get('btnJustClose'))
        {
            $ret = "success";
            $message = "Laporan diupdate.";
        }
        else
        {
            //Update status report => DONE
            $user = UmbUser::find($abuse->terlapor_id);

            if(!$user)
            {
                echo "<p>DATA USER NOT FOUND";
                $ret = "err";
                $message = "UserID tidak ditemukan";
            }
            else
            {
                $msisdn = $user->msisdn;

                echo "<p>USER FOUND:".$msisdn;

                if (Input::get('btnBlacklist'))
                {
                    ActivityLog::writeActiviy(Config::get('activity.BLACKLIST_USER'), 'abuse:'.$msisdn);

                    $url = "http://103.58.100.37/appgw/internal/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=2";
                    //$url =  "http://umb-api-dummy.test/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=2";

                    $message = "Terlapor ".$abuse->terlapor_id." ($msisdn) telah dimasukkan kedalam BLACKLIST.";
                }
                elseif (Input::get('btnDelete'))
                {
                    //PROCESS API
                    ActivityLog::writeActiviy(Config::get('activity.DELETE_USER'), 'abuse:'.$msisdn);
                    
                    $url = "http://103.58.100.37/appgw/internal/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=1";
                    //$url =  "http://umb-api-dummy.test/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=1";

                    $message = "Terlapor ".$abuse->terlapor_id." ($msisdn) telah dihapus dari UMB.";
                    
                }
                elseif (Input::get('btnDeleteProfile'))
                {
                    //PROCESS API
                    ActivityLog::writeActiviy(Config::get('activity.DELETE_PROFILE'), 'abuse:'.$msisdn);
                    
                    $url =  "http://103.58.100.37/appgw/internal/deleteProfilePict.php?id=".$abuse->terlapor_id ."&type=1";
                    //$url =  "http://umb-api-dummy.test/deleteProfilePict.php?id=".$abuse->terlapor_id ."&type=1";
                    
                    $message = "Avatar terlapor ".$abuse->terlapor_id." ($msisdn) telah dihapus.";
                }
                elseif (Input::get('btnUnDelete'))
                {
                    //PROCESS API
                    ActivityLog::writeActiviy(Config::get('activity.UNDELETE_USER'), 'abuse:'.$msisdn);

                    $url =   "http://103.58.100.37/appgw/internal/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=4";
                    //$url =   "http://umb-api-dummy.test/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=1";
                        
                    $message = "Terlapor ".$abuse->terlapor_id." ($msisdn) telah diaktifkan kembali dari UMB.";
                }
                elseif (Input::get('btnUnBlacklist'))
                {
                    //PROCESS API
                    ActivityLog::writeActiviy(Config::get('activity.UNBLACKLIST_USER'), 'abuse:'.$msisdn);
                    
                    $url =  "http://103.58.100.37/appgw/internal/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=3";
                    //$url =  "http://umb-api-dummy.test/deleteUserKepo.php?id=".$abuse->terlapor_id ."&type=3";
                    
                    $message = "Terlapor ".$abuse->terlapor_id." ($msisdn) telah dikeluarkan dari BLACKLIST.";
                }

                if (!empty($url))
                {
                    echo "URL OK";
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => false,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/x-www-form-urlencoded",
                            "cache-control: no-cache"
                        ),
                    ));
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);    
                    if ($err) {
                        $ret = "err";
                        $message = "Error API. ";
                        $debug = $err;
                    } else {
                        $ret = "success";
                    }

                }
                
            }
        }

        
        echo "<p>DONE";

        $abuse->n_status = 2;
        $abuse->tindak_lanjut = Input::get('tindak_lanjut');
        $abuse->save();

        return Redirect::to($ref)->with($ret, $message."<br>".$debug);
        //return Redirect::to($refdetail)->with($ret, $message."<br>".$debug);
        // echo "<p>refdetail=".$refdetail;
        // echo "<p>ref=".$ref;
        // echo "<p>message=".$message;
        
    }

    public function avatar()
    {
        $limitpage = 20;
        $data = $this->constructMenu();
        
        
        if (Input::get('searchName') <> "")    
        {
            $data['range'] = "[".Input::get('searchName')."]";
            
            $data['results'] = UmbUser::where("is_monaco_user",1)
                                ->WhereNotNull("avatar")
                                ->whereRaw("(name like '%".Input::get('searchName')."%' or msisdn like '%".Input::get('searchName')."%')")
                                ->orderBy("updated_at", "desc")
                                ->paginate($limitpage);
        }
        else
        {
            //echo "xxxxxx";
            $data['range'] = " ".Input::get('searchName')." ";
            $data['results'] = UmbUser::where("is_monaco_user",1)
                                ->WhereNotNull("avatar")
                                ->orderBy("updated_at", "desc")
                                ->paginate($limitpage);
        }

        $data['ref'] = \Request::getRequestUri();
        return View::make('abuse.index_avatar', $data);
    }

    public function avatar_del($id)
    {
        $user = UmbUser::find($id);
        $refdetail =  Input::get('ref');
        echo "refdetail= $refdetail";

        $ret = 'err';
        $message = "";
        $debug = "";

        if(!$user)
        {
            $ret = "err";
            $message = "UserID tidak ditemukan";
        }
        else
        {
            $url =  "http://103.58.100.37/appgw/internal/deleteProfilePict.php?id=".$id;
            //$url =  "http://umb-api-dummy.test/deleteProfilePict.php?id=".$id;
                    
            $message = "Avatar [".$user->name."] telah dihapus.";

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => false,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "cache-control: no-cache"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);    
            if ($err) {
                $ret = "err";
                $message = "Error API. ";
                $debug = $err;
            } else {
                $ret = "success";
            }

        }
        
        

        return Redirect::to($refdetail)->with($ret, $message."<br>".$debug);
    }


    public function rejected_avatar()
    {
        $limitpage = 20;
        $data = $this->constructMenu();
        
        
        if (Input::get('searchName') <> "")    
        {
            $data['range'] = "[".Input::get('searchName')."]";
            
            $data['results'] = RejectedAvatar::whereRaw("(msisdn like '%".Input::get('searchName')."%' or user_name like '%".Input::get('searchName')."%')")
                ->orderBy("n_status", "asc")                    
                ->orderBy("updated_at", "desc")
                ->paginate($limitpage);
        }
        else
        {
            //echo "xxxxxx";
            //$data['range'] = " ".Input::get('searchName')." ";
            $data['range'] = "";
            $data['results'] = RejectedAvatar::orderBy("n_status", "asc")                    
                ->orderBy("updated_at", "desc")
                ->paginate($limitpage);
        }

        $data['ref'] = \Request::getRequestUri();
        return View::make('abuse.index_reject', $data);
    }

    public function rejected_avatar_ok($id)
    {
        //Approve = http://172.16.69.1/internal/deleteProfilePict_v2.php?id=2066488&type=2&avatar=87139ccf-d759-44d8-83f4-3b16f51a78c5
        $ref =  Input::get('ref');

        $data = RejectedAvatar::find($id);
        $data->n_status = 2;
        $data->save();

        $urls = explode("/",$data->user_avatar);
        $files = explode(".",$urls[count($urls) - 1]);

        // echo "rejected_avatar_ok<p>";
        // echo "ID = " . $data->id . "<br>";
        // echo "user_avatar = " . $data->user_avatar . "<br>";

        // echo "<pre>";
        // print_r($urls);
        // echo "</pre>";

        // echo "<pre>";
        // print_r($files);
        // echo "</pre>";
        // echo "<p>yang diupdate: " . $files[0] . "<p>";

        if ($files[0] <> null)
        {
            //echo "<br>"."http://172.16.69.1/internal/deleteProfilePict_v2.php?id=".$data->user_id."&type=2&avatar=".$files[0]."<br>";
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://172.16.69.1/internal/deleteProfilePict_v2.php?id=".$data->user_id."&type=2&avatar=".$files[0],
                CURLOPT_RETURNTRANSFER => false,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "cache-control: no-cache"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);    
            if ($err) {
                //echo "error: $err";
                return Redirect::to($ref)->with('err', 'Failed update avatar user ' . $data->msisdn . ' ['.$data->user_id.']. <br>API error: ' . $err);
            } else {
                $ret = "success";
                //echo "CURL OK";
                return Redirect::to($ref)->with('success', 'Avatar user ' . $data->msisdn . ' ['.$data->user_id.']  updated!');
            }
        }
        else
        {
            //echo "Image URL not found!";
            return Redirect::to($ref)->with('error', 'Image URL not found!');
        }
    }

    public function rejected_avatar_reject($id)
    {
        $ref =  Input::get('ref');

        $data = RejectedAvatar::find($id);
        $data->n_status = 2;
        $data->save();
        echo "rejected_avatar_reject<p>";
        echo "ID = " . $data->id . "<br>";
        echo "user_avatar = " . $data->user_avatar . "<br>";
        echo "ref = " . $ref . "<br>";

        return Redirect::to($ref)->with('success', 'Close report [msisdn=' . $data->msisdn . '; ID='.$data->user_id.'] DONE');
    }
}
