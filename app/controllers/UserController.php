<?php

/**
 * Class UserController
 *
 */
class UserController extends BaseController {


    /**
     * Show charging 'home' screen of user controller
     * @return mixed
     */
    public function home(){
        $data = $this->constructMenu();
        return View::make('user.list_user', $data);
    }


    /**
     * The 'API' call, show list of users
     */
    public function apiGetListUser() {
        $data = array();

        //if (!Cache::has('users')) { //if the users has not already been cached
            $users = DB::table('dashboard_user')
                ->join('dashboard_role', 'dashboard_user.role_id', '=', 'dashboard_role.id')
                ->get(array('dashboard_user.id as id', 'username', 'fullname', 'role_name',
                            'dashboard_role.id as role_id', 'email', 'is_active'));

            $users_data = $users;
        //    Cache::put('users', $users_data, 10); //put the users into the cache for 10 minutes
        //} else { //if the users are already in the cache
        //    $users_data = Cache::get('users'); //just get the users from the cache
        //}

        $data['list'] = $users_data;
        return View::make('user.api.api_list_user', $data);
    }


	/**
	 * The 'API' call, show detail of a user
	 * @param $userId
	 */
	public function apiViewEditUser($userId) {
		$data = array();
		$user = DB::table('dashboard_user')
			->join('dashboard_role', 'dashboard_user.role_id', '=', 'dashboard_role.id')
			->where('dashboard_user.id','=',$userId)
			->get(array('dashboard_user.id as id', 'username', 'fullname', 'role_name',
				'dashboard_role.id as role_id', 'email', 'is_active'));

		if ($user && sizeof($user) > 0) {
			$data['user'] = $user[0];
		}

		if (!Cache::has('roles')) { //if the users has not already been cached
			$users = DB::table('dashboard_role')->get(array('id', 'role_name'));

			$users_data = $users;
			Cache::put('roles', $users_data, 10); //put the users into the cache for 10 minutes
		} else { //if the users are already in the cache
			$users_data = Cache::get('roles'); //just get the users from the cache
		}
		$data['roles'] = $users_data;
		return View::make('user.api.api_edit_user', $data);
	}



    /**
     * The 'API' call, show list of role
     */
    public function apiGetListRole() {
        $data = array();

        if (!Cache::has('roles')) { //if the users has not already been cached
            $users = DB::table('dashboard_role')->get(array('id','role_name'));

            $users_data = $users;
            Cache::put('roles', $users_data, 10); //put the users into the cache for 10 minutes
        } else { //if the users are already in the cache
            $users_data = Cache::get('roles'); //just get the users from the cache
        }

        $data['list'] = $users_data;
        return View::make('user.api.api_list_role', $data);
    }


    /**
     * The 'API' call for getting menus granted from Role
     * @return mixed
     */
    public function apiGetListRoleMenu() {
        $data = array();
        $roleId = Input::get("roleId");
        //if (!Cache::has('role_menus')) {
            $rms = DB::table('dashboard_role')
                ->join('dashboard_role_menu', 'dashboard_role.id', '=', 'dashboard_role_menu.role_id')
                ->join('dashboard_menu', 'dashboard_menu.id', '=', 'dashboard_role_menu.menu_id')
                ->where('dashboard_role.id', $roleId)
                //->take(1)
                ->get(array('dashboard_role.id', 'role_name', 'dashboard_menu.title', 'dashboard_menu.id as menu_id'));

            $users_data = $rms;
            /*
            if (empty($roleName) && sizeof($rms) > 0) {
                $roleName = $rms[0]->role_name;
            }
            */
        //    Cache::put('role_menus', $users_data, 10);
        //} else {
        //    $users_data = Cache::get('role_menus');
        //}

        $roleNameRec = DB::table('dashboard_role')->where('id', $roleId)->get(array('role_name'));
        $roleName = $roleNameRec[0]->role_name;

        $otherMenu = DB::select("SELECT id, title
                                 FROM dashboard_menu
                                 WHERE NOT EXISTS (SELECT 1
                                     FROM dashboard_role_menu r WHERE r.role_id = ? AND r.menu_id = dashboard_menu.id)",
                        array($roleId));

        $data['roleName'] = $roleName;
        $data['list'] = $users_data;
        $data['roleId'] = $roleId;
        $data['otherMenu'] = $otherMenu;
        return View::make('user.api.api_list_role_menu', $data);
    }


    /**
     * Remove Menu from Role
     */
    public function apiRemoveRoleMenu() {
        try {
            DB::table('dashboard_role_menu')
                ->where('role_id', '=', Input::get("roleId"))
                ->where('menu_id', '=', Input::get("menuId"))
                ->delete();

			ActivityLog::writeActiviy(
				Config::get('activity.ROLE_REMOVE_MENU'), '/api/remove_menu_from_role',
				"Remove this map, roleId: " . Input::get("roleId") . ", menuId: " . Input::get("menuId")
			);

			/**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * Add new menu for Role
     */
    public function apiSaveRoleMenu() {
        try {
            DB::table('dashboard_role_menu')
                ->insert(array('role_id' => Input::get("roleId"), 'menu_id' => Input::get("menuId")));

			ActivityLog::writeActiviy(
				Config::get('activity.ROLE_ADD_MENU'), '/api/save_menu_role',
				"Add this map, roleId: " . Input::get("roleId") . ", menuId: " . Input::get("menuId")
			);


			/**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * The 'API' view the create new user
     */
    public function apiViewCreateNewUser() {
        $data = array();
        $data['roles'] = DB::table('dashboard_role')->get(array('id', 'role_name'));
        return View::make('user.api.api_create_new_user', $data);
    }


    /**
     * The 'API' save new user
     */
    public function apiSaveNewUser() {
        $realPass = Input::get("password");
        $confirmPass = Input::get("confirmPassword");
        if ($realPass != $confirmPass) {
            return Response::json(array('Error' => '1', 'Message' => "Password not match"));
        }

        try {
            $pass = Hash::make($realPass);
            DB::table('dashboard_user')
                ->insert(array('username' => Input::get("username"),'fullname' => Input::get("name"),
                                'is_active' => 1,
                                'email' => Input::get("email"), 'role_id' => Input::get("roleId"), 'password' => $pass)
                );

			ActivityLog::writeActiviy(
				Config::get('activity.USER_CREATE'), '/api/create_new_user',
				"Add this user: " . Input::get("username") . ", email: " . Input::get("email")
			);


			/**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


	/**
	 * The 'API' edit user
	 */
	public function apiSaveEditUser() {
		try {
			DB::update("UPDATE dashboard_user SET fullname = ?, email = ?, role_id= ?, is_active = ? WHERE id = ?",
						array(Input::get("name"),Input::get("email"),
							Input::get("roleId"), Input::get("isActive"), Input::get('userId')));

			ActivityLog::writeActiviy(
				Config::get('activity.USER_EDIT'), '/api/edit_user',
				"Edit user with id: " . Input::get("userId") . ", name: " . Input::get("name"). ", email: " . Input::get("email")
							. ", roleId: " . Input::get("roleId") . ", is_active: " . Input::get("isActive")
			);

			/**
			 * Simply 'OK'
			 */
			return Response::json(array('OK' => '1'));
		} catch (Exception $e) {
			return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
		}
	}



    /**
     * Store a newly created user in database.
     *
     * @return Response
     */
    public function doSignup()
    {
        $data_error = '';

        $rules = array(
            'username' => 'Required|Min:3|Max:200|AlphaNum|unique:users,username',
            'fullname' => 'Required|Min:3',
            'email' => 'Required|Email|unique:users,email',
            'password' => 'Required|AlphaNum|Between:8,20'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('message_error', 'true');
            return Redirect::to('signup')->withErrors($validator)->withInput(Input::except('password'));
        }

        $user = new User;
        $user->username = Input::get('username');
        $user->fullname = Input::get('fullname');
        $user->password = Hash::make(Input::get('password'));
        $user->email = Input::get('email');
        $user->save();

        return $this->layout->content = View::make('signup');
    }


}
