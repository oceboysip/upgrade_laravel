<?php

const TELCO_ISAT_ID = 2;

/**
 * Class IndosatController
 *
 * Responsible for handling the General Indosat Configs
 *
 */
class IndosatController extends BaseController {

	/**
	 * The SDM Home
	 */
	public function sdmHome() {
		$data = $this->constructMenu();
		$data['sdc'] = MainServiceConfig::getAllSdc();
		$data['sdm_code'] = Indosat::getAllSdmCode(0,0);
		return View::make('indosat.isat_sdm', $data);
	}

	/**
	 * Get list of active services
	 *
	 * @param string $sdc
	 * @param int $serviceId
	 * @return string in JSON
	 */
    public function apiGetSdmCodeList($sdc = '', $serviceId = 0) {
		$data = array();
		$data['the_data'] = Indosat::getAllSdmCode($sdc, $serviceId);
		return View::make('indosat.api_list_isat_sdm_code',$data);
    }


    /**
     * Get list of active services
     *
     * @return string in JSON
     */
    public function apiGetAllSdmList() {
        //return json_encode(Indosat::getAllSdmCode());
    }


	/**
	 * API create SDM Code
	 *
	 */
	public function apiCreateSdmCode() {
		$serviceId = Input::get('serviceId');
		$sdc = Input::get('sdc');
		//Indosat
		$telcoId = TELCO_ISAT_ID;
		$keyword = Input::get('keyword');
		$sdmCode = Input::get('sdmCode');
		//Always trimmed and make it lower
		$keyword = strtolower(trim($keyword));

		try {
			if (Indosat::isSdmCodeExists($sdc, $keyword,$serviceId)) {
				return Response::json(array('Error' => '1',
					'Message' => "SDM Code for Keyword: {$keyword}, serviceId: {$serviceId}, sdc = {$sdc} already exists"));
			}

			Indosat::saveSdmCode($serviceId, $sdc, $keyword, $sdmCode);

			ActivityLog::writeActiviy(
				Config::get('activity.ISAT_CREATE_SDM_CODE'),
				'/api/isat/create_new_sdm_code',
				"Add SDM Code: {$sdmCode} for service for Id: {$serviceId}, Keyword: {$keyword}, SDC: {$sdc}"
			);

			/**
			 * Simply 'OK'
			 */
			return Response::json(array('OK' => '1'));
		} catch (Exception $e) {
			return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
		}
	}


	/**
	 * Remove SDM Code
	 * @return mixed
	 */
	public function apiRemoveSdmCode() {
		$serviceId = Input::get('serviceId');
		$sdc = Input::get('sdc');
		//Indosat
		$telcoId = TELCO_ISAT_ID;
		$keyword = Input::get('keyword');
		$sdmCode = Input::get('code');
		//Always trimmed and make it lower
		$keyword = strtolower(trim($keyword));

		try {
			Indosat::removeSdmCode($sdmCode, $serviceId, $sdc, $keyword);

			ActivityLog::writeActiviy(
				Config::get('activity.ISAT_REMOVE_SDM_CODE'),
				'/api/isat/remove_sdm_code',
				"Remove SDM Code: {$sdmCode} for service for Id: {$serviceId}, Keyword: {$keyword}, SDC: {$sdc}"
			);

			/**
			 * Simply 'OK'
			 */
			return Response::json(array('OK' => '1'));
		} catch (Exception $e) {
			return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
		}
	}
}

