<?php

/**
 * Class ChargingController
 * Responsible for the manage of charging
 *
 */
class ChargingController extends BaseController {

    /**
     * Show charging 'home' screen
     * @return mixed
     */
    public function home() {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $telcoId = Input::get('telcoId');
        if ($telcoId) $data['telcoId'] = $telcoId;
        $givenSdc = Input::get('givenSdc');
        if ($givenSdc) $data['givenSdc'] = $givenSdc;
        return View::make('charging.list_charging',$data);
    }


    /**
     * The 'API' of list of charging.
     */
    public function apiGetListCharging() {
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        //return json_encode(Charging::getListOfCharging($telcoId, $sdc));
        $data = array();
        //$data['header'] = $data['the_data']
        $data['the_data'] = Charging::getListOfCharging($telcoId, $sdc);
        return View::make('charging.api.api_list_charging', $data);
    }


    /**
     * The 'API' of creating new charging.
     */
    public function apiViewCreateNewCharging() {
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        $data = array();
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        return View::make('charging.api.api_create_new_charging', $data);
    }


    /**
     * Save to database
     */
    public function apiSaveNewCharging() {
        $telcoId = Input::get('telcoId');
        $telcoSid = Input::get('telcoSid');
        $sdc = Input::get('sdc');
        $chargingCode = Input::get('chargingCode');
        $euPrice = Input::get('euPrice');
        $cpPrice = Input::get('cpPrice');
        $costBearer = Input::get('costBearer');
        $description = Input::get('description');

        try {
            $theRoutingId = Charging::saveCharging($telcoId,$telcoSid,$sdc,$cpPrice,$euPrice,$costBearer,$description);
            Log::info("Adding new charging for TelcoId: {$telcoId}, ".
                "Sid: {$telcoSid}, SDC:  {$sdc}, CPPrice: {$cpPrice}, EUPrice: {$euPrice}, CostBearer: {$costBearer}, ".
                "Description: {$description} With RoutingId: {$theRoutingId}"
            );
            //$theRoutingId = 21751;
            Charging::saveChargingRouting($theRoutingId, $telcoId, $chargingCode);

			ActivityLog::writeActiviy(Config::get('activity.CHARGING_CREATE'), '/api/create_new_charging');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     *
     * @return mixed
     */
    public function apiViewEditCharging() {
        $chargingId = Input::get('chargingId');
        $chargingCode = Input::get('code');
        $charging = Charging::getChargingById($chargingId);
        $data = array();
        $data['chargingId'] = $chargingId;
        $data['chargingCode'] = $chargingCode;
        $data['telco'] = '--';
        if (sizeof($charging) > 0) {
            $data['charging'] = $charging[0];
            $qTelco = MainServiceConfig::getTelcoById($charging[0]->telco_id);
            $data['telco'] = $qTelco[0]->name;
        }
        return View::make('charging.api.api_edit_charging', $data);
    }


    /**
     * Save EDIT to database
     */
    public function apiSaveEditCharging()
    {
        $chargingId = Input::get('chargingId');
        //Not use
        //$chargingCode = Input::get('chargingCode');
        $euPrice = Input::get('euPrice');
        $cpPrice = Input::get('cpPrice');
        $costBearer = Input::get('costBearer');
        $description = Input::get('description');
        $isActive = Input::get('isActive');
        $telcoSid = Input::get('telcoSid');

        try {
            //saveEditCharging($chargingId, $telcoSid, $cpPrice, $euPrice, $costBearer, $descr, $isctive)
            Charging::saveEditCharging($chargingId, $telcoSid, $cpPrice, $euPrice, $costBearer, $description, $isActive);

			ActivityLog::writeActiviy(Config::get('activity.CHARGING_EDIT'), '/api/edit_charging');

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }

}
