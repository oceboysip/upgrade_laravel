<?php

/**
 * Class ContentController
 *
 */
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

class MonitoringController extends BaseController {


    protected $reporting;
    public function __construct()
    {
        $this->reporting = Config::get('reporting.database_reporting');
    }

    public function index(){
        $data = $this->constructMenu();
        
        $results = DB::select("SELECT m.server_name, r.serverid,FORMAT(100-r.avg_cpu,1) AS cpu,
                                    IF(r.io_hd2=0.0,(r.io_hd1),((r.io_hd1+r.io_hd2)/2)) AS io_disk,
                                    ((r.used_ram/r.total_ram)*100) AS ram_pct,r.used_ram,r.total_ram,
                                    (((r.slash_used+r.slashdata_used+r.slashbackup_used)/(r.slash_size+r.slashdata_size+r.slashbackup_size))*100) AS disk_used_pct,
                                    (r.slash_avail+r.slashdata_avail+r.slashbackup_avail) AS disk_free,

                                    (((r.slash_used)/(r.slash_size))*100) AS disk_slash_pct,
                                    (r.slash_avail) AS disk_slash_free,
                                    
                                    (((r.slashdata_used)/(r.slashdata_size))*100) AS disk_data_pct,
                                    (r.slashdata_avail) AS disk_data_free,
                                    
                                    (((r.slashbackup_used)/(r.slashbackup_size))*100) AS disk_backup_pct,
                                    (r.slashbackup_avail) AS disk_backup_free

                                FROM {$this->reporting}.report_system_srvkmi r
                                INNER JOIN {$this->reporting}.server_map m ON m.id=r.serverid 
                                WHERE r.dt_create>=(SELECT DATE_FORMAT((SELECT MAX(dt_create) FROM {$this->reporting}.report_system_srvkmi),'%Y-%m-%d %H:%i'))
                                ORDER BY 
                                ram_pct DESC, disk_used_pct DESC, cpu DESC");

        $data['results'] = $results;
		return View::make('monitoring.server_list', $data);
    }

    public function server_chart($serverid,$tipe)
    {
        $tipe = intval($tipe);
        if ($tipe <= 0)
        {
            return Redirect::to('/monitoring/server');
        }

        $data = $this->constructMenu();
    
        $data['captions'] = ($tipe <= 1) ? '(Last 24 Hour)' : '(Last '.$tipe.' days)';
        $data['serverid'] = $serverid;

        $nm = DB::select("SELECT server_name FROM {$this->reporting}.server_map WHERE id=$serverid")[0];
        $data['server_name']  = $nm->server_name;

        $results = DB::select("SELECT UNIX_TIMESTAMP(dt_create) AS dt_create, 
                        (100-avg_cpu) AS avg_cpu,
                        total_ram, used_ram,
                        (slash_size+slashdata_size+slashbackup_size) AS disk_size,
                        (slash_used+slashdata_used+slashbackup_used) AS disk_used,
                        IF(io_hd2=0.0,(io_hd1),((io_hd1+io_hd2)/2)) AS io_disk
                    FROM {$this->reporting}.report_system_srvkmi 
                    WHERE serverid=$serverid
                    AND dt_create > DATE_SUB(NOW(), INTERVAL $tipe DAY)
                    AND dt_create <= NOW()
                    ORDER BY dt_create ASC");
        
        $mib = "[";$mira = "[";$mirb = "[";$mida = "[";$midb = "[";$mio = "[";
        foreach($results as $dt)
        {
            $mib    .= "[" . $dt->dt_create . "," . $dt->avg_cpu . "],";
            $mira   .= "[" . $dt->dt_create . "," . $dt->total_ram . "],";
            $mirb   .= "[" . $dt->dt_create . "," . $dt->used_ram . "],";
            $mida   .= "[" . $dt->dt_create . "," . $dt->disk_size . "],";
            $midb   .= "[" . $dt->dt_create . "," . $dt->disk_used . "],";
            $mio    .= "[" . $dt->dt_create . "," . $dt->io_disk . "],";
        }
        $data['flot_cpu']       = rtrim($mib,",") . "]";
        $data['flot_ram']       = rtrim($mira,",") . "]";
        $data['flot_ram_used']  = rtrim($mirb,",") . "]";
        $data['flot_disk']      = rtrim($mida,",") . "]";
        $data['flot_disk_used'] = rtrim($midb,",") . "]";
        $data['flot_io']        = rtrim($mio,",") . "]";
        
        $data['labels_cpu']       = "CPU AVG";
        $data['labels_ram']       = "RAM Size";
        $data['labels_ram_used']  = "RAM Used";
        $data['labels_disk']      = "Disk Size";
        $data['labels_disk_used'] = "Disk Free";
        $data['labels_io']        = "IO Disk";

        $result = DB::select("SELECT 
                        UNIX_TIMESTAMP(MAX(dt_create)) AS dt_create_max, 
                        UNIX_TIMESTAMP(MIN(dt_create)) AS dt_create_min,
                        MAX(total_ram) AS dt_ram_max,
                        MAX((slash_size+slashdata_size+slashbackup_size)) AS dt_disk_max
                    FROM {$this->reporting}.report_system_srvkmi 
                    WHERE serverid=$serverid
                    AND dt_create > DATE_SUB(NOW(), INTERVAL $tipe day)
                    AND dt_create <= NOW()
                    ")[0];
        $data['cpu_time_min'] = $result->dt_create_min;
        $data['cpu_time_max'] = $result->dt_create_max;
        $data['ram_usage_min']  = 0;
        if ($result->dt_ram_max <= 1024)
        {
            $data['ram_usage_max']  = $result->dt_ram_max + 128;
            $data['ram_satuan'] = " MB";
            $data['ram_tick'] = 128;
            $data['ram_pembagi'] = 256;
        }
        elseif ($result->dt_ram_max > 10240)
        {
            $data['ram_usage_max']  = $result->dt_ram_max + 4096;
            $data['ram_satuan'] = " GB";
            $data['ram_tick'] = 4096;
            $data['ram_pembagi'] = 4096;
        }
        else
        {
            $data['ram_usage_max']  = $result->dt_ram_max + 1024;
            $data['ram_satuan'] = " GB";
            $data['ram_tick'] = 1024;
            $data['ram_pembagi'] = 1024;
        }
        
        $data['disk_usage_min']  = 0;
        if ($result->dt_disk_max <= 50)
        {
            $data['disk_usage_max']  = $result->dt_disk_max + 4;
            $data['disk_tick'] = 6;
            $data['disk_pembagi'] = 6;
        }
        else
        {
            $data['disk_usage_max']  = $result->dt_disk_max + 10;
            $data['disk_tick'] = 20;
            $data['disk_pembagi'] = 20;
        }
        
        return View::make('monitoring.server_chart', $data);
    }

    public function server_mapping()
    {
        $data = $this->constructMenu();
        
        

		return View::make('monitoring.server_mapping', $data);
    }
}
