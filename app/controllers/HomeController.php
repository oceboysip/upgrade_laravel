<?php

/**
 * Class HomeController, for all 'Home' access
 *
 *
 */
class HomeController extends BaseController
{

    public function showWelcome()
    {
        return View::make('hello');
    }

    public function mainView()
    {

        $data = $this->constructMenu();

        $data['username'] = Auth::user()->username;

        $r = Reporting::getTodayRevenueISAT();
        $data['todayRevISAT'] = $r['gross'];

        $r = Reporting::getTodayRevenueISATPlaymedia();
        $data['todayRevISATPlaymedia'] = $r['gross'];

        $r = Reporting::getTodayRevenueXL();
        $data['todayRevXL'] = $r['gross'];

        $r = Reporting::getTodayRevenueXLPlaymedia();
        $data['todayRevXLPlaymedia'] = $r['gross'];

        $r = Reporting::getTodayRevenueTSEL();
        $data['todayRevTSEL'] = $r['gross'];

        $rm = Reporting::getMonthRevenueISAT(date('Y'), date('m'));
        $data['thisMonthRevISAT'] = $rm['gross'];

        $rm = Reporting::getMonthRevenueISATPlaymedia(date('Y'), date('m'));
        $data['thisMonthRevISATPlaymedia'] = $rm['gross'];

        $rm = Reporting::getMonthRevenueXL(date('Y'), date('m'));
        $data['thisMonthRevXL'] = $rm['gross'];

        $rm = Reporting::getMonthRevenueXLPlaymedia(date('Y'), date('m'));
        $data['thisMonthRevXLPlaymedia'] = $rm['gross'];

        $rm = Reporting::getMonthRevenueTSEL(date('Y'), date('m'));
        $data['thisMonthRevTSEL'] = $rm['gross'];

        $lastMonth = date('Y-m-d', strtotime("-1 months"));

        $lrm = Reporting::getMonthRevenueISAT(date('Y', strtotime($lastMonth)), date('m', strtotime($lastMonth)));
        $data['lastMonthRevISAT'] = $lrm['gross'];

        $lrm = Reporting::getMonthRevenueISATPlaymedia(date('Y', strtotime($lastMonth)), date('m', strtotime($lastMonth)));
        $data['lastMonthRevISATPlaymedia'] = $lrm['gross'];

        $lrm = Reporting::getMonthRevenueXL(date('Y', strtotime($lastMonth)), date('m', strtotime($lastMonth)));
        $data['lastMonthRevXL'] = $lrm['gross'];

        $lrm = Reporting::getMonthRevenueXLPlaymedia(date('Y', strtotime($lastMonth)), date('m', strtotime($lastMonth)));
        $data['lastMonthRevXLPlaymedia'] = $lrm['gross'];

         $lrm = Reporting::getMonthRevenueTSEL(date('Y', strtotime($lastMonth)), date('m', strtotime($lastMonth)));
        $data['lastMonthRevTSEL'] = $lrm['gross'];

        $ry = Reporting::getYesterdayRevenueISAT();
        $data['yesterdayRevISAT'] = $ry['gross'];

        $ry = Reporting::getYesterdayRevenueISATPlaymedia();
        $data['yesterdayRevISATPlaymedia'] = $ry['gross'];

        $ry = Reporting::getYesterdayRevenueXL();
        $data['yesterdayRevXL'] = $ry['gross'];

        $ry = Reporting::getYesterdayRevenueXLPlaymedia();
        $data['yesterdayRevXLPlaymedia'] = $ry['gross'];

        $ry = Reporting::getYesterdayRevenueTSEL();
        $data['yesterdayRevTSEL'] = $ry['gross'];

        $dayBeforeYesterday = date('Y-m-d', strtotime("-2 days"));

        $rdb = Reporting::getRevenueOnDateISAT($dayBeforeYesterday);
        $data['beforeYesterdayRevISAT'] = $rdb['gross'];

        $rdb = Reporting::getRevenueOnDateISATPlaymedia($dayBeforeYesterday);
        $data['beforeYesterdayRevISATPlaymedia'] = $rdb['gross'];

        $rdb = Reporting::getRevenueOnDateXL($dayBeforeYesterday);
        $data['beforeYesterdayRevXL'] = $rdb['gross'];

        $rdb = Reporting::getRevenueOnDateXLPlaymedia($dayBeforeYesterday);
        $data['beforeYesterdayRevXLPlaymedia'] = $rdb['gross'];

        $rdb = Reporting::getRevenueOnDateTSEL($dayBeforeYesterday);
        $data['beforeYesterdayRevTSEL'] = $rdb['gross'];

        $days = array();
        $days[0] = date('j M');
        $days[1] = date('j M', strtotime("-1 days"));
        $days[2] = date('j M', strtotime("-2 days"));
        $days[3] = date('j M', strtotime("-3 days"));
        $days[4] = date('j M', strtotime("-4 days"));
        $days[5] = date('j M', strtotime("-5 days"));
        $days[6] = date('j M', strtotime("-6 days"));
        $days[7] = date('j M', strtotime("-7 days"));
        $days[8] = date('j M', strtotime("-8 days"));
        $days[9] = date('j M', strtotime("-9 days"));
        $days[10] = date('j M', strtotime("-10 days"));
        $days[11] = date('j M', strtotime("-11 days"));
        $days[12] = date('j M', strtotime("-12 days"));
        $days[13] = date('j M', strtotime("-13 days"));
        $days[14] = date('j M', strtotime("-14 days"));
        $days[15] = date('j M', strtotime("-15 days"));
        $days[16] = date('j M', strtotime("-16 days"));
        $days[17] = date('j M', strtotime("-17 days"));
        $days[18] = date('j M', strtotime("-18 days"));
        $days[19] = date('j M', strtotime("-19 days"));
        $days[20] = date('j M', strtotime("-20 days"));
        $days[21] = date('j M', strtotime("-21 days"));
        $days[22] = date('j M', strtotime("-22 days"));
        $days[23] = date('j M', strtotime("-23 days"));
        $days[24] = date('j M', strtotime("-24 days"));
        $days[25] = date('j M', strtotime("-25 days"));
        $days[26] = date('j M', strtotime("-26 days"));
        $days[27] = date('j M', strtotime("-27 days"));
        $days[28] = date('j M', strtotime("-28 days"));
        $days[29] = date('j M', strtotime("-29 days"));
        $days[30] = date('j M', strtotime("-30 days"));

        $label7day = array($days[30], $days[29], $days[28], $days[27], $days[26], $days[25], $days[24], $days[23], $days[22], $days[21], $days[20], $days[19], $days[18], $days[17], $days[16], $days[15], $days[14], $days[13], $days[12], $days[11], $days[10], $days[9], $days[8], $days[7], $days[6], $days[5], $days[4], $days[3], $days[2], $days[1], $days[0]);
        $data['label7day'] = json_encode($label7day);

        $last7DayISAT = Reporting::totalAllRevenueThisWeekISAT();
        $last7DayISATPlaymedia = Reporting::totalAllRevenueThisWeekISATPlaymedia();
        $last7DayXL = Reporting::totalAllRevenueThisWeekXL();
        $last7DayXLPlaymedia = Reporting::totalAllRevenueThisWeekXLPlaymedia();
        $last7DayTSEL = Reporting::totalAllRevenueThisWeekTSEL();

        $rs = array();
        $i = 0;
        foreach ($last7DayISAT as $aday) {
            $r = new RickshawChartPlot;
            $r->x = strtotime("-" . (6 - $i) . "days");
            $r->y = $aday;
            $rs[] = $r;
            $i++;
        }

        $data['dataLast7DaysISAT'] = json_encode($rs);

        $rs = array();
        $i = 0;
        foreach ($last7DayISATPlaymedia as $aday) {
            $r = new RickshawChartPlot;
            $r->x = strtotime("-" . (6 - $i) . "days");
            $r->y = $aday;
            $rs[] = $r;
            $i++;
        }

        $data['dataLast7DaysISATPlaymedia'] = json_encode($rs);

        $rs = array();
        $i = 0;
        foreach ($last7DayXL as $aday) {
            $r = new RickshawChartPlot;
            $r->x = strtotime("-" . (6 - $i) . "days");
            $r->y = $aday;
            $rs[] = $r;
            $i++;
        }

        $data['dataLast7DaysXL'] = json_encode($rs);

        $rs = array();
        $i = 0;
        foreach ($last7DayXLPlaymedia as $aday) {
            $r = new RickshawChartPlot;
            $r->x = strtotime("-" . (6 - $i) . "days");
            $r->y = $aday;
            $rs[] = $r;
            $i++;
        }

        $data['dataLast7DaysXLPlaymedia'] = json_encode($rs);

        $rs = array();
        $i = 0;
        foreach ($last7DayTSEL as $aday) {
            $r = new RickshawChartPlot;
            $r->x = strtotime("-" . (6 - $i) . "days");
            $r->y = $aday;
            $rs[] = $r;
            $i++;
        }

        $data['dataLast7DaysTSEL'] = json_encode($rs);


        $dataAllRevenueAllSdcISAT = array();
        $sdcs = MainServiceConfig::getAllSdc();
        foreach ($sdcs as $sdc) {
            $entry = new ERecord;
            $entry->name = $sdc->name;
            $entry->data = Reporting::totalAllRevenueThisWeekISAT($entry->name);
            $dataAllRevenueAllSdcISAT[] = $entry;
        }
        $data['dataLast7DaysPerSdcISAT'] = json_encode($dataAllRevenueAllSdcISAT);

        $dataAllRevenueAllSdcISATPlaymedia = array();
        $sdcs = MainServiceConfig::getAllSdc();
        foreach ($sdcs as $sdc) {
            $entry = new ERecord;
            $entry->name = $sdc->name;
            $entry->data = Reporting::totalAllRevenueThisWeekISATPlaymedia($entry->name);
            $dataAllRevenueAllSdcISATPlaymedia[] = $entry;
        }
        $data['dataLast7DaysPerSdcISATPlaymedia'] = json_encode($dataAllRevenueAllSdcISATPlaymedia);

        $dataAllRevenueAllSdcXL = array();
        $sdcs = MainServiceConfig::getAllSdc();
        foreach ($sdcs as $sdc) {
            $entry = new ERecord;
            $entry->name = $sdc->name;
            $entry->data = Reporting::totalAllRevenueThisWeekXL($entry->name);
            $dataAllRevenueAllSdcXL[] = $entry;
        }
        $data['dataLast7DaysPerSdcXL'] = json_encode($dataAllRevenueAllSdcXL);

        $dataAllRevenueAllSdcXLPlaymedia = array();
        $sdcs = MainServiceConfig::getAllSdc();
        foreach ($sdcs as $sdc) {
            $entry = new ERecord;
            $entry->name = $sdc->name;
            $entry->data = Reporting::totalAllRevenueThisWeekXLPlaymedia($entry->name);
            $dataAllRevenueAllSdcXLPlaymedia[] = $entry;
        }
        $data['dataLast7DaysPerSdcXLPlaymedia'] = json_encode($dataAllRevenueAllSdcXLPlaymedia);

        $dataAllRevenueAllSdcTSEL = array();
        $sdcs = MainServiceConfig::getAllSdc();
        foreach ($sdcs as $sdc) {
            $entry = new ERecord;
            $entry->name = $sdc->name;
            $entry->data = Reporting::totalAllRevenueThisWeekTSEL($entry->name);
            $dataAllRevenueAllSdcTSEL[] = $entry;
        }
        $data['dataLast7DaysPerSdcTSEL'] = json_encode($dataAllRevenueAllSdcTSEL);

        $role_array = array(2,5,6,8,11);
        if ( in_array(Auth::user()->role_id, $role_array) ) {
            return View::make('content.not_admin_dashboard', $data);
        }

        //coa report last today - MODEL P3
        $rs_total_bc = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'MODEL PA3'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total from (
							       select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
							       WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
							       and reg_sms = 'REG MODEL PA3'
							       union all
							       select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
							       WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
							       and reg_sms = 'REG MODEL PA3'
							       order by date desc
							       ) a
							group by datetime LIMIT 1");

        $total_bc = @$rs_total_bc[0];
        $total_reg_array = @$rs_total_reg[0];

        $total_sent_bc = @$total_bc->total_sent;
        $total_reg = @$total_reg_array->total;

        if($total_reg == 0 || $total_sent_bc == 0)
        {
            $coa = 0;
            $conv = 0;
        }
        else
        {
            $coa = round($total_sent_bc*65.40813176/$total_reg);
            $conv = round($total_reg/@$total_sent_bc*100, 2);
        }

        $data['total_reg'] = $total_reg;
        $data['total_bc'] = $total_sent_bc;
        $data['coa'] = $coa;
        $data['coa_conv'] = $conv;

        /* Panda Kuning
        //coa report last today - DISKON GPA1
        $rs_total_bc1 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent1 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'DISKON GPA1'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg1 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total1 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");
        */


        /* Koala */
        //coa report last today - DISKON GPA1C
        $rs_total_bc1 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent1 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'DISKON GPA1C'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg1 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total1 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1C'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1C'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc1 = @$rs_total_bc1[0];
        $total_reg_array1 = @$rs_total_reg1[0];

        $total_sent_bc1 = @$total_bc1->total_sent1;
        $total_reg1 = @$total_reg_array1->total1;

        if($total_reg1 == 0 || $total_sent_bc1 == 0)
        {
            $coa1 = 0;
            $conv1 = 0;
        }
        else
        {
            //$coa1 = round($total_sent_bc1*49.40813176/$total_reg1);
            $coa1 = round($total_sent_bc1*65.40813176/$total_reg1);
            $conv1 = round($total_reg1/@$total_sent_bc1*100, 2);
        }

        $data['total_reg1'] = $total_reg1;
        $data['total_bc1'] = $total_sent_bc1;
        $data['coa1'] = $coa1;
        $data['coa_conv1'] = $conv1;


        /* Panda Kuning
        //coa report last today - DISKON GPA1B
        $rs_total_bc2 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent2 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'DISKON GPA1B'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg2 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total2 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1B'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1B'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");
        */

        
        /*
        $rs_total_bc2 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent2 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'DISKON GPA1D'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg2 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total2 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1D'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG DISKON GPA1D'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc2 = @$rs_total_bc2[0];
        $total_reg_array2 = @$rs_total_reg2[0];

        $total_sent_bc2 = @$total_bc2->total_sent2;
        $total_reg2 = @$total_reg_array2->total2;

        if($total_reg2 == 0 || $total_sent_bc2 == 0)
        {
            $coa2 = 0;
            $conv2 = 0;
        }
        else
        {
            //$coa2 = round($total_sent_bc2*49.40813176/$total_reg2);
            //$coa2 = round($total_sent_bc2*65.40813176/$total_reg2);
            $coa2 = round($total_sent_bc2*61/$total_reg2);
            $conv2 = round($total_reg2/@$total_sent_bc2*100, 2);
        }

        $data['total_reg2'] = $total_reg2;
        $data['total_bc2'] = $total_sent_bc2;
        $data['coa2'] = $coa2;
        $data['coa_conv2'] = $conv2;
        */


        //coa report last today - CHATBC HPR XL
        $rs_total_bc2 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent2 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'CHATBC HPR'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg2 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total2 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG CHATBC HPR'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG CHATBC HPR'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc2 = @$rs_total_bc2[0];
        $total_reg_array2 = @$rs_total_reg2[0];

        $total_sent_bc2 = @$total_bc2->total_sent2;
        $total_reg2 = @$total_reg_array2->total2;

        if($total_reg2 == 0 || $total_sent_bc2 == 0)
        {
            $coa2 = 0;
            $conv2 = 0;
        }
        else
        {
            $coa2 = round($total_sent_bc2*35/$total_reg2);
            $conv2 = round($total_reg2/@$total_sent_bc2*100, 2);
        }

        $data['total_reg2'] = $total_reg2;
        $data['total_bc2'] = $total_sent_bc2;
        $data['coa2'] = $coa2;
        $data['coa_conv2'] = $conv2;


        //coa report last today - CB B
        $rs_total_bc3 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent3 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'CB B'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg3 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total3 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG CB B'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG CB B'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc3 = @$rs_total_bc3[0];
        $total_reg_array3 = @$rs_total_reg3[0];

        $total_sent_bc3 = @$total_bc3->total_sent3;
        $total_reg3 = @$total_reg_array3->total3;

        if($total_reg3 == 0 || $total_sent_bc3 == 0)
        {
            $coa3 = 0;
            $conv3 = 0;
        }
        else
        {
            //$coa3 = round($total_sent_bc3*76.70200721/$total_reg3);
            $coa3 = round($total_sent_bc3*29/$total_reg3);
            $conv3 = round($total_reg3/@$total_sent_bc3*100, 2);
        }

        $data['total_reg3'] = $total_reg3;
        $data['total_bc3'] = $total_sent_bc3;
        $data['coa3'] = $coa3;
        $data['coa_conv3'] = $conv3;


        //coa report last today - MODEL I1
        $rs_total_bc4 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent4 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'MODEL PM1'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg4 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total4 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG MODEL PM1'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG MODEL PM1'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc4 = @$rs_total_bc4[0];
        $total_reg_array4 = @$rs_total_reg4[0];

        $total_sent_bc4 = @$total_bc4->total_sent4;
        $total_reg4 = @$total_reg_array4->total4;

        if($total_reg4 == 0 || $total_sent_bc4 == 0)
        {
            $coa4 = 0;
            $conv4 = 0;
        }
        else
        {
            //$coa4 = round($total_sent_bc4*65.40813176/$total_reg4);
            $coa4 = round($total_sent_bc4*61/$total_reg4);
            $conv4 = round($total_reg4/@$total_sent_bc4*100, 2);
        }

        $data['total_reg4'] = $total_reg4;
        $data['total_bc4'] = $total_sent_bc4;
        $data['coa4'] = $coa4;
        $data['coa_conv4'] = $conv4;

        //coa report last today - GAME PM1
        /*
        $rs_total_bc4 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent4 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'GAME PM1'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg4 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total4 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG GAME PM1'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG GAME PM1'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc4 = @$rs_total_bc4[0];
        $total_reg_array4 = @$rs_total_reg4[0];

        $total_sent_bc4 = @$total_bc4->total_sent4;
        $total_reg4 = @$total_reg_array4->total4;

        if($total_reg4 == 0 || $total_sent_bc4 == 0)
        {
            $coa4 = 0;
            $conv4 = 0;
        }
        else
        {
            //$coa4 = round($total_sent_bc4*65.40813176/$total_reg4);
            $coa4 = round($total_sent_bc4*61/$total_reg4);
            $conv4 = round($total_reg4/@$total_sent_bc4*100, 2);
        }

        $data['total_reg4'] = $total_reg4;
        $data['total_bc4'] = $total_sent_bc4;
        $data['coa4'] = $coa4;
        $data['coa_conv4'] = $conv4;
        */

        //coa report last today - MODEL N1
        /*
        $rs_total_bc5 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent5 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'MODEL N1'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg5 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total5 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG MODEL N1'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG MODEL N1'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc5 = @$rs_total_bc5[0];
        $total_reg_array5 = @$rs_total_reg5[0];

        $total_sent_bc5 = @$total_bc5->total_sent5;
        $total_reg5 = @$total_reg_array5->total5;

        if($total_reg5 == 0 || $total_sent_bc5 == 0)
        {
            $coa5 = 0;
            $conv5 = 0;
        }
        else
        {
            //$coa5 = round($total_sent_bc5*65.40813176/$total_reg5);
            $coa5 = round($total_sent_bc5*61/$total_reg5);
            $conv5 = round($total_reg5/@$total_sent_bc5*100, 2);
        }

        $data['total_reg5'] = $total_reg5;
        $data['total_bc5'] = $total_sent_bc5;
        $data['coa5'] = $coa5;
        $data['coa_conv5'] = $conv5;
        */

        //coa report last today - JODOH MB1
        $rs_total_bc5 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent5 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'JODOH MB1'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg5 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total5 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG JODOH MB1'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG JODOH MB1'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc5 = @$rs_total_bc5[0];
        $total_reg_array5 = @$rs_total_reg5[0];

        $total_sent_bc5 = @$total_bc5->total_sent5;
        $total_reg5 = @$total_reg_array5->total5;

        if($total_reg5 == 0 || $total_sent_bc5 == 0)
        {
            $coa5 = 0;
            $conv5 = 0;
        }
        else
        {
            //$coa5 = round($total_sent_bc5*65.40813176/$total_reg5);
            $coa5 = round($total_sent_bc5*65/$total_reg5);
            $conv5 = round($total_reg5/@$total_sent_bc5*100, 2);
        }

        $data['total_reg5'] = $total_reg5;
        $data['total_bc5'] = $total_sent_bc5;
        $data['coa5'] = $coa5;
        $data['coa_conv5'] = $conv5;


        //coa report last today - GAME PM2
        /*
        $rs_total_bc5 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent5 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'GAME PM2'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg5 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total5 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG GAME PM2'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG GAME PM2'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc5 = @$rs_total_bc5[0];
        $total_reg_array5 = @$rs_total_reg5[0];

        $total_sent_bc5 = @$total_bc5->total_sent5;
        $total_reg5 = @$total_reg_array5->total5;

        if($total_reg5 == 0 || $total_sent_bc5 == 0)
        {
            $coa5 = 0;
            $conv5 = 0;
        }
        else
        {
            //$coa5 = round($total_sent_bc5*65.40813176/$total_reg5);
            $coa5 = round($total_sent_bc5*61/$total_reg5);
            $conv5 = round($total_reg5/@$total_sent_bc5*100, 2);
        }

        $data['total_reg5'] = $total_reg5;
        $data['total_bc5'] = $total_sent_bc5;
        $data['coa5'] = $coa5;
        $data['coa_conv5'] = $conv5;
        */

        //coa report last today - LIVEBC HPR
        
        $rs_total_bc6 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent6 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'LIVEBC HPR'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg6 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total6 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG LIVEBC HPR'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG LIVEBC HPR'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc6 = @$rs_total_bc6[0];
        $total_reg_array6 = @$rs_total_reg6[0];

        $total_sent_bc6 = @$total_bc6->total_sent6;
        $total_reg6 = @$total_reg_array6->total6;

        if($total_reg6 == 0 || $total_sent_bc6 == 0)
        {
            $coa6 = 0;
            $conv6 = 0;
        }
        else
        {
            $coa6 = round($total_sent_bc6*35/$total_reg6);
            $conv6 = round($total_reg6/@$total_sent_bc6*100, 2);
        }

        $data['total_reg6'] = $total_reg6;
        $data['total_bc6'] = $total_sent_bc6;
        $data['coa6'] = $coa6;
        $data['coa_conv6'] = $conv6;
    

        //coa report last today - MODEL GS2
        /*
        $rs_total_bc6 = DB::select("SELECT date_format(dtreport, '%Y-%m-%d') as dtreport, total_sent as total_sent6 FROM
                                `montessori`.application_keyword d INNER JOIN traffic_rpt.traffic_bc_summary e ON
                                d.id = e.keyword_id WHERE d.reg_sms = 'MODEL GS2'
                                AND dtreport >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                AND dtreport <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59') LIMIT 1");

        $rs_total_reg6 = DB::select("select date_format(a.date, '%Y-%m-%d') as datetime, count(*) total6 from (
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                  AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG MODEL GS2'
                                   union all
                                   select date_format(dtregister, '%Y-%m-%d') date, msisdn from montessori.deleted_subscriber
                                   WHERE dtregister >= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 00:00:00')
                                    AND dtregister <= DATE_FORMAT(DATE(NOW()), '%Y-%m-%d 23:59:59')
                                   and reg_sms = 'REG MODEL GS2'
                                   order by date desc
                                   ) a
                            group by datetime LIMIT 1");

        $total_bc6 = @$rs_total_bc6[0];
        $total_reg_array6 = @$rs_total_reg6[0];

        $total_sent_bc6 = @$total_bc6->total_sent6;
        $total_reg6 = @$total_reg_array6->total6;

        if($total_reg6 == 0 || $total_sent_bc6 == 0)
        {
            $coa6 = 0;
            $conv6 = 0;
        }
        else
        {
            //$coa6 = round($total_sent_bc6*35/$total_reg6); => Perhitungan COA XL BC
            $coa6 = round($total_sent_bc6*61/$total_reg6);
            $conv6 = round($total_reg6/@$total_sent_bc6*100, 2);
        }

        $data['total_reg6'] = $total_reg6;
        $data['total_bc6'] = $total_sent_bc6;
        $data['coa6'] = $coa6;
        $data['coa_conv6'] = $conv6;
        */


        //modem auto unreg report
        $url = 'http://202.43.162.252:8081/monitor';
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = curl_exec($ch);
        curl_close($ch);

        $str_clean = explode(',', str_replace('}','', str_replace('{', '', $response)));

        foreach($str_clean as $each_data) {
            $trimed = trim($each_data);
            $exploded = explode(':', $trimed);
            $data[trim($exploded[0])] = @$exploded[1];
        }

        $url = 'http://202.43.162.252:8082/monitor';
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = curl_exec($ch);
        curl_close($ch);

        $str_clean = explode(',', str_replace('}','', str_replace('{', '', $response)));

        foreach($str_clean as $each_data) {
            $trimed = trim($each_data);
            $exploded = explode(':', $trimed);
            $data[trim($exploded[0]).'_2'] = @$exploded[1];
        }

        $url = 'http://202.43.162.252:8083/monitor';
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = curl_exec($ch);
        curl_close($ch);

        $str_clean = explode(',', str_replace('}','', str_replace('{', '', $response)));

        foreach($str_clean as $each_data) {
            $trimed = trim($each_data);
            $exploded = explode(':', $trimed);
            $data[trim($exploded[0]).'_3'] = @$exploded[1];
        }

        //eof modem auto unreg report


        $role_array2 = array(7);
        if ( in_array(Auth::user()->role_id, $role_array2) ) {
            return View::make('main-bizdev', $data);
        }

        $role_array3 = array(9);
        if ( in_array(Auth::user()->role_id, $role_array3) ) {
            return View::make('main-playmedia-bizdev', $data);
        }

        return View::make('main', $data);
    }
}



/**
 * Uttility Class
 *
 */
class ERecord {
	/**
	 * Name of the records
	 * @var string
	 */
	public $name;

	/**
	 * Set of data
	 *
	 * @var array
	 */
	public $data;
}

/**
 * For Rickshaw Chart
 *
 */
class RickshawChartPlot {
	/**
	 * X value
	 * @var string
	 */
	public $x;

	/**
	 * Y value
	 *
	 * @var array
	 */
	public $y;
}
