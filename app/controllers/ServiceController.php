<?php

/**
 * Class ServiceController
 * Responsible for the manage of service
 *
 */
class ServiceController extends BaseController {

    /**
     * Show charging 'home' screen
     * @return mixed
     */
    public function home() {
        $data = $this->constructMenu();
        $data['sdc'] = MainServiceConfig::getAllSdc();
        $data['telco'] = MainServiceConfig::getAllTelco();
        $telcoId = Input::get('telcoId');
        if ($telcoId) $data['telcoId'] = $telcoId;
        $givenSdc = Input::get('givenSdc');
        if ($givenSdc) $data['givenSdc'] = $givenSdc;
        return View::make('service.list_service',$data);
    }


    /**
     * The 'API' for getting Service Group
     * @return mixed
     */
    public function apiGetListServiceGroup() {
        $data = array();
        $data['the_data'] = MainServiceConfig::getServiceGroup();
        return View::make('service.api.api_list_service_group', $data);
    }


    /**
     * The 'API' of list of service.
     */
    public function apiGetListService() {
        $data = array();
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        $data['the_data'] = GatewayService::getListOfService($telcoId, $sdc);
        return View::make('service.api.api_list_service', $data);
    }


    /**
     * The 'API', get list of service by the given sevice keywords
     *
     */
    public function apiGetListServiceByKeyword() {
        $data = array();
        $keyword = Input::get('keyword');
        /*
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        */
        //$data['the_data'] = GatewayService::getListOfService($telcoId, $sdc);
        $data['the_data'] = GatewayService::getListOfServiceByKeywordOnly($keyword);
        //$keyword
        return View::make('service.api.api_list_service', $data);
    }



    /**
     * The 'API' of creating new service.
     */
    public function apiViewCreateNewService() {
        $telcoId = Input::get('telcoId');
        if (!$telcoId) $telcoId = 0;
        $sdc = Input::get('sdc');
        if (!$sdc) $sdc = '--';
        $data = array();
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        $data['businessDelegateUrl'] = Config::get("app.business_delegate_url");
        $data['serviceGroup'] = MainServiceConfig::getServiceGroup();
        return View::make('service.api.api_create_new_service', $data);
    }


    /**
     * Save to database
     */
    public function apiSaveNewService() {
        $name = Input::get('name');
        $url = Input::get('url');
        $sdc = Input::get('sdc');
        $telcoId = Input::get('telcoId');
        $serviceGroupId = Input::get('serviceGroupId');
        try {
            $insertedId = GatewayService::saveService($name, $url, $sdc, $telcoId, $serviceGroupId);
            Log::info("Added new service for TelcoId: {$telcoId}, ".
                "Name: {$name}, SDC:  {$sdc}, BD-URL: {$url}, ServiceGroupId: {$serviceGroupId} with id: {$insertedId}"
            );

			ActivityLog::writeActiviy(
				Config::get('activity.SERVICE_CREATE'), '/api/create_new_service',
				"Added new service for TelcoId: {$telcoId}, " .
				"Name: {$name}, SDC:  {$sdc}, BD-URL: {$url}, ServiceGroupId: {$serviceGroupId} with id: {$insertedId}"
			);

            /**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * View Edit Service
     * @return mixed
     */
    public function apiViewEditService() {
        $serviceId = Input::get('serviceId');
        $data = array();
        $data['serviceId'] = $serviceId;
        $data['telco'] = '--';
        $data['serviceGroup'] = MainServiceConfig::getServiceGroup();
        $service = GatewayService::getById($serviceId);
        if (sizeof($service) > 0) {
            $data['service'] = $service[0];
            $qTelco = MainServiceConfig::getTelcoById($service[0]->telcoId);
            $data['telco'] = $qTelco[0]->name;
        }
        return View::make('service.api.api_edit_service', $data);
    }


    /**
     * Save EDIT to database
     */
    public function apiSaveEditService() {
        $serviceId = Input::get('serviceId');
        $serviceName = Input::get('serviceName');
        $url = Input::get('url');
        $isActive = Input::get('isActive');
        try {
            GatewayService::editService($serviceId, $serviceName, $url, $isActive);

			ActivityLog::writeActiviy(
				Config::get('activity.SERVICE_EDIT'), '/api/edit_service',
				"Edit service for Id: {$serviceId}, " .
				"Name: {$serviceName}, BD-URL: {$url}, Active: {$isActive}"
			);

			/**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


    /**
     * API get list service keywords
     *
     */
    public function apiGetListServiceKeyword() {
        $serviceId = Input::get('serviceId');
        $serviceName = Input::get('serviceName');
        if (empty($serviceName) || !isset($serviceName) || is_null($serviceName)) {
            $serviceNameRec = GatewayService::getById($serviceId);
            $serviceName = $serviceNameRec[0]->name;
        }

        $sdc = Input::get('sdc');
        $telcoId = Input::get('telcoId');
        $keywords = GatewayService::getServiceKeywords($serviceId);
        //return json_encode($keywords);
        $data = array();
        $data['serviceId'] = $serviceId;
        $data['serviceName'] = $serviceName;
        $data['sdc'] = $sdc;
        $data['telcoId'] = $telcoId;
        $data['the_data'] = $keywords;
        return View::make('service.api.api_list_service_keyword', $data);
    }


    /**
     * The 'API' of creating new service keyword (view).
     */
    public function apiViewCreateNewServiceKeyword() {
        $serviceId = Input::get('serviceId');
        $sdc = Input::get('sdc');
        $telcoId = Input::get('telcoId');
        $data = array();
        $data['sdc'] = $sdc;
        $data['serviceId'] = $serviceId;
        $data['telcoId'] = $telcoId;
        $qTelco = MainServiceConfig::getTelcoById($telcoId);
        $data['telcoName'] = $qTelco[0]->name;
        return View::make('service.api.api_create_new_service_keyword', $data);
    }


    /**
     * The 'API' of saving new service keyword
     */
    public function apiSaveNewServiceKeyword() {
        $serviceId = Input::get('serviceId');
        $sdc = Input::get('sdc');
        $telcoId = Input::get('telcoId');
        $keyword = Input::get('keyword');
        //Always trimmed and make it lower
        $keyword = strtolower(trim($keyword));

        try {
            if (GatewayService::isServiceKeywordExists($serviceId, $sdc, $keyword, $telcoId)) {
                return Response::json(array('Error' => '1',
                    'Message' => "Keyword {$keyword} for serviceId: {$serviceId}, sdc = {$sdc}, telcoId: {$telcoId} already exists"));
            }

            GatewayService::saveServiceKeyword($serviceId, $sdc, $keyword, $telcoId);

			ActivityLog::writeActiviy(
				Config::get('activity.SERVICE_KEYWORD_CREATE'), '/api/create_new_service_keyword',
				"Add keyword for service for Id: {$serviceId}, Keyword: {$keyword}"
			);

			/**
             * Simply 'OK'
             */
            return Response::json(array('OK' => '1'));
        } catch (Exception $e) {
            return Response::json(array('Error' => '1', 'Message' => $e->getMessage()));
        }
    }


}
