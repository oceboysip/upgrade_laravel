<?php

/**
 * Class ContentController
 *
 */
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;
use Redis;
class NodesmsController extends BaseController {


    protected $montessori_db, $umb_app_db, $umb_microsite;
    public function __construct()
    {
        $this->montessori_db = Config::get('reporting.database_application');
        $this->umb_app_db = Config::get('reporting.database_umb');
        $this->umb_microsite = Config::get('reporting.database_umb_microsite');
    }
    /**
     * Show 'home' screen of pull content
     * @return mixed
     */
    public function server(){
        $data = $this->constructMenu();
        $redis = Redis::get('promo_header_kumpulkan_koin');
        var_dump($redis);
		return View::make('nodesms.node-sms', $data);
    }

    public function get_area()
    {
        $result = DB::connection('montessori')->select("SELECT area, count('com_name') as total FROM devices GROUP BY area");
        echo json_encode($result);
    }

    public function get_pcname($area)
    {
        $result = DB::connection('montessori')->select("SELECT area, pc_name, count('com_name') as total FROM devices WHERE area='{$area}' GROUP BY pc_name");
        echo json_encode($result);   
    }

    public function get_device($area, $pc_name)
    {
        $result = DB::connection('montessori')->select("SELECT * FROM devices WHERE area = '{$area}' AND pc_name='{$pc_name}' LIMIT 1000");

        echo json_encode($result);
    }

    public function start()
    {
        $id = Input::get('id'); 
        $result = DB::connection('montessori')->select("UPDATE devices SET n_status = 2 WHERE id = {$id}");
        echo json_encode(['status' => 'OK']);
    }

    public function start_all()
    {
        $area = Input::get('area');
        $pc_name = Input::get('pc_name');

        $result = DB::connection('montessori')->select("UPDATE devices SET n_status = 2 WHERE area = '{$area}' 
                                                        AND pc_name = '{$pc_name}' AND n_status != 3");
        echo json_encode(['status' => 'OK']);
    }

    public function stop_all()
    {
        $area = Input::get('area');
        $pc_name = Input::get('pc_name');

        $result = DB::connection('montessori')->select("UPDATE devices SET n_status = 0 WHERE area = '{$area}' 
                                                        AND pc_name = '{$pc_name}' AND n_status != 1");
        echo json_encode(['status' => 'OK']);
    }

    public function stop()
    {
        $id = Input::get('id'); 
        $result = DB::connection('montessori')->select("UPDATE devices SET n_status = 0 WHERE id = {$id}");
        echo json_encode(['status' => 'OK', 'query' => "UPDATE devices SET n_status = 0 WHERE id = {$id}"]);
    }
}
